from django.contrib.auth import authenticate
from django.utils.translation import ugettext_lazy as _
from rest_framework import serializers, exceptions
from rest_framework_simplejwt.serializers import PasswordField, RefreshToken
from taggit_serializer.serializers import TagListSerializerField, TaggitSerializer

from users.models import User


class UserRegistrationSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ['email', 'password', 'password_confirm']


from taggit.models import Tag


class SkillsSerializer(TaggitSerializer, serializers.ModelSerializer):
    value = serializers.CharField(source='name')
    name = serializers.CharField()

    class Meta:
        model = Tag
        fields = ('id','value', 'name')


class SkillViewSerializer(TaggitSerializer, serializers.ModelSerializer):
    value = serializers.CharField(source='name')
    label = serializers.CharField(source='name')

    class Meta:
        model = Tag
        fields = ['value', 'label']


class UserInfoSerializer(serializers.ModelSerializer):
    """
        Короткая информация о Пользователе системы
    """

    class Meta:
        model = User
        fields = ['email', 'first_name', 'second_name', 'role', 'type_developer', 'status']


class ResumeCVFile(serializers.Serializer):
    """
        Файл резюме
    """
    file_name = serializers.CharField(source='get_cv_file_name')
    size = serializers.CharField(source='get_cv_file_size')
    url = serializers.URLField(source='get_cv_file_url')


class UserProfileSerializer(TaggitSerializer, serializers.ModelSerializer):
    render_skills = SkillViewSerializer(many=True, read_only=True, source='skills')
    resume_file = ResumeCVFile(source='*', read_only=True)
    skills = TagListSerializerField()

    class Meta:
        model = User
        exclude = (
            'last_login',
            'is_active',
            'is_staff',
            'password',
            'status',
            'role',
            'id',
            'email',
            'accept_reglament',
            'is_superuser')


class UserCVSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('resume_file',)


class InstituteSerializer(serializers.Serializer):
    id = serializers.CharField()
    name = serializers.CharField()


class CollageSerializer(serializers.Serializer):
    name = serializers.CharField()


class SchoolSerializer(serializers.Serializer):
    name = serializers.CharField()


class SpecializationSerializer(serializers.Serializer):
    id = serializers.CharField()
    name = serializers.CharField()


class InstituteAutocompleteSerializer(serializers.Serializer):
    q = serializers.CharField()


class TokenObtainSerializer(serializers.Serializer):
    username_field = User.USERNAME_FIELD

    default_error_messages = {
        'no_active_account': _('No active account found with the given credentials')
    }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.fields[self.username_field] = serializers.CharField()
        self.fields['password'] = PasswordField()

    def validate(self, attrs):
        authenticate_kwargs = {
            self.username_field: attrs[self.username_field],
            'password': attrs['password'],
        }
        try:
            authenticate_kwargs['request'] = self.context['request']
        except KeyError:
            pass

        self.user = authenticate(**authenticate_kwargs)

        # Prior to Django 1.10, inactive users could be authenticated with the
        # default `ModelBackend`.  As of Django 1.10, the `ModelBackend`
        # prevents inactive users from authenticating.  App designers can still
        # allow inactive users to authenticate by opting for the new
        # `AllowAllUsersModelBackend`.  However, we explicitly prevent inactive
        # users from authenticating to enforce a reasonable policy and provide
        # sensible backwards compatibility with older Django versions.
        if self.user is None:
            raise exceptions.AuthenticationFailed(
                self.error_messages['no_active_account'],
                'no_active_account',
            )

        return {}

    @classmethod
    def get_token(cls, user):
        raise NotImplementedError('Must implement `get_token` method for `TokenObtainSerializer` subclasses')


class TokenObtainPairSerializer(TokenObtainSerializer):
    @classmethod
    def get_token(cls, user):
        token = RefreshToken.for_user(user)
        token['user'] = {
            'role': user.role,
            'first_name': user.first_name,
            'second_name': user.second_name,
            'email': user.email,
            'type_developer': user.type_developer,
        }
        return token

    def validate(self, attrs):
        data = super().validate(attrs)

        refresh = self.get_token(self.user)

        data['refresh'] = str(refresh)
        data['access'] = str(refresh.access_token)

        return data
