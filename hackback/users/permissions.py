from rest_framework.permissions import BasePermission


class IsAdmin(BasePermission):
    """
    Allows access only to authenticated users.
    """

    def has_permission(self, request, view):
        return bool(request.user.role == 'admin')

class IsHacker(BasePermission):
    """
    Allows access only to authenticated users.
    """

    def has_permission(self, request, view):
        return bool(request.user.role == 'hacker')


class IsCompleteHacker(BasePermission):
    """
        Хакер полностью заполнил свою акенту
    """

    def has_permission(self, request, view):
        return bool(request.user.role == 'hacker' and request.user.status >= 2)


class IsExpert(BasePermission):
    """
    Allows access only to authenticated users.
    """

    def has_permission(self, request, view):
        return bool(request.user.role == 'expert')


class IsAdmin(BasePermission):
    """
    Allows access only to authenticated users.
    """

    def has_permission(self, request, view):
        return bool(request.user.role == 'admin')
