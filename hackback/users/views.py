from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework import status
from rest_framework.parsers import MultiPartParser
from rest_registration.utils.verification_notifications import send_register_verification_email_notification

from taggit.models import Tag

from .serializers import UserInfoSerializer, UserProfileSerializer, InstituteSerializer, SpecializationSerializer, \
    UserCVSerializer, InstituteAutocompleteSerializer, SkillsSerializer, ResumeCVFile, CollageSerializer, \
    SchoolSerializer, TokenObtainPairSerializer
from .models import Institute, Specialization, Collage, School

from rest_framework_simplejwt.views import TokenViewBase
from django.utils import timezone
from rest_framework import serializers
from users.signers import ConfirmSigner
from rest_registration.utils.verification import verify_signer_or_bad_request
from rest_registration.utils.users import get_user_by_verification_id

from django.conf import settings


class ConfirmUserSerializer(serializers.Serializer):
    user_id = serializers.CharField(required=True)
    timestamp = serializers.IntegerField(required=True)
    signature = serializers.CharField(required=True)


class ConfirmUserViewSet(APIView):
    serializer_class = ConfirmUserSerializer

    def post(self, request):
        serializer = self.serializer_class(data=self.request.data)
        if serializer.is_valid():
            data = serializer.validated_data.copy()
            signer = ConfirmSigner(data)
            verify_signer_or_bad_request(signer)
            user = get_user_by_verification_id(data['user_id'], require_verified=False)
            user.status = 3
            user.save()
            return Response({"complete": True})
        return Response(data=serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class TokenObtainPairView(TokenViewBase):
    """
    Takes a set of user credentials and returns an access and refresh JSON web
    token pair to prove the authentication of those credentials.
    """
    serializer_class = TokenObtainPairSerializer


from datetime import datetime


class CheckOpenRegistration(APIView):

    def get(self, request):
        if settings.CLOSE_REGISTRATION < datetime.now():
            return Response({"close": True}, status=status.HTTP_200_OK)
        return Response({"close": False}, status=status.HTTP_200_OK)


class UserViewSet(APIView):
    permission_classes = [IsAuthenticated]
    serializer_class = UserInfoSerializer

    def get(self, request):
        print("UserViewSet", request.user)
        return Response(self.serializer_class(request.user).data, status=status.HTTP_200_OK)


class UserProfile(APIView):
    permission_classes = [IsAuthenticated]
    serializer_class = UserProfileSerializer

    def get(self, request):
        return Response(self.serializer_class(self.request.user).data, status=status.HTTP_200_OK)

    def put(self, request, *args, **kwargs):
        data = request.data
        serializer = self.serializer_class(self.request.user, data=data)
        if serializer.is_valid():
            if not request.user.form_complete:
                serializer.save(form_complete=True, datetime_hacker_complete=timezone.now(), status=2)
            else:
                serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class InstituteAutocomplete(APIView):
    permission_classes = [IsAuthenticated]
    serializer_class = InstituteAutocompleteSerializer

    def get(self, request):
        q = request.query_params.get('q')
        return Response(InstituteSerializer(Institute.objects.filter(name__icontains=q)[:10], many=True).data)


class CollageAutocomplete(APIView):
    permission_classes = [IsAuthenticated]
    serializer_class = CollageSerializer

    def get(self, request):
        q = request.query_params.get('q')
        return Response(self.serializer_class(Collage.objects.filter(name__icontains=q)[:10], many=True).data)


class SchoolAutocomplete(APIView):
    permission_classes = [IsAuthenticated]
    serializer_class = SchoolSerializer

    def get(self, request):
        q = request.query_params.get('q')
        return Response(self.serializer_class(School.objects.filter(name__icontains=q)[:10], many=True).data)


class SkillsAutocomplete(APIView):
    permission_classes = [IsAuthenticated]
    serializer_class = SkillsSerializer

    def get(self, request):
        q = request.query_params.get('q')
        return Response(SkillsSerializer(Tag.objects.filter(name__icontains=q), many=True).data)


class SpecializationAutocomplete(APIView):
    permission_classes = [IsAuthenticated]
    serializer_class = SpecializationSerializer

    def get(self, request):
        q = request.query_params.get('q')
        return Response(SpecializationSerializer(Specialization.objects.filter(name__icontains=q)[:10], many=True).data)


class CVUploadView(APIView):
    permission_classes = [IsAuthenticated]
    parser_classes = (MultiPartParser,)
    serializer_class = UserCVSerializer

    def post(self, request, format=None):
        serializer = UserCVSerializer(request.user, data=request.data)
        if serializer.is_valid():
            instance = serializer.save()
            return Response(ResumeCVFile(instance).data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ResendEmailVerification(APIView):
    permission_classes = [IsAuthenticated]

    def post(self, request, format=None):
        try:
            if not self.request.user.is_active:
                send_register_verification_email_notification(request, self.request.user)
                return Response({"send": True}, status=status.HTTP_200_OK)
            return Response({"error": ["Пользватель уже подтвердил свою почту"]}, status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            return Response({"error": [e]}, status=status.HTTP_200_OK)
