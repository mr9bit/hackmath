from django.urls import path, include
from users import views

urlpatterns = [

    path('auth/resend_email/', views.ResendEmailVerification.as_view(), name='user_me_info'),
    path('auth/me/', views.UserViewSet.as_view(), name='user_me_info'),
    path('auth/profile/', views.UserProfile.as_view(), name='user_profile'),
    path('auth/profile/upload_cv/', views.CVUploadView.as_view(), name='cv_upload_view'),
    path('auth/confirm/', views.ConfirmUserViewSet.as_view(), name='confirm_user'),
    path('check_reg/', views.CheckOpenRegistration.as_view(), name='check_open_registration'),

    path('autocomplete/institute/', views.InstituteAutocomplete.as_view(), name='institute_autocomplete'),
    path('autocomplete/collage/', views.CollageAutocomplete.as_view(), name='collage_autocomplete'),
    path('autocomplete/school/', views.SchoolAutocomplete.as_view(), name='shool_autocomplete'),
    path('autocomplete/skills/', views.SkillsAutocomplete.as_view(), name='skill_autocomplete'),
    path('autocomplete/specialization/', views.SpecializationAutocomplete.as_view(), name='specialization_autocomplete'),
]
