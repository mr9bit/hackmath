import uuid

from django.db import models
from django.contrib import auth
from django.dispatch import receiver
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager

from taggit.managers import TaggableManager
from rest_registration.signals import user_activated


class UserManager(BaseUserManager):
    use_in_migrations = True

    def _create_user(self, email, password, **extra_fields):
        """
        Create and save a user with the given email, and password.
        """
        if not email:
            raise ValueError('The given email must be set')
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email=None, password=None, **extra_fields):
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email, password=None, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(email, password, **extra_fields)

    def with_perm(self, perm, is_active=True, include_superusers=True, backend=None, obj=None):
        if backend is None:
            backends = auth._get_backends(return_tuples=True)
            if len(backends) == 1:
                backend, _ = backends[0]
            else:
                raise ValueError(
                    'You have multiple authentication backends configured and '
                    'therefore must provide the `backend` argument.'
                )
        elif not isinstance(backend, str):
            raise TypeError(
                'backend must be a dotted import path string (got %r).'
                % backend
            )
        else:
            backend = auth.load_backend(backend)
        if hasattr(backend, 'with_perm'):
            return backend.with_perm(
                perm,
                is_active=is_active,
                include_superusers=include_superusers,
                obj=obj,
            )
        return self.none()


class Institute(models.Model):
    name = models.CharField(verbose_name="Название института", max_length=600)


class Collage(models.Model):
    name = models.CharField(verbose_name="Название института", max_length=900)


class School(models.Model):
    name = models.CharField(verbose_name="Название института", max_length=900)


class Specialization(models.Model):
    name = models.CharField(verbose_name="Название специальности", max_length=600)


from taggit.models import GenericUUIDTaggedItemBase, TaggedItemBase


class UUIDTaggedItem(GenericUUIDTaggedItemBase, TaggedItemBase):
    # If you only inherit GenericUUIDTaggedItemBase, you need to define
    # a tag field. e.g.
    # tag = models.ForeignKey(Tag, related_name="uuid_tagged_items", on_delete=models.CASCADE)

    class Meta:
        verbose_name = _("Tag")
        verbose_name_plural = _("Tags")


class User(AbstractBaseUser):
    GENDER_MALE = 'М'
    GENDER_FEMALE = 'Ж'
    GENDER_CHOICES = (
        (GENDER_MALE, 'Мужской'),
        (GENDER_FEMALE, 'Женский')
    )

    ROLE_CHOICES = (
        ('hacker', 'Hacker'),
        ('admin', 'Administration'),
        ('mentor', 'Mentor'),
        ('expert', 'Expert'),
        ('jury', 'Jury'),
    )

    CHOICES_WHERE_TO_STUDY = (
        ('no_learning', 'Не учусь'),
        ('school', 'Школа'),
        ('univ', 'Университет'),
        ('college', 'Колледж'),
    )
    # Сайт хакатона ; сайт маи; рассылка , соцсети, друзья сказали, другое
    CHOICES_WHERE_DID_YOU_KNOW = (
        ('hack', 'Сайт хакатона'),
        ('site_mai', 'Сайт МАИ'),
        ('mail', 'Рассылка по почте'),
        ('social', 'Социальные сети'),
        ('friend', 'Друзья сказали'),
        ('other', 'Другое'),

    )

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    email = models.EmailField(unique=True)
    secret_tg = models.CharField(max_length=300, null=True, blank=True) # secret code для привязки пользователя
    chat_id_tg = models.CharField(max_length=300, null=True, blank=True) # chat id tg
    vk = models.CharField(null=True, blank=True, max_length=350)  # Вконтакте
    instagram = models.CharField(null=True, blank=True, max_length=350)  # Инстаграмм
    have_vaccine = models.CharField(null=True, blank=True, max_length=350)  # Инстаграмм
    departament = models.CharField(verbose_name="Кафедра", null=True, blank=True, max_length=600)  # Кафедра
    faculty = models.CharField(verbose_name="Факультет", null=True, blank=True, max_length=600)  # Факультет
    first_name = models.CharField(null=True, blank=True, max_length=255)  # Имя
    second_name = models.CharField(null=True, blank=True, max_length=255)  # Фамилия
    middle_name = models.CharField(null=True, blank=True, max_length=255)  # Отчество
    gender = models.CharField(null=True, blank=True, choices=GENDER_CHOICES, max_length=50)  # Пол
    birth_date = models.DateField(null=True, blank=True, )  # Дата рождения
    country_live = models.CharField(null=True, blank=True, max_length=350)  # Страна проживания
    born_city = models.CharField(null=True, blank=True, max_length=350)  # Страна рождения
    city = models.CharField(null=True, blank=True, max_length=350)  # Город проживания
    telegram_link = models.CharField(null=True, blank=True, max_length=350)  # Telegram
    phone = models.CharField(null=True, blank=True, max_length=350)  # Телефон
    status = models.IntegerField(default=0)  # Статус заявки
    role = models.CharField(choices=ROLE_CHOICES, max_length=300, default='hacker')

    institute = models.CharField(verbose_name="Название", null=True, blank=True, max_length=600)  # Институт
    specialization = models.CharField(verbose_name="Название", null=True, blank=True,
                                      max_length=600)  # Учебная специализация
    year_end = models.IntegerField(null=True, blank=True, )  # Год окончания
    science_degree = models.CharField(null=True, blank=True, max_length=100)  # Научная степень

    type_developer = models.CharField(null=True, blank=True, max_length=350)  # Позиционирование себя как
    company = models.CharField(null=True, blank=True, max_length=350) # Комания откуда эксперт
    expert_task = models.ForeignKey("organization.Task", on_delete=models.CASCADE, verbose_name="Выбранные треки", null=True, blank=True, related_name="expert_tasks")

    employment_form = models.CharField(null=True, blank=True, max_length=350)  # Форма занятости
    seniority = models.CharField(null=True, blank=True, max_length=350)  # Трудовой стаж
    github = models.CharField(null=True, blank=True, max_length=255)  # GitHub
    gitlab = models.CharField(null=True, blank=True, max_length=255)  # GitLab
    linkedin = models.CharField(null=True, blank=True, max_length=255)  # LinkedIn
    hh = models.CharField(null=True, blank=True, max_length=255)  # hh.ru
    site = models.CharField(null=True, blank=True, max_length=255)  # custom lik
    resume_file = models.FileField(null=True, blank=True)  # Файл с резюме
    resume_text = models.TextField(null=True, blank=True)  # Текст резюме
    hack_visited = models.CharField(null=True, blank=True, max_length=255)  # Количество посещенных хакатонов
    select_task = models.ManyToManyField("organization.Task", verbose_name="Выбранные треки", null=True, blank=True)
    # Админские дела
    is_staff = models.BooleanField(_('staff status'), default=False)
    is_superuser = models.BooleanField(_('superuser status'), default=False)
    is_active = models.BooleanField(_('active'), default=True)

    where_to_study = models.CharField(max_length=300, null=True, blank=True,
                                      choices=CHOICES_WHERE_TO_STUDY)  # Где ты учишься
    learning_class = models.IntegerField(null=True, blank=True)  # Курс/Класс

    where_did_you_know = models.CharField(max_length=300, null=True, blank=True, choices=CHOICES_WHERE_DID_YOU_KNOW)
    want_take_part = models.TextField(null=True, blank=True)  # Почему ты хочешь участвовать в Хакатоне
    expect_from_hack = models.TextField(null=True, blank=True)  # Ожидаешь от Хакатона
    something_else = models.TextField(null=True, blank=True)  # Рассказать нам еще что-нибудь
    accept_reglament = models.BooleanField(null=True, blank=True)  # согласиться со всеми

    confirm_send = models.BooleanField(null=True, blank=False, default=False)  # Были разосланы приглашения

    form_complete = models.BooleanField(default=False)
    datetime_hacker_complete = models.DateTimeField(null=True, blank=True, )  # Время когда пользователь созадлся
    skills = TaggableManager(through=UUIDTaggedItem)

    datetime = models.DateTimeField(auto_now_add=True, blank=True, verbose_name="Дата создания")
    datetime_update = models.DateTimeField(auto_now=True, blank=True, verbose_name="Дата обновления")

    USERNAME_FIELD = 'email'

    objects = UserManager()

    def __str__(self):
        return f"{self.role} {self.first_name} {self.second_name}"

    def has_perm(self, perm, obj=None):
        return self.is_superuser

    def full_username(self):
        if self.middle_name:
            return f"{self.second_name} {self.first_name} {self.middle_name}"
        return f"{self.second_name} {self.first_name}"

    def has_module_perms(self, app_label):
        return self.is_superuser

    def get_cv_file_name(self):
        if self.resume_file:
            return self.resume_file.name
        return ''

    def get_cv_file_size(self):
        if self.resume_file:
            return self.resume_file.size
        return ''

    def get_cv_file_url(self):
        if self.resume_file:
            return self.resume_file.url
        return ''


@receiver(user_activated)
def activate_callback(sender, **kwargs):
    user = kwargs['user']
    user.status = max(user.status, 1)
    user.save()
