from django.contrib import admin
from .models import Institute, Specialization, User

admin.site.register(Institute)
admin.site.register(Specialization)
admin.site.register(User)
