# Generated by Django 3.1.7 on 2021-04-20 15:50

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0008_auto_20210407_1016'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='confirm_send',
            field=models.BooleanField(null=True),
        ),
    ]
