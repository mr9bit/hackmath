# Generated by Django 3.1.7 on 2021-04-07 07:16

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('organization', '0016_auto_20210407_1016'),
        ('users', '0007_auto_20210319_2355'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='select_task',
            field=models.ManyToManyField(blank=True, null=True, to='organization.Task', verbose_name='Выбранные треки'),
        ),
    ]
