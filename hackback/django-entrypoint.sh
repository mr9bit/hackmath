echo "REMOVE OLD STATIC"
python manage.py collectstatic --noinput --nooutput --clear --settings=djangoreactredux.settings.prod

echo "COLLECT STATIC"
python manage.py collectstatic --noinput --settings=djangoreactredux.settings.prod

echo "RUN SERVER"
uwsgi --ini /usr/src/app/uwsgi.ini --http :8000
