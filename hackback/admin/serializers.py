from rest_framework import serializers

from users.models import User
from team.models import Team
from team.serializers import TeamSerializer
from taggit_serializer.serializers import TagListSerializerField, TaggitSerializer
from organization.models import Checkpoint


class CheckpointSerializer(serializers.ModelSerializer):
    class Meta:
        model = Checkpoint
        fields = '__all__'


class CountTaskSerializer(serializers.ModelSerializer):
    count = serializers.IntegerField(
        source='author_set.count',
        read_only=True
    )


class LinkSerializer(serializers.Serializer):
    url = serializers.CharField()


class ImageUploadSerializer(serializers.Serializer):
    image = serializers.ImageField()


class HackersSerializer(TaggitSerializer, serializers.ModelSerializer):
    """
        Короткая информация о Пользователе системы
    """
    full_name = serializers.CharField(source='full_username')
    team = TeamSerializer(source='team_mate_set', many=True)
    skills = TagListSerializerField()
    where_did_you_know = serializers.CharField(source='get_where_did_you_know_display')

    class Meta:
        model = User
        fields = '__all__'


class HackersShortSerializer(TaggitSerializer, serializers.ModelSerializer):
    """
        Короткая информация о Пользователе системы
    """
    full_name = serializers.CharField(source='full_username')

    class Meta:
        model = User
        fields = [
            'id',
            'full_name',
            'institute',
            'confirm_send',
            'type_developer',
            'telegram_link',
            'status',
            'first_name',
            'email'
        ]


class CapitanSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'first_name', 'second_name', 'institute']


from organization.models import Task


class TaskName(serializers.ModelSerializer):
    class Meta:
        model = Task
        fields = ['name']


class AdminTeamSerializer(serializers.ModelSerializer):
    capitan = CapitanSerializer()
    task = TaskName()

    class Meta:
        model = Team
        fields = ['id', 'name', 'teammate', 'capitan', 'task', 'report']


class TeamMate(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'phone', 'telegram_link', 'institute', 'birth_date',
                  'email', 'first_name', 'second_name', 'type_developer', ]


class DetailViewAdminTeamSerializer(serializers.ModelSerializer):
    teammate = TeamMate(many=True)
    capitan = TeamMate()

    class Meta:
        model = Team
        fields = ['id', 'name', 'task', 'capitan', 'secret_code', 'datetime', 'datetime_update',
                  'teammate', 'report']
