from django.urls import path, include
from admin import views
from rest_framework import routers

router = routers.DefaultRouter()
router.register('hackers', views.HackersViewSet)
router.register('teams', views.TeamViewSet)
router.register('company', views.CompanyAdminViewSet)
router.register('task', views.TaskAdminViewSet)
router.register('nominations', views.NominationAdminViewSet)
router.register('checkpoint', views.CheckpointViewSet)

urlpatterns = [
    path('admin/', include(router.urls)),
    path('admin/statistic/', views.StatisticsViewSet.as_view(), name='statistic_view_set'),
    path('admin/analytics/', views.AnalyticsViewSet.as_view(), name='analytics_view_set'),
    path('admin/uploadimage/', views.ImageUploadViewSet.as_view(), name='upload_image'),
    path('admin/linkupload/', views.LinkUploadViewSet.as_view(), name='upload_link'),
    path('admin/send_confirm_email/', views.SendConfirmEmail.as_view(), name='send_confirm_link'),
    path('admin/massiv_send_confirm_email/', views.BigSendConfirmEmail.as_view(), name='massiv_send_confirm_email'),
    path('admin/massiv_send_confirm_email_all_hacker/', views.BigSendConfirmEmailForAllHacker.as_view(), name='massiv_send_confirm_email_all_hacker'),
]
