from requests_html import HTMLSession

def get_site_seo(link):
    session = HTMLSession()
    response = session.get(link)
    print(link)
    res = {'title': response.html.find('title', first=True).text}
    for elem in response.html.find('meta'):
        try:
            if "og:image" == elem.attrs['property']:
                res['image'] = {"url": elem.attrs['content']}
        except:
            pass
        try:
            if "og:description" == elem.attrs['property']:
                res['description'] = elem.attrs['content']
        except:
            pass
        try:

            if "description" in elem.attrs["name"]:
                res['description'] = elem.attrs['content']
        except:
            pass
    return res
