from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string
from django.conf import settings


def send_confirm_email(user, signer):
    hacker_name = f"{user.first_name}"
    subject = f"[АвиаХакатон ✈️] -  {hacker_name}, подтверди свое участие!"
    context = {
        'title': subject,
        'confirm_url': signer.get_url(),
        'hacker_name': hacker_name,
    }
    template_name = "confirm/hacker.html"
    to_email = user.email
    rendered = render_to_string(template_name, context)
    msg = EmailMultiAlternatives(subject, rendered, settings.EMAIL_HOST_USER, [to_email])
    msg.attach_alternative(rendered, "text/html")
    msg.send()
