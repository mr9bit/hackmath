from datetime import timedelta
from django.utils import timezone

from rest_framework.pagination import PageNumberPagination
from rest_framework import mixins, viewsets
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.parsers import MultiPartParser
from django.db.models import Count
from django.db.models.functions import ExtractYear
from django_filters.rest_framework import DjangoFilterBackend

from users.models import User
from users.permissions import IsAdmin
from team.models import Team
from admin.serializers import AdminTeamSerializer, DetailViewAdminTeamSerializer
from organization.models import Task, Company, Nomination
from organization.serializers import TaskSerializer, CompanySerializer, NominationSerializer, TaskSaveSerializer, \
    CompanyListSerializer, SaveNominationSerializer, TaskListSerializer
from admin import serializers
from django.conf import settings
import os
import datetime
from .utils import get_site_seo
from users.signers import ConfirmSigner
import time

upload_folder = 'upload'
from rest_framework import serializers as rest_serializers
from admin.email import send_confirm_email
from admin.serializers import CheckpointSerializer
from organization.models import Checkpoint


class CheckpointViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAuthenticated, IsAdmin]
    serializer_class = CheckpointSerializer
    queryset = Checkpoint.objects.all()


class SendConfirmUserSerializer(rest_serializers.Serializer):
    user_id = rest_serializers.CharField(required=True)


class BigSendConfirmEmailForAllHacker(APIView):
    permission_classes = [IsAuthenticated, IsAdmin]
    serializer_class = SendConfirmUserSerializer
    queryset = User.objects.filter(role='hacker', status=2)

    def post(self, request):
        for user in User.objects.filter(role='hacker', status=2):
            signer = ConfirmSigner({'user_id': user.id, }, request=request)
            try:
                send_confirm_email(user, signer)
                user.confirm_send = True
                user.save()
                print(user)
                time.sleep(6)
            except Exception as e:
                print("Ошибка при отправке Email сообщения", e)

        return Response({"complete": True})


class BigSendConfirmEmail(APIView):
    permission_classes = [IsAuthenticated, IsAdmin]
    serializer_class = SendConfirmUserSerializer

    def post(self, request):
        for user in User.objects.filter(role='hacker', status=2, confirm_send=False):
            signer = ConfirmSigner({'user_id': user.id, }, request=request)
            try:
                send_confirm_email(user, signer)
                user.confirm_send = True
                user.save()
                print(user)
                time.sleep(6)
            except Exception as e:
                print("Ошибка при отправке Email сообщения", e)

        return Response({"complete": True})


class SendConfirmEmail(APIView):
    permission_classes = [IsAuthenticated, IsAdmin]
    serializer_class = SendConfirmUserSerializer
    queryset = User.objects.filter(role='hacker', status=2)

    def post(self, request):
        serializer = self.serializer_class(data=self.request.data)
        if serializer.is_valid():
            try:
                user = self.queryset.get(id=serializer.validated_data.get('user_id'))

            except User.DoesNotExist:
                return Response({'error': ["Пользователь не найден или не имеет статус допущен"]},
                                status=status.HTTP_404_NOT_FOUND)
            signer = ConfirmSigner({
                'user_id': user.id,
            }, request=request)
            send_confirm_email(user, signer)
            user.confirm_send = True
            user.save()
            return Response({"complete": True})
        return Response(
            data=serializer.errors,
            status=status.HTTP_400_BAD_REQUEST
        )


class LinkUploadViewSet(APIView):
    def get(self, request):
        if request.GET['url']:
            site = request.GET['url']
            return Response({
                "success": 1,
                "meta": get_site_seo(site)
            })
        return Response({'Error': 'Not Url in request'}, status=status.HTTP_400_BAD_REQUEST)


class ImageUploadViewSet(APIView):
    parser_classes = (MultiPartParser,)
    permission_classes = [IsAuthenticated, IsAdmin]

    def post(self, request):
        serializer = serializers.ImageUploadSerializer(data=self.request.data)
        if serializer.is_valid():
            file_name = request.data.get('image')
            file_full_name = '%s_%s' % ('{0:%Y%m%d%H%M%S%f}'.format(datetime.datetime.now()), file_name,)
            media_root = settings.MEDIA_ROOT
            file_path = os.path.join(media_root, upload_folder)
            with open(os.path.join(file_path, file_full_name), 'wb+') as file:
                for chunk in request.FILES['image'].chunks():
                    file.write(chunk)
            return Response({
                "success": 1,
                "file": {
                    "url": f"{settings.BASE_URL}{os.path.join(settings.MEDIA_URL, upload_folder, file_full_name)}"}
            })
        return Response(
            data=serializer.errors,
            status=status.HTTP_400_BAD_REQUEST
        )


class NonPageResultsSetPagination(PageNumberPagination):
    page_size = 30
    page_size_query_param = 'page_size'
    max_page_size = 1000


class NominationAdminViewSet(viewsets.ModelViewSet):
    """
        Просмотр Компаний
    """
    pagination_class = NonPageResultsSetPagination
    permission_classes = [IsAuthenticated, IsAdmin]
    serializer_class = NominationSerializer
    queryset = Nomination.objects.all()

    def get_serializer_class(self):
        if self.action == 'list':
            return NominationSerializer
        return SaveNominationSerializer


class CompanyAdminViewSet(viewsets.ModelViewSet):
    """
        Просмотр Компаний
    """
    pagination_class = NonPageResultsSetPagination
    permission_classes = [IsAuthenticated, IsAdmin]
    serializer_class = CompanySerializer
    queryset = Company.objects.all()

    def get_serializer_class(self):
        if self.action == 'list':
            return CompanyListSerializer
        return CompanySerializer


class TaskAdminViewSet(viewsets.ModelViewSet):
    """
        Просмотр задач
    """
    pagination_class = NonPageResultsSetPagination
    permission_classes = [IsAuthenticated, IsAdmin]
    serializer_class = TaskSerializer
    queryset = Task.objects.all()

    def get_serializer_class(self):
        if self.action == 'list':
            return TaskListSerializer
        elif self.action in ['update', 'create']:
            return TaskSaveSerializer
        return TaskSerializer


class StandardResultsSetPagination(PageNumberPagination):
    page_size = 10
    page_size_query_param = 'page_size'
    max_page_size = 1000


class BIGResultsSetPagination(PageNumberPagination):
    page_size = 10000
    page_size_query_param = 'page_size'
    max_page_size = 1000


from taggit.models import Tag


class HackersViewSet(mixins.ListModelMixin, mixins.RetrieveModelMixin, viewsets.GenericViewSet):
    """
        Просмотр всех зарегистрованных пользователей
    """
    pagination_class = BIGResultsSetPagination
    permission_classes = [IsAuthenticated, IsAdmin]
    serializer_class = serializers.HackersSerializer
    queryset = User.objects.filter(role='hacker')
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['where_to_study', 'science_degree', "hack_visited", "form_complete", "status", ]

    def get_serializer_class(self):
        if self.action == 'list':
            return serializers.HackersShortSerializer
        return serializers.HackersSerializer

    def get_queryset(self):
        print(self.request.query_params)
        if self.request.query_params.get("skill[]"):
            req_skill = self.request.query_params.getlist("skill[]")
            skills = Tag.objects.filter(name__in=req_skill)
            return User.objects.filter(role='hacker', skills__in=skills)
        if self.request.query_params.get('team') == 'true':
            team = True
        else:
            team = False
        if self.request.query_params.get('team') is not None:
            if len(self.request.query_params.get('team')) > 0:
                queryset = User.objects.filter(role='hacker', team__isnull=team)
                return queryset
        return self.queryset


class TeamViewSet(mixins.ListModelMixin, mixins.RetrieveModelMixin, viewsets.GenericViewSet):
    """
        Просмотр всех созданных команд
    """
    pagination_class = BIGResultsSetPagination
    permission_classes = [IsAuthenticated, IsAdmin]
    serializer_class = AdminTeamSerializer
    queryset = Team.objects.all()

    def get_serializer_class(self):
        if self.action == 'list':
            return AdminTeamSerializer
        return DetailViewAdminTeamSerializer


class StatisticsViewSet(APIView):
    """
        Статистика хакатона
    """
    permission_classes = [IsAuthenticated, IsAdmin]

    def get(self, request):
        return Response({
            'user_count': User.objects.filter(form_complete=False).count(),
            'new_user_count': User.objects.filter(datetime__gte=timezone.now() - timedelta(days=1),
                                                  form_complete=False).count(),
            'hackers_count': User.objects.filter(form_complete=True).count(),
            'new_hacker_count': User.objects.filter(form_complete=True,
                                                    datetime_hacker_complete__gte=timezone.now() - timedelta(
                                                        days=1)).count(),
            'new_team_count': Team.objects.filter(datetime__gte=timezone.now() - timedelta(days=1)).count(),
            'team_count': Team.objects.all().count(),
            'all_users': User.objects.all().count(),
            'user_hist_week': User.objects.filter(datetime__gte=timezone.now() - timedelta(days=7)).extra(
                select={'day': 'date( datetime )'}).values('day') \
                .annotate(available=Count('datetime')).order_by('day'),
            'task_rating': User.objects.exclude(select_task__isnull=True).values('select_task').annotate(
                count=Count('select_task')).order_by('-count'),
            'team_task_rating': Team.objects.exclude(task__isnull=True).values('task').annotate(
                count=Count('task')).order_by('-count'),
            'status_user': {
                "not_confirmed": len(User.objects.filter(status=0)),
                "incomplete": len(User.objects.filter(status=1)),
                "allowed": len(User.objects.filter(status=2)),
                "confirmed": len(User.objects.filter(status=3)),
            }
        }, status=status.HTTP_200_OK)


class AnalyticsViewSet(APIView):
    permission_classes = [IsAuthenticated, IsAdmin]

    def get(self, request):
        return Response({
            'city': User.objects.exclude(city=None).values('city').annotate(count=Count('city')).order_by('-count'),
            'hack_visited': User.objects.exclude(hack_visited=None).values('hack_visited').annotate(
                count=Count('hack_visited')),
            'where_to_study': User.objects.exclude(where_to_study=None).values('where_to_study').annotate(
                count=Count('where_to_study')),
            'science_degree': User.objects.exclude(science_degree=None).values('science_degree').annotate(
                count=Count('science_degree')),
            'specialization': User.objects.exclude(specialization=None).values('specialization').annotate(
                count=Count('specialization')).order_by('-count'),
            'institute': User.objects.exclude(institute=None).values('institute').annotate(
                count=Count('institute')).order_by('-count'),
            'birth_date': User.objects.exclude(birth_date=None).annotate(year=ExtractYear('birth_date')).values(
                'year').annotate(count=Count('year')).order_by('-year'),
        })
