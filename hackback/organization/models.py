from django.db import models
from users.models import User


class Point(models.Model):
    """
        Очки команды
    """
    team = models.ForeignKey("team.Team", on_delete=models.CASCADE)
    count = models.IntegerField(default=0)
    comment = models.TextField(null=True, blank=True)
    user_added = models.ForeignKey(User, on_delete=models.CASCADE)
    datetime = models.DateTimeField(auto_now_add=True, blank=True)


class Schedule(models.Model):
    title = models.TextField(verbose_name="Название меропрития")
    speaker = models.CharField(max_length=300, null=True, blank=True)
    location = models.TextField(null=True, blank=True)
    time_start = models.DateTimeField(blank=True)
    time_end = models.DateTimeField(blank=True)

    class Meta:
        ordering = ["time_start"]


class Company(models.Model):
    CHOICES_TYPE = (
        ('general', 'Генеральный партнер'),
        ('partner', 'Партнер'),
        ('info', 'Инфопартнер'),
    )
    name = models.CharField(max_length=300, verbose_name="Название компании")
    url = models.URLField(verbose_name="Ссылкка")
    logo = models.ImageField(verbose_name="Логотип")
    logo_eng = models.ImageField(verbose_name="Логотип ENG", default='')
    datetime = models.DateTimeField(auto_now_add=True, blank=True)
    datetime_update = models.DateTimeField(auto_now=True, blank=True)
    type = models.CharField(verbose_name="Тип", max_length=320, choices=CHOICES_TYPE)


class Nomination(models.Model):
    name = models.CharField(max_length=300, verbose_name="Название задачи", default='')
    name_eng = models.CharField(max_length=300, verbose_name="Название задачи ENG", default='')
    color = models.CharField(max_length=300, verbose_name="Цвет номинации", default='')

    datetime = models.DateTimeField(auto_now_add=True, blank=True)
    datetime_update = models.DateTimeField(auto_now=True, blank=True)


class Task(models.Model):
    name = models.CharField(max_length=300, verbose_name="Название задачи")
    name_eng = models.CharField(max_length=300, verbose_name="Название задачи ENG")

    short_description = models.TextField(verbose_name="Короткое описание", null=True, blank=True)
    short_description_eng = models.TextField(verbose_name="Короткое описание ENG", null=True, blank=True)

    description = models.JSONField(verbose_name="Описание задачи", null=True, blank=True)
    description_eng = models.JSONField(verbose_name="Описание задачи ENG", null=True, blank=True)

    maintainer = models.ForeignKey(Company, on_delete=models.CASCADE)
    partner = models.ManyToManyField(Company, related_name="partner_set", blank=True)
    nomination = models.ForeignKey(Nomination, on_delete=models.CASCADE, null=True, blank=True)
    datetime = models.DateTimeField(auto_now_add=True, blank=True)
    datetime_update = models.DateTimeField(auto_now=True, blank=True)


class Checkpoint(models.Model):
    name = models.CharField(max_length=300, verbose_name="Название задачи", default="")


class CheckpointResponse(models.Model):
    team = models.ForeignKey("team.Team", on_delete=models.CASCADE)
    expert = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True)
    checkpoint = models.ForeignKey(Checkpoint, on_delete=models.CASCADE, null=True, blank=True)
    good = models.TextField()
    better = models.TextField()
    comment = models.TextField()


class OnlineActivity(models.Model):
    hacker = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name="Пользователь")
    org = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name="Организатор", null=True, blank=True,
                            related_name="organization")
    post_url = models.TextField(null=True, blank=True)
    comment = models.TextField(null=True, blank=True)
    complete = models.BooleanField(default=False)
    datetime = models.DateTimeField(auto_now_add=True, blank=True)
    datetime_update = models.DateTimeField(auto_now=True, blank=True)
