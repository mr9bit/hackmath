from rest_framework import serializers
from organization.models import Task, OnlineActivity, Company, Nomination
from organization.models import Schedule, Point
from team.models import Team


##################
## Рейтинг команд
##################
class TeamRating(serializers.ModelSerializer):
    class Meta:
        model = Team
        fields = ["name"]


class RatingSerializer(serializers.Serializer):
    team = TeamRating()
    count = serializers.IntegerField()


##########
##########

class ScheduleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Schedule
        fields = '__all__'


#############################
### Чекпоинты
#############################
from organization.models import CheckpointResponse, Checkpoint
from users.models import User


class ExpertSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['first_name', 'second_name', 'type_developer', 'company']


class CheckpointResponseSerializer(serializers.ModelSerializer):
    expert = ExpertSerializer()

    class Meta:
        model = CheckpointResponse
        fields = ['team', 'checkpoint', 'expert', 'good', 'better', 'comment']


class CheckpointSerializer(serializers.ModelSerializer):
    """
        Детальный просмотр Компании
    """

    class Meta:
        model = Checkpoint
        fields = ['name', 'id']


#############################
### Компании
#############################

class CompanyListSerializer(serializers.ModelSerializer):
    """
        Список компаний
    """
    logo = serializers.ImageField(required=False)
    type = serializers.CharField(source='get_type_display')

    class Meta:
        model = Company
        fields = ['id', 'name', 'url', 'logo', 'logo_eng', 'datetime', 'datetime_update', 'type']


class CompanySerializer(serializers.ModelSerializer):
    """
        Детальный просмотр Компании
    """
    logo = serializers.ImageField(required=False)

    class Meta:
        model = Company
        fields = ['id', 'name', 'url', 'logo', 'logo_eng', 'datetime', 'datetime_update', 'type']


class CompanyShortSerializer(serializers.ModelSerializer):
    """
        Детальный просмотр Компании
    """

    class Meta:
        model = Company
        depth = 1
        fields = ['id', ]


#############################
### Номинации
#############################

class NominationSerializer(serializers.ModelSerializer):
    """
        Номинации
    """

    class Meta:
        model = Nomination
        fields = ['id', 'name', 'name_eng', 'color']


class SaveNominationSerializer(serializers.ModelSerializer):
    """
        Номинации
    """

    class Meta:
        model = Nomination
        fields = ['id', 'name', 'name_eng', 'color']


#############################
### Задачи
#############################

class TaskSaveSerializer(serializers.ModelSerializer):
    """
        Сохранение Задачи
    """
    partner = serializers.PrimaryKeyRelatedField(many=True, queryset=Company.objects.all(), required=False)

    class Meta:
        model = Task
        fields = ['id', 'name', 'name_eng', 'short_description', 'short_description_eng', 'description',
                  'description_eng', 'nomination', 'partner', 'maintainer', 'datetime']


class TaskListSerializer(serializers.ModelSerializer):
    """
        Сохранение Задачи
    """
    maintainer = CompanySerializer()
    nomination = NominationSerializer()

    class Meta:
        model = Task
        fields = ['id', 'name', 'name_eng', 'short_description', 'short_description_eng', 'nomination', 'maintainer',
                  'datetime']


class ShortTaskSerializer(serializers.ModelSerializer):
    """
        Короткий прсомтр Задачи
    """

    class Meta:
        model = Task
        fields = ['id', 'name', 'name_eng']


class TaskSerializer(serializers.ModelSerializer):
    """
        Полный просмотр задачи
    """
    maintainer = CompanySerializer()
    partner = CompanySerializer(many=True)

    class Meta:
        model = Task
        fields = ['id', 'name', 'name_eng', 'short_description', 'short_description_eng', 'description',
                  'description_eng', 'maintainer', 'partner', 'datetime']


#############################
### OnlineActivity
#############################

class OnlineActivitySerializer(serializers.ModelSerializer):
    """
        Просмтри Активности
    """

    class Meta:
        model = OnlineActivity
        fields = ['post_url', 'comment', 'complete', 'datetime']


class OnlineActivitySaveSerializer(serializers.ModelSerializer):
    """
        Сохранение Актинвости
    """

    class Meta:
        model = OnlineActivity
        fields = ['post_url', ]
