from django.urls import path, include
from organization.views import OnlineActivityViewSet
from rest_framework import routers
from organization import views

router = routers.DefaultRouter()
router.register('task', views.TaskAutoCompleteViewSet)
router.register('track', views.TaskDetailViewSet)
router.register('checkpoint_response', views.CheckpointResponseViewSet)
router.register('checkpoint', views.CheckpointViewSet)

urlpatterns = [
    path('org/onlineactivity/', OnlineActivityViewSet.as_view(), name='onlineactivity_url'),
    path('org/schedule/', views.ScheduleViewSet.as_view(), name='ScheduleViewSet_url'),
    path('org/schedule/today', views.ScheduleTodayViewSet.as_view(), name='ScheduleTodayViewSet_url'),
    path('org/schedule/nextday', views.ScheduleNextDayViewSet.as_view(), name='ScheduleTodayViewSet_url'),
    path('org/team/rating', views.TeamRating.as_view(), name='TeamRating_url'),
    path('org/', include(router.urls)),
]
