from rest_framework import mixins, viewsets
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

from organization.serializers import ShortTaskSerializer, OnlineActivitySerializer, OnlineActivitySaveSerializer
from organization.models import Task, OnlineActivity
from organization import serializers

from users.permissions import IsHacker

from organization.serializers import CheckpointResponseSerializer, CheckpointSerializer, ScheduleSerializer, \
    RatingSerializer
from organization.models import Checkpoint, CheckpointResponse, Schedule, Point
from team.models import Team
from django.db.models import Count, Sum
from django.db.models.functions import TruncDay
import datetime


#######################
### Рейтинг команд
######################
class TeamRating(APIView):
    queryset = Point.objects.all()

    def get(self, request):
        teams_name = Team.objects.all()
        res = []
        for item in self.queryset.values('team').annotate(count=Sum('count')).order_by('-count'):
            res.append({
                'team': teams_name.get(id=item['team']).name,
                'points': item['count']
            })

        return Response(res)


#######################
# Расписание мероприятия
#######################

class ScheduleViewSet(APIView):
    """
        Расписание всего выезда
    """
    serializer_class = ScheduleSerializer
    queryset = Schedule.objects.all()

    def get(self, request):
        all_counted_days = self.queryset.annotate(day=TruncDay('time_start')).values('day').annotate(
            available=Count('day')).order_by("day")
        results = []
        for item in all_counted_days:
            results.append({
                "day": item['day'],
                "results": self.serializer_class(self.queryset.filter(time_end__day=item['day'].day), many=True).data
            })
        return Response(results)


class ScheduleTodayViewSet(APIView):
    """
        Расписание на текущий день
    """
    serializer_class = ScheduleSerializer
    queryset = Schedule.objects.all()

    def get(self, request):
        day = datetime.datetime.today().day
        return Response(self.serializer_class(self.queryset.filter(time_end__day=day), many=True).data)


class ScheduleNextDayViewSet(APIView):
    """
        Расписание на текущий день
    """
    serializer_class = ScheduleSerializer
    queryset = Schedule.objects.all()

    def get(self, request):
        day = (datetime.datetime.today() + datetime.timedelta(days=1)).day
        return Response(self.serializer_class(self.queryset.filter(time_end__day=day), many=True).data)


#######################
# Чек-Поинты участников
#######################

class CheckpointViewSet(mixins.ListModelMixin, viewsets.GenericViewSet):
    """
        Список чекпоинтов
    """
    permission_classes = [IsAuthenticated, IsHacker]
    serializer_class = CheckpointSerializer
    queryset = Checkpoint.objects.all()


class CheckpointResponseViewSet(mixins.ListModelMixin, viewsets.GenericViewSet):
    """
        Список отзывов экспертов
    """
    permission_classes = [IsAuthenticated, IsHacker]
    serializer_class = CheckpointResponseSerializer
    queryset = CheckpointResponse.objects.all()

    def get_queryset(self):
        team = Team.objects.filter(teammate=self.request.user).first()
        if team:
            return CheckpointResponse.objects.filter(team=team)
        else:
            return CheckpointResponse.objects.none()


#######################
# Задачи
#######################
class TaskAutoCompleteViewSet(mixins.ListModelMixin, viewsets.GenericViewSet):
    """
        Автозаполнение
    """
    permission_classes = [IsAuthenticated, IsHacker]
    serializer_class = ShortTaskSerializer
    queryset = Task.objects.all()


class TaskDetailViewSet(mixins.ListModelMixin, mixins.RetrieveModelMixin, viewsets.GenericViewSet):
    """
        Просмотр задач.

        GET
            Список задач
        GET [id]
            Детальный просмотр задачи

        Права:
            Только Хакер
    """
    permission_classes = [IsAuthenticated, IsHacker]
    serializer_class = serializers.TaskSerializer
    queryset = Task.objects.all()

    def get_serializer_class(self):
        if self.action == 'list':
            return serializers.TaskListSerializer
        return serializers.TaskSerializer


class OnlineActivityViewSet(APIView):
    permission_classes = [IsAuthenticated, IsHacker]
    serializer_class = OnlineActivitySerializer
    save_serializer_class = OnlineActivitySaveSerializer
    queryset = OnlineActivity.objects.all()

    def get(self, request):
        try:
            snippet = self.queryset.get(hacker=self.request.user)
            serializer = self.serializer_class(snippet)
            return Response(serializer.data, status=status.HTTP_200_OK)
        except OnlineActivity.DoesNotExist:
            return Response({'post_url': None}, status=status.HTTP_404_NOT_FOUND)

    def post(self, request, format=None):
        try:
            snippet = self.queryset.get(hacker=self.request.user)
            return Response(status=status.HTTP_404_NOT_FOUND)
        except OnlineActivity.DoesNotExist:
            serializer = self.save_serializer_class(data=request.data)
            if serializer.is_valid():
                instance = serializer.save(hacker=self.request.user)
                return Response(self.serializer_class(instance).data, status=status.HTTP_201_CREATED)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
