from django.db import transaction
from rest_framework import status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response

from rest_registration import signals
from rest_registration.decorators import (
    api_view_serializer_class_getter
)
from rest_registration.exceptions import UserWithoutEmailNonverifiable
from rest_registration.settings import registration_settings
from rest_registration.utils.users import (
    get_user_email_field_name,
    get_user_setting
)
from rest_registration.utils.verification_notifications import (
    send_register_verification_email_notification
)
from rest_framework.views import APIView
from expert.serializers import ExpertProfileSerializer
from django.utils import timezone
from team.models import Team
from django.db.models import Count
from django.db.models.functions import ExtractYear


@api_view_serializer_class_getter(lambda: registration_settings.REGISTER_SERIALIZER_CLASS)
@api_view(['POST'])
@permission_classes(registration_settings.NOT_AUTHENTICATED_PERMISSION_CLASSES)
def register(request):
    '''
    Register new user.
    '''
    serializer_class = registration_settings.REGISTER_SERIALIZER_CLASS
    serializer = serializer_class(data=request.data, context={'request': request})
    serializer.is_valid(raise_exception=True)

    kwargs = {}

    if registration_settings.REGISTER_VERIFICATION_ENABLED:
        verification_flag_field = get_user_setting('VERIFICATION_FLAG_FIELD')
        kwargs[verification_flag_field] = False
        email_field_name = get_user_email_field_name()
        if (email_field_name not in serializer.validated_data
                or not serializer.validated_data[email_field_name]):
            raise UserWithoutEmailNonverifiable()

    with transaction.atomic():
        user = serializer.save(**kwargs, role="expert")
        if registration_settings.REGISTER_VERIFICATION_ENABLED:
            send_register_verification_email_notification(request, user)

    signals.user_registered.send(sender=None, user=user, request=request)
    output_serializer_class = registration_settings.REGISTER_OUTPUT_SERIALIZER_CLASS
    output_serializer = output_serializer_class(
        instance=user,
        context={'request': request},
    )
    user_data = output_serializer.data
    return Response(user_data, status=status.HTTP_201_CREATED)


from users.permissions import IsExpert
from rest_framework.permissions import IsAuthenticated
from rest_framework import mixins, viewsets
from organization.models import Task, Schedule
from expert.serializers import TaskListSerializer
from admin.views import BIGResultsSetPagination

from admin import serializers
from users.models import User
from django_filters.rest_framework import DjangoFilterBackend
from expert.serializers import HackersShortSerializer
from admin.serializers import CheckpointSerializer
from organization.models import Checkpoint, CheckpointResponse, Point
from organization.serializers import ScheduleSerializer
from expert.serializers import CheckpointResponseSerializer, MyCheckpointResponseSerializer, TeamResultSerializer, \
    TeamCoinSerializer, PointSerializer
from team.models import Result


######################
## Добавление ITCoin
######################
class TeamCoinList(viewsets.GenericViewSet, mixins.ListModelMixin):
    """
        Список команд для выставления очков
    """
    permission_classes = [IsAuthenticated, IsExpert]
    serializer_class = TeamCoinSerializer
    queryset = Team.objects.all()


class SecretPointAdd(viewsets.GenericViewSet, mixins.RetrieveModelMixin, mixins.CreateModelMixin,
                     mixins.ListModelMixin):
    """
         Добавление очков
    """
    permission_classes = [IsAuthenticated, IsExpert]
    serializer_class = PointSerializer
    queryset = Point.objects.all()

    def perform_create(self, serializer):
        serializer.save(user_added=self.request.user)


#######################
## Результаты команд
#######################

class TeamResultsViewSet(mixins.ListModelMixin, mixins.RetrieveModelMixin, viewsets.GenericViewSet):
    permission_classes = [IsAuthenticated, IsExpert]
    serializer_class = TeamResultSerializer
    queryset = Result.objects.all()

    def get_queryset(self):
        return Result.objects.filter(team__task=self.request.user.expert_task)


#######################
# Чек-Поинты
#######################

class CheckPointAlreadyViewSet(mixins.ListModelMixin, viewsets.GenericViewSet):
    """
        Отзывы которые заполнил эксепрт
    """
    pagination_class = BIGResultsSetPagination
    permission_classes = [IsAuthenticated, IsExpert]
    serializer_class = MyCheckpointResponseSerializer
    queryset = CheckpointResponse.objects.all()

    def get_queryset(self):
        return CheckpointResponse.objects.filter(expert=self.request.user)


class CheckpointResponseViewSet(viewsets.ModelViewSet):
    """
        Чек поинты
    """
    permission_classes = [IsAuthenticated, IsExpert]
    serializer_class = CheckpointResponseSerializer
    queryset = Checkpoint.objects.all()

    def perform_create(self, serializer):
        serializer.save(expert=self.request.user)


#######################
# Задачи
#######################
class TaskAutoCompleteViewSet(mixins.ListModelMixin, viewsets.GenericViewSet):
    """
        Автозаполнение
    """
    pagination_class = BIGResultsSetPagination
    permission_classes = [IsAuthenticated, IsExpert]
    serializer_class = TaskListSerializer
    queryset = Task.objects.all()


from admin.serializers import AdminTeamSerializer, DetailViewAdminTeamSerializer


class TeamViewSet(mixins.ListModelMixin, mixins.RetrieveModelMixin, viewsets.GenericViewSet):
    """
        Просмотр всех созданных команд
    """
    pagination_class = BIGResultsSetPagination
    permission_classes = [IsAuthenticated, IsExpert]
    serializer_class = AdminTeamSerializer
    queryset = Team.objects.all()

    def get_queryset(self):
        return self.queryset.filter(task=self.request.user.expert_task)

    def get_serializer_class(self):
        if self.action == 'list':
            return AdminTeamSerializer
        return DetailViewAdminTeamSerializer


class CheckpointViewSet(mixins.ListModelMixin, mixins.RetrieveModelMixin, viewsets.GenericViewSet):
    permission_classes = [IsAuthenticated, IsExpert]
    serializer_class = CheckpointSerializer
    queryset = Checkpoint.objects.all()


class HackersViewSet(mixins.ListModelMixin, mixins.RetrieveModelMixin, viewsets.GenericViewSet):
    """
        Просмотр всех зарегистрованных пользователей
    """
    pagination_class = BIGResultsSetPagination
    permission_classes = [IsAuthenticated, IsExpert]
    serializer_class = serializers.HackersSerializer
    queryset = User.objects.filter(role='hacker')
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['where_to_study', 'science_degree', "hack_visited", "form_complete", "status"]

    def get_serializer_class(self):
        if self.action == 'list':
            return HackersShortSerializer
        return serializers.HackersSerializer

    def get_queryset(self):
        return self.queryset.filter(team_mate_set__task_id=self.request.user.expert_task.id)


class UserProfile(APIView):
    permission_classes = [IsAuthenticated, IsExpert]
    serializer_class = ExpertProfileSerializer

    def get(self, request):
        return Response(self.serializer_class(self.request.user).data, status=status.HTTP_200_OK)

    def put(self, request, *args, **kwargs):
        data = request.data
        serializer = self.serializer_class(self.request.user, data=data)
        if serializer.is_valid():
            if not request.user.form_complete:
                serializer.save(form_complete=True, datetime_hacker_complete=timezone.now(), status=3)
            else:
                serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


from users.models import User


class StatisticsViewSet(APIView):
    """
        Статистика хакатона
    """
    permission_classes = [IsAuthenticated, IsExpert]

    def get(self, request):
        users = User.objects.filter(team_mate_set__task_id=self.request.user.expert_task.id)
        return Response({
            "count_team": len(Team.objects.filter(task=self.request.user.expert_task)),
            "count_users": len(users),
            "where_to_study": users.exclude(where_to_study=None).values('where_to_study').annotate(
                count=Count('where_to_study')),
            "hack_visited": users.exclude(hack_visited=None).values('hack_visited').annotate(
                count=Count('hack_visited')),
            "birth_date": users.exclude(birth_date=None).annotate(year=ExtractYear('birth_date')).values(
                'year').annotate(count=Count('year')).order_by('-year'),
        })
