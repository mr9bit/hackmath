from django.urls import path, include
from organization.views import OnlineActivityViewSet
from rest_framework import routers
from expert import views

router = routers.DefaultRouter()
router.register('tasks', views.TaskAutoCompleteViewSet)
router.register('teams', views.TeamViewSet)
router.register('hackers', views.HackersViewSet)
router.register('checkpoint', views.CheckpointViewSet)
router.register('checkpoint_response', views.CheckpointResponseViewSet)
router.register('my_check_point', views.CheckPointAlreadyViewSet)
router.register('team_results', views.TeamResultsViewSet)
router.register('coins', views.SecretPointAdd)
router.register('team_coins', views.TeamCoinList)

urlpatterns = [
    path('expert/', include(router.urls)),
    path('expert/profile/', views.UserProfile.as_view(), name='expert_profile'),
    path('expert/statistics/', views.StatisticsViewSet.as_view(), name='expert_statistics'),

]
