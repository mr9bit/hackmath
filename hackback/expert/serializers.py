from organization.models import Task
from organization.models import Company, Point
from rest_framework import serializers
from team.models import Team, Result
from users.models import User
from taggit_serializer.serializers import TaggitSerializer
from admin.serializers import TeamSerializer
from admin.serializers import AdminTeamSerializer, DetailViewAdminTeamSerializer


################
## IT Баллы
################

class TeamCoinSerializer(serializers.ModelSerializer):
    class Meta:
        model = Team
        fields = ["id", "name"]


class PointSerializer(serializers.ModelSerializer):
    class Meta:
        model = Point
        fields = ['team', 'count', 'comment']


####
## Результаты
####
class TeamResultSerializer(serializers.ModelSerializer):
    team = AdminTeamSerializer()

    class Meta:
        model = Result
        fields = ['link_to_git', 'team', 'link_to_presentation']


class HackerTeamSerializer(serializers.ModelSerializer):
    class Meta:
        model = Team
        fields = ["name"]


class HackersShortSerializer(TaggitSerializer, serializers.ModelSerializer):
    """
        Короткая информация о Пользователе системы
    """
    full_name = serializers.CharField(source='full_username')
    team = TeamSerializer(source='team_mate_set', many=True)

    class Meta:
        model = User
        fields = [
            'id',
            'full_name',
            'institute',
            'confirm_send',
            'type_developer',
            'telegram_link',
            'team',
            'status',
            'first_name',
            'email'
        ]


class CompanySerializer(serializers.ModelSerializer):
    """
        Детальный просмотр Компании
    """

    class Meta:
        model = Company
        fields = ['name', ]


class TaskListSerializer(serializers.ModelSerializer):
    """
        Сохранение Задачи
    """
    maintainer = CompanySerializer()

    class Meta:
        model = Task
        fields = ['id', 'name', 'maintainer']


from users.models import User


class ExpertProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ["first_name", "second_name", "middle_name", "gender", "birth_date", "city",
                  "telegram_link", "phone", "expert_task", "type_developer", "company"]


from organization.models import CheckpointResponse


class MyCheckpointResponseSerializer(serializers.ModelSerializer):
    """
        Детальный просмотр Компании
    """

    class Meta:
        model = CheckpointResponse
        fields = ['team', 'checkpoint']


class CheckpointResponseSerializer(serializers.ModelSerializer):
    """
        Детальный просмотр Компании
    """

    class Meta:
        model = CheckpointResponse
        fields = ['team', 'expert', 'good', 'better', 'checkpoint', 'comment']
