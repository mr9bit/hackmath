from rest_framework import serializers
from .models import Team, SearchTeam, Result
from users.models import User
from users.serializers import UserInfoSerializer


###
# Результаты
###
class ResultSerializer(serializers.ModelSerializer):
    class Meta:
        model = Result
        fields = ['link_to_git', 'link_to_presentation']


###
###    Интерфейс API Команды
###

class InviteTeamSerializer(serializers.Serializer):
    invite = serializers.CharField()


class DeleteTeamSerializer(serializers.Serializer):
    id = serializers.IntegerField()


class CreateTeamSerializer(serializers.ModelSerializer):
    class Meta:
        model = Team
        fields = ['name', ]


class TeamMate(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'email', 'first_name', 'second_name', 'type_developer', ]


class TeamSerializer(serializers.ModelSerializer):
    teammate = TeamMate(many=True)

    class Meta:
        model = Team
        fields = ['id', 'name', 'capitan', 'task', 'secret_code', 'datetime', 'datetime_update', 'teammate', 'report']


###
###    Интерфейс API Поиск сокомандника
###
from organization.serializers import ShortTaskSerializer


class SearchTeamSerializer(serializers.ModelSerializer):
    team = CreateTeamSerializer()
    user = UserInfoSerializer()
    selected_tracks = ShortTaskSerializer(many=True)

    class Meta:
        model = SearchTeam
        exclude = (
            'datetime',
        )


class CreateSearchTeamSerializer(serializers.ModelSerializer):
    class Meta:
        model = SearchTeam
        exclude = (
            'type',
            'user',
            'team',
            'datetime'
        )


class InviteToSearchTeamSerializer(serializers.Serializer):
    search_from = serializers.IntegerField()
    search_to = serializers.IntegerField()


class TeamSelectTaskSerializer(serializers.Serializer):
    team = serializers.IntegerField()
    task = serializers.IntegerField()


class TeamSendReportSerializer(serializers.Serializer):
    team = serializers.IntegerField()
    report = serializers.CharField()
