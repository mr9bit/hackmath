from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string
from django.conf import settings


def send_join_to_team(team, invited_user):
    hacker_name = f"{invited_user.first_name} {invited_user.second_name}"
    subject = f"[Авиахакатон ✈️] - К вашей команде присоединился(лась) {hacker_name}"
    context = {
        'title': subject,
        'name': team.capitan.first_name,
        'team_name': team.name,
        'hacker_name': hacker_name,
    }
    template_name = "team/join.html"
    to_email = team.capitan.email
    rendered = render_to_string(template_name, context)
    msg = EmailMultiAlternatives(subject, rendered, settings.EMAIL_HOST_USER, [to_email])
    msg.attach_alternative(rendered, "text/html")
    msg.send()


def send_leave_from_team(team, leave_user):
    hacker_name = f"{leave_user.first_name} {leave_user.second_name}"
    subject = f"[Авиахакатон ✈️] - Из вашей команды ушел(а) {hacker_name}"
    context = {
        'title': subject,
        'name': team.capitan.first_name,
        'team_name': team.name,
        'hacker_name': hacker_name,
    }
    template_name = "team/leave.html"
    to_email = team.capitan.email
    rendered = render_to_string(template_name, context)
    msg = EmailMultiAlternatives(subject, rendered, settings.EMAIL_HOST_USER, [to_email])
    msg.attach_alternative(rendered, "text/html")
    msg.send()


def send_invite_to_team(search_from, search_to):
    if search_from.type == 'team' and search_to.type == 'hacker':  # команда зовет пользователя
        subject = f"[Авиахакатон ✈️] - Вас позвали в команду {search_from.team.name}"
        to_email = search_to.user.email
        template_name = "searchteam/team_user.html"
        context = {
            'name': search_to.user.first_name,
            'team_name': search_from.team.name,
            'tasks': search_from.selected_tracks.all(),
            'about': search_from.about,
            'capitan_contact': search_from.team.capitan.email,
            'contact_fast': search_from.contact,
            'url_leave': f"{settings.BASE_DOMAIN_URL}/team"
        }
    elif search_from.type == 'hacker' and search_to.type == 'team':  # пользователь хочет присоединиться к команде
        subject = f"[Авиахакатон ✈️] - К вашей команде хочет присоединиться {search_from.user.first_name} {search_from.user.second_name}"
        to_email = search_to.team.capitan.email
        template_name = "searchteam/user_team.html"
        context = {
            'name': search_to.team.capitan.first_name,
            'roles': search_from.role,
            'about': search_from.about,
            'email': search_from.user.email,
            'hacker_name': f"{search_from.user.first_name} {search_from.user.second_name}",
            'contact_fast': search_from.contact,
            'url_leave': f"{settings.BASE_DOMAIN_URL}/team"
        }
    elif search_from.type == 'hacker' and search_to.type == 'hacker':  # пользователь хочется присоединиться к пользователю
        subject = f"[Авиахакатон ✈️] - К вам хочет присоединиться {search_from.user.first_name} {search_from.user.second_name}"
        to_email = search_to.user.email
        template_name = "searchteam/hacker_hacker.html"
        context = {
            'name': search_to.user.first_name,
            'tasks': search_from.selected_tracks.all(),
            'roles': search_from.role,
            'about': search_from.about,
            'email': search_from.user.email,
            'hacker_name': f"{search_from.user.first_name} {search_from.user.second_name}",
            'contact_fast': search_from.contact,
            'url_leave': f"{settings.BASE_DOMAIN_URL}/team"
        }

    elif search_from.type == 'team' and search_to.type == 'team':  # команда хочет слиться с командой
        subject = f"[Авиахакатон ✈️] - К вам хочет присоединиться команда {search_from.team.name}"
        to_email = search_to.team.capitan.email
        template_name = "searchteam/team_team.html"
        context = {
            'name': search_to.team.capitan.first_name,
            'tasks': search_from.selected_tracks.all(),
            'team_name': search_from.team.name,
            'about': search_from.about,
            'capitan_contact': search_from.team.capitan.email,
            'contact_fast': search_from.contact,
            'url_leave': f"{settings.BASE_DOMAIN_URL}/team"
        }

    from_email = settings.EMAIL_HOST_USER
    rendered = render_to_string(template_name, context)
    msg = EmailMultiAlternatives(subject, rendered, from_email, [to_email])
    msg.attach_alternative(rendered, "text/html")
    msg.send()
