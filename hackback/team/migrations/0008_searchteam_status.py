# Generated by Django 3.1.7 on 2021-03-07 21:32

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('team', '0007_auto_20210306_2228'),
    ]

    operations = [
        migrations.AddField(
            model_name='searchteam',
            name='status',
            field=models.CharField(blank=True, choices=[('p', 'Опубликовано'), ('d', 'Черновик')], max_length=1, null=True, verbose_name='Статус поиска'),
        ),
    ]
