from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

from users.permissions import IsHacker
from team.serializers import TeamSerializer, CreateTeamSerializer, DeleteTeamSerializer, InviteTeamSerializer, \
    CreateSearchTeamSerializer, SearchTeamSerializer, InviteToSearchTeamSerializer, TeamSelectTaskSerializer, \
    ResultSerializer, TeamSendReportSerializer

from team.models import Team, generate_humanize_code, SearchTeam, SearchTeamInvite, get_request_per_day, \
    get_instance_request_per_day, Result
from team.email import send_invite_to_team, send_join_to_team, send_leave_from_team
from django.conf import settings


class ResultViewSet(APIView):
    permission_classes = [IsAuthenticated, IsHacker]
    serializer = ResultSerializer
    queryset = Result.objects.all()

    def post(self, request):
        serializer = self.serializer(data=request.data)
        if serializer.is_valid():
            team = Team.objects.filter(teammate=self.request.user).first()
            serializer.save(team=team)
            return Response(serializer.data, status=200)
        return Response(serializer.errors)

    def get(self, request):
        team = Team.objects.filter(teammate=self.request.user).first()
        if not team:
            return Response(status=404)
        try:
            result = Result.objects.get(team=team)
        except Result.DoesNotExist:
            return Response({'error': ["Результата еще нет"]}, status=404)
        serializer = ResultSerializer(result)
        if result:
            return Response(serializer.data, status=200)
        else:
            return Response(serializer.errors, status=404)


class InviteToSearchTeamViewSet(APIView):
    permission_classes = [IsAuthenticated, IsHacker]
    serializer = InviteToSearchTeamSerializer
    queryset = SearchTeam.objects.all()

    def post(self, request):
        serializer = self.serializer(data=request.data)
        if serializer.is_valid():

            if settings.NUMBER_SEARCH_REQUEST_PER_DAY - get_request_per_day(self.request.user) > 0:
                instance_to = self.queryset.get(id=serializer.validated_data.get('search_to'))  # кому запрос

                if instance_to.type == 'hacker' and instance_to.user == self.request.user:
                    return Response({'error': ["Вы не можете выслать приглашение сами себе"]},
                                    status=status.HTTP_400_BAD_REQUEST)
                try:
                    if instance_to.team.teammate.filter(id=self.request.user.id):
                        return Response({'error': ["Вы не можете выслать приглашение своей команде"]},
                                        status=status.HTTP_400_BAD_REQUEST)
                except Exception as e:
                    pass

                if get_instance_request_per_day(self.request.user).filter(to_searchteam=instance_to):
                    return Response({'error': ["Вы уже высылали запрос сегодня"]},
                                    status=status.HTTP_400_BAD_REQUEST)

                instance_from = self.queryset.get(id=serializer.validated_data.get('search_from'))  # от кого запрос
                instance_requests = SearchTeamInvite.objects.create(
                    # создаем лог, соклько пользвателей в день отсылает запросов
                    from_searchteam=instance_from,
                    to_searchteam=instance_to,
                    user_send=self.request.user,
                )
                send_invite_to_team(instance_from, instance_to)
                count_request = instance_requests.get_request_per_day(self.request.user)
                return Response({'available_of_requests': settings.NUMBER_SEARCH_REQUEST_PER_DAY - count_request},
                                status=status.HTTP_200_OK)
            return Response({'error': ["Вы привысили лимит запросов в день, попробуйте через 24 часа"]},
                            status=status.HTTP_400_BAD_REQUEST)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


###
### Интерфейс API Поиск сокомандника
###

class SearchTeamViewSet(APIView):
    permission_classes = [IsAuthenticated, IsHacker]
    create_serializer = CreateSearchTeamSerializer
    queryset = SearchTeam.objects.all()
    view_serializer = SearchTeamSerializer

    def delete(self, request):
        try:
            instance = self.queryset.get(team__teammate=self.request.user, status='p')
            instance.status = 'd'
            instance.save()
            return Response({'complete': True}, status=status.HTTP_200_OK)
        except SearchTeam.DoesNotExist:
            try:
                instance = self.queryset.get(user=self.request.user, status='p')
                instance.status = 'd'
                instance.save()
                return Response({'complete': True}, status=status.HTTP_200_OK)
            except SearchTeam.DoesNotExist:
                return Response({'error': ['You dont have a search request']}, status=status.HTTP_404_NOT_FOUND)

    def post(self, request):
        """
            Create SearchTeam
        """
        serializer = self.create_serializer(data=request.data)
        if serializer.is_valid():
            if len(self.queryset.filter(team__teammate=self.request.user,
                                        status='p')):  # если команда уже создала свою вакансию и она опубликовано
                return Response({'error': ['You team already search teammate']}, status=status.HTTP_403_FORBIDDEN)
            if len(self.queryset.filter(user=self.request.user,
                                        status='p')):  # если пользвоатель создает запрос на поиск и при этом у него уже есть публикация
                return Response({'error': ['You already search team']}, status=status.HTTP_403_FORBIDDEN)

            try:
                # Пытаемся найти запрос на поиск если пользвоатель уже создал запрос и обновляем его
                if self.request.user.team_mate_set.first():
                    instance = self.queryset.get(team=self.request.user.team_mate_set.first(), type='team',
                                                 status='d')  # экземпляр поиск от команды
                    instance.contact = serializer.validated_data.get('contact')
                    instance.role = serializer.validated_data.get('role')
                    instance.selected_tracks.set(serializer.validated_data.get('selected_tracks'))
                    instance.about = serializer.validated_data.get('about')
                    instance.type = "team"
                else:  # если нет команды то смотрим у самого пользователя
                    instance = self.queryset.get(user=self.request.user, type='hacker', status='d')
                    instance.contact = serializer.validated_data.get('contact')
                    instance.role = serializer.validated_data.get('role')
                    instance.selected_tracks.set(serializer.validated_data.get('selected_tracks'))
                    instance.about = serializer.validated_data.get('about')
                    instance.type = "hacker"
                instance.status = 'p'
                instance.save()
                return Response({
                    'available_of_requests': settings.NUMBER_SEARCH_REQUEST_PER_DAY - get_request_per_day(
                        self.request.user),
                    'my': self.view_serializer(instance).data,
                    'teams': self.view_serializer(self.queryset.filter(status='p', type='team'), many=True).data,
                    'hackers': self.view_serializer(self.queryset.filter(status='p', type='hacker'), many=True).data
                }, status=status.HTTP_201_CREATED)
            except SearchTeam.DoesNotExist:  # ничего не нашли, то создаем
                if self.request.user.team_mate_set.first():  # если пользователь состоит к команде
                    instance = serializer.save(status='p',
                                               team=self.request.user.team_mate_set.first(),
                                               type='team')
                else:
                    serializer.is_valid()
                    instance = serializer.save(user=self.request.user,
                                               status='p',
                                               type='hacker')

                return Response({
                    'available_of_requests': settings.NUMBER_SEARCH_REQUEST_PER_DAY - get_request_per_day(
                        self.request.user),
                    'my': self.view_serializer(instance).data,
                    'teams': self.view_serializer(self.queryset.filter(status='p', type='team'), many=True).data,
                    'hackers': self.view_serializer(self.queryset.filter(status='p', type='hacker'), many=True).data
                }, status=status.HTTP_201_CREATED)
            except Exception as e:  # другие возможные ошибки
                return Response({'error': [str(e)]}, status=status.HTTP_400_BAD_REQUEST)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get(self, request):
        if len(self.queryset.filter(team__teammate=self.request.user, status='p')) or \
                len(self.queryset.filter(user=self.request.user, status='p')):
            try:
                # пытаемся получить запрос если есть запрос от команды
                instance = self.queryset.get(team=self.request.user.team_mate_set.first(), type='team', status='p')
                return Response({
                    'available_of_requests': settings.NUMBER_SEARCH_REQUEST_PER_DAY - get_request_per_day(
                        self.request.user),
                    'my': self.view_serializer(instance).data,
                    'teams': self.view_serializer(self.queryset.filter(type='team', status='p'), many=True).data,
                    'hackers': self.view_serializer(self.queryset.filter(type='hacker', status='p'), many=True).data
                }, status=status.HTTP_200_OK)
            except SearchTeam.DoesNotExist:
                instance = self.queryset.get(user=self.request.user, type='hacker', status='p')  # заявка на просмотр
                return Response({
                    'available_of_requests': settings.NUMBER_SEARCH_REQUEST_PER_DAY - get_request_per_day(
                        self.request.user),
                    'my': self.view_serializer(instance).data,
                    'teams': self.view_serializer(self.queryset.filter(type='team', status='p'), many=True).data,
                    'hackers': self.view_serializer(self.queryset.filter(type='hacker', status='p'), many=True).data
                }, status=status.HTTP_200_OK)
            except Exception as e:
                return Response({'error': [e]}, status=status.HTTP_400_BAD_REQUEST)

        elif len(self.queryset.filter(team__teammate=self.request.user, status='d')) or \
                len(self.queryset.filter(user=self.request.user, status='d')):
            try:  # сначала проверям как у члена команды
                instance = self.queryset.get(team__teammate=self.request.user, status='d')
                return Response({
                    'available_of_requests': settings.NUMBER_SEARCH_REQUEST_PER_DAY - get_request_per_day(
                        self.request.user),
                    'my': self.view_serializer(instance).data,
                    'teams': None,
                    'hackers': None
                }, status=status.HTTP_200_OK)
            except SearchTeam.DoesNotExist:  # если не было как у члена команды, то выдаем ему его же форму
                try:
                    instance = self.queryset.get(user=self.request.user, status='d')
                    return Response({
                        'available_of_requests': settings.NUMBER_SEARCH_REQUEST_PER_DAY - get_request_per_day(
                            self.request.user),
                        'my': self.view_serializer(instance).data,
                        'teams': None,
                        'hackers': None
                    }, status=status.HTTP_200_OK)
                except SearchTeam.DoesNotExist:
                    return Response({'error': ['Form does not exist']}, status=status.HTTP_404_NOT_FOUND)

        return Response({'error': ['Form does not exist']}, status=status.HTTP_404_NOT_FOUND)


###
###    Интерфейс API Команды
###

class InviteTeamViewSet(APIView):
    """
        Присоединиться к команде
    """
    permission_classes = [IsAuthenticated, IsHacker]
    serializer_class = InviteTeamSerializer
    team_serializer_class = TeamSerializer
    queryset = Team.objects.all()

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            try:  # Проверям, вдруг пользовать уже есть в команде
                self.queryset.get(teammate=self.request.user)
                return Response({'error': ['У вас уже есть команда']}, status=status.HTTP_403_FORBIDDEN)
            except Team.DoesNotExist:
                try:  # проверям, есть ли команда с таким secret code
                    team = self.queryset.get(secret_code=serializer.validated_data.get('invite'))
                    if len(team.teammate.all()) == 5:
                        return Response({'error': ['Превышено максимальное количество участников']},
                                        status=status.HTTP_403_FORBIDDEN)
                    team.teammate.add(self.request.user)
                    try:
                        inst = SearchTeam.objects.get(user=self.request.user)
                        inst.status = 'd'
                        inst.save()
                    except SearchTeam.DoesNotExist:
                        pass
                    send_join_to_team(team, self.request.user)
                    serializer = self.team_serializer_class(team)
                    return Response(serializer.data, status=status.HTTP_200_OK)
                except Team.DoesNotExist:
                    return Response({'error': ["Команда не найдена"]}, status=status.HTTP_404_NOT_FOUND)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class TeamViewSet(APIView):
    permission_classes = [IsAuthenticated, IsHacker]
    serializer_class = TeamSerializer
    create_team_serializer = CreateTeamSerializer
    delete_serializer = DeleteTeamSerializer
    queryset = Team.objects.all()

    def post(self, request):
        """
            Create team
            Check if team exist or user has a team
        """
        serializer = self.create_team_serializer(data=request.data)
        if serializer.is_valid():
            try:
                self.queryset.get(name=serializer.validated_data.get('name'))
                return Response({'error': ['Такая команда уже сушествует']}, status=status.HTTP_403_FORBIDDEN)
            except Team.DoesNotExist:
                try:
                    self.queryset.get(teammate=self.request.user)
                    return Response({'error': ['У вас уже есть команда']}, status=status.HTTP_403_FORBIDDEN)
                except Team.DoesNotExist:
                    instance = serializer.save(capitan=self.request.user,
                                               secret_code=generate_humanize_code(5))
                    try:
                        inst = SearchTeam.objects.get(user=self.request.user)
                        inst.status = 'd'
                        inst.save()
                    except SearchTeam.DoesNotExist:
                        pass
                    instance.teammate.add(self.request.user)
                    return Response(self.serializer_class(instance).data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get(self, request):
        try:
            snippet = self.queryset.get(teammate=self.request.user)
            serializer = self.serializer_class(snippet)
            return Response(serializer.data, status=status.HTTP_200_OK)
        except Team.DoesNotExist:
            return Response({'error': ["У вас нет команды"]}, status=status.HTTP_404_NOT_FOUND)

    def delete(self, request):
        serializer = self.delete_serializer(data=request.data)
        if serializer.is_valid():
            try:
                snippet = self.queryset.get(id=serializer.validated_data.get('id'), teammate=self.request.user)
                if snippet.capitan == self.request.user:  # Если капитан удаляет группу
                    try:
                        SearchTeam.objects.get(team=snippet).delete()
                    except SearchTeam.DoesNotExist:
                        pass
                    snippet.delete()
                else:  # Если участник решил выйти из команды
                    snippet.teammate.remove(self.request.user)
                    send_leave_from_team(snippet, self.request.user)
                return Response({'complete': True}, status=status.HTTP_200_OK)
            except Team.DoesNotExist:
                return Response({'error': ["У вас нет команды"]}, status=status.HTTP_404_NOT_FOUND)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


from organization.models import Task


#### Выбор задачи от команды
class TeamSelectTaskViewSet(APIView):
    permission_classes = [IsAuthenticated, IsHacker]
    serializer = TeamSelectTaskSerializer
    queryset = Team.objects.all()
    task_queryset = Task.objects.all()

    def post(self, request):
        serializer = self.serializer(data=request.data)
        if serializer.is_valid():
            try:
                team = self.queryset.get(id=serializer.validated_data.get('team'))  # Получаем команду
            except Team.DoesNotExist:
                return Response({'error': ["Данной команды не найдено"]}, status=status.HTTP_404_NOT_FOUND)
            try:
                team.task = self.task_queryset.get(id=serializer.validated_data.get('task'))
                team.save()
            except Task.DoesNotExist:
                return Response({'error': ["Данной задачи не найдено"]}, status=status.HTTP_404_NOT_FOUND)
            return Response({"complete": True}, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class TeamSendReportTaskViewSet(APIView):
    permission_classes = [IsAuthenticated, IsHacker]
    serializer = TeamSendReportSerializer
    queryset = Team.objects.all()
    task_queryset = Task.objects.all()

    def post(self, request):
        serializer = self.serializer(data=request.data)
        if serializer.is_valid():
            try:
                team = self.queryset.get(id=serializer.validated_data.get('team'))  # Получаем команду
            except Team.DoesNotExist:
                return Response({'error': ["Данной команды не найдено"]}, status=status.HTTP_404_NOT_FOUND)
            try:
                team.report = serializer.validated_data.get('report')
                team.save()
            except Task.DoesNotExist:
                return Response({'error': ["Данной задачи не найдено"]}, status=status.HTTP_404_NOT_FOUND)
            return Response({"complete": True}, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
