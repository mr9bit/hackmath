from django.urls import path
from .views import TeamViewSet, InviteTeamViewSet, SearchTeamViewSet, InviteToSearchTeamViewSet, TeamSelectTaskViewSet, \
    ResultViewSet, TeamSendReportTaskViewSet

urlpatterns = [
    path('org/team/', TeamViewSet.as_view(), name='team_url'),
    path('org/team/invite', InviteTeamViewSet.as_view(), name='team_invite_url'),
    path('org/team/search', SearchTeamViewSet.as_view(), name='team_search_url'),
    path('org/team/search/invite', InviteToSearchTeamViewSet.as_view(), name='team_search_url'),
    path('org/team/select', TeamSelectTaskViewSet.as_view(), name='team_task_select_url'),
    path('org/team/report', TeamSendReportTaskViewSet.as_view(), name='team_task_report_url'),
    path('org/team/send_result', ResultViewSet.as_view(), name='team_send_result'),
]
