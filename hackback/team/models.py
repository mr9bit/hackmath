from xkcdpass.xkcd_password import generate_wordlist, generate_xkcdpassword

from django.db import models
from organization.models import Task
from users.models import User
from datetime import timedelta
from django.utils import timezone


def generate_humanize_code(numwords=4):
    my_word_list = generate_wordlist(min_length=3, max_length=6)
    return generate_xkcdpassword(
        my_word_list,
        interactive=False,
        numwords=numwords,
        acrostic=False,
        delimiter='-',
        case="lower",
    )


from organization.models import Task


class Team(models.Model):
    """
        Команда
    """
    name = models.CharField(max_length=400, verbose_name="Название команды")
    capitan = models.ForeignKey(User, on_delete=models.CASCADE)
    secret_code = models.CharField(max_length=300)
    task = models.ForeignKey(Task, on_delete=models.CASCADE, blank=True, null=True)
    report = models.TextField(verbose_name="Отчет", blank=True, null=True)
    datetime = models.DateTimeField(auto_now_add=True, blank=True, verbose_name="Дата создания")
    datetime_update = models.DateTimeField(auto_now=True, blank=True, verbose_name="Дата обновления")
    teammate = models.ManyToManyField(User, related_name='team_mate_set')

    class Meta:
        ordering = ['-datetime']


class SearchTeam(models.Model):
    CHOICES_TYPE = (
        ('team', 'Команда'),
        ('hacker', 'Хакер')
    )
    CHOICES_STATUS = (
        ('p', 'Опубликовано'),
        ('d', 'Черновик'),
    )
    contact = models.CharField(max_length=400, verbose_name="Контакты")
    role = models.TextField(verbose_name="Роль")
    selected_tracks = models.ManyToManyField(Task, verbose_name="Выбранные треки")
    about = models.TextField(verbose_name="О себе", blank=True, null=True)
    status = models.CharField(choices=CHOICES_STATUS, max_length=1, blank=True, null=True, verbose_name="Статус поиска")

    # не обязательные
    type = models.CharField(choices=CHOICES_TYPE, max_length=10, blank=True, null=True, verbose_name="Тип соискателя")
    user = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True)
    team = models.ForeignKey(Team, on_delete=models.CASCADE, blank=True, null=True)
    datetime = models.DateTimeField(auto_now_add=True, blank=True, verbose_name="Дата создания")
    datetime_update = models.DateTimeField(auto_now=True, blank=True, verbose_name="Дата обновления")


class SearchTeamInvite(models.Model):
    from_searchteam = models.ForeignKey(SearchTeam, on_delete=models.CASCADE, related_name="searchteam_from_set")
    to_searchteam = models.ForeignKey(SearchTeam, on_delete=models.CASCADE, related_name="searchteam_to_set")
    user_send = models.ForeignKey(User, on_delete=models.CASCADE)
    datetime = models.DateTimeField(auto_now_add=True, blank=True, verbose_name="Дата создания")

    @staticmethod
    def get_request_per_day(user):
        """
            Получить количество сделанных запросов пользователем
        """
        today = timezone.now()
        yesterday = today - timedelta(hours=24)
        search_requests = SearchTeamInvite.objects.filter(user_send=user, datetime__gte=yesterday,
                                                          datetime__lte=today)
        return len(search_requests)


def get_request_per_day(user):
    """
        Получить количество сделанных запросов пользователем
    """
    today = timezone.now()
    yesterday = today - timedelta(hours=24)
    search_requests = SearchTeamInvite.objects.filter(user_send=user, datetime__gte=yesterday,
                                                      datetime__lte=today)
    return len(search_requests)


def get_instance_request_per_day(user):
    """
        Получить количество сделанных запросов пользователем
    """
    today = timezone.now()
    yesterday = today - timedelta(hours=24)
    return SearchTeamInvite.objects.filter(user_send=user, datetime__gte=yesterday,
                                           datetime__lte=today)


class Result(models.Model):
    team = models.ForeignKey(Team, on_delete=models.CASCADE)
    link_to_git = models.CharField(max_length=500)
    link_to_presentation = models.TextField()
    datetime = models.DateTimeField(auto_now_add=True, blank=True, verbose_name="Дата отправки")
