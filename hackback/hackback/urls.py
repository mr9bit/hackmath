"""hackback URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from rest_framework_simplejwt.views import (
    TokenRefreshView,
)
from rest_registration.api.views import (
    change_password,
    logout,
    register,
    reset_password,
    send_reset_password_link,
    verify_registration
)
from django.conf import settings
from django.conf.urls.static import static
from users import urls as users_urls
from expert import urls as expert_urls
from users import views
from expert.views import register as expert_register

rest_registration_url = [
    path('register/', register, name='register'),
    path('verify-registration/', verify_registration, name='verify-registration'),
    path('send-reset-password-link/', send_reset_password_link, name='send-reset-password-link'),
    path('reset-password/', reset_password, name='reset-password'),
    path('logout/', logout, name='logout'),
    path('change-password/', change_password, name='change-password'),
    path('register-expert/', expert_register, name='expert_register'),
]

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/auth/', include(rest_registration_url)),
    path('api/', include(users_urls)),
    path('api/', include(expert_urls)),
    path('api/', include('organization.urls')),
    path('api/', include('team.urls')),
    path('api/', include('admin.urls')),
    path('api/auth/token/', views.TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('api/auth/token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
]
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
