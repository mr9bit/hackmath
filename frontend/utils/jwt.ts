export function tokenExpired(token: string) {
    if (token) {
        const atob = require('atob');
        const {exp} = JSON.parse(atob(token.split('.')[1]));
        return (Math.floor((new Date).getTime() / 1000)) >= exp;
    }
    return false
}

export function parseJwt(token) {
    const atob = require('atob');
    var base64Url = token.split('.')[1];
    var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
    var jsonPayload = decodeURIComponent(atob(base64).split('').map(function (c) {
        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
    }).join(''));

    return JSON.parse(jsonPayload);
}
