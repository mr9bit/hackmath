import React from 'react';
import Router from 'next/router';

const login = '/login'; // Define your login route address.

/*
    Только те маршруты которые авторизованы
 */
export default WrappedComponent => {
    const hocComponent = ({...props}) => <WrappedComponent {...props} />;

    hocComponent.getInitialProps = async (context) => {
        const userAuth = context.store.getState().auth

        // Если не авторизован, то выкидывем на страницу авторизации
        if (!userAuth?.isAuthenticated) {
            // Handle server-side and client-side rendering.
            if (context.res) {
                context.res?.writeHead(302, {
                    Location: login,
                });
                context.res?.end();
            } else {
                Router.replace(login);
            }
        } else if (WrappedComponent.getInitialProps) {
            const wrappedProps = await WrappedComponent.getInitialProps({...context, auth: userAuth});
            return {...wrappedProps, userAuth};
        }

        return {userAuth};
    };

    return hocComponent;
};
