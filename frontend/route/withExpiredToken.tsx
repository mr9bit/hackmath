import React from 'react';
import Router from 'next/router';
import {tokenExpired} from "../utils/jwt";
import {authMeInfo} from "../redux/actions/auth.action";
import {AuthAction} from '../redux/actions/auth.action'

const login = '/login'; // Define your login route address.

/**
 * Check user authentication and authorization
 * It depends on you and your auth service provider.
 * @returns {{auth: null}}
 */
const checkUserAuthentication = () => {
    return {auth: null}; // change null to { isAdmin: true } for test it.
};
/*
  Проверка если у пользователь кончился токен, то выкидываем его на страницу логина и стираем токен
 */
export default WrappedComponent => {
    const hocComponent = ({...props}) => <WrappedComponent {...props} />;

    hocComponent.getInitialProps = async (context) => {
        const userAuth = context.store.getState().auth

        // Если у пользователя кончился токен, выкидываем его на логин
        if (tokenExpired(userAuth?.access_token)) {
            context.store.dispatch(AuthAction.logout());
            if (context.res) {
                context.res?.writeHead(302, {
                    Location: login,
                });
                context.res?.end();
            } else {
                Router.replace(login);
            }
        } else if (WrappedComponent.getInitialProps) {
            const wrappedProps = await WrappedComponent.getInitialProps({...context, auth: userAuth});
            return {...wrappedProps, userAuth};
        }

        return {userAuth};
    };

    return hocComponent;
};
