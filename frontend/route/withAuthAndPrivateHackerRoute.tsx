import React from 'react';
import Router from 'next/router';
import {tokenExpired} from "../utils/jwt";

const login = '/'; // Define your login route address.

/**
 * Check user authentication and authorization
 * It depends on you and your auth service provider.
 * @returns {{auth: null}}
 */
const checkUserAuthentication = () => {
    return {auth: null}; // change null to { isAdmin: true } for test it.
};
/*
  Проверка если пользователь уже авторизован то выкидывать его из страниц авторизации и регистрации
 */
export default WrappedComponent => {
    const hocComponent = ({...props}) => <WrappedComponent {...props} />;

    hocComponent.getInitialProps = async (context) => {
        const userAuth = context.store.getState().auth

        if (!userAuth?.isAuthenticated || userAuth?.user.role !== 'hacker') {

            if (context.res) {
                context.res?.writeHead(302, {
                    Location: login,
                });
                context.res?.end();
            } else {
                Router.replace(login);
            }
        }
        // Если пользователь авторизован, то не даем зайти на страницу логина|логина
        if (!userAuth?.isAuthenticated && !(userAuth?.user.role === "admin")) {


            if (context.res) {
                context.res?.writeHead(302, {
                    Location: login,
                });
                context.res?.end();
            } else {
                Router.replace(login);
            }
        } else if (WrappedComponent.getInitialProps) {
            const wrappedProps = await WrappedComponent.getInitialProps({...context, auth: userAuth});
            return {...wrappedProps, userAuth};
        }
        return {userAuth};
    };

    return hocComponent;
};
