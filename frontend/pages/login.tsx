import React, {useEffect} from 'react';
import Link from 'next/link';
import {connect} from "react-redux";
import {useRouter} from "next/router";
import {bindActionCreators} from "redux";
import setLanguage from "next-translate/setLanguage";
import useTranslation from "next-translate/useTranslation";
import {LockOutlined, MailOutlined} from '@ant-design/icons';
import {MathModeIcon} from "../components/ExternalIcon/MathMode";
import {Card, Row, Col, Button, Form, Input, Divider, Layout, notification, Menu} from "antd";
// Действия
import {AuthAction} from '../redux/actions/auth.action'
// Роутинг
import withAuthRoute from '../route/withAuthRoute';


const backgroundStyle = {
    backgroundImage: 'url(/img/others/img-17.jpg)',
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'cover',
}

interface Props {
    actionAuth: any,
    registerComplete: any,
    i18n: any
}


export const login = (props: Props) => {
    const {t, lang} = useTranslation('login')

    let loading = false;
    const onSignUp = (form) => {
        props.actionAuth.login(form).then((e) => {
        }).catch(error => {
            notification.error({
                message: t('user_not_found'),
                description: t('invalid_email'),
            })
        });
    }
    const {locale, defaultLocale} = useRouter()
    useEffect(persistLocaleCookie, [locale, defaultLocale])

    function persistLocaleCookie() {
        const date = new Date()
        const expireMs = 100 * 365 * 24 * 60 * 60 * 1000 // 100 days
        date.setTime(date.getTime() + expireMs)
        document.cookie = `NEXT_LOCALE=${locale};expires=${date.toUTCString()};path=/`
    }

    return (
        <Layout>
            <div className={'auth-container'}>
                <div className="h-100" style={backgroundStyle}>
                    <div className="container d-flex flex-column justify-content-center h-100"
                         style={{padding: "20px 0"}}>
                        <Row justify="center">
                            <Col xs={20} sm={20} md={20} lg={8}>
                                <Card>

                                    <div className="my-2">
                                        <div className="text-center">
                                           <MathModeIcon style={{fontSize:"200px"}}/>
                                        </div>
                                        <Row justify="center">
                                            <Col xs={24} sm={24} md={20} lg={20}>
                                                <Form
                                                    layout="vertical"
                                                    name="login-form"
                                                    onFinish={onSignUp}>
                                                    <Form.Item
                                                        name="email"
                                                        label="Email"
                                                        rules={[
                                                            {
                                                                required: true,
                                                                message: t('enter_email'),
                                                            },
                                                            {
                                                                type: 'email',
                                                                message: t('enter_valid_email')
                                                            }
                                                        ]}>
                                                        <Input prefix={<MailOutlined className="text-primary"/>}/>
                                                    </Form.Item>
                                                    <Form.Item
                                                        name="password"
                                                        label={
                                                            <div
                                                                className={"d-flex justify-content-center align-items-baseline"}>
                                                                <span>{t('password')}</span>
                                                                <Link href={"/reset_password"}>
                                                                    <a style={{
                                                                        marginLeft: "10px",
                                                                        fontWeight: "normal",
                                                                        fontSize: "14px",
                                                                    }}>
                                                                        {t('lost_password')}
                                                                    </a>
                                                                </Link>
                                                            </div>

                                                        }
                                                        rules={[
                                                            {
                                                                required: true,
                                                                message: t('password_valid'),
                                                            }
                                                        ]}>
                                                        <Input.Password
                                                            prefix={<LockOutlined className="text-primary"/>}/>
                                                    </Form.Item>
                                                    <Form.Item>
                                                        <Button type="primary" htmlType="submit" block
                                                                loading={loading}>
                                                            {t("enter")}
                                                        </Button>
                                                    </Form.Item>
                                                    <div>
                                                        <Divider>
                                                            <span
                                                                className="text-muted font-size-base font-weight-normal">{t('or')}</span>
                                                        </Divider>
                                                        <div className="d-flex justify-content-center">
                                                            <Link href={"/registration"}>
                                                                <Button type="primary" block
                                                                        danger
                                                                        loading={loading}>
                                                                    {t("register")}
                                                                </Button>
                                                            </Link>
                                                        </div>
                                                    </div>
                                                </Form>
                                            </Col>
                                        </Row>
                                    </div>

                                </Card>
                            </Col>
                        </Row>
                    </div>
                </div>
            </div>
        </Layout>

    )
}


const mapDispatchToProps = (dispatch) => {
    return {
        actionAuth: bindActionCreators(AuthAction, dispatch),
    };
};
export default withAuthRoute(connect(null, mapDispatchToProps)(login));
