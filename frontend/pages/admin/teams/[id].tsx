import React from 'react';
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {AdminAction} from '../../../redux/actions/admin.action'
import withAuthPrivateAdminRoute from "../../../route/withAuthPrivateAdminRoute";
import withExpiredToken from "../../../route/withExpiredToken";
import PageHeaderAlt from '../../../components/layout-components/PageHeaderAlt'
import TeamHeader from '../../../components/layout/team/TeamHeader'
import {Team} from "../../../redux/interface/team";
import {Col, Row} from "antd";
import AboutTeam from "../../../components/layout/team/AboutTeam";
import TeamMate from "../../../components/layout/team/TeamMate";

const mapStateToProps = (state) => {
    return {
        active: state.admin.teams.active,
    };
};


interface Props {
    active: Team,
    id: number,
    actionAdmin: any,
    theme: any,
}

class team extends React.Component<Props> {
    static async getInitialProps(ctx) {
        const id = ctx.query.id;
        await ctx.store.dispatch(AdminAction.getTeam(ctx.auth.access_token, id));
        const {active} = mapStateToProps(ctx.store.getState());
        return {active, id}
    }

    render() {
        const {active, id} = this.props;
        return (
            <>
                <PageHeaderAlt background="/img/others/team-image.jpg" cssClass="bg-primary" overlap>
                    <div className="container text-center">
                        <div className="py-5 my-md-5">
                        </div>
                    </div>
                </PageHeaderAlt>
                <div className="container my-4">
                    <TeamHeader team={active}/>
                    <Row gutter={16}>
                        <Col xs={24} sm={24} md={24}>
                            <AboutTeam team={active}/>
                        </Col>
                        <Col xs={24} sm={24} md={24}>
                            <TeamMate prefix={"admin"} team={active}/>
                        </Col>
                    </Row>
                </div>
            </>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        actionAdmin: bindActionCreators(AdminAction, dispatch),
    };
};

export default withAuthPrivateAdminRoute(withExpiredToken(connect((state => (
    {
        auth: state.auth,
        teams: state.admin.teams,
        theme: state.theme,

    })), mapDispatchToProps)(team)))

