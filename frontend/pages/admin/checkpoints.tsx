import PageHeaderAlt from "../../components/layout-components/PageHeaderAlt";
import React from "react";
import Flex from '../../components/shared-components/Flex'
import {bindActionCreators} from "redux";
import {AdminAction} from "../../redux/actions/admin.action";
import withAuthPrivateAdminRoute from "../../route/withAuthPrivateAdminRoute";
import withExpiredToken from "../../route/withExpiredToken";
import {connect} from "react-redux";
import {Col, Row} from "antd";
import {Form, Card, Input, Button, Select} from 'antd';

const checkpoints = (props) => {
    const [form] = Form.useForm();
    const sendCheckpoint = (form) => {
        props.actionAdmin.checkpointCreate(props.auth.access_token, form).then((r) => {
            props.actionAdmin.checkpointList(props.auth.access_token)
        })
    }
    return (
        <React.Fragment>
            <PageHeaderAlt className="bg-primary border-bottom mb-5">
                <div className="container-fluid">
                    <Flex justifyContent="between" alignItems="center" className="py-4">
                        <h2 className={"text-white display-4"}>Чек-поинты</h2>
                    </Flex>
                </div>
            </PageHeaderAlt>
            <div className={`my-4 container`}>
                <Form form={form} onFinish={sendCheckpoint}>
                    <Card>
                        <Row gutter={16}>
                            <Col xs={24} sm={24} lg={12} xl={12} xxl={20}>
                                <Form.Item name="name" rules={[{required: true}]}>
                                    <Input placeholder={"Название чек-поинта"}/>
                                </Form.Item>
                            </Col>
                            <Col xs={24} sm={24} lg={8} xl={8} xxl={4}>
                                <Form.Item>
                                    <Button type="primary" style={{width: "100%"}} htmlType="submit">
                                        Добавить
                                    </Button>
                                </Form.Item>
                            </Col>

                        </Row>
                    </Card>
                </Form>

                <Row gutter={16}>
                    {
                        props.checkpoints.results.map(item => {
                            return <Col key={item.id} xs={24} sm={24} lg={24} xl={24} xxl={24}>
                                <Card style={{textAlign: "center"}}>
                                    {item.name}
                                </Card>
                            </Col>
                        })
                    }
                </Row>
            </div>

        </React.Fragment>
    )
}

checkpoints.getInitialProps = async (ctx) => {
    await ctx.store.dispatch(AdminAction.checkpointList(ctx.auth.access_token))
    const {checkpoints} = mapStateToProps(ctx.store.getState())
    return {checkpoints}
}

const mapStateToProps = (state) => {
    return {
        checkpoints: state.admin.checkpoints,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        actionAdmin: bindActionCreators(AdminAction, dispatch),
    };
};

export default withAuthPrivateAdminRoute(withExpiredToken(connect((state => (
    {
        auth: state.auth,
        checkpoints: state.admin.checkpoints
    })), mapDispatchToProps)(checkpoints)))

