import React from 'react';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {FormInstance} from "antd/lib/form";
import Icon, {LoadingOutlined} from '@ant-design/icons';
import {UploadRequestOption} from "rc-upload/lib/interface";
import {Form, Input, message, Row, Col, Upload, Button, Card, notification, Select} from 'antd'
// Действия
import {AdminAction} from '../../../redux/actions/admin.action'
// Компоненты
import PageHeaderAlt from '../../../components/layout-components/PageHeaderAlt'
import Flex from '../../../components/shared-components/Flex'
import {ImageSvg} from '../../../assets/svg/icon';
// Интерфейсы
import {Company} from "../../../redux/interface/company";
// Роутинг
import withAuthPrivateAdminRoute from "../../../route/withAuthPrivateAdminRoute";
import withExpiredToken from "../../../route/withExpiredToken";


const {Option} = Select;
const {Dragger} = Upload;

const mapStateToProps = (state) => {
    return {
        active: state.admin.company.active,
    };
};


interface Props {
    active: Company,
    auth: any,
    id: number,
    actionAdmin: any,
    theme: any,
}

interface State {
    uploadLoading: boolean,
    active: Company,
    uploadedImg: string,
    uploadImg: any,
    uploadImgENG: any,
    uploadedImg_ENG: string,
    uploadLoadingENG: boolean,
}

const getBase64 = (img, callback) => {
    const reader = new FileReader();
    reader.addEventListener('load', () => callback(reader.result));
    reader.readAsDataURL(img);
}


class team extends React.Component<Props, State> {
    formRef = React.createRef<FormInstance>();

    static async getInitialProps(ctx) {
        const id = ctx.query.id;
        await ctx.store.dispatch(AdminAction.getCompany(ctx.auth.access_token, id));
        const {active} = mapStateToProps(ctx.store.getState());
        return {active, id}
    }

    constructor(props: Props) {
        super(props);

        this.state = {
            ...this.props,
            uploadedImg: this.props.active.logo ? this.props.active.logo : '',
            uploadedImg_ENG: this.props.active.logo_eng ? this.props.active.logo_eng : '',
            uploadLoading: false,
            uploadLoadingENG: false,
            uploadImg: '',
            uploadImgENG: '',
        }

        this.beforeUpload = this.beforeUpload.bind(this);
        this.handleUploadChange = this.handleUploadChange.bind(this);
        this.dummyRequest = this.dummyRequest.bind(this);
        this.onFinish = this.onFinish.bind(this);
    }

    beforeUpload = file => {
        const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
        if (!isJpgOrPng) {
            message.error('Вы можете загружать только файлы JPG/PNG!');
        }
        const isLt2M = file.size / 1024 / 1024 < 2;
        if (!isLt2M) {
            message.error('Изображение должно быть меньше 2 МБ!');
        }
        return isJpgOrPng && isLt2M;
    }


    dummyRequest = (upload: UploadRequestOption) => {
        upload.onSuccess("ok", null);
    };
    handleUploadChange = info => {
        if (info.file.status === 'uploading') {
            this.setState({
                uploadLoading: false
            })
            return;
        }
        if (info.file.status === 'done') {

            getBase64(info.file.originFileObj, imageUrl => {
                this.setState({
                    uploadImg: info.file.originFileObj,
                    uploadedImg: imageUrl,
                    uploadLoading: true
                })
            });
        }
    };

    handleUploadEngChange = info => {
        if (info.file.status === 'uploading') {
            this.setState({
                uploadLoading: false
            })
            return;
        }
        if (info.file.status === 'done') {

            getBase64(info.file.originFileObj, imageUrl => {
                this.setState({
                    uploadImgENG: info.file.originFileObj,
                    uploadedImg_ENG: imageUrl,
                    uploadLoadingENG: true
                })
            });
        }
    };


    onFinish = form => {
        let form_data = new FormData();
        form_data.append('name', form['name']);
        form_data.append('url', form['url']);
        form_data.append('type', form['type']);
        if (this.state.uploadImg) form_data.append('logo', this.state.uploadImg);
        if (this.state.uploadImgENG) form_data.append('logo_eng', this.state.uploadImgENG);

        this.props.actionAdmin.editCompany(this.props.auth.access_token, this.props.id, form_data).then(() => {
            notification.success({
                message: `Данные сохранены`,
                duration: 2.3,
            })
        }).catch(() => {
            notification.error({
                message: `Ошибка при сохранении`,
                description: 'Попробуйте обновить страницу и внести данные снова.',
                duration: 2.3,
            })
        })
    }

    render() {
        const {active} = this.props;
        return (
            <>
                <Form
                    layout="vertical"
                    onFinish={this.onFinish}
                    ref={this.formRef}
                    name="advanced_search"
                    className="ant-advanced-search-form">
                    <PageHeaderAlt className="border-bottom">
                        <div className="container">
                            <Flex className="py-2" mobileFlex={false} justifyContent="between" alignItems="center">
                                <h2 className="mb-3">Редактировать {active.name}</h2>
                                <div className="mb-3">
                                    <Button className="mr-2">Отмена</Button>
                                    <Button type="primary" htmlType="submit">
                                        Сохранить
                                    </Button>
                                </div>
                            </Flex>
                        </div>
                    </PageHeaderAlt>
                    <div className={`my-4 container`}>


                        <Row gutter={16}>
                            <Col xs={24} sm={24} md={17}>
                                <Card>
                                    <Form.Item name="name" initialValue={this.props.active.name} label="Название">
                                        <Input placeholder="Название"/>
                                    </Form.Item>
                                    <Form.Item name="url" initialValue={this.props.active.url} label="Ссылка">
                                        <Input placeholder="https://"/>
                                    </Form.Item>
                                    <Form.Item name="type" initialValue={this.props.active.type} label="Тип Компании">
                                        <Select style={{width: '100%'}}>
                                            <Option value="general">Генеральный партнер</Option>
                                            <Option value="partner">Партнер</Option>
                                            <Option value="info">Инфопартнер</Option>
                                        </Select>
                                    </Form.Item>
                                </Card>
                            </Col>
                            <Col xs={24} sm={24} md={7}>
                                <Card title="Логотип">
                                    <Dragger name={'logo'} beforeUpload={this.beforeUpload} multiple={true}
                                             onChange={this.handleUploadChange}
                                             customRequest={this.dummyRequest}
                                             listType={"picture-card"}
                                             showUploadList={false}>
                                        {
                                            this.state.uploadedImg ?
                                                <img src={this.state.uploadedImg} alt="avatar" className="img-fluid"/>
                                                :
                                                <div>
                                                    {
                                                        this.state.uploadLoading ?
                                                            <div>
                                                                <LoadingOutlined
                                                                    className="font-size-xxl text-primary"/>
                                                                <div className="mt-3">Загрузика</div>
                                                            </div>
                                                            :
                                                            <div>
                                                                <Icon component={ImageSvg} className={"display-3"}/>
                                                                <p>Нажмите для загрузки</p>
                                                            </div>
                                                    }
                                                </div>
                                        }
                                    </Dragger>
                                </Card>

                                <Card title="Логотип ENG">
                                    <Dragger name={'logo_eng'} beforeUpload={this.beforeUpload} multiple={true}
                                             onChange={this.handleUploadEngChange}
                                             customRequest={this.dummyRequest}
                                             listType={"picture-card"}
                                             showUploadList={false}>
                                        {
                                            this.state.uploadedImg_ENG ?
                                                <img src={this.state.uploadedImg_ENG} alt="avatar_eng"
                                                     className="img-fluid"/>
                                                :
                                                <div>
                                                    {
                                                        this.state.uploadLoadingENG ?
                                                            <div>
                                                                <LoadingOutlined
                                                                    className="font-size-xxl text-primary"/>
                                                                <div className="mt-3">Загрузика</div>
                                                            </div>
                                                            :
                                                            <div>
                                                                <Icon component={ImageSvg} className={"display-3"}/>
                                                                <p>Нажмите для загрузки</p>
                                                            </div>
                                                    }
                                                </div>
                                        }
                                    </Dragger>
                                </Card>


                            </Col>
                        </Row>
                    </div>
                </Form>

            </>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        actionAdmin: bindActionCreators(AdminAction, dispatch),
    };
};

export default withAuthPrivateAdminRoute(withExpiredToken(connect((state => (
    {
        auth: state.auth,
        teams: state.admin.teams,
        theme: state.theme,

    })), mapDispatchToProps)(team)))

