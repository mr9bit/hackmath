import React, {useState} from 'react';
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {Row, Col, Tag, Image, Tooltip, Button, Grid, Card, Menu, notification} from "antd";
import PageHeaderAlt from '../../../components/layout-components/PageHeaderAlt'
import {SIDE_NAV_WIDTH, SIDE_NAV_COLLAPSED_WIDTH,} from '../../../constants/ThemeConstant';
import withAuthPrivateAdminRoute from '../../../route/withAuthPrivateAdminRoute';
import withExpiredToken from '../../../route/withExpiredToken';
import {AdminAction} from '../../../redux/actions/admin.action'
import utils from '../../../utils';
import {ClockCircleOutlined, EditOutlined, DeleteOutlined, PlusOutlined} from '@ant-design/icons';
import Flex from '../../../components/shared-components/Flex'
import Link from 'next/link'
import EllipsisDropdown from '../../../components/shared-components/EllipsisDropdown'
import moment from "moment";


const {useBreakpoint} = Grid;


const ItemInfo = ({datetime, type}) => {
    return <Flex flexDirection={'row'}>
        <div className="mr-3">
            <Tooltip title="Дата создания">
                <ClockCircleOutlined className="text-muted font-size-md"/>
                <span className="ml-1 text-muted">{moment(datetime).locale('ru').format('lll')}</span>
            </Tooltip>
        </div>
        <div className="mr-3">


            {
                type === 'Партнер' ? <Tooltip title="Тип">
                    <Tag color="#87d068">{type}</Tag>
                </Tooltip> : type === 'Инфопартнер' ? <Tooltip title="Тип">
                    <Tag color="#108ee9">{type}</Tag>
                </Tooltip> : type === 'Генеральный партнер' ? <Tooltip title="Тип">
                    <Tag color="#f50">{type}</Tag>
                </Tooltip> : null
            }

        </div>
    </Flex>
}
const ItemHeader = ({name}) => (
    <div>
        <h4 className="mb-0">{name}</h4>
    </div>
)
const ItemAction = ({id, removeId}) => (
    <EllipsisDropdown
        menu={
            <Menu>
                <Menu.Item key="1">

                    <Link href={{
                        pathname: '/admin/company/[id]',
                        query: {id: id},
                    }}>
                        <a>
                            <EditOutlined/>
                            <span>Редактировать</span>
                        </a>
                    </Link>
                </Menu.Item>
                <Menu.Divider/>
                <Menu.Item key="2" onClick={() => removeId(id)}>
                    <DeleteOutlined/>
                    <span>Удалить</span>
                </Menu.Item>
            </Menu>
        }
    />
)
const GridItem = ({company, removeId}) => (
    <Card style={{height: '290px'}} bodyStyle={{height: "100%"}}>
        <Flex alignItems="center" justifyContent="between">
            <ItemHeader name={company.name}/>
            <ItemAction id={company.id} removeId={removeId}/>
        </Flex>
        <div style={{height: '180px', display: 'flex', flexDirection: 'column', justifyContent: 'center'}}>
            <Image style={{maxHeight: "120px", objectFit: 'contain'}} className="mt-2" preview={false}
                   src={company.logo}/>
        </div>
        <ItemInfo datetime={company.datetime} type={company.type}/>
    </Card>
)

export const index = (props) => {


    const deleteCompany = (id) => {
        props.actionAdmin.deleteCompany(props.auth.access_token, id).then(() =>
            notification.success({
                message: `Данные сохранены`,
                duration: 2.3,
            })
        ).catch(error => {
            error.response.json().then((e) => {
                console.log(e)
            })
            notification.error({
                message: `Ошибка при сохранении`,
                description: 'Попробуйте обновить страницу и внести данные снова.',
                duration: 2.3,
            })
        })
    }


    return (
        <React.Fragment>
            <PageHeaderAlt className="bg-primary border-bottom mb-5">
                <div className="container-fluid">
                    <Flex justifyContent="between" alignItems="center" className="py-4">
                        <h2 className={"text-white display-4"}>Компании</h2>
                        <div>
                            <Link href={'/admin/company/add'}>
                                <a>
                                    <Button className="ml-2">
                                        <PlusOutlined/>
                                        <span>Добавить</span>
                                    </Button>
                                </a>
                            </Link>
                        </div>
                    </Flex>
                </div>
            </PageHeaderAlt>


            <div className={`my-4 container`}>
                <Row gutter={16}>
                    {
                        props.company.list.results.map(item => {

                            return <Col key={item.id} xs={24} sm={24} lg={8} xl={8} xxl={6}>
                                <GridItem removeId={deleteCompany} company={item}/>
                            </Col>

                        })
                    }
                </Row>
            </div>
        </React.Fragment>

    )
}
index.getInitialProps = async (ctx) => {
    await ctx.store.dispatch(AdminAction.getCompanyList(ctx.auth.access_token))
    const {company} = mapStateToProps(ctx.store.getState())
    return {company}
}

const mapStateToProps = (state) => {
    return {
        company: state.admin.company,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        actionAdmin: bindActionCreators(AdminAction, dispatch),
    };
};

export default withAuthPrivateAdminRoute(withExpiredToken(connect((state => (
    {
        auth: state.auth,
        company: state.admin.company
    })), mapDispatchToProps)(index)))

