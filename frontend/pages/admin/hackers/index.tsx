import React, {useRef, useState} from 'react';
import Link from 'next/link';
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {Input, Button, Table, Tag, Tooltip, Spin} from "antd";
import {Breakpoint} from 'antd/lib/_util/responsiveObserve';
import Highlighter from 'react-highlight-words';
import {SearchOutlined} from '@ant-design/icons';
import PageHeaderAlt from '../../../components/layout-components/PageHeaderAlt';
import withAuthPrivateAdminRoute from '../../../route/withAuthPrivateAdminRoute';
import withExpiredToken from '../../../route/withExpiredToken';
import {AdminAction} from '../../../redux/actions/admin.action'
import AvatarStatus from '../../../components/shared-components/AvatarStatus';
import {EyeOutlined, SwapRightOutlined} from '@ant-design/icons';
import Flex from '../../../components/shared-components/Flex';
import DrawerInfoUser from '../../../components/layout/hacker/DrawerInfoUser';
import {Card, Col, Row} from "antd";
import {Select, Radio, Form, Checkbox} from 'antd';
import param from 'jquery-param';
import {URl_BACKEND} from "../../../redux/settings";

const {Option} = Select;

export const getStatusTag = (status) => {
    switch (status) {
        case 0:
            return <Tag color="#8c8c8c">Неподтверждён</Tag>
        case 1:
            return <Tag color="geekblue">Незавершен</Tag>
        case 2:
            return <Tag color="green">Допущен</Tag>
        case 3:
            return <Tag color="#87d068">Подтвержден</Tag>
    }
}

export const index = (props) => {

    const [showDrawer, setShowDrawer] = useState(false)
    const [searchText, setSearchText] = useState('')
    const [searchedColumn, setSearchedColumn] = useState('')

    const getUserActiveInfo = (hacker) => {
        props.actionAdmin.getHacker(props.auth.access_token, hacker.id).then().catch((e) => {
                console.log(e)
            }
        )
    }
    const responsive: Breakpoint[] = ['sm']

    const handleTableChange = (page) => {
        props.actionAdmin.hackersListNextPage(props.auth.access_token, page).then().catch((e) => {
                console.log(e)
            }
        )
    }

    const handleSearch = (selectedKeys, confirm, dataIndex) => {
        confirm();
        setSearchText(selectedKeys[0]);
        setSearchedColumn(dataIndex);
    };

    const handleReset = clearFilters => {
        clearFilters();
        setSearchText('');
    };

    const getColumnSearchProps = dataIndex => ({
        filterDropdown: ({setSelectedKeys, selectedKeys, confirm, clearFilters}) => (
            <div style={{padding: 8}}>
                <Input

                    placeholder={`Поиск ${dataIndex}`}
                    value={selectedKeys[0]}
                    onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
                    onPressEnter={() => handleSearch(selectedKeys, confirm, dataIndex)}
                    style={{width: 188, marginBottom: 8, display: 'block'}}
                />
                <Button
                    type="primary"
                    onClick={() => handleSearch(selectedKeys, confirm, dataIndex)}
                    icon={<SearchOutlined/>}
                    size="small"
                    style={{width: 90, marginRight: 8}}
                >
                    Поиск
                </Button>
                <Button onClick={() => handleReset(clearFilters)} size="small" style={{width: 90}}>
                    Сбросить
                </Button>
            </div>
        ),
        filterIcon: filtered => <SearchOutlined style={{color: filtered ? '#1890ff' : undefined}}/>,
        onFilter: (value, record) =>
            record[dataIndex]
                ? record[dataIndex].toString().toLowerCase().includes(value.toLowerCase())
                : '',
        render: text =>
            searchedColumn === dataIndex ? (
                <Highlighter
                    highlightStyle={{backgroundColor: '#ffc069', padding: 0}}
                    searchWords={[searchText]}
                    autoEscape
                    textToHighlight={text ? text.toString() : ''}
                />
            ) : (
                text
            ),
    });

    const columns = [
        {
            title: 'ФИО',
            dataIndex: 'full_name',
            key: 'full_name',
            render: (_, record) => (
                <div className="d-flex">
                    <Link href={{
                        pathname: '/admin/hackers/[id]',
                        query: {id: record.id},
                    }}>
                        <a>
                            {JSON.stringify(record)}
                            <AvatarStatus text={record?.first_name ? record?.first_name[0] : ''}
                                          name={record?.first_name ? record?.first_name : ''}
                                          subTitle={record.email}/>
                        </a>
                    </Link>
                </div>
            ),
            ...getColumnSearchProps('full_name'),
        }, {
            title: '🤌 Роль',
            dataIndex: 'type_developer',
            key: 'type_developer',
            width: "10%",
            responsive: responsive,
            sorter: (a, b) => {
                return a.type_developer?.length - b.type_developer?.length
            },
        }, {
            title: '📡 Телеграмм',
            dataIndex: 'telegram_link',
            key: 'telegram_link',
            responsive: responsive,
        }, {
            title: 'Стус письма с подтвеждением',
            dataIndex: 'confirm_send',
            key: 'confirm_send',
            responsive: responsive,
            render: confirm => (
                confirm ? '✅' : '❌'
            ),
        }, {
            title: '📜 Статус',
            dataIndex: 'status',
            key: 'status',
            responsive: responsive,
            render: status => (
                getStatusTag(status)
            ),
            sorter: (a, b) => a.status.length - b.status.length,
        }, {
            title: '🎓 Институт',
            dataIndex: 'institute',
            key: 'institute',
            responsive: responsive,
            sorter: (a, b) => a.institute?.length - b.institute?.length,
            ...getColumnSearchProps('institute'),
        }, {
            title: '',
            dataIndex: 'actions',
            key: 'actions',
            render: (_, record) => (
                <div className="text-right">
                    <Link href={{
                        pathname: '/admin/hackers/[id]',
                        query: {id: record.id},
                    }}>
                        <a>
                            <Tooltip title="Полный просмотр анкеты">
                                <Button type="primary" className="mr-2 mt-2" icon={<EyeOutlined/>} size="small"/>
                            </Tooltip>

                        </a>
                    </Link>
                    <Tooltip title="Короткий просмотр">
                        <Button type="primary" className="mr-2 mt-2" onClick={() => {
                            getUserActiveInfo(record)
                            setShowDrawer(!showDrawer)
                        }} icon={<SwapRightOutlined/>} size="small"/>
                    </Tooltip>
                </div>
            )
        }
    ]

    const [autoSkill, setAutoSkill] = useState([])
    const [fetching, setFetching] = useState(false)
    const [selectSkill, setSelectSkill] = useState(false)


    function filter(form) {
        form['skill'] = form['skill'].map(item => item.value)
        props.actionAdmin.hackersListFilter(props.auth.access_token, param(form)).then().catch((e) => {
                console.log(e)
            }
        )
    }

    const fetchSkills = value => {
        setAutoSkill([])
        setFetching(true)
        fetch(`${URl_BACKEND}/api/autocomplete/skills/?q=${value}`, {
            method: 'GET',
            headers: {
                'Authorization': `Bearer ${props.auth.access_token}`
            },
        })
            .then(response => response.json())
            .then(body => {
                const data = body.map(user => {
                    return {
                        id: user.id,
                        label: user.name,
                        value: user.value
                    }
                });
                setAutoSkill(data)
                setFetching(false)
            });
    };

    const changeSkill = (selectedItems) => {
        setSelectSkill(selectedItems)
    };

    const [form] = Form.useForm();
    return (
        <React.Fragment>
            <PageHeaderAlt className="bg-primary mb-5" overlap>
                <div className="container text-center">
                    <div className="py-lg-4">
                        <h1 className="text-white display-4">🐱‍💻 Hackers</h1>
                    </div>
                </div>
            </PageHeaderAlt>

            <div className="code-box" style={{padding: "20px"}}>
                <Form form={form} onFinish={filter}>
                    <Row gutter={16}>

                        <Col sm={24} md={6}>
                            <Form.Item
                                name={"where_to_study"}
                                initialValue={null}
                                label="Место учебы">
                                <Select>
                                    <Option value={null}>Не важно</Option>
                                    <Option value="school">Школа</Option>
                                    <Option value="univ">Институт</Option>
                                    <Option value="college">Коледж</Option>
                                    <Option value="no_learning">Не учиться</Option>
                                </Select>
                            </Form.Item>
                        </Col>
                        <Col sm={24} md={6}>
                            <Form.Item
                                name={"science_degree"}
                                initialValue={null}
                                label="Уровень образования">
                                <Select>
                                    <Option value={null}>Не важно</Option>
                                    <Option value="Аспирант">Аспирант</Option>
                                    <Option value="Специалист">Специалист</Option>
                                    <Option value="Магистр">Магистр</Option>
                                    <Option value="Бакалавр">Бакалавр</Option>
                                </Select>
                            </Form.Item>
                        </Col>
                        <Col sm={24} md={6}>
                            <Form.Item initialValue={null} label={"Команда"} name={"team"}>
                                <Select>
                                    <Option value={null}>Не важно</Option>
                                    <Option key={"with_team"} value={"false"}>С командой</Option>
                                    <Option key={"without_team"} value={"true"}>Без команды</Option>
                                </Select>
                            </Form.Item>
                        </Col>
                        <Col sm={24} md={6}>
                            <Form.Item initialValue={null} name="hack_visited" label="Хакатонов посетили">
                                <Select>
                                    <Option value={null}>Не важно</Option>
                                    <Option value="0-1">0-1</Option>
                                    <Option value="2-5">2-5</Option>
                                    <Option value="5-8">5-8</Option>
                                    <Option value="10+">10+</Option>
                                </Select>
                            </Form.Item>
                        </Col>

                        <Col sm={24} md={6}>
                            <Form.Item initialValue={null} name="status" label="Статус">
                                <Select>
                                    <Option value={null}>Не важно</Option>
                                    <Option value="0">Не Подтверждён</Option>
                                    <Option value="1">Незавершен</Option>
                                    <Option value="2">Допущен</Option>
                                    <Option value="3">Подтвержден</Option>
                                </Select>
                            </Form.Item>
                        </Col>
                        <Col sm={24} md={6}>
                            <Form.Item name="skill" label="Навык">
                                <Select
                                    mode="tags"
                                    labelInValue
                                    tokenSeparators={[',']}
                                    placeholder={"Навики"}
                                    notFoundContent={fetching ? <Spin size="small"/> : null}
                                    onSearch={fetchSkills}
                                    onChange={changeSkill}
                                    style={{width: '100%'}}>
                                    {autoSkill && autoSkill.map(d => (
                                        <Option value={d.label}>{d.label}</Option>
                                    ))}
                                </Select>
                            </Form.Item>
                        </Col>

                        <Col md={24}>
                            <Button htmlType="submit" type={"primary"} style={{width: "100%"}}>Поиск</Button>
                        </Col>
                    </Row>

                </Form>

            </div>

            <div className="code-box">
                <Table columns={columns}
                       rowKey='id'
                       dataSource={props.hackers.list.results}
                       pagination={{position: ["bottomCenter"]}}
                />

            </div>
            <DrawerInfoUser data={props.hackers.active} visible={showDrawer} close={() => setShowDrawer(!showDrawer)}/>
        </React.Fragment>

    )
}

index.getInitialProps = async (ctx) => {
    await ctx.store.dispatch(AdminAction.hackersList(ctx.auth.access_token))
    const {admin} = mapStateToProps(ctx.store.getState())
    return {admin}
}

const mapStateToProps = (state) => {
        return {
            admin: state.admin,
        };
    }
;

const mapDispatchToProps = (dispatch) => {
    return {
        actionAdmin: bindActionCreators(AdminAction, dispatch),
    };
};

export default withAuthPrivateAdminRoute(withExpiredToken(connect((state => (
    {
        auth: state.auth,
        hackers: state.admin.hackers
    })), mapDispatchToProps)(index)))

