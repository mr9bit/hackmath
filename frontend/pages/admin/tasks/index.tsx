import React from 'react';
import moment from "moment";
import Link from 'next/link'
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {Row, Col, Image, Tooltip, Button, Card, Menu, notification, Tag} from "antd";
import {ClockCircleOutlined, EditOutlined, DeleteOutlined, PlusOutlined} from '@ant-design/icons';
// Компоненты
import PageHeaderAlt from '../../../components/layout-components/PageHeaderAlt'
import Flex from '../../../components/shared-components/Flex'
import EllipsisDropdown from '../../../components/shared-components/EllipsisDropdown'
// Роутинг
import withAuthPrivateAdminRoute from '../../../route/withAuthPrivateAdminRoute';
import withExpiredToken from '../../../route/withExpiredToken';
// Действия
import {AdminAction} from '../../../redux/actions/admin.action'

const ItemInfo = ({datetime}) => {
    return <Flex flexDirection={'column'}>
        <div className="mr-3">
            <Tooltip title="Дата создания">
                <ClockCircleOutlined className="text-muted font-size-md"/>
                <span className="ml-1 text-muted">{moment(datetime).locale('ru').format('lll')}</span>
            </Tooltip>
        </div>
    </Flex>
}
const ItemHeader = ({name}) => (
    <div>
        <h4 className="mb-0">{name}</h4>
    </div>
)
const ItemAction = ({id, removeId}) => (
    <EllipsisDropdown
        menu={
            <Menu>
                <Menu.Item key="1">

                    <Link href={{
                        pathname: '/admin/tasks/[id]',
                        query: {id: id},
                    }}>
                        <a>
                            <EditOutlined/>
                            <span>Редактировать</span>
                        </a>
                    </Link>
                </Menu.Item>
                <Menu.Divider/>
                <Menu.Item key="2" onClick={() => removeId(id)}>
                    <DeleteOutlined/>
                    <span>Удалить</span>
                </Menu.Item>
            </Menu>
        }
    />
)
const GridItem = ({data, removeId}) => (
    <Card style={{height: '360px'}} bodyStyle={{height: "100%"}}>
        <div style={{height: "70px"}}>
            <Flex alignItems="center" justifyContent="between">
                <ItemHeader name={data.name}/>
                <ItemAction id={data.id} removeId={removeId}/>
            </Flex>
        </div>
        <div style={{height: '180px', display: 'flex', flexDirection: 'column', justifyContent: 'center'}}>
            <Image style={{maxHeight: "120px", objectFit: 'contain'}} className="mt-2" preview={false}
                   src={data.maintainer.logo}/>
        </div>
        <div className={"mt-2 mb-3"}>
            <Tag
                color={data.nomination?.color}
                style={{
                    whiteSpace: "break-spaces",
                    width: "100%",
                    lineHeight: "14px",
                    textAlign: "center",
                    padding: "5px"
                }}>{data.nomination?.name}</Tag>
        </div>
        <ItemInfo datetime={data.datetime}/>
    </Card>
)

export const index = (props) => {

    const deleteCompany = (id) => {
        props.actionAdmin.deleteTask(props.auth.access_token, id).then(() =>
            notification.success({
                message: `Задача удалена`,
                duration: 2.3,
            })
        ).catch(error => {
            error.response.json().then((e) => {
                console.log(e)
            })
            notification.error({
                message: `Ошибка при удалении`,
                description: 'Попробуйте обновить страницу и внести данные снова.',
                duration: 2.3,
            })
        })
    }

    return (
        <React.Fragment>
            <PageHeaderAlt className="bg-primary border-bottom mb-5">
                <div className="container-fluid">
                    <Flex justifyContent="between" alignItems="center" className="py-4">
                        <h2 className={"text-white display-4"}>Задачи</h2>
                        <div>
                            <Link href={'/admin/tasks/add'}>
                                <a>
                                    <Button className="ml-2">
                                        <PlusOutlined/>
                                        <span>Добавить</span>
                                    </Button>
                                </a>
                            </Link>
                        </div>
                    </Flex>
                </div>
            </PageHeaderAlt>
            <div className={`my-4 container`}>
                <Row gutter={16}>
                    {
                        props.tasks.list.results.map(item => {

                            return <Col key={item.id} xs={24} sm={24} lg={8} xl={8} xxl={6}>
                                <GridItem removeId={deleteCompany} data={item}/>
                            </Col>

                        })
                    }
                </Row>
            </div>
        </React.Fragment>
    )
}

index.getInitialProps = async (ctx) => {
    await ctx.store.dispatch(AdminAction.getTaskList(ctx.auth.access_token))
    const {tasks} = mapStateToProps(ctx.store.getState())
    return {tasks}
}

const mapStateToProps = (state) => {
    return {
        tasks: state.admin.tasks,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        actionAdmin: bindActionCreators(AdminAction, dispatch),
    };
};

export default withAuthPrivateAdminRoute(withExpiredToken(connect((state => (
    {
        auth: state.auth,
        tasks: state.admin.tasks
    })), mapDispatchToProps)(index)))

