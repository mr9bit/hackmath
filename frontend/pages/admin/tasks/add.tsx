import React from 'react';
import {Select, Tabs} from 'antd';
import Router from "next/router";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {FormInstance} from "antd/lib/form";
import {Form, Input, Row, Col, Button, Card, notification} from 'antd';
// Компоненты
import PageHeaderAlt from '../../../components/layout-components/PageHeaderAlt';
import Flex from '../../../components/shared-components/Flex';
// Действия
import {AdminAction} from '../../../redux/actions/admin.action';
import withAuthPrivateAdminRoute from "../../../route/withAuthPrivateAdminRoute";
// Интерфейсы
import {Tasks} from "../../../redux/interface/tasks";
import {Company} from "../../../redux/interface/company";
import {Nomination} from "../../../redux/interface/nomination";

const {Option} = Select;

const mapStateToProps = (state) => {
    return {
        active: state.admin.tasks.active,
        companyList: state.admin.company.list.results,
        nominations: state.admin.nominations.list,
    };
};


interface Props {
    active: Tasks,
    companyList: Company[],
    auth: any,
    id: number,
    actionAdmin: any,
    theme: any,
    nominations: Nomination[]

}

interface State {
    content: any,
    content_eng: any
}


const {TabPane} = Tabs;


class add extends React.Component<Props, State> {
    private editor: null;
    private editor_eng: null;
    formRef = React.createRef<FormInstance>();

    static async getInitialProps(ctx) {
        const id = ctx.query.id;
        await ctx.store.dispatch(AdminAction.getCompanyList(ctx.auth.access_token))
        await ctx.store.dispatch(AdminAction.getNominationList(ctx.auth.access_token))
        const {active, companyList, nominations} = mapStateToProps(ctx.store.getState());
        return {active, companyList, nominations, id}
    }

    constructor(props: Props) {
        super(props);
        this.onFinish = this.onFinish.bind(this);
        this.handleChangeEngArticle = this.handleChangeArticle.bind(this);
        this.handleChangeArticle = this.handleChangeArticle.bind(this);
        this.callback = this.callback.bind(this);
        this.editor = null;
        this.editor_eng = null;
        this.state = {
            content: '',
            content_eng: ''
        }
    }

    onFinish = form => {
        form['description'] = this.state.content
        form['description_eng'] = this.state.content_eng
        if (form['partner'].length === 0) delete form['partner']
        this.props.actionAdmin.createTask(this.props.auth.access_token, form).then((r) => {
            notification.success({
                message: `Данные сохранены`,
                duration: 2.3,
            })
            Router.push(`/admin/tasks/${r.id}`);
        }).catch((e) => {
            notification.error({
                message: `Ошибка при сохранении`,
                description: 'Попробуйте обновить страницу и внести данные снова.',
                duration: 2.3,
            })
        })
    }

    componentDidMount() {
        setTimeout(() => {
            let initEditor = require('../../../components/EditorJs');
            this.editor = initEditor.initEditorJs('editorjs', {
                blocks: [
                    {
                        type: "paragraph",
                        data: {
                            text: "Начните писать тут."
                        }
                    }
                ]
            }, this.handleChangeArticle)
        }, 400)
    }


    handleChangeEngArticle = (api) => {
        api.saver.save().then((outputData) => {
            this.setState({
                content_eng: outputData
            })
        }).catch((e) => {
            // console.log(e)
        })
    }

    handleChangeArticle = (api) => {
        api.saver.save().then((outputData) => {
            this.setState({
                content: outputData
            })
        }).catch((e) => {
            // console.log(e)
        })
    }

    callback(key) {
        if (key === 'en') {
            setTimeout(() => {
                let initEditor = require('../../../components/EditorJs');
                this.editor_eng = initEditor.initEditorJs('editorjs_eng', {
                    blocks: [
                        {
                            type: "paragraph",
                            data: {
                                text: "Начните писать тут."
                            }
                        }
                    ]
                }, this.handleChangeEngArticle)
            }, 400)
        }
    }


    render() {
        return (
            <Form
                layout="vertical"
                onFinish={this.onFinish}
                ref={this.formRef}
                name="advanced_search"
                className="ant-advanced-search-form">
                <PageHeaderAlt className="border-bottom">
                    <div className="container">
                        <Flex className="py-2" mobileFlex={false} justifyContent="between" alignItems="center">
                            <h2 className="mb-3">Создать</h2>
                            <div className="mb-3">
                                <Button className="mr-2">Отмена</Button>
                                <Button type="primary" htmlType="submit">
                                    Сохранить
                                </Button>
                            </div>
                        </Flex>
                    </div>
                </PageHeaderAlt>
                <div className={`my-4 container`}>


                    <Row gutter={16}>
                        <Col xs={24} sm={24} md={24}>
                            <Card>
                                <Form.Item name="name" label="🇷🇺 Название">
                                    <Input placeholder="Название"/>
                                </Form.Item>
                                <Form.Item name="name_eng"
                                           label="🇬🇧 Название">
                                    <Input placeholder="Название на Англ"/>
                                </Form.Item>

                                <Form.Item label="Номинация"
                                           name={"nomination"}>
                                    <Select style={{width: '100%'}}>
                                        {
                                            this.props.nominations && this.props.nominations.map(item => {
                                                return <Option key={item.id}
                                                               value={item.id}>{item.name}</Option>
                                            })
                                        }
                                    </Select>
                                </Form.Item>

                                <Form.Item label="Партнеры задачи"
                                           name={"partner"} initialValue={[]}>
                                    <Select mode="multiple" style={{width: '100%'}}
                                            tokenSeparators={[',']}>
                                        {
                                            this.props.companyList && this.props.companyList.map(item => {
                                                return <Option key={item.name}
                                                               value={item.id}>{item.name}</Option>
                                            })
                                        }
                                    </Select>
                                </Form.Item>


                                <Form.Item name="maintainer"
                                           label="Компания">
                                    <Select>
                                        {
                                            this.props.companyList.map(item => {
                                                return <Option value={item.id} key={item.id}>{item.name}</Option>
                                            })
                                        }
                                    </Select>
                                </Form.Item>

                                <Tabs defaultActiveKey="1" onChange={this.callback}>

                                    <TabPane tab="🇷🇺 Описание" key="ru">
                                        <Form.Item name="short_description"

                                                   label="🇷🇺 Короткое описание ">
                                            <Input.TextArea placeholder="🇷🇺 Короткое описание "/>
                                        </Form.Item>

                                        <Form.Item label={"🇷🇺 Описание"}>
                                            <div id={"editorjs"}/>
                                        </Form.Item>
                                    </TabPane>

                                    <TabPane tab="🇬🇧 Описание" key="en">
                                        <Form.Item name="short_description_eng"
                                                   label="🇬🇧 Короткое описание">
                                            <Input.TextArea placeholder="🇬🇧 Короткое описание"/>
                                        </Form.Item>

                                        <Form.Item label={"🇬🇧 Описание"}>
                                            <div id={"editorjs_eng"}/>
                                        </Form.Item>

                                    </TabPane>
                                </Tabs>
                            </Card>
                        </Col>
                    </Row>
                </div>
            </Form>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return ({
        actionAdmin: bindActionCreators(AdminAction, dispatch),
    })
};

export default withAuthPrivateAdminRoute(connect((state => ({
    auth: state.auth,
    teams: state.admin.teams,
})), mapDispatchToProps)(add))

