import React from 'react';
import Router from "next/router";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {FormInstance} from "antd/lib/form";
import {Form, Input, Row, Col, Button, Card, notification} from 'antd'
// Компоненты
import PageHeaderAlt from '../../../components/layout-components/PageHeaderAlt'
import Flex from '../../../components/shared-components/Flex'
// Действия
import {AdminAction} from '../../../redux/actions/admin.action'
import withAuthPrivateAdminRoute from "../../../route/withAuthPrivateAdminRoute";
// Интерфейсы
import {Tasks} from "../../../redux/interface/tasks";
import {Company} from "../../../redux/interface/company";
import {HexColorPicker} from "react-colorful";


interface Props {
    active: Tasks,
    companyList: Company[],
    auth: any,
    id: number,
    actionAdmin: any,
    theme: any,
}

interface State {
    content: any,
    content_eng: any,
    color: string
}


class add extends React.Component<Props, State> {
    private editor: null;
    private editor_eng: null;
    formRef = React.createRef<FormInstance>();


    constructor(props: Props) {
        super(props);
        this.onFinish = this.onFinish.bind(this);
        this.editor = null;
        this.editor_eng = null;
        this.state = {
            content: '',
            content_eng: '',
            color: ''
        }
    }

    onFinish = form => {
        this.props.actionAdmin.createNomination(this.props.auth.access_token, form).then((r) => {
            notification.success({
                message: `Данные сохранены`,
                duration: 2.3,
            })
            Router.push(`/admin/nominations/${r.id}`);
        }).catch((e) => {
            notification.error({
                message: `Ошибка при сохранении`,
                description: 'Попробуйте обновить страницу и внести данные снова.',
                duration: 2.3,
            })
        })
    }


    render() {
        return (
            <Form
                layout="vertical"
                onFinish={this.onFinish}
                ref={this.formRef}
                name="advanced_search"
                className="ant-advanced-search-form">
                <PageHeaderAlt className="border-bottom">
                    <div className="container">
                        <Flex className="py-2" mobileFlex={false} justifyContent="between" alignItems="center">
                            <h2 className="mb-3">Создать номинацию</h2>
                            <div className="mb-3">
                                <Button className="mr-2">Отмена</Button>
                                <Button type="primary" htmlType="submit">
                                    Сохранить
                                </Button>
                            </div>
                        </Flex>
                    </div>
                </PageHeaderAlt>
                <div className={`my-4 container`}>

                    <Row gutter={16}>
                        <Col xs={24} sm={24} md={24}>
                            <Card>
                                <Form.Item name="name" label="🇷🇺 Название">
                                    <Input placeholder="Название"/>
                                </Form.Item>
                                <Form.Item name="name_eng"
                                           label="🇬🇧 Название">
                                    <Input placeholder="Название на Англ"/>
                                </Form.Item>
                                <Form.Item name="color"
                                           label="Цвет">
                                    <HexColorPicker color={this.state.color}
                                                    onChange={(value) => this.setState({color: value})}/>
                                </Form.Item>
                            </Card>
                        </Col>
                    </Row>

                </div>
            </Form>

        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return ({
        actionAdmin: bindActionCreators(AdminAction, dispatch),
    })
};

export default withAuthPrivateAdminRoute(connect((state => ({
    auth: state.auth,
})), mapDispatchToProps)(add))

