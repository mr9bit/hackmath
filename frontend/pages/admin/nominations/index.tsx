import React from 'react';
import moment from "moment";
import Link from 'next/link'
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {Row, Col, Image, Tooltip, Button, Card, notification, Table} from "antd";
import {ClockCircleOutlined, EyeOutlined, PlusOutlined, DeleteOutlined} from '@ant-design/icons';
// Компоненты
import PageHeaderAlt from '../../../components/layout-components/PageHeaderAlt'
import Flex from '../../../components/shared-components/Flex'
// Роутинг
import withAuthAndPrivateHackerRoute from "../../../route/withAuthAndPrivateHackerRoute";
import withExpiredToken from '../../../route/withExpiredToken';
// Действия
import {TaskAction} from '../../../redux/actions/task.action'
import useTranslation from "next-translate/useTranslation";
import {AdminAction} from "../../../redux/actions/admin.action";
import withAuthPrivateAdminRoute from "../../../route/withAuthPrivateAdminRoute";

const ItemInfo = ({datetime}) => {
    return <Flex flexDirection={'column'}>
        <div className="mr-3">
            <Tooltip title="Дата создания">
                <ClockCircleOutlined className="text-muted font-size-md"/>
                <span className="ml-1 text-muted">{moment(datetime).locale('ru').format('lll')}</span>
            </Tooltip>
        </div>
    </Flex>
}
const ItemHeader = ({name}) => (
    <div>
        <h2 className="mb-0">{name}</h2>
    </div>
)

const GridItem = ({data, deleteEvent}) => {
    const {t, lang} = useTranslation('common')
    return <Card bodyStyle={{height: "100%"}}>
        <Row gutter={16}>
            <Col md={18} xs={24} sm={24}>
                <div>
                    <ItemHeader name={data.name}/>
                </div>
            </Col>
            <Col md={6} xs={24} sm={24}>
                <div style={{display: "flex", justifyContent: "center"}}>
                    <Link href={{
                        pathname: '/admin/nominations/[id]',
                        query: {id: data.id},
                    }}>
                        <a>
                            <Button type={"primary"}>{t('view')}</Button>
                        </a>
                    </Link>
                    <Button onClick={() => deleteEvent(data.id)} className={"ml-2"} type={"primary"}
                            danger>Удалить</Button>

                </div>
            </Col>
        </Row>
    </Card>
}

export const index = (props) => {
    const deleteCompany = (id) => {
        props.actionAdmin.removeNomination(props.auth.access_token, id).then(() =>
            notification.success({
                message: `Номинация удалена`,
                duration: 2.3,
            })
        ).catch(error => {
            error.response.json().then((e) => {
                console.log(e)
            })
            notification.error({
                message: `Ошибка при удалении`,
                description: 'Попробуйте обновить страницу и внести данные снова.',
                duration: 2.3,
            })
        })
    }
    const columns = [
        {
            title: 'Название',
            dataIndex: 'name',
            key: 'name',
        },
        {

            title: '',
            dataIndex: 'actions',
            key: 'actions',
            render: (_, record) => (
                <div className="text-right">
                    <Link href={{
                        pathname: '/admin/nominations/[id]',
                        query: {id: record.id},
                    }}>
                        <a>
                            <Tooltip title="Полный просмотр">
                                <Button type="primary" className="mr-2 mt-2" icon={<EyeOutlined/>} size="small"/>
                            </Tooltip>
                        </a>
                    </Link>
                    <Tooltip title="Удаление">
                        <Button danger type="primary" className="mr-2 mt-2" onClick={() => {
                            deleteCompany(record.id)
                        }} icon={<DeleteOutlined/>} size="small"/>
                    </Tooltip>
                </div>
            )
        },
    ]

    return (
        <React.Fragment>
            <PageHeaderAlt className="bg-primary border-bottom mb-5">
                <div className="container-fluid">
                    <Flex justifyContent="between" alignItems="center" className="py-4">
                        <h2 className={"text-white display-4"}>Номинации</h2>
                        <div>
                            <Link href={'/admin/nominations/add'}>
                                <a>
                                    <Button className="ml-2">
                                        <PlusOutlined/>
                                        <span>Добавить</span>
                                    </Button>
                                </a>
                            </Link>
                        </div>
                    </Flex>
                </div>
            </PageHeaderAlt>

            <div className={`my-4 container`}>
                <Row gutter={16}>
                    <Table columns={columns}
                           rowKey='id'
                           style={{width:"100%"}}
                           dataSource={props.nominations_list}
                           pagination={false}
                    />
                </Row>
            </div>
        </React.Fragment>
    )
}

index.getInitialProps = async (ctx) => {
    await ctx.store.dispatch(AdminAction.getNominationList(ctx.auth.access_token))
    const {nominations_list} = mapStateToProps(ctx.store.getState())
    return {nominations_list}
}

const mapStateToProps = (state) => ({nominations_list: state.admin.nominations.list,});

const mapDispatchToProps = (dispatch) => ({actionAdmin: bindActionCreators(AdminAction, dispatch),});

export default withAuthPrivateAdminRoute(withExpiredToken(connect((state => ({
    auth: state.auth,
    nominations_list: state.admin.nominations.list
})), mapDispatchToProps)(index)))

