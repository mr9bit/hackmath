import React, {useEffect, useState} from "react";
import {bindActionCreators} from "redux";
import {AdminAction} from "../../../redux/actions/admin.action";
import withAuthPrivateAdminRoute from "../../../route/withAuthPrivateAdminRoute";
import withExpiredToken from "../../../route/withExpiredToken";
import {connect} from "react-redux";
import dynamic from "next/dynamic";
import {COLORS} from '../../../constants/ChartConstant';
import {Card, Col, Row} from "antd";
import ReactResizeDetector from 'react-resize-detector';

const ApexChart = dynamic(() => import("react-apexcharts"), {ssr: false});
import {apexPieChartDefaultOption} from '../../../constants/ChartConstant';
import moment from "moment";
import PageHeaderAlt from "../../../components/layout-components/PageHeaderAlt";

const PieChart = dynamic(() => import("../../../components/shared-components/PieChart"), {ssr: false});

export const statistics = (props) => {
    const [city, setCity] = useState({series: [], categories: []})
    const [institute, setInstitute] = useState({series: [], categories: []})
    const [HackVisited, setHackVisited] = useState({series: [], categories: []})
    const [science_degree, setScienceDegree] = useState({series: [], categories: []})
    const [specialization, setSpecialization] = useState({series: [], categories: []})
    const [where_to_study, setWhere_to_study] = useState({series: [], categories: []})
    const [birth_date, setBirthDate] = useState({series: [], categories: []})
    const defaultPieHackVisited = apexPieChartDefaultOption;
    useEffect(() => {
        let city_label = props.analytics.city.map(item => item.city)
        let city_data = props.analytics.city.map(item => item.count)
        setCity({
            series: city_data,
            categories: city_label
        })
        let institute_label = props.analytics.institute.map(item => item.institute)
        let institute_data = props.analytics.institute.map(item => item.count)
        setInstitute({
            series: institute_data,
            categories: institute_label
        })
        let HackVisited_label = props.analytics.hack_visited.map(item => item.hack_visited)
        let HackVisited_data = props.analytics.hack_visited.map(item => item.count)
        setHackVisited({
            series: HackVisited_data,
            categories: HackVisited_label
        })
        defaultPieHackVisited.labels = HackVisited_label;


        let specialization_label = props.analytics.specialization.map(item => item.specialization)
        let specialization_data = props.analytics.specialization.map(item => item.count)

        setSpecialization({
            series: specialization_data,
            categories: specialization_label
        })

        let science_degree_label = props.analytics.science_degree.map(item => item.science_degree)
        let science_degree_data = props.analytics.science_degree.map(item => item.count)

        setScienceDegree({
            series: science_degree_data,
            categories: science_degree_label
        })

        let where_to_study_label = props.analytics.where_to_study.map(item => item.where_to_study)
        let where_to_study_data = props.analytics.where_to_study.map(item => item.count)

        setWhere_to_study({
            series: where_to_study_data,
            categories: where_to_study_label
        })

        let birth_date_label = props.analytics.birth_date.map(item => moment().diff(new Date(item.year, 12, 12), 'years'))
        let birth_date_data = props.analytics.birth_date.map(item => item.count)

        setBirthDate({
            series: birth_date_data,
            categories: birth_date_label
        })


        setTimeout(() => window.dispatchEvent(new Event('resize')), 500)

    }, [])

    return <div>
        <div>
            <PageHeaderAlt className="bg-primary mb-5" overlap>
                <div className="container text-center">
                    <div className="py-lg-4">
                        <h1 className="text-white display-4">Статистика</h1>
                    </div>
                </div>
            </PageHeaderAlt>
            <Row gutter={16}>
                <Col xs={24} sm={24} md={12} lg={12}>
                    <Card title="Откуда участники">

                        <ApexChart
                            options={{
                                plotOptions: {
                                    bar: {
                                        horizontal: true,
                                    }
                                },
                                colors: COLORS,
                                xaxis: {
                                    categories: city.categories,
                                }
                            }}
                            series={[{
                                data: city.series
                            }]}
                            type="bar"
                            height={600}
                        />
                    </Card>
                </Col>
                <Col xs={24} sm={24} md={12} lg={12}>
                    <Card title="Институты">
                        <ApexChart
                            options={{
                                plotOptions: {
                                    bar: {
                                        horizontal: true,
                                    }
                                },
                                colors: COLORS,
                                xaxis: {
                                    categories: institute.categories,
                                }
                            }}
                            series={[{
                                data: institute.series
                            }]}
                            type="bar"
                            height={600}
                        />
                    </Card>
                </Col>
                <Col xs={24} sm={24} md={12} lg={12}>
                    <Card title={"Сколько хакатонов посетили"}>
                        <PieChart
                            options={{
                                colors: COLORS,
                                labels: HackVisited.categories,
                                responsive: [{
                                    breakpoint: 480,
                                    options: {
                                        chart: {
                                            width: 200
                                        },
                                        legend: {
                                            position: 'bottom'
                                        }
                                    }
                                }]
                            }}
                            series={HackVisited.series}
                        />
                    </Card>
                    <Card title={"Уровень образования"}>
                        <PieChart
                            options={{
                                colors: COLORS,
                                labels: science_degree.categories,
                                responsive: [{
                                    breakpoint: 480,
                                    options: {
                                        chart: {
                                            width: 200
                                        },
                                        legend: {
                                            position: 'bottom'
                                        }
                                    }
                                }]
                            }}
                            series={science_degree.series}
                        />
                    </Card>
                </Col>
                <Col xs={24} sm={24} md={12} lg={12}>
                    <Card title={"Из каких учреждений участники"}>
                        <PieChart
                            options={{
                                colors: COLORS,
                                labels: where_to_study.categories,
                                responsive: [{
                                    breakpoint: 480,
                                    options: {
                                        chart: {
                                            width: 200
                                        },
                                        legend: {
                                            position: 'bottom'
                                        }
                                    }
                                }]
                            }}
                            series={where_to_study.series}
                        />
                    </Card>
                    <Card title="Возраст участников">

                        <ApexChart
                            options={{
                                plotOptions: {
                                    bar: {
                                        horizontal: false,
                                    }
                                },
                                colors: COLORS,
                                xaxis: {
                                    categories: birth_date.categories,
                                }
                            }}
                            series={[{
                                data: birth_date.series
                            }]}
                            type="bar"
                            height={250}
                        />
                    </Card>
                </Col>
                <Col xs={24} sm={24} md={24} lg={24}>
                    <Card title="Направления обучения">

                        <ApexChart
                            options={{
                                plotOptions: {
                                    bar: {
                                        horizontal: false,
                                    }
                                },
                                colors: COLORS,
                                xaxis: {
                                    categories: specialization.categories,
                                }
                            }}
                            series={[{
                                data: specialization.series
                            }]}
                            type="bar"
                            height={600}
                        />
                    </Card>
                </Col>
            </Row>


        </div>


    </div>
}


const mapStateToProps = (state) => {
    return {
        user: state.auth.user,
        analytics: state.admin.analytics,
    };
};

statistics.getInitialProps = async (ctx) => {
    await ctx.store.dispatch(AdminAction.getAnalytics(ctx.auth.access_token))
    const {analytics} = mapStateToProps(ctx.store.getState())
    return {analytics}
}

const mapDispatchToProps = (dispatch) => ({
    actionAdmin: bindActionCreators(AdminAction, dispatch),
});

export default withAuthPrivateAdminRoute(withExpiredToken(connect((state => ({
    auth: state.auth,
    analytics: state.admin.analytics
})), mapDispatchToProps)(statistics)))

