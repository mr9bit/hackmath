import React from "react";
import {Row, Form, notification, Input, Button, Col} from "antd";

import withTranslation from "next-translate/withTranslation";
import withExpiredToken from "../route/withExpiredToken";
import withAuthAndPrivateHackerRoute from "../route/withAuthAndPrivateHackerRoute";
import {bindActionCreators} from "redux";
import {AuthAction} from "../redux/actions/auth.action";
import {connect} from "react-redux";
import PageHeaderAlt from "../components/layout-components/PageHeaderAlt";
import {TeamAction} from "../redux/actions/team.action";
import {index} from "./index";

const results = (props) => {
    const {t, lang} = props.i18n;

    const onFinish = (form) => {
        props.actionTeam.sendResult(props.auth.access_token, form).then((res) => {
            notification.success({
                message: "Выполненео",
                description: "Ваш результат отправлен",
                duration: 5,
            })
        }).catch(() => {
            notification.error({
                message: "Ошибка",
                description: "Обратитесь к администратору",
                duration: 5,
            })
        })
    }
    return (
        <>
            <PageHeaderAlt className=" mb-5" overlap>
                <div className="container text-center">
                    <div className="py-lg-4">
                        <h1 className="text-primary display-4">{t('results')}</h1>
                    </div>
                </div>
            </PageHeaderAlt>

            <div className="code-box">
                <div className="container my-4">
                    <Row gutter={16}>
                        <div style={{width: "100%"}}>

                            <Col xs={24} sm={24} md={24} lg={{span: 14, offset: 5}}>
                                <Form onFinish={onFinish} layout={"vertical"}>
                                    <Form.Item label={"Ссылка на репозиторий"}
                                               name={"link_to_git"}
                                               help={<p>
                                                   Можете загрузить на один из Git сервисов <a
                                                   href={"https://github.com/"}>GitHub</a>,
                                                   <a href={"https://gitlab.com/users/sign_in"}>GitLab</a>,
                                                   <a href={"https://bitbucket.org/"}>BitBucket</a>. Репозиторий
                                                   обязательно
                                                   должен быть откртым и оформленым readme.md
                                               </p>}
                                               initialValue={props.team.result?.link_to_git}>
                                        <Input disabled={props.team.result?.link_to_git != null}/>
                                    </Form.Item>
                                    <Form.Item label={"Ссылка на презентацию"} name={"link_to_presentation"}
                                               help={<p> Свою презентацию вы можете загрузить в <a
                                                   href={"https://github.com/"}>Google Drive</a> или <a
                                                   href={"https://gitlab.com/users/sign_in"}>Яндекс.Диск</a>. Доступ к
                                                   презентации обязательно должен быть открыт. <a
                                                       href={"https://drive.google.com/file/d/1p2cPOSLg2OzU5uuu-VFp5b4SEWMy1dkv/view?usp=sharing"}>Шаблон
                                                       презентации</a>
                                               </p>}
                                               initialValue={props.team.result?.link_to_presentation}>
                                        <Input disabled={props.team.result?.link_to_presentation != null}/>
                                    </Form.Item>
                                    {
                                        props.team.result?.link_to_presentation ? null :
                                            <Form.Item>
                                                <Button type="primary" htmlType="submit" block>
                                                    {t('common:send')}
                                                </Button>
                                            </Form.Item>
                                    }
                                </Form>
                            </Col>

                        </div>
                    </Row>
                </div>
            </div>
        </>
    )
}


const mapStateToProps = (state) => {
    return {
        user: state.auth.user,
        team: state.team,
    };
};


results.getInitialProps = async (ctx) => {
    await ctx.store.dispatch(TeamAction.getResult(ctx.auth.access_token))
    const {user} = mapStateToProps(ctx.store.getState())
    return {user}
}

const mapDispatchToProps = (dispatch) => {
    return {
        actionTeam: bindActionCreators(TeamAction, dispatch),
    };
};
export default withTranslation(withExpiredToken(withAuthAndPrivateHackerRoute(connect((state => ({
    auth: state.auth,
    team: state.team
})), mapDispatchToProps)(results))), "common")
