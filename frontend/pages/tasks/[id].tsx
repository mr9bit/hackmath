import React from 'react';
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {Form, Typography, Row, Col, Card, Image} from 'antd'
import {FormInstance} from "antd/lib/form";
import withTranslation from 'next-translate/withTranslation';

import PageHeaderAlt from '../../components/layout-components/PageHeaderAlt'
import {LinkTool} from '../../components/Link'
import Flex from '../../components/shared-components/Flex'
import ReactDOM from 'react-dom'
import {Tasks} from "../../redux/interface/tasks";
import {Company} from "../../redux/interface/company";
import {TaskAction} from "../../redux/actions/task.action";
import withAuthAndPrivateHackerRoute from "../../route/withAuthAndPrivateHackerRoute";
import Link from 'next/link'
import {stringify} from "postcss";

interface Props {
    active: Tasks,
    companyList: Company[],
    auth: any,
    id: number,
    actionAdmin: any,
    theme: any,
    i18n: any
}

interface State {
    content: any
}

const {Paragraph, Text, Title} = Typography;
const mapStateToProps = (state) => ({active: state.tasks.active});
const HtmlToReactParser = require('html-to-react').Parser;
var htmlToReactParser = new HtmlToReactParser();

class task extends React.Component<Props, State> {
    formRef = React.createRef<FormInstance>();

    static async getInitialProps(ctx) {
        const id = ctx.query.id;
        await ctx.store.dispatch(TaskAction.get(ctx.auth.access_token, id));
        const {active} = mapStateToProps(ctx.store.getState());
        return {active, id}
    }

    render() {
        const {t, lang} = this.props.i18n;
        return (
            <Form
                layout="vertical"
                ref={this.formRef}
                name="advanced_search"
                className="ant-advanced-search-form">

                <PageHeaderAlt className=" mb-5" overlap>
                    <div className="container text-center">
                        <div className="py-lg-4">
                            <h1 className="text-primary display-4">
                                {lang === "en" ? this.props.active.name_eng : this.props.active.name}
                            </h1>
                        </div>
                    </div>
                </PageHeaderAlt>


                <div className={`my-4 container`}>


                    <Row gutter={16}>
                        <Col xs={24} sm={24} md={24}>
                            <Card bodyStyle={{display: 'flex', flexDirection: 'column', alignItems: 'center'}}>
                                <Col xs={24} sm={24} md={16}>

                                    <div>
                                        <Title level={2}>{t('short_description')}</Title>
                                        <Paragraph>
                                            {lang === "en" ? this.props.active.short_description_eng : this.props.active.short_description}
                                        </Paragraph>
                                    </div>
                                    <div>
                                        {
                                            lang === "en" ?
                                                this.props.active.description_eng?.blocks?.map((item) => {
                                                    return item.type === 'paragraph' ?
                                                        <div key={item.data.text}
                                                             className={"ant-typography font-size-base"}>
                                                            {htmlToReactParser.parse(item.data.text)}
                                                        </div> : item.type === 'header' ?
                                                            <Title key={item.data.text} level={2}>
                                                                {htmlToReactParser.parse(item.data.text)}
                                                            </Title> : item.type === 'image' ?
                                                                <Image key={item.data.file.url}
                                                                       src={item.data.file.url}/>
                                                                : item.type === 'linkTool' ?
                                                                    <LinkTool
                                                                        key={item.data.meta} {...item.data}/> : null
                                                }) :
                                                this.props.active.description?.blocks?.map((item) => {
                                                    return item.type === 'paragraph' ?
                                                        <div key={item.data.text}
                                                             className={"ant-typography font-size-base"}>
                                                            {htmlToReactParser.parse(item.data.text)}
                                                        </div> : item.type === 'header' ?
                                                            <Title key={item.data.text} level={2}>
                                                                {htmlToReactParser.parse(item.data.text)}
                                                            </Title> : item.type === 'image' ?
                                                                <Image key={item.data.file.url}
                                                                       src={item.data.file.url}/>
                                                                : item.type === 'linkTool' ?
                                                                    <LinkTool
                                                                        key={item.data.meta} {...item.data}/> : null
                                                })
                                        }
                                    </div>
                                    <div>
                                        <Title level={2}>{t('problem_setter')}</Title>
                                        <Row gutter={16}>
                                            <Col xs={24} sm={24} md={6}>
                                                <Link href={this.props.active.maintainer.url}>
                                                    <a>
                                                        <Image style={{maxHeight: "120px", objectFit: 'contain'}}
                                                               className="mt-2"
                                                               preview={false}
                                                               src={lang === 'en' ? this.props.active.maintainer.logo_eng : this.props.active.maintainer.logo}/>
                                                    </a>
                                                </Link>
                                            </Col>
                                        </Row>
                                    </div>
                                    <div>
                                        {this.props.active.partner.length ?
                                            <>
                                                <Title level={2}>{t('partners')}</Title>
                                                <Row gutter={16}>
                                                    {this.props.active.partner.map((item) => {
                                                        return <Col key={item.id} xs={24} sm={24} md={6}>
                                                            <Link href={item.url}>
                                                                <a>
                                                                    <Image style={{
                                                                        maxHeight: "120px",
                                                                        objectFit: 'contain'
                                                                    }}
                                                                           className="mt-2"
                                                                           preview={false}
                                                                           src={item.logo}/>
                                                                </a>
                                                            </Link>
                                                        </Col>
                                                    })}
                                                </Row>
                                            </> : null}


                                    </div>

                                </Col>
                            </Card>
                        </Col>
                    </Row>

                </div>
            </Form>

        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return ({
        actionTask: bindActionCreators(TaskAction, dispatch),
    })
};

export default withTranslation(withAuthAndPrivateHackerRoute(connect((state => ({
    auth: state.auth,
    tasks: state.tasks.active,
})), mapDispatchToProps)(task)), "tasks")

