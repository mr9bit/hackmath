import React from 'react';
import moment from "moment";
import Link from 'next/link'
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {Row, Col, Image, Tooltip, Button, Card, notification, Tag} from "antd";
import {ClockCircleOutlined, PlusOutlined} from '@ant-design/icons';
// Компоненты
import PageHeaderAlt from '../../components/layout-components/PageHeaderAlt'
import Flex from '../../components/shared-components/Flex'
// Роутинг
import withAuthAndPrivateHackerRoute from "../../route/withAuthAndPrivateHackerRoute";
import withExpiredToken from '../../route/withExpiredToken';
// Действия
import {TaskAction} from '../../redux/actions/task.action'
import useTranslation from "next-translate/useTranslation";

const ItemInfo = ({datetime}) => {
    return <Flex flexDirection={'column'}>
        <div className="mr-3">
            <Tooltip title="Дата создания">
                <ClockCircleOutlined className="text-muted font-size-md"/>
                <span className="ml-1 text-muted">{moment(datetime).locale('ru').format('lll')}</span>
            </Tooltip>
        </div>
    </Flex>
}
const ItemHeader = ({name}) => (
    <div>
        <h3 className="mb-0">{name}</h3>
    </div>
)

const GridItem = ({data}) => {
    const {t, lang} = useTranslation('common')
    return <Card bodyStyle={{height: "100%"}} className={"mb-2"} style={{height: "100%"}}>

        <div>
            <Flex alignItems="center" justifyContent="between">
                <Link href={{
                    pathname: '/tasks/[id]',
                    query: {id: data.id},
                }}>
                    <a>
                        <ItemHeader name={lang === 'en' ? data.name_eng : data.name}/>
                    </a>
                </Link>
            </Flex>
        </div>
        <div className={"mt-2 mb-3"}>
            <Tag
                color={data.nomination?.color}
                style={{
                    whiteSpace: "break-spaces",
                    width: "100%",
                    lineHeight: "14px",
                    textAlign: "center",
                    padding: "5px"
                }}>{lang === 'en' ? data.nomination?.name_eng : data.nomination?.name}</Tag>
        </div>
        <div style={{height: '180px', display: 'flex', flexDirection: 'column', justifyContent: 'center'}}>
            <Image style={{maxHeight: "120px", objectFit: 'contain'}} className="mt-2" preview={false}
                   src={lang === 'en' ? data.maintainer.logo_eng : data.maintainer.logo}/>
        </div>
        <div className={"mt-2"}>
            {lang === 'en' ? data.short_description_eng : data.short_description}
        </div>

    </Card>
}

export const index = (props) => {
    const {t, lang} = useTranslation('tasks')

    return (
        <React.Fragment>
            <PageHeaderAlt className=" mb-5" overlap>
                <div className="container text-center">
                    <div className="py-lg-4">
                        <h1 className="text-primary display-4">{t('title')}</h1>
                    </div>
                </div>
            </PageHeaderAlt>

            <div className={`my-4 container`}>
                <Row gutter={16}>
                    {
                        props.tasks.list.results && props.tasks.list.results.map(item => {

                            return <Col className={"mb-3"} key={item.id} xs={24} sm={24} lg={8} md={8} xl={8} xxl={8}>
                                <GridItem data={item}/>
                            </Col>

                        })
                    }
                </Row>
            </div>
        </React.Fragment>
    )
}

index.getInitialProps = async (ctx) => {
    await ctx.store.dispatch(TaskAction.list(ctx.auth.access_token))
    const {tasks} = mapStateToProps(ctx.store.getState())
    return {tasks}
}

const mapStateToProps = (state) => ({tasks: state.tasks,});

const mapDispatchToProps = (dispatch) => ({actionTask: bindActionCreators(TaskAction, dispatch),});

export default withAuthAndPrivateHackerRoute(withExpiredToken(connect((state => ({
    auth: state.auth,
    tasks: state.tasks
})), mapDispatchToProps)(index)))

