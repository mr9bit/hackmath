import React from 'react';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {FormInstance} from 'antd/lib/form';
import {CheckOutlined} from '@ant-design/icons';
import withTranslation from 'next-translate/withTranslation';
import {Input, Form, Button, Spin, Typography, Col, notification} from 'antd';
// Компоненты
import PageHeaderAlt from '../components/layout-components/PageHeaderAlt';
// Действия
import {OnlineActivityAction} from '../redux/actions/onlineactivity.action';
// Роутинг
import withAuthAndPrivateHackerRoute from '../route/withAuthAndPrivateHackerRoute';
import withExpiredToken from '../route/withExpiredToken';
import Trans from "next-translate/Trans";

const {Title, Text} = Typography;

type OnlineActivity = {
    post_url: string,
    comment: string,
    complete: boolean

}

interface IOnlineActivity {
    post_url: string,
    comment: string,
    complete: boolean
}

interface Props {
    activity: OnlineActivity,
    actionOnlineActivity: any,
    access_token: string;
    auth: any
    activityReducer: any,
    i18n: any
}


const mapStateToProps = (state) => {
    return {
        activity: state.activity,
    };
};

interface State extends IOnlineActivity {


}

class onlineactivity extends React.Component<Props, State> {
    formRef = React.createRef<FormInstance>();

    static async getInitialProps(ctx) {
        await ctx.store.dispatch(OnlineActivityAction.get(ctx.auth.access_token))
        const {activity} = ctx.store.getState().onlineActivity
        return {activity}

    }


    constructor(props: Props) {
        super(props);
        this.state = {
            ...this.props.activityReducer.activity,
        }
        this.onSubmit = this.onSubmit.bind(this);

    }

    onSubmit = (form) => {
        const {t, lang} = this.props.i18n;

        if (form) {
            notification.info({
                message: t('common:saving'),
                duration: 1.2
            })
            this.props.actionOnlineActivity.save(this.props.auth.access_token, form).then(() => {
                    this.setState({
                        ...this.props.activityReducer.activity
                    });
                    notification.success({
                        message: t('common:data_save'),
                        duration: 2.3,
                    })
                }
            ).catch((e) => {
                notification.error({
                    message: t('common:error'),
                    duration: 2.3,
                })
            })
        }
    }

    render() {
        const {t, lang} = this.props.i18n;
        return (
            <React.Fragment>

                <PageHeaderAlt className=" mb-5" overlap>
                    <div className="container text-center">
                        <div className="py-lg-4">
                            <h1 className="text-primary display-4">#OnlineActivity</h1>
                        </div>
                    </div>
                </PageHeaderAlt>

                <Form size={"large"} layout="vertical" ref={this.formRef} onFinish={this.onSubmit}>
                    <div className="code-box">
                        <div className="container my-4">
                            <Col xs={24} sm={24} md={24} lg={{span: 14, offset: 5}}>

                                <Trans
                                    i18nKey="onlineactivity:description_1"
                                    components={[<b/>,]}
                                />


                                <br/><br/>

                                <Trans
                                    i18nKey="onlineactivity:description_2"
                                    components={[<b/>,]}
                                />

                            </Col>
                            <Col xs={24} sm={24} md={24} lg={{span: 14, offset: 5}}>
                                {this.state.post_url ?
                                    <React.Fragment>
                                        <Form.Item label={this.state.complete ?
                                            <div style={{display: "flex", alignItems: "center"}}>
                                                <div style={{marginRight: '10px'}}>{t('check')}</div>
                                                <CheckOutlined/>
                                            </div>
                                            : <div style={{display: "flex", alignItems: "center"}}>
                                                <div style={{marginRight: '10px'}}>{t('check_now')}</div>
                                                <Spin/>
                                            </div>} name={"post_url"}
                                                   initialValue={this.state.post_url}>
                                            <Input disabled={true}
                                                   placeholder={"https://vk.com/itmai?w=wall-153639485_3731"}/>
                                        </Form.Item>
                                        {
                                            this.state.comment ? <>
                                                <Title level={5}>{t('comment')}</Title>
                                                <Text>{this.state.comment}</Text>
                                            </> : null
                                        }
                                    </React.Fragment> :


                                    <React.Fragment>
                                        <Form.Item label={t('url_post')} name={"post_url"}
                                                   initialValue={this.state.post_url}>
                                            <Input placeholder={"https://vk.com/itmai?w=wall-153639485_3731"}/>
                                        </Form.Item>
                                        <Form.Item wrapperCol={{span: 24}}>
                                            <Button style={{width: "100%"}} className="mr-2" type="primary"
                                                    onClick={() => this.onSubmit}
                                                    htmlType="submit">
                                                {t('common:send')}

                                            </Button>
                                        </Form.Item>
                                    </React.Fragment>
                                }
                            </Col>

                        </div>
                    </div>

                </Form>


            </React.Fragment>
        )
    }
}


const mapDispatchToProps = (dispatch) => {
    return {
        actionOnlineActivity: bindActionCreators(OnlineActivityAction, dispatch),
    };
};
export default withTranslation(withAuthAndPrivateHackerRoute(connect((state => ({
    auth: state.auth,
    activityReducer: state.onlineActivity
})), mapDispatchToProps)(onlineactivity)), "onlineactivity");

