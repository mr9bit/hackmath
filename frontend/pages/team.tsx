import React from 'react';
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {NotificationTwoTone, FrownTwoTone, LogoutOutlined} from '@ant-design/icons';
import {
    Alert, Divider, Input, Form, Button, Radio, Card, Row,
    Modal, Typography, Col, notification, Select
} from 'antd';
import {FormInstance} from 'antd/lib/form';
import PageHeaderAlt from '../components/layout-components/PageHeaderAlt';
import {TeamAction} from '../redux/actions/team.action';
import withAuthAndPrivateHackerRoute from '../route/withAuthAndPrivateHackerRoute';
import withExpiredToken from '../route/withExpiredToken';
import {MAIN_URL, URl_BACKEND} from '../redux/settings';
import Router from "next/router";
import withTranslation from 'next-translate/withTranslation';
import Trans from 'next-translate/Trans';

const {Option} = Select;
const {Title, Paragraph} = Typography;
const {TextArea} = Input;

type OnlineActivity = {
    post_url: string,
    comment: string,
    complete: boolean

}

interface IOnlineActivity {
    post_url: string,
    comment: string,
    complete: boolean
}

interface Props {
    activity: OnlineActivity;
    actionOnlineActivity: any;
    access_token: string;
    auth: any;
    actionTeam: any;
    activityReducer: any;
    team: any;
    invite: string;
    i18n: any
}


type TaskComplete = {
    id: number,
    name: string
}

interface State {
    screen: string;
    invite: string;
    tasksList: TaskComplete[];
    traks: string;
    selectedItems: string[],
    searchTeamScreen: string,
    isModalVisible: boolean,
    modalData: any,
    selecting_tasks: number[]
}

const OPTIONS = ['Фронтенд разработчик', 'Бэкенд разработчик', 'Data Scientist', 'Инженер', 'Full-Stack Developer', 'Дизайнер',
    'Генерация идей', 'Проработка бизнес-составляющей', 'Продукт менеджер', 'Маркетинг', 'Инженер-электроник', 'Моральная поддержка', 'I do everything', 'Что-то другое'];

const OPTIONS_ENG = ["Front-End Developer", "Back-End Developer", "Data Scientist", "Engineer", "Full-Stack Developer", "Designer",
    "Idea generation", "Business component development", "Product manager", "Marketing", "Electronic engineer", "Moral support", "I do everything", "Something else"];


class team extends React.Component<Props, State> {
    formRef = React.createRef<FormInstance>();
    isMounted = false

    static async getInitialProps(ctx) {
        const {invite} = ctx.query;
        await ctx.store.dispatch(TeamAction.get(ctx.auth.access_token));
        await ctx.store.dispatch(TeamAction.search_list(ctx.auth.access_token));
        return {team: ctx.store.getState().team, invite: invite}
    }


    constructor(props: Props) {
        super(props);

        this.state = {
            screen: this.props.team.name ? 'my_team' : 'find_team',
            invite: this.props.invite,
            isModalVisible: false,
            modalData: false,
            tasksList: [],
            searchTeamScreen: 'hackers',
            traks: '',
            selectedItems: [],
            selecting_tasks: []
        }
        this.openModal = this.openModal.bind(this);
        this.setScreen = this.setScreen.bind(this)
        this.createTeam = this.createTeam.bind(this)
        this.leaveTeam = this.leaveTeam.bind(this)
        this.joinTeam = this.joinTeam.bind(this);
        this.searchStart = this.searchStart.bind(this);
        this.handleChangeTasks = this.handleChangeTasks.bind(this);
        this.leaveSearch = this.leaveSearch.bind(this);
        this.leaveSearch = this.leaveSearch.bind(this);
        this.handleChangeSelectedTasks = this.handleChangeSelectedTasks.bind(this)
        this.onSelectTask = this.onSelectTask.bind(this)
        this.sendReport = this.sendReport.bind(this)
    }

    componentDidMount() {
        this.isMounted = true
        const {t, lang} = this.props.i18n;
        fetch(`${URl_BACKEND}/api/org/task/`, {
            method: 'GET',
            headers: {
                'Authorization': `Bearer ${this.props.auth.access_token}`
            },
        }).then((e) => e.json()).then((res) => {
            if (this.isMounted) this.setState({tasksList: res.results})
        })
        if (this.state.invite) {
            this.props.actionTeam.invite(this.props.auth.access_token, {'invite': this.state.invite})
                .then(() => {
                    notification.success({
                        message: t('common:data_save'),
                        duration: 2.3,
                    })
                    if (this.isMounted) this.setState({invite: null, screen: 'my_team'})
                    Router.replace("/team", undefined, {shallow: true});
                })
                .catch(() => {
                    Router.replace("/team", undefined, {shallow: true});
                })
        }
        const selected_tasks = this.props.team.search.my?.selected_tracks.map(item => item.id);
        if (selected_tasks) this.setState({selecting_tasks: selected_tasks})
    }

    componentWillUnmount() {
        this.isMounted = false;
    }

    // Смена экрана при переключении
    setScreen = (e) => {
        if (e.target.value === 'my_team') {
            this.props.actionTeam.get(this.props.auth.access_token)
        }
        this.setState({
            screen: e.target.value
        })
    }

    /*
     *   Start Команда
     */
    // Создание команды
    createTeam = (form) => {
        const {t, lang} = this.props.i18n;
        this.props.actionTeam.create(this.props.auth.access_token, form).then((e) => {
                this.props.actionTeam.search_list(this.props.auth.access_token)
                notification.success({
                    message: t('common:team_create'),
                    duration: 2.3,
                })
                this.setState({
                    screen: 'my_team'
                })

            }
        ).catch((error) => {
            error.response.json().then((e) => {
                e.error.map(item => {
                    notification.error({
                        message: t('common:error'),
                        description: item,
                        duration: 2.3,
                    })
                })
            })
        })
    }
    // Покинуть команду
    leaveTeam = (form) => {
        const {t, lang} = this.props.i18n;
        this.props.actionTeam.leave(this.props.auth.access_token, form).then((e) => {
            this.props.actionTeam.search_list(this.props.auth.access_token)
            notification.success({
                message: t('common:data_save'),
                duration: 2.3,
            })

            this.setState({
                screen: 'find_team'
            })

        }).catch((error) => {
            error.response.json().then((e) => {
                e.error.map(item => {
                    notification.error({
                        message: t('common:error'),
                        description: item,
                        duration: 2.3,
                    })
                })
            })
        })
    }
    // Присоединиться к команде
    joinTeam = (form) => {
        const {t, lang} = this.props.i18n;
        this.props.actionTeam.invite(this.props.auth.access_token, form).then((e) => {
            this.props.actionTeam.search_list(this.props.auth.access_token)
            notification.success({
                message: t('common:enter_team'),
                duration: 2.3,
            })
            this.setState({
                screen: 'my_team'
            })
        }).catch((error) => {
            error.response.json().then((e) => {
                e.error.map(item => {
                    notification.error({
                        message: t('common:error'),
                        description: item,
                        duration: 2.3,
                    })
                })
            })
        })
    }
    /*
     *  End Команда
     */


    /*
        Start Модальные окна
     */
    // Сохраняем данные для показа в модально окне
    handleChangeTasks = selectedItems => {
        this.setState({selectedItems});
    };
    // Смена экрана при поиске комнады
    setSearchTeamScreen = (e) => {
        this.props.actionTeam.search_list(this.props.auth.access_token)
        const selected_tasks = this.props.team.search.my?.selected_tracks.map(item => item.id);
        if (selected_tasks) this.setState({selecting_tasks: selected_tasks})

        this.setState({
            searchTeamScreen: e.target.value
        })
    }
    // Открыть модальное окно
    openModal = (item) => {
        this.setState({
            modalData: item,
            isModalVisible: !this.state.isModalVisible
        })
    }
    // Закрыть модальное окно
    closeModal = () => {
        this.setState({
            isModalVisible: !this.state.isModalVisible
        })
    }
    /*
     *  End Модальные окна
     */


    /*
        Start Поиск сокомандников
     */
    // Создание запроса на поиск сокомандника
    searchStart = (form) => {
        const {t, lang} = this.props.i18n;
        form.role = form.role.map((val) => {
            return val;
        }).join(',');

        this.props.actionTeam.search(this.props.auth.access_token, form).then((e) => {
            notification.success({
                message: t('common:send_request_search_team'),
                duration: 2.3,
            })
        }).catch((e) => {
            notification.error({
                message: t('common:error'),
                description: t('common:try_update_page'),
                duration: 2.3,
            })
        })
    }
    // Выйти из поиска сокомандников
    leaveSearch = () => {
        const {t, lang} = this.props.i18n;
        const selected_tasks = this.props.team.search.my?.selected_tracks.map(item => item.id);
        if (selected_tasks) this.setState({selecting_tasks: selected_tasks})
        this.props.actionTeam.leave_search(this.props.auth.access_token).then(() => {
                notification.success({
                    message: t('common:out_from_search_team'),
                    duration: 2.3,
                })
            }
        ).catch(() => {
            notification.error({
                message: t('common:error'),
                description: t('common:try_update_page'),
                duration: 2.3,
            })
        })
    }
    // Выслать приглашениет
    sendInvite = (searchTeamId_to) => {
        const {t, lang} = this.props.i18n;
        this.props.actionTeam.sendInvite(this.props.auth.access_token, {
            search_from: this.props.team.search.my.id,
            search_to: searchTeamId_to
        }).then((e) => {
            notification.success({
                message: t('common:you_send_request'),
                duration: 2.3,
            })
        }).catch((error) => {
            error.response.json().then((e) => {
                e.error.map(item => {
                    notification.error({
                        message: t('common:error'),
                        description: item,
                        duration: 2.3,
                    })
                })
            })
        })
    }

    /*
       End Поиск сокомандников
    */


    handleChangeSelectedTasks = (selectedItems) => {
        this.setState({selecting_tasks: selectedItems})
    };


    onSelectTask(id, item) {

        fetch(`${URl_BACKEND}/api/org/team/select`, {
            method: 'POST',
            headers: {
                'Authorization': `Bearer ${this.props.auth.access_token}`,
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({"team": this.props.team.id, "task": id})

        }).then((e) => e.json()).then((res) => {
            notification.success({
                message: `Выбрана задача ${item.children}`,
                duration: 2.3,
            })
        }).catch((error) => {
            error.response.json().then((e) => {
                notification.error({
                    message: e,
                    duration: 2.3,
                })
            })
        })
    }

    sendReport(input) {
        const report_link = input.target.value;
        fetch(`${URl_BACKEND}/api/org/team/report`, {
            method: 'POST',
            headers: {
                'Authorization': `Bearer ${this.props.auth.access_token}`,
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({"team": this.props.team.id, "report": report_link})
        }).then((e) => e.json()).then((res) => {
            notification.success({
                message: `План работы отправлен`,
                duration: 2.3,
            })
        }).catch((error) => {
            error.response.json().then((e) => {
                notification.error({
                    message: e,
                    duration: 2.3,
                })
            })
        })
    }

    render() {
        const {selectedItems} = this.state;
        const {t, lang} = this.props.i18n;
        let filteredOptions = lang === 'en' ? OPTIONS_ENG.filter(o => !selectedItems.includes(o)) : OPTIONS.filter(o => !selectedItems.includes(o))

        return (
            <React.Fragment>

                <Modal
                    bodyStyle={{color: 'black!important', fontSize: "16px"}}
                    title={t('profile')}
                    width={920}
                    centered
                    okText={t('common:send_request', {count: this.props.team.search.available_of_requests})}
                    onOk={() => this.sendInvite(this.state.modalData.id)}
                    onCancel={this.closeModal} cancelText={t('common:cancel')}
                    visible={this.state.isModalVisible}>


                    {this.state.modalData && this.state.modalData.type == 'hacker' ?
                        <Title level={2}
                               style={{color: 'black'}}>{this.state.modalData && this.state.modalData.user.first_name} {this.state.modalData && this.state.modalData.user.second_name}</Title>
                        :
                        <Title level={2}
                               style={{color: 'black'}}>{this.state.modalData && this.state.modalData.team.name}</Title>
                    }


                    <p style={{fontSize: "16px", color: 'black'}}>
                        <b> {this.state.modalData && this.state.modalData.contact}</b>
                    </p>
                    <Divider/>
                    <p style={{fontSize: "16px", color: 'black'}}>
                        <b>{t('priority_task')}:</b> {this.state.modalData && this.state.modalData.selected_tracks.map(track => {
                        return ` ${track.name}`
                    }).join(', ')}</p>
                    {
                        this.state.modalData.type === 'team' ?
                            <p style={{fontSize: "16px", color: 'black'}}>
                                <b>{t('looking_for')}:</b> {this.state.modalData.role?.split(',').map(item => {
                                return item
                            }).join(', ')}
                            </p>
                            :
                            <p style={{fontSize: "16px", color: 'black'}}>
                                <b>{t('my_role')}:</b> {this.state.modalData.role?.split(',').map(item => {
                                return item
                            }).join(', ')}
                            </p>
                    }


                    {
                        this.state.modalData.type === 'team' ?
                            <p style={{fontSize: "16px", color: 'black'}}><b>
                                {t('about')}:</b> {this.state.modalData && this.state.modalData.about}
                            </p>
                            :
                            <p style={{fontSize: "16px", color: 'black'}}>
                                <b>{t('about_me')}:</b> {this.state.modalData && this.state.modalData.about}
                            </p>
                    }


                </Modal>


                <PageHeaderAlt className=" mb-5" overlap>
                    <div className="container text-center">
                        <div className="py-lg-4">
                            <h1 className="text-primary display-4">{t('title')}</h1>
                        </div>
                    </div>
                </PageHeaderAlt>

                <Col xs={24} sm={24} md={24} lg={{span: 24}} style={{display: "flex", justifyContent: "center"}}>
                    <Radio.Group value={this.state.screen}
                                 className={"team_screen_select"} buttonStyle="solid"
                                 size="large">
                        {this.props.team.name ? null :
                            <Radio.Button onClick={this.setScreen} className={"team_screen_select-button"}
                                          value="find_team">{t('join')}</Radio.Button>
                        }
                        {this.props.team.name ?
                            <Radio.Button onClick={this.setScreen} className={"team_screen_select-button"}
                                          value="my_team">{t('my_team')}</Radio.Button> : null}
                        {this.props.team.name ? null :
                            <Radio.Button onClick={this.setScreen} className={"team_screen_select-button"}
                                          value="create_team">{t('create_team')}</Radio.Button>
                        }

                    </Radio.Group>
                </Col>

                <div className="code-box" style={{marginTop: "25px"}}>
                    <div className="container my-4">

                        {this.state.screen === 'my_team' ?
                            <Form size={"large"} layout="vertical" onFinish={this.leaveTeam} ref={this.formRef}>
                                <Col xs={24} sm={24} md={24} lg={{span: 14, offset: 5}}>
                                    <Title style={{textAlign: "center"}} level={2}>{t('team_name')}:</Title>
                                    <Title style={{textAlign: "center"}} level={3}>
                                        <b>{this.props.team.name}</b>
                                    </Title>

                                    <Paragraph style={{fontSize: '16px'}}>
                                        {t('join_for_team')}:
                                        <ul>
                                            <li>{t('enter_code')}: <b>{this.props.team.secret_code}</b></li>
                                            <li>{t('follow_link')}: <a
                                                href={`${MAIN_URL}/team?invite=${this.props.team.secret_code}`}>{MAIN_URL}/team?invite={this.props.team.secret_code}</a>
                                            </li>
                                        </ul>
                                        {t('remember_register')}.
                                    </Paragraph>
                                    <Divider/>
                                    <Title style={{textAlign: "center"}} level={3}>
                                        Над какой задачей вы работаете?
                                    </Title>

                                    <Select onSelect={this.onSelectTask} style={{width: "100%"}}
                                            defaultValue={this.props.team.task ? this.props.team.task : null}>
                                        {this.state.tasksList && this.state.tasksList.map((item) => {
                                            return <Select.Option value={item.id}
                                                                  key={item.id}>{item.name}</Select.Option>
                                        })}
                                    </Select>
                                    <Divider/>
                                      <Title  style={{textAlign: "center"}} level={3}>
                                        План работ (ссылка на Google Document)
                                    </Title>
                                    <Input defaultValue={this.props.team.report} onChange={this.sendReport}/>

                                    <Divider/>
                                    <Paragraph style={{fontSize: '16px'}}>
                                        <Title style={{textAlign: "center"}} level={2}>{t('line_up')}: </Title>
                                        {this.props.team.teammate.map(item => {
                                            return <Title key={item.id} style={{textAlign: "center"}}
                                                          level={4}>
                                                {this.props.team.capitan == item.id ?
                                                    <>
                                                        <b>{item.first_name} {item.second_name}</b> (Капитан)
                                                    </>
                                                    :
                                                    <>
                                                        <b>{item.first_name} {item.second_name}</b> ({item.type_developer})
                                                    </>
                                                }
                                            </Title>
                                        })}
                                    </Paragraph>
                                    <Divider/>
                                    <Form.Item name={"id"} initialValue={this.props.team.id} style={{display: 'none'}}>
                                        <Input name={"id"} type={'hidden'}/>
                                    </Form.Item>
                                    <Form.Item wrapperCol={{span: 24}}>
                                        <Button style={{width: "100%"}} className="mr-2" type="primary"
                                                onClick={() => this.leaveTeam}
                                                htmlType="submit">
                                            {t('leave_team')}
                                        </Button>
                                    </Form.Item>
                                    <Divider/>
                                    <Paragraph style={{fontSize: '16px'}}>{t('access_to_the_checkpoint')}
                                    </Paragraph>
                                </Col>
                            </Form> : null}

                        {this.state.screen === 'find_team' ?
                            <Form size={"large"} layout="vertical" onFinish={this.joinTeam} ref={this.formRef}>
                                <Col xs={24} sm={24} md={24} lg={{span: 14, offset: 5}}>
                                    <Title style={{textAlign: "center"}} level={3}>{t('join_team')}</Title>
                                    {t('join_team_secret_code')}<br/><br/>
                                    <Form.Item name={"invite"}>
                                        <Input placeholder={t('special_code')}/>
                                    </Form.Item>
                                    <Form.Item wrapperCol={{span: 24}}>
                                        <Button style={{width: "100%"}} className="mr-2" type="primary"
                                                onClick={() => this.joinTeam}
                                                htmlType="submit">
                                            {t('join_team')}
                                        </Button>
                                    </Form.Item>
                                </Col>
                            </Form> : null}

                        {this.state.screen === 'create_team' ?
                            <Form size={"large"} layout="vertical" onFinish={this.createTeam} ref={this.formRef}>
                                <Col xs={24} sm={24} md={24} lg={{span: 14, offset: 5}}>
                                    <Title style={{textAlign: "center"}} level={3}>{t('create_team')}</Title>
                                    {t('create_team_desc')}<br/><br/>
                                    <Form.Item name={"name"}>
                                        <Input placeholder={t('create_team_name')}/>
                                    </Form.Item>
                                    <Form.Item wrapperCol={{span: 24}}>
                                        <Button style={{width: "100%"}} className="mr-2" type="primary"
                                                onClick={() => this.createTeam}
                                                htmlType="submit">
                                            {t('create_team')}
                                        </Button>
                                    </Form.Item>
                                </Col>
                            </Form> : null}

                        {this.state.screen === 'search_team' ?
                            <Form size={"large"} layout="vertical" onFinish={this.searchStart} ref={this.formRef}>

                                {this.props.team.search.hackers || this.props.team.search.teams ?

                                    <Col xs={24} sm={24} md={24} lg={{span: 18, offset: 3}}>
                                        <Title style={{textAlign: "center"}} level={3}>{t('here_ycview')}</Title>
                                        <Paragraph style={{fontSize: '16px'}}>
                                            <ul>
                                                <li>
                                                    <Trans
                                                        i18nKey="team:team_search"
                                                        components={[<b/>,]}
                                                    />
                                                </li>
                                                <li>
                                                    <Trans
                                                        i18nKey="team:hacker_search"
                                                        components={[<b/>,]}
                                                    />
                                                </li>
                                            </ul>
                                            <Trans
                                                i18nKey="team:search_team_description"
                                                components={[<b/>,]}
                                                values={{count: this.props.team.search.available_of_requests}}
                                            />

                                            <br/>
                                            <br/>
                                            <Trans
                                                i18nKey="team:search_team_description_end"
                                                components={[<b/>,]}
                                            />
                                        </Paragraph>
                                        <Divider/>
                                        <div style={{
                                            display: "flex",
                                            justifyContent: "center",
                                            width: '100%',
                                            marginBottom: "24px"
                                        }}>
                                            <Radio.Group value={this.state.searchTeamScreen}
                                                         className={"team_screen_select"} buttonStyle="solid"
                                                         size="large">
                                                <Radio.Button onClick={this.setSearchTeamScreen}
                                                              className={"team_screen_select-button"}
                                                              value="teams">
                                                    {t('team')}
                                                </Radio.Button>
                                                <Radio.Button onClick={this.setSearchTeamScreen}
                                                              className={"team_screen_select-button"}
                                                              value="hackers">
                                                    {t('participants_without_team')}
                                                </Radio.Button>
                                            </Radio.Group>
                                        </div>

                                        <div style={{marginTop: "10px"}}>

                                            {this.state.searchTeamScreen === 'hackers' ?
                                                <Row gutter={16}>
                                                    {
                                                        this.props.team.search.hackers.length > 0 && this.props.team.search.hackers.map((item) => {
                                                            return <Col key={item.id} xs={24} sm={24} md={12} lg={8}>
                                                                <Card
                                                                    onClick={() => this.openModal(item)}
                                                                    hoverable
                                                                    style={{fontSize: "16px"}}
                                                                    bodyStyle={{height: "370px"}}
                                                                    actions={[
                                                                        <Button
                                                                            onClick={this.openModal}
                                                                            style={{width: "90%"}}>
                                                                            Подробнее
                                                                        </Button>
                                                                    ]}

                                                                    title={`${item.user.first_name} ${item.user.second_name}`}>
                                                                    <b>{t('my_role')}:</b> {item.role.split(',').map(item => {
                                                                    return item
                                                                }).join(', ')}
                                                                    <Divider/>
                                                                    <b>{t('tasks')}:</b> {item.selected_tracks.map(track => {
                                                                    return `${track.name}`
                                                                }).join(', ')}
                                                                    <Divider/>
                                                                    <b>{t('about_me')}:</b> {item.about && item.about.substring(0, 100)}...
                                                                </Card>
                                                            </Col>
                                                        })
                                                    }
                                                    {
                                                        this.props.team.search.hackers.length ? null :
                                                            <div style={{
                                                                width: "100%",
                                                                margin: "25px 0",
                                                                display: "flex",
                                                                justifyContent: "center",
                                                                fontSize: "16px"
                                                            }}>
                                                                <Typography style={{
                                                                    textAlign: "center",
                                                                }}>
                                                                    <FrownTwoTone style={{fontSize: "32px"}}/> <br/>
                                                                    {t('not_found_hackers')}
                                                                </Typography>
                                                            </div>
                                                    }
                                                </Row>
                                                : null}

                                            {this.state.searchTeamScreen === 'teams' ?
                                                <Row gutter={16}>
                                                    {
                                                        this.props.team.search.teams.length > 0 && this.props.team.search.teams.map((item) => {
                                                            return <Col key={item.id} xs={24} sm={24} md={12} lg={8}>
                                                                <Card
                                                                    onClick={() => this.openModal(item)}
                                                                    hoverable
                                                                    style={{fontSize: "16px"}}
                                                                    bodyStyle={{height: "370px"}}
                                                                    actions={[
                                                                        <Button
                                                                            style={{width: "90%"}}>
                                                                            Подробнее
                                                                        </Button>
                                                                    ]}
                                                                    title={`${item.team.name}`}>
                                                                    <b>{t('looking_for')}:</b> {item.role.split(',').map(item => {
                                                                    return item
                                                                }).join(', ')}
                                                                    <Divider/>
                                                                    <b>{t('tasks')}:</b> {item.selected_tracks.map(track => {
                                                                    return `${track.name}`
                                                                }).join(', ')}
                                                                    <Divider/>
                                                                    <b>{t('about')}:</b> {item.about && item.about.substring(0, 100)}...
                                                                </Card>
                                                            </Col>
                                                        })
                                                    }
                                                    {
                                                        this.props.team.search.teams.length ? null :
                                                            <div style={{
                                                                width: "100%",
                                                                margin: "25px 0",
                                                                display: "flex",
                                                                justifyContent: "center",
                                                                fontSize: "16px"
                                                            }}>
                                                                <Typography style={{
                                                                    textAlign: "center",
                                                                }}>
                                                                    <FrownTwoTone style={{fontSize: "32px"}}/> <br/>
                                                                    {t('not_found_team')}
                                                                </Typography>
                                                            </div>
                                                    }
                                                </Row>
                                                : null}

                                        </div>
                                        <Divider/>
                                        <div style={{display: "flex", justifyContent: "center"}}>
                                            <Button
                                                onClick={this.leaveSearch}
                                                icon={<LogoutOutlined/>}>
                                                {t('end_search')}
                                            </Button>
                                        </div>
                                    </Col>
                                    :
                                    <Col xs={24} sm={24} md={24} lg={{span: 14, offset: 5}}>
                                        <Alert
                                            message={t('info_me')}
                                            icon={<NotificationTwoTone style={{fontSize: "32px"}}/>}
                                            type="info"
                                            style={{marginBottom: "15px", fontSize: "16px"}}
                                            showIcon
                                        />

                                        {this.props.team.name ?
                                            <Title style={{textAlign: "center"}}
                                                   level={3}>{t('info_about_team')}</Title>
                                            :
                                            <Title style={{textAlign: "center"}} level={3}>{t('info_about_you')}</Title>
                                        }

                                        <Form.Item name={"contact"}
                                                   rules={[{required: true, message: t('contact__error')}]}
                                                   initialValue={this.props.team.search?.my ? this.props.team.search?.my.contact : ''}
                                                   help={t('contact__help')}
                                                   label={this.props.team.name ? t('contact__help') : t('contact__hacker')}

                                        >

                                            <Input placeholder={t('contact__hacker')}/>
                                        </Form.Item>
                                        <Form.Item name={"role"}
                                                   rules={[{
                                                       required: true,
                                                       message: t('role__error'),
                                                   }]}
                                                   initialValue={this.props.team.search?.my ? this.props.team.search?.my.role.split(',') : []}
                                                   label={this.props.team.name ? t('role__team') : t('role__hacker')}>
                                            <Select
                                                mode="multiple"
                                                placeholder={t('role__placeholder')}
                                                value={selectedItems}
                                                notFoundContent={t('role__notfound')}
                                                maxLength={3}
                                                onChange={this.handleChangeTasks}
                                                style={{width: '100%'}}
                                            >
                                                {this.state.selectedItems.length < 3 && filteredOptions.map(item => (
                                                    <Select.Option key={item} value={item}>
                                                        {item}
                                                    </Select.Option>
                                                ))}
                                            </Select>
                                        </Form.Item>
                                        <Form.Item
                                            name={"selected_tracks"}
                                            rules={[{required: true, message: t('selected_tracks__error')}]}
                                            label={t('selected_tracks')}
                                            initialValue={this.props.team.search?.my ? this.props.team.search?.my?.selected_tracks.map((item) => {
                                                return item.id
                                            }) : []}
                                        >
                                            <Select mode="multiple" style={{width: '100%'}}
                                                    tokenSeparators={[',']}
                                                    onChange={this.handleChangeSelectedTasks}
                                                    maxTagCount={3}>
                                                {
                                                    this.state.tasksList && this.state.selecting_tasks.length < 3 && this.state.tasksList.map(item => {
                                                        return <Option key={item.name}
                                                                       value={item.id}>{item.name}</Option>
                                                    })
                                                }
                                                {
                                                    this.state.selecting_tasks.length >= 3 ? this.state.tasksList.filter(item => this.state.selecting_tasks.includes(item.id)).map(item => {
                                                        return <Option key={item.name}
                                                                       value={item.id}>{item.name}</Option>
                                                    }) : null
                                                }
                                            </Select>
                                        </Form.Item>


                                        <Form.Item name={"about"}
                                                   label={this.props.team.name ? t('about_team') : t('about_hacker')}
                                                   initialValue={this.props.team.search.my ? this.props.team.search.my.about : ''}
                                                   help={t('about_help')}
                                        >
                                            <TextArea
                                                placeholder={this.props.team.name ? "Расскажи о команде" : "Расскажи о себе"}/>
                                        </Form.Item>
                                        <Form.Item wrapperCol={{span: 24}}>
                                            <Button style={{width: "100%"}} className="mr-2" type="primary"
                                                    onClick={() => this.searchStart}
                                                    htmlType="submit">
                                                {t('common:send')}
                                            </Button>
                                        </Form.Item>
                                    </Col>

                                }


                            </Form> : null}

                    </div>
                </div>


            </React.Fragment>
        )
    }

}


const mapDispatchToProps = (dispatch) => {
    return {actionTeam: bindActionCreators(TeamAction, dispatch),};
};

export default withTranslation(withExpiredToken(withAuthAndPrivateHackerRoute(connect((state => ({
    auth: state.auth,
    team: state.team
})), mapDispatchToProps)(team))), "team")

