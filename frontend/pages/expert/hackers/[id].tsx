import React, {Fragment} from 'react';
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {Hacker} from '../../../redux/interface/hacker';
import {AdminAction} from '../../../redux/actions/admin.action'
import {ExpertAction} from '../../../redux/actions/expert.action'
import withAuthPrivateAdminRoute from "../../../route/withAuthPrivateAdminRoute";
import withExpiredToken from "../../../route/withExpiredToken";
import HackerHeader from '../../../components/layout/hacker/HackerHeader'
import TeamCard from '../../../components/layout/hacker/TeamCard';
import Education from '../../../components/layout/hacker/Education';
import Interest from '../../../components/layout/hacker/Interest';
import AboutHack from '../../../components/layout/hacker/AboutHack';
import AboutHacker from '../../../components/layout/hacker/AboutHacker';
import SocialHacker from '../../../components/layout/hacker/SocialHacker';
import PageHeaderAlt from '../../../components/layout-components/PageHeaderAlt';
import {Card, Col, Form, Row} from "antd";


import {
    GlobalOutlined,
    MailOutlined,
    HomeOutlined,
    PhoneOutlined
} from '@ant-design/icons';
import {FormInstance} from "antd/lib/form";
import withAuthPrivateExpertRoute from "../../../route/withAuthPrivateExpertRoute";

const mapStateToProps = (state) => {
    return {
        active: state.admin.hackers.active,
    };
};


interface Props {
    active: Hacker,
    id: number,
    actionAdmin: any,
    auth: any
    theme: any,
}

class hacker extends React.Component<Props> {
    formRef = React.createRef<FormInstance>();

    static async getInitialProps(ctx) {
        const id = ctx.query.id;
        await ctx.store.dispatch(ExpertAction.getHacker(ctx.auth.access_token, id));
        const {active} = mapStateToProps(ctx.store.getState());
        return {active, id}
    }

    render() {
        const {active, id} = this.props;
        const not_full_screen = active.team.length || active?.github || active?.gitlab || active?.linkedin || active?.hh || active?.site
        return (
            <>
                <PageHeaderAlt background="/img/others/hacker-image.jpg" cssClass="bg-primary" overlap>
                    <div className="container text-center">
                        <div className="py-5 my-md-5">
                        </div>
                    </div>
                </PageHeaderAlt>
                <div className="container my-4">
                    <HackerHeader hacker={active} auth={this.props.auth}/>
                    <Row gutter={16}>
                        <Col xs={24} sm={24} md={6}>
                            {active.team.length ? <TeamCard prefix_url={"expert"} team={active?.team} hacker_id={active.id}/> : null}
                            {
                                active?.github || active?.gitlab || active?.linkedin || active?.hh || active?.site ?
                                    <SocialHacker hacker={active} theme={this.props.theme.currentTheme}/>
                                    :
                                    null
                            }
                        </Col>
                        <Col xs={24} sm={24} md={not_full_screen ? 18 : 24}>

                            <AboutHacker hacker={active}/>
                            {
                                active?.institute || active?.employment_form || active?.seniority ?
                                    <Education hacker={active}/>
                                    :
                                    null
                            }
                            {
                                active?.resume_text || active?.resume_file || active.skills?.length ?
                                    <Interest hacker={active}/>
                                    : null
                            }
                            {
                                active?.want_take_part || active?.expect_from_hack || active?.where_did_you_know || active?.something_else ?
                                    <AboutHack hacker={active}/>
                                    : null
                            }
                        </Col>
                    </Row>

                </div>
            </>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        expertAdmin: bindActionCreators(ExpertAction, dispatch),
    };
};

export default withAuthPrivateExpertRoute(withExpiredToken(connect((state => (
    {
        auth: state.auth,
        hacker: state.expert.hacker.active,
        theme: state.theme,

    })), mapDispatchToProps)(hacker)))

