import React, {useState} from 'react';
import Link from 'next/link';
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {Input, Button, Table, Tag, Tooltip} from "antd";
import {Breakpoint} from 'antd/lib/_util/responsiveObserve';
import Highlighter from 'react-highlight-words';
import {SearchOutlined} from '@ant-design/icons';
import PageHeaderAlt from '../../../components/layout-components/PageHeaderAlt';
import withExpiredToken from '../../../route/withExpiredToken';
import {ExpertAction} from '../../../redux/actions/expert.action'
import AvatarStatus from '../../../components/shared-components/AvatarStatus';
import {EyeOutlined, SwapRightOutlined} from '@ant-design/icons';
import DrawerInfoUser from '../../../components/layout/hacker/DrawerInfoUser';
import {Select, Form} from 'antd';
import param from 'jquery-param';
import withAuthPrivateExpertRoute from "../../../route/withAuthPrivateExpertRoute";

export const index = (props) => {

    const [showDrawer, setShowDrawer] = useState(false)
    const [searchText, setSearchText] = useState('')
    const [searchedColumn, setSearchedColumn] = useState('')

    const getUserActiveInfo = (hacker) => {
        props.expertAction.getHacker(props.auth.access_token, hacker.id).then().catch((e) => {
                console.log(e)
            }
        )
    }
    const responsive: Breakpoint[] = ['sm']

    const handleTableChange = (page) => {
        props.expertAction.hackersListNextPage(props.auth.access_token, page).then().catch((e) => {
                console.log(e)
            }
        )
    }


    const handleSearch = (selectedKeys, confirm, dataIndex) => {
        confirm();
        setSearchText(selectedKeys[0]);
        setSearchedColumn(dataIndex);
    };

    const handleReset = clearFilters => {
        clearFilters();
        setSearchText('');
    };


    const getColumnSearchProps = dataIndex => ({
        filterDropdown: ({setSelectedKeys, selectedKeys, confirm, clearFilters}) => (
            <div style={{padding: 8}}>
                <Input

                    placeholder={`Поиск ${dataIndex}`}
                    value={selectedKeys[0]}
                    onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
                    onPressEnter={() => handleSearch(selectedKeys, confirm, dataIndex)}
                    style={{width: 188, marginBottom: 8, display: 'block'}}
                />
                <Button
                    type="primary"
                    onClick={() => handleSearch(selectedKeys, confirm, dataIndex)}
                    icon={<SearchOutlined/>}
                    size="small"
                    style={{width: 90, marginRight: 8}}
                >
                    Поиск
                </Button>
                <Button onClick={() => handleReset(clearFilters)} size="small" style={{width: 90}}>
                    Сбросить
                </Button>
            </div>
        ),
        filterIcon: filtered => <SearchOutlined style={{color: filtered ? '#1890ff' : undefined}}/>,
        onFilter: (value, record) =>
            record[dataIndex]
                ? record[dataIndex].toString().toLowerCase().includes(value.toLowerCase())
                : '',
        render: text =>
            searchedColumn === dataIndex ? (
                <Highlighter
                    highlightStyle={{backgroundColor: '#ffc069', padding: 0}}
                    searchWords={[searchText]}
                    autoEscape
                    textToHighlight={text ? text.toString() : ''}
                />
            ) : (
                text
            ),
    });

    const columns = [
        {
            title: 'ФИО',
            dataIndex: 'full_name',
            key: 'full_name',
            render: (_, record) => (
                <div className="d-flex">
                    <Link href={{
                        pathname: '/expert/hackers/[id]',
                        query: {id: record.id},
                    }}>
                        <a>
                            {JSON.stringify(record)}
                            <AvatarStatus text={record?.first_name ? record?.first_name[0] : ''}
                                          name={record?.first_name ? record?.first_name : ''}
                                          subTitle={record.email}/>
                        </a>
                    </Link>
                </div>
            ),
            ...getColumnSearchProps('full_name'),
        }, {
            title: '🤌 Роль',
            dataIndex: 'type_developer',
            key: 'type_developer',
            width: "10%",
            responsive: responsive,
            sorter: (a, b) => {
                return a.type_developer - b.type_developer
            },
        }, {
            title: '📡 Телеграмм',
            dataIndex: 'telegram_link',
            key: 'telegram_link',
            responsive: responsive,
        }, {
            title: '🎓 Институт',
            dataIndex: 'institute',
            key: 'institute',
            responsive: responsive,
            render: (_, record) => (
                <span>
                    {record.institute ? record.institute : "Не учится"}
                 </span>
            ),
            sorter: (a, b) => a.institute.localeCompare(b.institute),
            ...getColumnSearchProps('institute'),
        }, {
            title: '',
            dataIndex: 'actions',
            key: 'actions',
            render: (_, record) => (
                <div className="text-right">
                    <Link href={{
                        pathname: '/expert/hackers/[id]',
                        query: {id: record.id},
                    }}>
                        <a>
                            <Tooltip title="Полный просмотр анкеты">
                                <Button type="primary" className="mr-2 mt-2" icon={<EyeOutlined/>} size="small"/>
                            </Tooltip>

                        </a>
                    </Link>
                    <Tooltip title="Короткий просмотр">
                        <Button type="primary" className="mr-2 mt-2" onClick={() => {
                            getUserActiveInfo(record)
                            setShowDrawer(!showDrawer)
                        }} icon={<SwapRightOutlined/>} size="small"/>
                    </Tooltip>
                </div>
            )
        }
    ]

    function filter(form) {
        console.log(form)
        props.expertAction.hackersListFilter(props.auth.access_token, param(form)).then().catch((e) => {
                console.log(e)
            }
        )
    }

    const [form] = Form.useForm();

    return (
        <React.Fragment>
            <PageHeaderAlt className="bg-primary mb-5" overlap>
                <div className="container text-center">
                    <div className="py-lg-4">
                        <h1 className="text-white display-4">🐱‍💻 Hackers</h1>
                    </div>
                </div>
            </PageHeaderAlt>


            <div className="code-box">
                <Table columns={columns}
                       rowKey='id'
                       dataSource={props.hackers.list.results}
                       pagination={{position: ["bottomCenter"]}}
                />

            </div>
            <DrawerInfoUser data={props.hackers.active} visible={showDrawer} close={() => setShowDrawer(!showDrawer)}/>
        </React.Fragment>

    )
}

index.getInitialProps = async (ctx) => {
    await ctx.store.dispatch(ExpertAction.hackersList(ctx.auth.access_token))
    const {admin} = mapStateToProps(ctx.store.getState())
    return {admin}
}

const mapStateToProps = (state) => {
        return {
            admin: state.admin,
        };
    }
;

const mapDispatchToProps = (dispatch) => {
        return {
            expertAction: bindActionCreators(ExpertAction, dispatch),
        };
    }
;

export default withAuthPrivateExpertRoute(withExpiredToken(connect((state => (
    {
        auth: state.auth,
        hackers: state.admin.hackers
    })), mapDispatchToProps)(index)))

