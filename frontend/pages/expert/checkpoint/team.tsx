import {ExpertAction} from "../../../redux/actions/expert.action";
import {bindActionCreators} from "redux";
import withAuthPrivateExpertRoute from "../../../route/withAuthPrivateExpertRoute";
import withExpiredToken from "../../../route/withExpiredToken";
import PageHeaderAlt from "../../../components/layout-components/PageHeaderAlt";
import React from "react";
import {connect} from "react-redux";
import {Col, Card, Typography, Form, Button, Row, Input, notification} from 'antd'
import team from "../../team";
import {useRouter} from 'next/router'

const {Title} = Typography;
const {TextArea} = Input;
export const index = (props) => {
    const [form] = Form.useForm();
    const router = useRouter()

    const sendCheckpoint = (form) => {
        form['team'] = props.team.id
        form['checkpoint'] = props.check

        props.expertAction.checkpointResponseSend(props.auth.access_token, form).then((e) => {
            notification.success({
                message: "Ваш отзыв отправлен",
                duration: 2.3,
            })
            setTimeout(() => {
                router.push('/expert/checkpoint')
            })
        })
    }
    return (
        <div>
            <PageHeaderAlt className="bg-primary mb-5" overlap>
                <div className="container text-center">
                    <div className="py-lg-4">
                        <h1 className="text-white display-4">☑️ Check Point: {props.team.name}</h1>
                    </div>
                </div>
            </PageHeaderAlt>

            <div className={"container my-4"}>
                <Form layout="vertical" form={form} onFinish={sendCheckpoint}>
                    <Card>
                        <Row gutter={16}>
                            <Col xs={24} sm={24} lg={12} xl={16} offset={4} xxl={16}>
                                <Form.Item name={"good"} rules={[{required: true}]} label={"Что хорошо?"}>
                                    <TextArea rows={4}/>
                                </Form.Item>

                                <Form.Item name={"better"} rules={[{required: true}]} label={"Что улучшить?"}>
                                    <TextArea rows={4}/>
                                </Form.Item>
                                <Form.Item name={"comment"} rules={[{required: true}]} label={"Комментарий"}>
                                    <TextArea rows={4}/>
                                </Form.Item>

                                <Form.Item>
                                    <Button type="primary" style={{width: "100%"}} htmlType="submit">
                                        Отправить
                                    </Button>
                                </Form.Item>
                            </Col>
                        </Row>
                    </Card>
                </Form>
            </div>
        </div>
    )
}

index.getInitialProps = async (ctx) => {
    const {check, team_id} = ctx.query;
    await ctx.store.dispatch(ExpertAction.getTeam(ctx.auth.access_token, team_id))
    const {team} = mapStateToProps(ctx.store.getState())
    return {check, team}
}

const mapStateToProps = (state) => {
    return {
        team: state.expert.team.active
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        expertAction: bindActionCreators(ExpertAction, dispatch),
    };
};

export default withAuthPrivateExpertRoute(withExpiredToken(connect((state => (
    {
        auth: state.auth,
        checkpoints: state.expert.checkpoints,
        teams: state.expert.team.list,
    })), mapDispatchToProps)(index)))

