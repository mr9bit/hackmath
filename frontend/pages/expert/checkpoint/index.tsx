import {ExpertAction} from "../../../redux/actions/expert.action";
import {bindActionCreators} from "redux";
import withAuthPrivateExpertRoute from "../../../route/withAuthPrivateExpertRoute";
import withExpiredToken from "../../../route/withExpiredToken";
import PageHeaderAlt from "../../../components/layout-components/PageHeaderAlt";
import React from "react";
import {connect} from "react-redux";
import {Col, Card, Typography, Row, Button} from 'antd'
import Link from "next/link";
import {Table, Tag, Space} from 'antd';

const {Title} = Typography;

import {Collapse} from 'antd';
import {getColumns} from "../../../components/Table/Expert/CheckPoint";

const {Panel} = Collapse;
export const index = (props) => {
    const myResponse = props.my.map(item => item.team)


    return (
        <div>
            <PageHeaderAlt className="bg-primary mb-5" overlap>
                <div className="container text-center">
                    <div className="py-lg-4">
                        <h1 className="text-white display-4">☑️ Check Point</h1>
                    </div>
                </div>
            </PageHeaderAlt>

            <div className={"container my-4"}>
                <Card>
                    <Collapse defaultActiveKey={[props.checkpoints.results[props.checkpoints.results.length - 1].id]}>
                        {
                            props.checkpoints.results.map(check => {
                                return <Panel key={check.id}
                                              header={<Title style={{textAlign: "center"}}
                                                             level={2}>{check.name}</Title>}
                                >
                                    <Table columns={getColumns(props.my, check)} dataSource={props.teams.results}
                                           pagination={false}/>
                                </Panel>
                            })
                        }
                    </Collapse>
                </Card>
            </div>
        </div>
    )
}

index.getInitialProps = async (ctx) => {
    await ctx.store.dispatch(ExpertAction.getTeamList(ctx.auth.access_token))
    await ctx.store.dispatch(ExpertAction.checkpointList(ctx.auth.access_token))
    await ctx.store.dispatch(ExpertAction.myCheckpointList(ctx.auth.access_token))
    const {checkpoints, my, teams} = mapStateToProps(ctx.store.getState())
    return {checkpoints, my, teams}
}

const mapStateToProps = (state) => {
    return {
        checkpoints: state.expert.checkpoints,
        my: state.expert.my,
        teams: state.expert.team.list
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        expertAction: bindActionCreators(ExpertAction, dispatch),
    };
};

export default withAuthPrivateExpertRoute(withExpiredToken(connect((state => (
    {
        auth: state.auth,
        checkpoints: state.expert.checkpoints,
        teams: state.expert.team.list,
    })), mapDispatchToProps)(index)))

