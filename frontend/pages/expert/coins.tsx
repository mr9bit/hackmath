import React from 'react';
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {
    Input, Form, Button, Radio, Select, AutoComplete, Spin,
    DatePicker, Typography, Col, notification, Slider, Result
} from 'antd';
import {FormInstance} from 'antd/lib/form';
import PageHeaderAlt from '../../components/layout-components/PageHeaderAlt';
import CountryPhoneInput from '../../components/util-components/PhoneInput';
import {AuthAction} from '../../redux/actions/auth.action';
import {ExpertAction} from '../../redux/actions/expert.action';
import {ExpertProfile, User} from '../../redux/interface/auth';
import withAuthPrivateExpertRoute from '../../route/withAuthPrivateExpertRoute';
import withExpiredToken from '../../route/withExpiredToken';
import {URl_BACKEND} from '../../redux/settings'
import locale from 'antd/lib/date-picker/locale/ru_RU'
import moment from "moment";
import Link from 'next/link'

const {TextArea} = Input;

interface Props {
    lastFetchId: number,
    actionAuth: any,
    user: User,
    profile: ExpertProfile,
    access_token: string,
    i18n: any
    teams: any
}


const mapStateToProps = (state) => {
    return {
        teams: state.expert.coins.teams,
    };
};
type AutoComplete = {
    value: string
}
type TaskComplete = {
    name: string;
    id: number;
}


interface State {
    fetchingCountry: boolean,
    autoCompleteCityData: AutoComplete[],
    telegramPaste: boolean,
    tasksList: TaskComplete[],
    isSend: boolean,
    isFetching: boolean,
}

import withTranslation from 'next-translate/withTranslation'
import {checkHttpStatus} from "../../redux/utils";

class questionary extends React.Component<Props, State> {
    formRef = React.createRef<FormInstance>();

    static async getInitialProps(ctx) {
        await ctx.store.dispatch(ExpertAction.getTeamListForCoins(ctx.auth.access_token))
        const {teams} = mapStateToProps(ctx.store.getState())
        return {teams}
    }

    constructor(props: Props) {
        super(props);
        this.clear = this.clear.bind(this)
        this.onFinish = this.onFinish.bind(this)
        this.state = {
            ...this.props.profile,
            fetchingCountry: false,
            autoCompleteCityData: [],
            telegramPaste: false,
            tasksList: [],
            isSend: false,
            isFetching: false,
        }
    }

    clear() {
        this.setState({
            isSend: false,
            isFetching: false
        })
    }

    onFinish(form) {
        this.setState({
            isFetching: true,
            isSend: false
        })
        fetch(`${URl_BACKEND}/api/expert/coins/`, {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${this.props.access_token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(form)
            }
        ).then(checkHttpStatus)
            .then((response) => response.json())
            .then((response) => {
                this.setState({
                    isFetching: false,
                    isSend: true
                })
                return response;
            })
    }

    render() {
        console.log(this.props)
        const {t, lang} = this.props.i18n;
        return (

            <React.Fragment>

                <PageHeaderAlt className=" mb-5" overlap>
                    <div className="container text-center">
                        <div className="py-lg-4">
                            <h1 className="text-primary display-4">ITCoin</h1>
                        </div>
                    </div>
                </PageHeaderAlt>
                <Form size={"large"} onFinish={this.onFinish} layout="vertical" ref={this.formRef}
                >
                    <div className="code-box">
                        <div className="container my-4">
                            <Col xs={24} sm={24} md={24} lg={{span: 14, offset: 5}}>
                                {
                                    this.state.isFetching ?
                                        <div style={{display: "flex", justifyContent: "center"}}><Spin/></div> :
                                        this.state.isSend ? <Result
                                                status="success"
                                                title="ITCoin отправлены"
                                                extra={[
                                                    <Button onClick={this.clear} type="primary" key="console">
                                                        Отправить еще
                                                    </Button>
                                                    ,
                                                ]}
                                            /> :
                                            <>
                                                <Form.Item label="Команда" name={"team"}>
                                                    <Select>
                                                        {
                                                            this.props.teams.map((item) => {
                                                                return <Select.Option key={item.id}
                                                                                      value={item.id}>{item.name}</Select.Option>
                                                            })
                                                        }
                                                    </Select>
                                                </Form.Item>
                                                <Form.Item label="Баллы" name={"count"}>
                                                    <Slider defaultValue={0} min={-30} max={30} tooltipVisible/>
                                                </Form.Item>
                                                <Form.Item name={"comment"} label="Комментарий"
                                                           help={"За что вручаем баллы"}>
                                                    <TextArea rows={4}/>
                                                </Form.Item>
                                                <Button type={"primary"} htmlType={"submit"} style={{width: "100%"}}>
                                                    Отправить
                                                </Button>
                                            </>
                                }

                            </Col>

                        </div>
                    </div>
                </Form>


            </React.Fragment>

        )
    }
}


const mapDispatchToProps = (dispatch) => {
    return {
        actionAuth: bindActionCreators(AuthAction, dispatch),
    };
};
export default withTranslation(withExpiredToken(withAuthPrivateExpertRoute(connect((state => (state.auth)), mapDispatchToProps)(questionary))), "questionary");

