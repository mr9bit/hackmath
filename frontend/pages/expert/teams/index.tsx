import React from 'react';
import Link from 'next/link';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {EyeOutlined} from '@ant-design/icons';
import {Pagination, Table, Tooltip, Button} from "antd";
import {Breakpoint} from 'antd/lib/_util/responsiveObserve';
// Компоненты
import PageHeaderAlt from '../../../components/layout-components/PageHeaderAlt'
import Flex from '../../../components/shared-components/Flex'
// Роутинг
import withAuthPrivateAdminRoute from '../../../route/withAuthPrivateAdminRoute';
import withExpiredToken from '../../../route/withExpiredToken';
// Дейсвтия
import {ExpertAction} from '../../../redux/actions/expert.action'
import withAuthPrivateExpertRoute from "../../../route/withAuthPrivateExpertRoute";


export const index = (props) => {

    const handleTableChange = (page) => {
        props.expertAction.teamNextPage(props.auth.access_token, page).then().catch((e) => {
                console.log(e)
            }
        )
    }


    const responsive: Breakpoint[] = ['sm']
    const columns = [
        {
            title: 'Название',
            dataIndex: 'full_name',
            key: 'full_name',
            render: (_, record) => (
                <div className="d-flex">
                    <Link href={{
                        pathname: '/expert/teams/[id]',
                        query: {id: record.id},
                    }}>
                        <a>
                            {record.name}
                        </a>
                    </Link>
                </div>
            )
        },
        {
            title: 'Количество участников',
            dataIndex: 'teammate',
            key: 'teammate',
            responsive: responsive,
            sorter: (a, b) => {
                return a?.teammate?.length - b?.teammate?.length
            },
            render: (_, record) => (
                <span>
                    {record.teammate.length}
                </span>
            )

        },
        {
            title: '📡 Капитан',
            dataIndex: 'capitan',
            key: 'capitan',
            responsive: responsive,
            render: (_, item) => (
                <Link href={{
                    pathname: '/expert/hackers/[id]',
                    query: {id: item.capitan.id},
                }}>
                    <a>
                            <span>
                                {item.capitan.first_name} {item.capitan.second_name}
                            </span>

                    </a>
                </Link>
            )
        },
        {
            title: 'Институт',
            dataIndex: 'institute',
            key: 'institute',
            render: (_, item) => (
                <span>
                                {item.capitan.institute}
                            </span>
            ),
            sorter: (a, b) => {
                return a.capitan?.institute?.length - b.capitan?.institute?.length
            },
        },
        {
            title: '',
            dataIndex: 'actions',
            key: 'actions',
            render: (_, record) => (
                <div className="text-right">
                    <Link href={{
                        pathname: '/expert/teams/[id]',
                        query: {id: record.id},
                    }}>
                        <a>
                            <Tooltip title="Полный просмотр команды">
                                <Button type="primary" className="mr-2 mt-2" icon={<EyeOutlined/>} size="small"/>
                            </Tooltip>
                        </a>
                    </Link>

                </div>
            )
        }
    ]


    return (
        <React.Fragment>
            <PageHeaderAlt className="bg-primary mb-5" overlap>
                <div className="container text-center">
                    <div className="py-lg-4">
                        <h1 className="text-white display-4">👨‍👦‍👦 Команды</h1>
                    </div>
                </div>
            </PageHeaderAlt>
            {/*<Card>*/}
            {/*    <Flex alignItems="center" justifyContent="between" mobileFlex={false}>*/}
            {/*        <Flex className="mb-1" mobileFlex={false}>*/}
            {/*            <div className="mr-md-3 mb-3">*/}
            {/*                <Input placeholder="Поиск по ФИО" prefix={<SearchOutlined/>}/>*/}
            {/*            </div>*/}
            {/*        </Flex>*/}
            {/*    </Flex>*/}
            {/*</Card>*/}
            <div className="code-box">
                <Table columns={columns}
                       rowKey='id'
                       dataSource={props.list.results}
                       pagination={{position: ["bottomCenter"]}}
                />
            </div>
        </React.Fragment>

    )
}

index.getInitialProps = async (ctx) => {
    await ctx.store.dispatch(ExpertAction.getTeamList(ctx.auth.access_token))
    const {list} = mapStateToProps(ctx.store.getState())
    return {list}
}

const mapStateToProps = (state) => {
    return {
        list: state.expert.team.list,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        expertAction: bindActionCreators(ExpertAction, dispatch),
    };
};

export default withAuthPrivateExpertRoute(withExpiredToken(connect((state => (
    {
        auth: state.auth,
        list: state.expert.team.list
    })), mapDispatchToProps)(index)))

