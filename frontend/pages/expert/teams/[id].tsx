import React from 'react';
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {ExpertAction} from '../../../redux/actions/expert.action'
import withAuthPrivateAdminRoute from "../../../route/withAuthPrivateAdminRoute";
import withExpiredToken from "../../../route/withExpiredToken";
import PageHeaderAlt from '../../../components/layout-components/PageHeaderAlt'
import TeamHeader from '../../../components/layout/team/TeamHeader'
import {Team} from "../../../redux/interface/team";
import {Col, Row, Card, Button} from "antd";
import AboutTeam from "../../../components/layout/team/AboutTeam";
import TeamMate from "../../../components/layout/team/TeamMate";
import withAuthPrivateExpertRoute from "../../../route/withAuthPrivateExpertRoute";

const mapStateToProps = (state) => {
    return {
        active: state.expert.team.active,
    };
};


interface Props {
    active: Team,
    id: number,
    expertAction: any,
}

class team extends React.Component<Props> {
    static async getInitialProps(ctx) {
        const id = ctx.query.id;
        await ctx.store.dispatch(ExpertAction.getTeam(ctx.auth.access_token, id));
        const {active} = mapStateToProps(ctx.store.getState());
        console.log("active", active)
        return {active, id}
    }

    render() {
        const {active, id} = this.props;
        return (
            <>
                <PageHeaderAlt background="/img/others/team-image.jpg" cssClass="bg-primary" overlap>
                    <div className="container text-center">
                        <div className="py-5 my-md-5">
                        </div>
                    </div>
                </PageHeaderAlt>
                <div className="container my-4">

                    <TeamHeader team={active}/>
                    <Row gutter={16}>

                        <Col xs={24}>

                            <Card>
                                План работы: {
                                active.report ? <a href={active.report}> <Button> Просмотр</Button> </a> : <>Не загружен</>
                            }

                            </Card>
                        </Col>


                        <Col xs={24} sm={24} md={24}>
                            <AboutTeam team={active}/>
                        </Col>
                        <Col xs={24} sm={24} md={24}>
                            <TeamMate prefix={"hacker"} team={active}/>
                        </Col>

                    </Row>
                </div>
            </>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        expertAction: bindActionCreators(ExpertAction, dispatch),
    };
};

export default withAuthPrivateExpertRoute(withExpiredToken(connect((state => (
    {
        auth: state.auth,
        active: state.expert.team.active,
    })), mapDispatchToProps)(team)))

