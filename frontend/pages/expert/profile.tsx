import React from 'react';
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {
    Input, Form, Button, Radio, Select, AutoComplete, Spin,
    DatePicker, Typography, Col, notification
} from 'antd';
import {FormInstance} from 'antd/lib/form';
import PageHeaderAlt from '../../components/layout-components/PageHeaderAlt';
import CountryPhoneInput from '../../components/util-components/PhoneInput';
import {AuthAction} from '../../redux/actions/auth.action';
import {ExpertProfile, User} from '../../redux/interface/auth';
import withAuthPrivateExpertRoute from '../../route/withAuthPrivateExpertRoute';
import withExpiredToken from '../../route/withExpiredToken';
import {URl_BACKEND} from '../../redux/settings'
import locale from 'antd/lib/date-picker/locale/ru_RU'
import moment from "moment";

const {Title} = Typography;
const {Option} = Select;

interface Props {
    lastFetchId: number,
    actionAuth: any,
    user: User,
    profile: ExpertProfile,
    access_token: string,
    i18n: any
}


const mapStateToProps = (state) => {
    return {
        profile: state.auth.profile,
    };
};
type AutoComplete = {
    value: string
}
type TaskComplete = {
    name: string;
    id: number;
}


interface State extends ExpertProfile {
    fetchingCountry: boolean,
    autoCompleteCityData: AutoComplete[],
    telegramPaste: boolean,
    tasksList: TaskComplete[],
}

import withTranslation from 'next-translate/withTranslation'

class questionary extends React.Component<Props, State> {
    formRef = React.createRef<FormInstance>();

    static async getInitialProps(ctx) {
        await ctx.store.dispatch(AuthAction.getProfile(ctx.auth.access_token))
        const {profile} = mapStateToProps(ctx.store.getState())
        return {profile}
    }

    constructor(props: Props) {
        super(props);
        this.onSubmit = this.onSubmit.bind(this);
        this.onFailedSave = this.onFailedSave.bind(this);
        this.onAutoCompleteCountry = this.onAutoCompleteCountry.bind(this);
        this.handleTelegram = this.handleTelegram.bind(this);
        this.state = {
            ...this.props.profile,
            fetchingCountry: false,
            autoCompleteCityData: [],
            telegramPaste: false,
            tasksList: [],
        }
    }


    componentDidMount() {
        fetch(`${URl_BACKEND}/api/expert/tasks/`, {
            method: 'GET',
            headers: {
                'Authorization': `Bearer ${this.props.access_token}`
            },
        }).then((e) => e.json()).then((res) => {
            this.setState({tasksList: res.results})
        })
    }

    onSubmit = (form) => {
        const {t, lang} = this.props.i18n;
        if (form) {
            form.birth_date = moment(form.birth_date).format("YYYY-MM-DD")
            form.year_end = Number(moment(form.year_end).format("YYYY"))
            if (form.traks) {
                form.traks = form.traks.map(function (val) {
                    return val;
                }).join(',');
            }

            if (form.skills) {
                form.skills = form.skills.map(function (val) {
                    return val.value
                });
            }

            notification.info({
                message: t('common:saving'),
                duration: 1.2
            })

            this.props.actionAuth.updateExpertProfile(this.props.access_token, form).then(
                () => notification.success({
                    message: t('common:data_saved'),
                    duration: 2.3
                })
            ).catch((e) => {
                notification.error({
                    message: t('common:error_on_save'),
                    description: t('common:try_update_page_and_enter_data'),
                    duration: 2.3,
                })
            })
        } else {
            notification.info({
                message: `😶 Нечего сохранять`,
                description: 'Внесите данные',
                duration: 2.3,
            })
        }
    };


    onAutoCompleteCountry = (value) => {
        const searchUrl = `https://autocomplete.geocoder.ls.hereapi.com/6.2/suggest.json?apiKey=ej64jGu0EMqqTyVAhQuLZLT9e3x8cOvDYm3av-Llm3Y&resultType=city&query=${value}`
        fetch(searchUrl)
            .then(response => response.json())
            .then(response => {
                if (response.suggestions) {
                    const data = response.suggestions.map(user => ({
                        value: `${user.address.country}, ${user.address.city}`,
                    }));
                    this.setState({autoCompleteCityData: data, fetchingCountry: false});
                }

            });
    };


    handleChangeCity = value => {
        this.setState({
            city: value,
            autoCompleteCityData: [],
            fetchingCountry: false,
        });
    };


    handleTelegram = e => {
        e.preventDefault();
        const pasteValue = e.clipboardData.getData('Text');
        const rexexpUrl = new RegExp(/(https?:\/\/(.+?\.)?t\.me(\/[A-Za-z0-9\-\._~:\/\?#\[\]@!$&'\(\)\*\+,;\=]*)?)/g);
        if (rexexpUrl.test(pasteValue)) {
            let new_value = pasteValue.split('/');
            new_value = new_value[new_value.length - 1];
            this.formRef.current!.setFieldsValue({telegram_link: new_value});
        }
    }


    onFailedSave = ({values, errorFields, outOfDate}) => {
        errorFields.map((item) => {
            notification.error({
                message: item.errors[0],
                duration: 2,
            })
        })
    }


    render() {
        const {t, lang} = this.props.i18n;
        return (

            <React.Fragment>

                <PageHeaderAlt className=" mb-5" overlap>
                    <div className="container text-center">
                        <div className="py-lg-4">
                            <h1 className="text-primary display-4">{t("title")}</h1>
                        </div>
                    </div>
                </PageHeaderAlt>

                <div className={"m-4"}>
                    <Title style={{textAlign: "center"}} level={2}>Заполните анкету, что бы получить доступ к командам и
                        чекпоинтам</Title>
                </div>
                <Form size={"large"} onFinishFailed={this.onFailedSave} layout="vertical" ref={this.formRef}
                      onFinish={this.onSubmit}>
                    <div className="code-box">
                        <div className="container my-4">
                            <Title style={{textAlign: "center"}} className={"mb-4 text-primary"}
                                   level={1}>{t('main_info')}</Title>
                            <Col xs={24} sm={24} md={24} lg={{span: 14, offset: 5}}>
                                <Form.Item label="Email">
                                    <Input value={this.props.user.email} disabled={true}/>
                                </Form.Item>
                                <Form.Item name="first_name" label={t('first_name')}
                                           initialValue={this.state.first_name}
                                           rules={[{required: true, message: 'Введите свое имя'}]}>
                                    <Input/>
                                </Form.Item>
                                <Form.Item label={t('second_name')} initialValue={this.state.second_name}
                                           name="second_name"
                                           rules={[{required: true, message: 'Введите свою фамилию'}]}>
                                    <Input/>
                                </Form.Item>
                                <Form.Item label={t('middle_name')} name={"middle_name"}
                                           initialValue={this.state.middle_name}>
                                    <Input/>
                                </Form.Item>
                                <Form.Item label={t('gender')} name={"gender"}
                                           rules={[{required: true, message: t('gender')}]}
                                           initialValue={this.state.gender}>
                                    <Radio.Group>
                                        <Radio value={"М"}>{t('male')}</Radio>
                                        <Radio value={"Ж"}>{t('female')}</Radio>
                                    </Radio.Group>
                                </Form.Item>
                                <Form.Item label={t('date_of_birth')} name={"birth_date"}
                                           rules={[{required: true, message: t('date_of_birth__error')}]}
                                           initialValue={this.state.birth_date ? moment(this.state.birth_date) : moment(new Date(2001, 1, 1))}>
                                    <DatePicker style={{width: "100%"}}
                                                locale={locale}
                                                format={'DD.MM.YYYY'}
                                                placeholder={t('date_of_birth__placeholder')}/>
                                </Form.Item>

                                <Form.Item initialValue={this.state.city}
                                           name={"city"}
                                           rules={[{required: true, message: t('city__error')}]}
                                           label={"Из какого города вы участвуете?"}>
                                    <AutoComplete
                                        notFoundContent={this.state.fetchingCountry ?
                                            <Spin size="small"/> : null}
                                        filterOption={false}
                                        onSearch={this.onAutoCompleteCountry}
                                        onChange={this.handleChangeCity}
                                        options={this.state.autoCompleteCityData}
                                        style={{width: '100%'}}
                                    />
                                </Form.Item>
                                <Form.Item label="Telegram" name={"telegram_link"}
                                           rules={[{required: true, message: t('telegram_link_error')}]}
                                           initialValue={this.state.telegram_link}>
                                    <Input addonBefore="https://t.me/"
                                           onPaste={this.handleTelegram}/>
                                </Form.Item>
                                <Form.Item label={t('phone')} name={"phone"}
                                           initialValue={this.state.phone ? this.state.phone : '+7/'}
                                           extra={t('phone_extra')}>
                                    <CountryPhoneInput/>
                                </Form.Item>

                                <Form.Item name="company" label={"Компании в которой вы работаете"}
                                           initialValue={this.state.company}
                                           rules={[{
                                               required: true,
                                               message: 'Введите компании в которой вы работаете'
                                           }]}>
                                    <Input/>
                                </Form.Item>

                                <Form.Item name="type_developer" label={"Ваша должность"}
                                           initialValue={this.state.type_developer}
                                           rules={[{required: true, message: 'Введите свою должность'}]}>
                                    <Input/>
                                </Form.Item>

                                <Form.Item name="expert_task" label={"Выберите задачу в которой вы эксперт"}
                                           initialValue={this.state.expert_task}
                                           rules={[{required: true, message: 'Выберите задачу в которой вы эксперт'}]}>
                                    <Select disabled={this.props.profile.expert_task != null}>
                                        {
                                            this.state.tasksList && this.state.tasksList.map((item: any) => {
                                                return <Option key={item.id}
                                                               value={item.id}>{item.maintainer.name} - {item.name}</Option>
                                            })
                                        }
                                    </Select>

                                </Form.Item>

                                <Form.Item wrapperCol={{span: 24}}>
                                    <Button style={{width: "100%"}} className="mr-2" type="primary"
                                            onClick={() => this.onSubmit}
                                            htmlType="submit">
                                        {t('common:save')}
                                    </Button>
                                </Form.Item>
                            </Col>
                        </div>
                    </div>
                </Form>
            </React.Fragment>

        )
    }
}


const mapDispatchToProps = (dispatch) => {
    return {
        actionAuth: bindActionCreators(AuthAction, dispatch),
    };
};
export default withTranslation(withExpiredToken(withAuthPrivateExpertRoute(connect((state => (state.auth)), mapDispatchToProps)(questionary))), "questionary");

