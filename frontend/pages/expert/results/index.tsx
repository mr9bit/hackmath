import React from 'react';
import Link from 'next/link';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {EyeOutlined} from '@ant-design/icons';
import {Pagination, Table, Tooltip, Button} from "antd";
import {Breakpoint} from 'antd/lib/_util/responsiveObserve';
// Компоненты
import PageHeaderAlt from '../../../components/layout-components/PageHeaderAlt'
import Flex from '../../../components/shared-components/Flex'
// Роутинг
import withAuthPrivateAdminRoute from '../../../route/withAuthPrivateAdminRoute';
import withExpiredToken from '../../../route/withExpiredToken';
// Дейсвтия
import {ExpertAction} from '../../../redux/actions/expert.action'
import withAuthPrivateExpertRoute from "../../../route/withAuthPrivateExpertRoute";


export const index = (props) => {

    const handleTableChange = (page) => {
        props.expertAction.teamNextPage(props.auth.access_token, page).then().catch((e) => {
                console.log(e)
            }
        )
    }

    console.log(props)


    const responsive: Breakpoint[] = ['sm']
    const columns = [
        {
            title: 'Название',
            dataIndex: 'full_name',
            key: 'full_name',
            render: (_, record) => (
                <div className="d-flex">
                    <Link href={{
                        pathname: '/expert/teams/[id]',
                        query: {id: record.id},
                    }}>
                        <a>
                            {record.team.name}
                        </a>
                    </Link>
                </div>
            )
        },
        {
            title: 'Репозиторий',
            dataIndex: 'link_to_git',
            key: 'link_to_git',
            responsive: responsive,
            render: (_, record) => (
                <Link href={record.link_to_git}>
                    <a>
                        <Button type="dashed" className="mr-2 mt-2" style={{whiteSpace: "normal", height: 'auto'}}
                                size={"middle"}>Просмотр кода</Button>
                    </a>
                </Link>
            )
        },
        {
            title: 'Презентация',
            dataIndex: 'link_to_presentation',
            key: 'link_to_presentation',
            responsive: responsive,
            render: (_, record) => (
                <Link href={record.link_to_presentation}>
                    <a>
                        <Button type="default" className="mr-2 mt-2" style={{whiteSpace: "normal", height: 'auto'}}
                                size={"middle"}>Просмотр презентации</Button>
                    </a>
                </Link>
            )
        },
        // {
        //     title: '',
        //     dataIndex: 'actions',
        //     key: 'actions',
        //     render: (_, record) => (
        //         <div className="text-right">
        //             <Link href={{
        //                 pathname: '/expert/teams/[id]',
        //                 query: {id: record.id},
        //             }}>
        //                 <a>
        //                     <Button type="primary" className="mr-2 mt-2" style={{whiteSpace: "normal", height: 'auto'}}
        //                             size={"middle"}>Просмотр решения</Button>
        //                 </a>
        //             </Link>
        //
        //         </div>
        //     )
        // }
    ]


    return (
        <React.Fragment>
            <PageHeaderAlt className="bg-primary mb-5" overlap>
                <div className="container text-center">
                    <div className="py-lg-4">
                        <h1 className="text-white display-4">Результаты</h1>
                    </div>
                </div>
            </PageHeaderAlt>
            {/*<Card>*/}
            {/*    <Flex alignItems="center" justifyContent="between" mobileFlex={false}>*/}
            {/*        <Flex className="mb-1" mobileFlex={false}>*/}
            {/*            <div className="mr-md-3 mb-3">*/}
            {/*                <Input placeholder="Поиск по ФИО" prefix={<SearchOutlined/>}/>*/}
            {/*            </div>*/}
            {/*        </Flex>*/}
            {/*    </Flex>*/}
            {/*</Card>*/}
            <div className="code-box">
                <Table columns={columns}
                       rowKey='id'
                       dataSource={props.list.results}
                       pagination={{position: ["bottomCenter"]}}
                />
            </div>
        </React.Fragment>

    )
}

index.getInitialProps = async (ctx) => {
    await ctx.store.dispatch(ExpertAction.resultTeam(ctx.auth.access_token))
    const {list} = mapStateToProps(ctx.store.getState())
    return {list}
}

const mapStateToProps = (state) => {
    return {
        list: state.expert.result.list,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        expertAction: bindActionCreators(ExpertAction, dispatch),
    };
};

export default withAuthPrivateExpertRoute(withExpiredToken(connect((state => (
    {
        auth: state.auth,
        list: state.expert.result.list
    })), mapDispatchToProps)(index)))

