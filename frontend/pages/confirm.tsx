import React from 'react';
import Link from "next/link";
import Router from "next/router";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {LoadingOutlined} from '@ant-design/icons';
import {Spin, Result, Card, Row, Col, Button} from 'antd';
// Действия
import {AuthAction} from '../redux/actions/auth.action'
import withTranslation from "next-translate/withTranslation";

const backgroundStyle = {
    backgroundImage: 'url(/img/others/img-17.jpg)',
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'cover'
}

interface Props {
    actionAuth: any,
    query: any,
    i18n: any
}

interface State {
    verifyComplete: boolean,
    error: string
}

class verify extends React.Component<Props, State> {

    static async getInitialProps(ctx) {
        const query = ctx.query;
        return {query}
    }

    constructor(props) {
        super(props);
        this.state = {
            verifyComplete: false,
            error: '',
        }
    }

    componentDidMount() {
        const {t, lang} = this.props.i18n;
        this.props.actionAuth.confirm(this.props.query).then((e) => {
            Router.push('/')
        }).catch((error) => {
            error.response.json().then((e) => {
                try {
                    if (e.detail === 'Invalid signature') {
                        this.setState({
                            error: t('invalid_signature')
                        })
                    } else if (e.detail === 'Signature expired') {
                        this.setState({
                            error: t('signature_expired')
                        })
                    } else if (e.detail === 'User not found') {
                        this.setState({
                            error: t('user_not_found')
                        })
                    }
                } catch {
                    console.log()
                }
            })
        })
    }

    render() {
        const {t, lang} = this.props.i18n;
        return (
            <div className={'auth-container'}>
                <div className="h-100" style={backgroundStyle}>
                    <div className="container d-flex flex-column justify-content-center h-100"
                         style={{padding: "20px 0"}}>
                        <Row justify="center">
                            <Col xs={20} sm={20} md={20} lg={10}>
                                <Card style={{
                                    display: "flex",
                                    justifyContent: 'center',
                                    justifyItems: 'center',
                                    alignItems: 'center',
                                    height: this.state.error ? 'auto' : '250px'
                                }}>
                                    {this.state.error ?
                                        <Result
                                            status="error"
                                            title={t('common:error')}
                                            style={{margin: "20px 0"}}
                                            subTitle={this.state.error}
                                            extra={[
                                                <Link href={"/"}>
                                                    <a>
                                                        <Button type="primary">
                                                            {t('common:to_main')}
                                                        </Button>
                                                    </a>
                                                </Link>,
                                            ]}
                                        />
                                        :
                                        <Spin indicator={<LoadingOutlined style={{fontSize: 60}} spin/>}/>
                                    }
                                </Card>
                            </Col>
                        </Row>
                    </div>
                </div>

            </div>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        actionAuth: bindActionCreators(AuthAction, dispatch),
    };
};

export default withTranslation(connect(null, mapDispatchToProps)(verify), "confirm")

