import React, {useState, useEffect} from 'react';
import {bindActionCreators} from "redux";
import useTranslation from 'next-translate/useTranslation'
import {connect} from "react-redux";
import {
    Tag,
    Divider,
    Button,
    Steps,
    Badge,
    Popover,
    Typography,
    Grid,
    Row,
    Col,
    Card,
    notification,
    List,
    Timeline
} from "antd";
// Компоненты
import PageHeaderAlt from '../components/layout-components/PageHeaderAlt'
import StatisticWidget from '../components/shared-components/StatisticWidget';
import GoalWidget from '../components/shared-components/GoalWidget';
import Flex from '../components/shared-components/Flex'
import Icon, {ArrowDownOutlined, YoutubeOutlined, SendOutlined} from '@ant-design/icons';

import {Funnel} from '../components/Funnel';
import NumberFormat from 'react-number-format';
// Дейсвтия
import {AuthAction} from '../redux/actions/auth.action'
import {AdminAction, hackerList, statisticsHackathon} from "../redux/actions/admin.action";
// Роутинг
import withPrivateRoute from '../route/withPrivateRoute';
import withExpiredToken from '../route/withExpiredToken';
// Утилиты
import utils from '../utils';
import {apexLineChartDefaultOption, apexPieChartDefaultOption, COLOR_2, COLORS} from '../constants/ChartConstant';
import dynamic from "next/dynamic";
import Link from 'next/link'
import {URl_BACKEND} from "../redux/settings";
import {checkHttpStatus} from "../redux/utils";
import moment from "moment";

const DonutChartWidget = dynamic(() => import("../components/shared-components/DonutChartWidget"), {ssr: false});
const ApexChart = dynamic(() => import("react-apexcharts"), {ssr: false});
const PieChart = dynamic(() => import("../components/shared-components/PieChart"), {ssr: false});


const {useBreakpoint} = Grid;
const {Step} = Steps;
const {Title, Paragraph} = Typography;

import {DiscordIcon} from "../components/ExternalIcon";
import {Expert} from '../components/Dashboard/Expert'
import {getStatusTag} from "../components/Utils/StatusTag";

const memberChartOption = {
    ...apexLineChartDefaultOption,
    ...{
        chart: {
            sparkline: {
                enabled: true,
            },
        },
        colors: [COLOR_2],
    }
}

const customDot = (dot, {status, index}) => (
    <Popover content={<span>   Этап {index} Статус: {status}  </span>}>
        {dot}
    </Popover>
);


const jointSessionData = (Data, Labels, Colors) => {
    let arr = []
    for (let i = 0; i < Data.length; i++) {
        const data = Data[i];
        const label = Labels[i];
        const color = Colors[i]
        arr = [...arr, {
            data: data,
            label: label,
            color: color
        }]
    }
    return arr
}


export const index = (props) => {
    const {t, lang} = useTranslation('index')

    const screens = utils.getBreakPoint(useBreakpoint());
    const isMobile = !screens.includes('md')
    const [count, setCount] = useState(0);
    const [countButton, setCountButton] = useState(0);
    const [activeMember, setActiveMember] = useState([]);
    const [task, setTask] = useState([]);
    const [labelTask, setLabelTask] = useState([]);
    const [dataTask, setDataTask] = useState([]);

    const [labelTaskTeam, setLabelTaskTeam] = useState([]);
    const [dataTaskTeam, setDataTaskTeam] = useState([]);
    const [taskTeam, setTaskTeam] = useState([]);


    useEffect(() => {
        if (props.auth.user.role === 'admin') {
            let date2 = (new Date()).toISOString().split('T')[0];
            let ddate = new Date();
            ddate.setMonth(ddate.getMonth() - 1);
            let date1 = ddate.toISOString().split('T')[0];
            const all_user = `https://api-metrika.yandex.net/stat/v1/data?metrics=ym:s:visits,%20ym:s:sumGoalReachesAny&date1=${date1}&date2=${date2}&id=73080592`
            fetch(all_user)
                .then((response) => response.json())
                .then((response) => {
                    setCount(response.data[0].metrics[0]);
                    setCountButton(response.data[0].metrics[1]);
                })

            props.actionAdmin.getStatistics(props.auth.access_token).then((item) => {

                let array = item.user_hist_week.map((user) => {
                    return {
                        x: user.day,
                        y: user.available
                    }
                })
                setActiveMember([{
                    name: 'Количество участников',
                    data: array
                }])

                const labelTeam = item.team_task_rating.map((item) => item.count)
                setLabelTaskTeam(labelTeam)

                console.log(item.team_task_rating)
                props.actionAdmin.getTaskList(props.auth.access_token).then((e) => {
                    const label = item.task_rating.map((item) => item.count)
                    setLabelTask(label)
                    let data = item.task_rating.map((item) => e.results.find(obj => {
                            return obj.id === item.select_task
                        })
                    )
                    data = data.map(item => item.name)
                    setDataTask(data)
                    setTask(jointSessionData(data, label, COLORS))
                    let dataTeam = item.team_task_rating.map((item) => e.results.find(obj => {
                            return obj.id === item.task
                        })
                    )
                    dataTeam = dataTeam.map(item => item.name)
                    setDataTaskTeam(dataTeam)
                    setTaskTeam(jointSessionData(dataTeam, labelTeam, COLORS))

                })
            })


        }


    }, [])

    return (
        <React.Fragment>
            <PageHeaderAlt className="bg-primary mb-5" overlap>
                <div className="container text-center">
                    <div className="py-lg-4">
                        <h1 className="text-white display-4">{t('main_title')}</h1>
                    </div>
                </div>
            </PageHeaderAlt>

            {props.user.role === 'hacker' ?
                <div className="code-box">
                    <div className="container my-4">
                        <Row gutter={16}>
                            <Title level={2} style={{textAlign: "center", width: "100%"}}>{t('your_status')}</Title>
                            <div style={{display: "flex", justifyContent: 'center', width: '100%'}}>
                                {props.user ? getStatusTag(props.user.status, t) : null}
                            </div>
                        </Row>
                    </div>

                    <Divider/>
                    <div className="container my-4">
                        {isMobile ?
                            props.user ?
                                <Row>
                                    <Steps direction="vertical" size="small" current={props.user.status}>
                                        <Step title={t('confirm_email')}/>
                                        <Step title={t('fill_form')}/>
                                        <Step title={t('wait_invitations')}/>
                                        <Step title={t('confirm_participation')}/>
                                    </Steps>
                                </Row>
                                : null
                            :
                            props.user ?
                                <Row gutter={16}>
                                    <Steps current={props.user.status} progressDot={customDot}>
                                        <Step title={t('confirm_email')}/>
                                        <Step title={t('fill_form')}/>
                                        <Step title={t('confirm_participation')}/>
                                    </Steps>
                                </Row>
                                : null
                        }
                    </div>
                    <Divider/>
                    <div className="container my-4">
                        <Row gutter={isMobile ? 0 : 16}>
                            {props.user ?
                                props.user.status == 0 ?
                                    <Typography style={{width: "100%"}}>

                                        <Paragraph style={{textAlign: "center", fontSize: "16px"}}>
                                            <b>{t("unconfirmed__desc_1", {email: props.user.email})}</b><br/>
                                            {t('unconfirmed__desc_2')}.<br/>
                                            <Button type={"primary"} onClick={(e) => {
                                                AuthAction.resend_email(props.auth.access_token).then((e) => {
                                                    notification.success({
                                                        message: t('resend_email'),
                                                        duration: 2.3,
                                                    })
                                                }).catch((error) => {
                                                    error.response.json().then((e) => {
                                                        e.error.map(item => {
                                                            notification.error({
                                                                message: t('common:error'),
                                                                description: item,
                                                                duration: 2.3,
                                                            })
                                                        })
                                                    })
                                                })
                                            }} danger={true} style={{marginTop: "15px"}}>
                                                {t('resend_confirm_email')}
                                            </Button>
                                        </Paragraph>
                                    </Typography> : props.user.status == 1 ? <Typography style={{width: "100%"}}>
                                        <Paragraph style={{textAlign: "center", fontSize: "16px"}}>
                                            {t('complete')}
                                        </Paragraph>
                                    </Typography> : props.user.status == 2 ? <Typography style={{width: "100%"}}>
                                        <Paragraph style={{textAlign: "center", fontSize: "16px"}}>
                                            {t('wait_admitted')}<br/>

                                            <Link href={"https://t.me/MathSchoolMAI_bot"}>
                                                <a target={"_blank"}>
                                                    <Button
                                                        style={{marginTop: "15px", marginLeft: "10px", marginRight: "10px"}}
                                                        type="primary" shape="round"
                                                        icon={<SendOutlined/>} size={'large'}>
                                                        Телеграм бот с расписанием и VPN
                                                    </Button>
                                                </a>
                                            </Link>


                                        </Paragraph>
                                    </Typography> : null

                                : null
                            }
                        </Row>
                    </div>
                </div>
                :
                null}
            {props.user.role === 'admin' ?
                <div>
                    <Row gutter={16}>
                        <Col xs={24} sm={24} md={24} lg={18}>
                            <Row gutter={16}>
                                <Col xs={24} sm={24} md={24} lg={24} xl={8}>
                                    <StatisticWidget
                                        title={"Количество Пользователей"}
                                        value={`${props.admin.statistics.user_count}`}
                                        status={props.admin.statistics.new_user_count}
                                        subtitle={"Сегодня присоединилось"}
                                    />
                                </Col>
                                <Col xs={24} sm={24} md={24} lg={24} xl={8}>
                                    <StatisticWidget
                                        title={"Количество Хакеров"}
                                        value={`${props.admin.statistics.hackers_count}`}
                                        status={props.admin.statistics.new_hacker_count}
                                        subtitle={"Сегодня присоединилось"}
                                    />
                                </Col>
                                <Col xs={24} sm={24} md={24} lg={24} xl={8}>
                                    <StatisticWidget
                                        title={"Количество Команд"}
                                        value={`${props.admin.statistics.team_count}`}
                                        status={props.admin.statistics.new_team_count}
                                        subtitle={"Сегодня создало"}
                                    />
                                </Col>
                            </Row>
                            <Row gutter={16}>
                                <Col xs={24} sm={24} md={24} lg={24}>
                                    <Card title={`Воронка участников`}>
                                        <Funnel
                                            height={252}
                                            valueKey='value'
                                            labelKey='label'
                                            responsive={true}
                                            colors={{
                                                graph: ['#3e79f7', '#BAE7FF'],
                                                percent: '#72849a',
                                                label: '#1a3353',
                                                value: '#1a3353'
                                            }}
                                            displayPercent={true}
                                            width={350}
                                            style={{width: "100%"}}
                                            data={[
                                                {
                                                    "label": "Посещений",
                                                    "value": count
                                                }, {
                                                    "label": "Кнопка на сайте",
                                                    "value": countButton
                                                }, {
                                                    "label": "Пользователей",
                                                    "value": props.admin.statistics.all_users
                                                }, {
                                                    "label": "Хакеров",
                                                    "value": props.admin.statistics.hackers_count
                                                },
                                            ]
                                            }/>
                                    </Card>

                                </Col>
                            </Row>
                            <Row gutter={16}>
                                <Col xs={24} sm={24} md={24} lg={12} xxl={24}>
                                    <StatisticWidget
                                        title={
                                            <ApexChart options={memberChartOption}
                                                       series={activeMember}
                                                       height={145}
                                                       width={"100%"}
                                            />
                                        }
                                        value={`${props.admin.statistics.all_users}`}
                                        subtitle="Участников"
                                    />
                                </Col>
                            </Row>
                        </Col>


                        <Col xs={24} sm={24} md={24} lg={6}>
                            <Card>
                                <h4 className="mb-0">Статус</h4>
                                <div className="d-flex mt-3">
                                    <div>
                                        <div className="d-flex align-items-center">
                                            <h1 className="mb-0 font-weight-bold">{props.admin.statistics.status_user?.not_confirmed}/{props.admin.statistics.status_user?.incomplete}/{props.admin.statistics.status_user?.allowed}/{props.admin.statistics.status_user?.confirmed}</h1>
                                        </div>
                                        <div className="text-gray-light mt-1">Не подтвержден/Не
                                            завершен/Допущен/Подтвержден
                                        </div>
                                    </div>
                                </div>
                            </Card>

                            <GoalWidget
                                title="Цель Хакатона"
                                value={Math.round(props.admin.statistics.all_users * 100 / 500)}
                                subtitle={`Необходимое количество пользователей (${props.admin.statistics.all_users}/500)`}
                            />
                            <Card title="Рейтинг Задач (среди Хакеров)">

                                <DonutChartWidget
                                    series={labelTask}
                                    labels={dataTask}
                                    customOptions={COLORS}
                                    extra={
                                        <Row justify="center">
                                            <Col xs={24} sm={24} md={24} lg={24}>
                                                <div className="mt-4 mx-auto" style={{maxWidth: 600}}>
                                                    {task.map((elm, index) => (
                                                        <Flex alignItems="center" justifyContent={"between"}
                                                              className="mb-3" key={index}>
                                                            <div>
                                                                <Badge color={elm.color}/>
                                                                <span className="text-gray-light">{elm.data}</span>
                                                            </div>
                                                            <span
                                                                className="font-weight-bold text-dark ml-2">{elm.label}</span>
                                                        </Flex>
                                                    ))}
                                                </div>
                                            </Col>
                                        </Row>
                                    }
                                />
                            </Card>
                            <Card title="Рейтинг Задач (среди Команд)">

                                <DonutChartWidget
                                    series={labelTask}
                                    title="Команды"
                                    labels={dataTaskTeam}
                                    customOptions={COLORS}
                                    extra={
                                        <Row justify="center">
                                            <Col xs={24} sm={24} md={24} lg={24}>
                                                <div className="mt-4 mx-auto" style={{maxWidth: 600}}>
                                                    {taskTeam.map((elm, index) => (
                                                        <Flex alignItems="center" justifyContent={"between"}
                                                              className="mb-3" key={index}>
                                                            <div>
                                                                <Badge color={elm.color}/>
                                                                <span className="text-gray-light">{elm.data}</span>
                                                            </div>
                                                            <span
                                                                className="font-weight-bold text-dark ml-2">{elm.label}</span>
                                                        </Flex>
                                                    ))}
                                                </div>
                                            </Col>
                                        </Row>
                                    }
                                />
                            </Card>
                        </Col>
                    </Row>
                </div>
                :
                null}
            {props.user.role === 'expert' ?
                <>
                    <Expert {...props}/>
                </>
                :
                null}
        </React.Fragment>

    )
}


index.getInitialProps = async (ctx) => {
    await ctx.store.dispatch(AuthAction.getMeInfo(ctx.auth.access_token))
    const {user} = mapStateToProps(ctx.store.getState())
    return {user}
}

const mapStateToProps = (state) => {
    return {
        user: state.auth.user,
        admin: state.admin,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        actionAdmin: bindActionCreators(AdminAction, dispatch),
    };
};

export default withPrivateRoute(withExpiredToken(connect((state => (
    {
        auth: state.auth,
        admin: state.admin
    })), mapDispatchToProps)(index)))

