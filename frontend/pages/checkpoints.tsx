import PageHeaderAlt from "../components/layout-components/PageHeaderAlt";
import React from "react";
import {bindActionCreators} from "redux";
import {TeamAction} from "../redux/actions/team.action";
import withTranslation from "next-translate/withTranslation";
import withExpiredToken from "../route/withExpiredToken";
import withAuthAndPrivateHackerRoute from "../route/withAuthAndPrivateHackerRoute";
import {connect} from "react-redux";
import {Col, Card, Typography, Row, Button} from "antd";
import AvatarStatus from '../components/shared-components/AvatarStatus';

const {Title} = Typography;

const checkpoints = (props) => {
    return (
        <React.Fragment>
            <PageHeaderAlt className=" mb-5" overlap>
                <div className="container text-center">
                    <div className="py-lg-4">
                        <h1 className="text-primary display-4">Обратная связь</h1>
                    </div>
                </div>
            </PageHeaderAlt>
            <div className={"container my-4"}>
                {
                    props.checkpoints.results.map(item => {
                        return <Col key={item.id} xs={24} sm={24} lg={24} xl={24} xxl={24}>
                            <Card>
                                <Title style={{textAlign: "center"}} level={2}>{item.name}</Title>
                                <Row gutter={16}>
                                    {props.response.results.map(resp => (
                                        resp.checkpoint === item.id ?
                                            <Col xs={24} sm={24} lg={12} xl={8}>
                                                <Card>
                                                    <b>Что хорошо?</b>
                                                    <p>{resp.good}</p>
                                                    <b>Что улучшить?</b>
                                                    <p>{resp.better}</p>
                                                    <b>Комментарий</b>
                                                    <p>{resp.comment}</p>
                                                    <AvatarStatus text={resp.expert.first_name[0]}
                                                                  name={`${resp.expert.first_name} ${resp.expert.second_name}`}
                                                                  subTitle={<>
                                                                      {resp.expert.company}, {resp.expert.type_developer}
                                                                  </>}/>
                                                </Card>
                                            </Col> : null
                                    ))}
                                </Row>
                            </Card>
                        </Col>
                    })
                }
            </div>
        </React.Fragment>
    )
}

checkpoints.getInitialProps = async (ctx) => {
    await ctx.store.dispatch(TeamAction.checkPoint(ctx.auth.access_token))
    await ctx.store.dispatch(TeamAction.checkPointResponse(ctx.auth.access_token))
    const {checkpoints, response} = mapStateToProps(ctx.store.getState())
    return {checkpoints, response}
}

const mapStateToProps = (state) => {
    return {
        checkpoints: state.team.check.checkPoint,
        response: state.team.check.response
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        actionTeam: bindActionCreators(TeamAction, dispatch),
    };
};
export default withTranslation(withExpiredToken(withAuthAndPrivateHackerRoute(connect((state => (
    {
        auth: state.auth,
        checkpoints: state.team.check.checkPoint,
        response: state.team.check.response
    })), mapDispatchToProps)(checkpoints))), "common")
