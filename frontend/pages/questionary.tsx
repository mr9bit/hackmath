import React from 'react';
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {GithubFilled, GitlabFilled, UploadOutlined, LinkedinFilled,} from '@ant-design/icons';
import {
    message, Input, Form, Button, Radio, Select, AutoComplete, Spin,
    DatePicker, Typography, Upload, Col, notification, InputNumber
} from 'antd';
import {FormInstance} from 'antd/lib/form';
import PageHeaderAlt from '../components/layout-components/PageHeaderAlt';
import CountryPhoneInput from '../components/util-components/PhoneInput';
import {AuthAction} from '../redux/actions/auth.action';
import {Profile, User} from '../redux/interface/auth';
import withAuthAndPrivateHackerRoute from '../route/withAuthAndPrivateHackerRoute';
import withExpiredToken from '../route/withExpiredToken';
import {URl_BACKEND} from '../redux/settings'
import {UploadFile} from 'antd/lib/upload/interface'
import locale from 'antd/lib/date-picker/locale/ru_RU'
import moment from "moment";

const {Title} = Typography;
const {TextArea} = Input;
const {Option} = Select;

interface Props {
    lastFetchId: number,
    actionAuth: any,
    user: User,
    profile: Profile,
    access_token: string,
    i18n: any
}


const mapStateToProps = (state) => {
    return {
        profile: state.auth.profile,
    };
};
type AutoComplete = {
    value: string
}
type TaskComplete = {
    name: string;
    id: number;
}
type Skill = {
    label: string;
    value: string;
}

interface State extends Profile {
    fetchingCountry: boolean,
    autoCompleteCityData: AutoComplete[],
    autoCompleteSpecialization: AutoComplete[],
    autoCompleteInstitute: AutoComplete[],
    autoCompleteCollage: AutoComplete[],
    autoCompleteSchool: AutoComplete[],
    telegramPaste: boolean,
    skillSelected: string[],
    autoSkillComplete: Skill[],
    renderResume?: UploadFile,
    tasksList: TaskComplete[],
    selected_tasks: number[],
}

import withTranslation from 'next-translate/withTranslation'

class questionary extends React.Component<Props, State> {
    formRef = React.createRef<FormInstance>();

    static async getInitialProps(ctx) {
        await ctx.store.dispatch(AuthAction.getProfile(ctx.auth.access_token))
        const {profile} = mapStateToProps(ctx.store.getState())
        return {profile}
    }

    constructor(props: Props) {
        super(props);
        this.onSubmit = this.onSubmit.bind(this);
        this.onFailedSave = this.onFailedSave.bind(this);
        this.onAutoCompleteCountry = this.onAutoCompleteCountry.bind(this);
        this.handleChangeCityLive = this.handleChangeCityLive.bind(this);
        this.handleTelegram = this.handleTelegram.bind(this);
        this.handleGithub = this.handleGithub.bind(this);
        this.handleChangeWhereLearning = this.handleChangeWhereLearning.bind(this);
        this.onUploadCV = this.onUploadCV.bind(this);
        this.handleChangeTasks = this.handleChangeTasks.bind(this);
        this.state = {
            ...this.props.profile,
            fetchingCountry: false,
            autoCompleteCityData: [],
            telegramPaste: false,
            autoCompleteSpecialization: [],
            autoCompleteInstitute: [],
            autoCompleteCollage: [],
            skillSelected: [],
            autoSkillComplete: [],
            autoCompleteSchool: [],
            renderResume: this.props.profile.resume_file.file_name ? {
                uid: '1',
                size: parseInt(this.props.profile.resume_file.size),
                type: 'text',
                name: this.props.profile.resume_file.file_name,
                status: 'done',
                url: this.props.profile.resume_file.url,
            } : null,
            tasksList: [],
            selected_tasks: []
        }
    }

    fetchUser = value => {
        this.setState({autoSkillComplete: [], fetchingCountry: true});
        fetch(`${URl_BACKEND}/api/autocomplete/skills/?q=${value}`, {
            method: 'GET',
            headers: {
                'Authorization': `Bearer ${this.props.access_token}`
            },
        })
            .then(response => response.json())
            .then(body => {
                const data = body.map(user => {
                    return {
                        label: user.name,
                        value: user.value
                    }
                });
                this.setState({autoSkillComplete: data, fetchingCountry: false});
            });
    };

    handleChange = value => {
        this.setState({
            skills: value,
            autoSkillComplete: this.state.autoSkillComplete,
            fetchingCountry: false,
        });
    };

    componentDidMount() {
        fetch(`${URl_BACKEND}/api/org/task/`, {
            method: 'GET',
            headers: {
                'Authorization': `Bearer ${this.props.access_token}`
            },
        }).then((e) => e.json()).then((res) => {
            this.setState({tasksList: res.results})
        })
        const selected_tasks = this.props.profile.select_task.map(item => item);
        this.setState({selected_tasks: selected_tasks})
    }

    onSubmit = (form) => {
        const {t, lang} = this.props.i18n;
        if (form) {
            form.birth_date = moment(form.birth_date).format("YYYY-MM-DD")
            form.year_end = Number(moment(form.year_end).format("YYYY"))
            if (form.traks) {
                form.traks = form.traks.map(function (val) {
                    return val;
                }).join(',');
            }

            if (form.skills) {
                form.skills = form.skills.map(function (val) {
                    return val.value
                });
            }

            notification.info({
                message: t('common:saving'),
                duration: 1.2
            })

            this.props.actionAuth.updateProfile(this.props.access_token, form).then(
                () => notification.success({
                    message: t('common:data_saved'),
                    duration: 2.3
                })
            ).catch((e) => {
                notification.error({
                    message: t('common:error_on_save'),
                    description: t('common:try_update_page_and_enter_data'),
                    duration: 2.3,
                })
            })
        } else {
            notification.info({
                message: `😶 Нечего сохранять`,
                description: 'Внесите данные',
                duration: 2.3,
            })
        }
    };

    onAutoCompleteCountry = (value) => {
        const searchUrl = `https://autocomplete.geocoder.ls.hereapi.com/6.2/suggest.json?apiKey=ej64jGu0EMqqTyVAhQuLZLT9e3x8cOvDYm3av-Llm3Y&resultType=city&query=${value}`
        fetch(searchUrl)
            .then(response => response.json())
            .then(response => {
                if (response.suggestions) {
                    const data = response.suggestions.map(user => ({
                        value: `${user.address.country}, ${user.address.city}`,
                    }));
                    this.setState({autoCompleteCityData: data, fetchingCountry: false});
                }

            });
    };

    handleChangeCityLive = value => {
        this.setState({
            country: value,
            autoCompleteCityData: [],
            fetchingCountry: false,
        });
    };

    handleChangeCity = value => {
        this.setState({
            country_live: value,
            autoCompleteCityData: [],
            fetchingCountry: false,
        });
    };

    handleChangeBornCity = value => {
        this.setState({
            born_city: value,
            autoCompleteCityData: [],
            fetchingCountry: false,
        })
    }

    handleTelegram = e => {
        e.preventDefault();
        const pasteValue = e.clipboardData.getData('Text');
        const rexexpUrl = new RegExp(/(https?:\/\/(.+?\.)?t\.me(\/[A-Za-z0-9\-\._~:\/\?#\[\]@!$&'\(\)\*\+,;\=]*)?)/g);
        if (rexexpUrl.test(pasteValue)) {
            let new_value = pasteValue.split('/');
            new_value = new_value[new_value.length - 1];
            this.formRef.current!.setFieldsValue({telegram_link: new_value});
        }
    }

    onAutoCompleteInstituteSpecialization = (value) => {
        const searchUrl = `${URl_BACKEND}/api/autocomplete/specialization?q=${value}`
        fetch(searchUrl, {
            method: 'GET',
            headers: {
                'Authorization': `Bearer ${this.props.access_token}`
            },
        })
            .then(response => response.json())
            .then(response => {
                if (response) {
                    let data = response.map(item => {
                        return {
                            'value': item.name
                        }
                    })
                    this.setState({autoCompleteSpecialization: data, fetchingCountry: false});
                }
            });
    }

    handleChangeInstituteSpecialization = value => {
        this.setState({
            specialization: value,
            autoCompleteSpecialization: [],
            fetchingCountry: false,
        });
    };

    handleGithub = e => {
        e.preventDefault();
        const pasteValue = e.clipboardData.getData('Text');
        const rexexpUrl = new RegExp(/(https?:\/\/(.+?\.)?github\.com(\/[A-Za-z0-9\-\._~:\/\?#\[\]@!$&'\(\)\*\+,;\=]*)?)/g);
        if (rexexpUrl.test(pasteValue)) {
            let new_value = pasteValue.split('/');
            new_value = new_value[new_value.length - 1];
            this.formRef.current!.setFieldsValue({github: new_value});
        }
    }

    onAutoCompleteInstitute = (value) => {
        const searchUrl = `${URl_BACKEND}/api/autocomplete/institute/?q=${value}`
        fetch(searchUrl, {
            method: 'GET',
            headers: {
                'Authorization': `Bearer ${this.props.access_token}`
            },
        }).then(response => response.json())
            .then(response => {
                if (response) {
                    let data = response.map(item => {
                        return {
                            'value': item.name
                        }
                    })

                    this.setState({autoCompleteInstitute: data, fetchingCountry: false});
                }
            });

    }

    handleChangeInstitute = (label) => {
        this.setState({
            institute: label,
            autoCompleteInstitute: [],
            fetchingCountry: false,
        });
    };

    handleChangeWhereLearning = value => {
        this.setState({
            where_to_study: value
        })
    }

    onUploadCV = (info: any) => {
        if (info.file.status == 'done') {
            message.success(`${info.file.name} файл загружен`);

            this.setState({
                renderResume: {
                    uid: '1',
                    size: parseInt(info.file.response.size),
                    type: 'text',
                    name: info.file.response.file_name,
                    status: 'done',
                    url: info.file.response.path,
                }
            })
        }
    }


    onAutoCompleteCollage = (value) => {
        const searchUrl = `${URl_BACKEND}/api/autocomplete/collage/?q=${value}`
        fetch(searchUrl, {
            method: 'GET',
            headers: {
                'Authorization': `Bearer ${this.props.access_token}`
            },
        })
            .then(response => response.json())
            .then(response => {
                if (response) {
                    let data = response.map(item => {
                        return {
                            'value': item.name
                        }
                    })
                    this.setState({autoCompleteCollage: data, fetchingCountry: false});
                }
            });

    }

    handleChangeCollage = (label) => {
        this.setState({
            institute: label,
            autoCompleteCollage: [],
            fetchingCountry: false,
        });
    };


    onAutoCompleteSchool = (value) => {
        const searchUrl = `${URl_BACKEND}/api/autocomplete/school/?q=${value}`
        fetch(searchUrl, {
            method: 'GET',
            headers: {
                'Authorization': `Bearer ${this.props.access_token}`
            },
        })
            .then(response => response.json())
            .then(response => {
                if (response) {
                    let data = response.map(item => {
                        return {
                            'value': item.name
                        }
                    })
                    this.setState({autoCompleteSchool: data, fetchingCountry: false});
                }
            });
    }

    handleChangeSchool = (label) => {
        this.setState({
            institute: label,
            autoCompleteSchool: [],
            fetchingCountry: false,
        });
    };

    onFailedSave = ({values, errorFields, outOfDate}) => {
        errorFields.map((item) => {
            notification.error({
                message: item.errors[0],
                duration: 2,
            })
        })
    }

    handleChangeTasks = (selectedItems) => {
        this.setState({selected_tasks: selectedItems})
    };

    render() {
        const select_task = this.state;
        const {t, lang} = this.props.i18n;
        return (

            <React.Fragment>
                <PageHeaderAlt className=" mb-5" overlap>
                    <div className="container text-center">
                        <div className="py-lg-4">
                            <h1 className="text-primary display-4">{t("title")}</h1>
                        </div>
                    </div>
                </PageHeaderAlt>

                <div className={"m-4"}>
                    <Title style={{textAlign: "center"}} level={2}>
                        {t('second_title')}</Title>
                </div>
                <Form size={"large"} onFinishFailed={this.onFailedSave} layout="vertical" ref={this.formRef}
                      onFinish={this.onSubmit}>
                    <div className="code-box">
                        <div className="container my-4">
                            <Title style={{textAlign: "center"}} className={"mb-4 text-primary"}
                                   level={1}>{t('main_info')}</Title>
                            <Col xs={24} sm={24} md={24} lg={{span: 14, offset: 5}}>
                                <Form.Item label="Email">
                                    <Input value={this.props.user.email} disabled={true}/>
                                </Form.Item>
                                <Form.Item name="first_name" label={t('first_name')}
                                           initialValue={this.state.first_name}
                                           rules={[{required: true, message: 'Введите свое имя'}]}>
                                    <Input/>
                                </Form.Item>
                                <Form.Item label={t('second_name')} initialValue={this.state.second_name}
                                           name="second_name"
                                           rules={[{required: true, message: 'Введите свою фамилию'}]}>
                                    <Input/>
                                </Form.Item>
                                <Form.Item label={t('middle_name')} name={"middle_name"}
                                           initialValue={this.state.middle_name}>
                                    <Input/>
                                </Form.Item>
                                <Form.Item label={t('gender')} name={"gender"}
                                           rules={[{required: true, message: t('gender')}]}
                                           initialValue={this.state.gender}>
                                    <Radio.Group>
                                        <Radio value={"М"}>{t('male')}</Radio>
                                        <Radio value={"Ж"}>{t('female')}</Radio>
                                    </Radio.Group>
                                </Form.Item>
                                <Form.Item label={t('date_of_birth')} name={"birth_date"}
                                           rules={[{required: true, message: t('date_of_birth__error')}]}
                                           initialValue={this.state.birth_date ? moment(this.state.birth_date) : moment(new Date(2001, 1, 1))}>
                                    <DatePicker style={{width: "100%"}}
                                                locale={locale}
                                                format={'DD.MM.YYYY'}
                                                placeholder={t('date_of_birth__placeholder')}/>
                                </Form.Item>

                                <Form.Item initialValue={this.state.city}
                                           name={"city"}
                                           rules={[{required: true, message: t('city__error')}]}
                                           label={t('city')}>
                                    <AutoComplete
                                        notFoundContent={this.state.fetchingCountry ?
                                            <Spin size="small"/> : null}
                                        filterOption={false}
                                        onSearch={this.onAutoCompleteCountry}
                                        onChange={this.handleChangeCity}
                                        options={this.state.autoCompleteCityData}
                                        style={{width: '100%'}}
                                    />
                                </Form.Item>
                                <Form.Item initialValue={this.state.city}
                                           rules={[{required: true, message: t('city_born__error')}]}
                                           name={"born_city"}
                                           label={t('city_born')}>
                                    <AutoComplete
                                        notFoundContent={this.state.fetchingCountry ?
                                            <Spin size="small"/> : null}
                                        filterOption={false}
                                        onSearch={this.onAutoCompleteCountry}
                                        onChange={this.handleChangeBornCity}
                                        options={this.state.autoCompleteCityData}
                                        style={{width: '100%'}}
                                    />
                                </Form.Item>
                                <Form.Item label="Telegram" name={"telegram_link"}
                                           rules={[{required: true, message: t('telegram_link_error')}]}
                                           initialValue={this.state.telegram_link}>
                                    <Input addonBefore="https://t.me/"
                                           onPaste={this.handleTelegram}/>
                                </Form.Item>
                                <Form.Item label={t('phone')} name={"phone"}
                                           initialValue={this.state.phone ? this.state.phone : '+7/'}
                                           extra={t('phone_extra')}>
                                    <CountryPhoneInput/>
                                </Form.Item>

                            </Col>
                        </div>
                    </div>
                    <div className="code-box mt-4">
                        <div className="container my-4">
                            <Title style={{textAlign: "center"}} className={"mb-4 text-primary"}
                                   level={1}>{t('education')}</Title>
                            <Col xs={24} sm={24} md={24} lg={{span: 14, offset: 5}}>
                                <Form.Item label={t('where_to_study')} initialValue={this.state.where_to_study}
                                           rules={[{required: true, message: t('where_to_study_error')}]}
                                           name={"where_to_study"}>
                                    <Select onChange={this.handleChangeWhereLearning}>
                                        <Select.Option value="school">{t('where_to_study_school')}</Select.Option>
                                        <Select.Option value="college">{t('where_to_study_college')}</Select.Option>
                                        <Select.Option value="univ">{t('where_to_study_univ')}</Select.Option>
                                        <Select.Option
                                            value="no_learning">{t('where_to_study_no_learning')}</Select.Option>
                                    </Select>
                                </Form.Item>
                                {this.state.where_to_study == 'school' ?
                                    <React.Fragment>
                                        <Form.Item label={t('institute')}
                                                   initialValue={this.state.institute}
                                                   rules={[{required: true, message: t('institute_error')}]}
                                                   name={"institute"}>
                                            <AutoComplete notFoundContent={this.state.fetchingCountry ?
                                                <Spin size="small"/> : null}
                                                          filterOption={false}
                                                          onSearch={this.onAutoCompleteSchool}
                                                          onChange={this.handleChangeSchool}
                                                          options={this.state.autoCompleteSchool}
                                                          style={{width: '100%'}}
                                            />
                                        </Form.Item>

                                        <Form.Item label={t('learning_class')}
                                                   rules={[{required: true, message: t('learning_class__error')}]}
                                                   initialValue={this.state.learning_class}
                                                   name={"learning_class"}>
                                            <InputNumber min={9} max={11}/>
                                        </Form.Item>

                                    </React.Fragment>
                                    : null}
                                {this.state.where_to_study == "college" ?
                                    <React.Fragment>
                                        <Form.Item label={t('institute')}
                                                   initialValue={this.state.institute}
                                                   rules={[{required: true, message: t('institute_error')}]}
                                                   name={"institute"}>
                                            <AutoComplete notFoundContent={this.state.fetchingCountry ?
                                                <Spin size="small"/> : null}
                                                          filterOption={false}
                                                          onSearch={this.onAutoCompleteCollage}
                                                          onChange={this.handleChangeCollage}
                                                          options={this.state.autoCompleteCollage}
                                                          style={{width: '100%'}}
                                            />
                                        </Form.Item>
                                        <Form.Item
                                            label={t('learning_course')}
                                            rules={[{required: true, message: t('learning_course__error')}]}
                                            initialValue={this.state.learning_class}
                                            name={"learning_class"}>
                                            <InputNumber min={1} max={4}/>
                                        </Form.Item>
                                        <Form.Item label={t('specialization')}
                                                   name={"specialization"}
                                                   rules={[{
                                                       required: true,
                                                       message: t('specialization__error')
                                                   }]}
                                                   initialValue={this.state.specialization}>
                                            <AutoComplete notFoundContent={this.state.fetchingCountry ?
                                                <Spin size="small"/> : null}
                                                          filterOption={false}
                                                          onSearch={this.onAutoCompleteInstituteSpecialization}
                                                          onChange={this.handleChangeInstituteSpecialization}
                                                          options={this.state.autoCompleteSpecialization}
                                                          style={{width: '100%'}}
                                            />
                                        </Form.Item>
                                    </React.Fragment>
                                    : null}
                                {this.state.where_to_study == "univ" ?
                                    <React.Fragment>
                                        <Form.Item label={t('institute_univ')}
                                                   initialValue={this.state.institute}
                                                   rules={[{required: true, message: t('institute_univ__error')}]}
                                                   name={"institute"}>
                                            <AutoComplete notFoundContent={this.state.fetchingCountry ?
                                                <Spin size="small"/> : null}
                                                          filterOption={false}
                                                          onSearch={this.onAutoCompleteInstitute}
                                                          onChange={this.handleChangeInstitute}
                                                          options={this.state.autoCompleteInstitute}
                                                          style={{width: '100%'}}
                                            />
                                        </Form.Item>
                                        <Form.Item label={t('specialization')}
                                                   name={"specialization"}
                                                   rules={[{
                                                       required: true,
                                                       message: t('specialization__error')
                                                   }]}
                                                   initialValue={this.state.specialization}>
                                            <AutoComplete notFoundContent={this.state.fetchingCountry ?
                                                <Spin size="small"/> : null}
                                                          filterOption={false}
                                                          onSearch={this.onAutoCompleteInstituteSpecialization}
                                                          onChange={this.handleChangeInstituteSpecialization}
                                                          options={this.state.autoCompleteSpecialization}
                                                          style={{width: '100%'}}
                                            />
                                        </Form.Item>

                                        <Form.Item
                                            label={t('learning_course')}
                                            rules={[{required: true, message: t('learning_course__error')}]}
                                            initialValue={this.state.learning_class}
                                            name={"learning_class"}>
                                            <InputNumber min={1} max={6}/>
                                        </Form.Item>

                                        <Form.Item label={t("science_degree")}
                                                   initialValue={this.state.science_degree}
                                                   rules={[{required: true, message: t('science_degree__error')}]}
                                                   name={"science_degree"}>
                                            <Select>
                                                <Select.Option value="Бакалавр">{t('bachelor')}</Select.Option>
                                                <Select.Option value="Магистр">{t('master')}</Select.Option>
                                                <Select.Option value="Специалист">{t('specialist')}</Select.Option>
                                                <Select.Option value="Аспирант">{t('postgraduate')}</Select.Option>
                                            </Select>
                                        </Form.Item>
                                    </React.Fragment> : null
                                }
                            </Col>
                        </div>
                    </div>


                    <div className="code-box mt-4">
                        <div className="container my-4">
                            <Title style={{textAlign: "center"}} className={"mb-4 text-primary"}
                                   level={1}>{t('skill_and_interests')}</Title>

                            <Col xs={24} sm={24} md={24} lg={{span: 14, offset: 5}}>

                                <Form.Item label={t('type_developer')} name={"type_developer"}
                                           initialValue={this.state.type_developer}>
                                    <Input placeholder={t('type_developer__placeholder')}/>

                                </Form.Item>
                                <Form.Item label={t('employment_form')} name={"employment_form"}
                                           initialValue={this.state.employment_form}>
                                    <Select>
                                        <Select.Option value="Full-time">{t('fulltime')}</Select.Option>
                                        <Select.Option value="Part-time">{t('parttime')}</Select.Option>
                                        <Select.Option value="Не работаю">{t('dontwork')}</Select.Option>
                                    </Select>
                                </Form.Item>
                                <Form.Item label={t('seniority')} name={"seniority"}
                                           initialValue={this.state.seniority}>
                                    <InputNumber min={0} max={50}/>
                                </Form.Item>
                                <Form.Item label={t('portfolio')} name={"github"} initialValue={this.state.github}
                                >
                                    <Input addonBefore={<GithubFilled/>}
                                           placeholder={"GitHub"}/>
                                </Form.Item>
                                <Form.Item name={"gitlab"} initialValue={this.state.gitlab}>
                                    <Input addonBefore={<GitlabFilled/>} placeholder={"GitLab"}/>
                                </Form.Item>
                                <Form.Item name={"linkedin"} initialValue={this.state.linkedin}>
                                    <Input addonBefore={<LinkedinFilled/>} placeholder={"LinkedIn"}/>
                                </Form.Item>
                                <Form.Item name={"hh"} initialValue={this.state.hh}>
                                    <Input addonBefore="https://hh.ru/"/>
                                </Form.Item>
                                <Form.Item name={"site"} initialValue={this.state.site}>
                                    <Input addonBefore="https://"/>
                                </Form.Item>


                                <Form.Item label={t('resume')}>
                                    <Upload
                                        name={"resume_file"}
                                        type={'select'}
                                        maxCount={1}
                                        accept={".pdf, .doc, .docx"}
                                        onChange={this.onUploadCV}
                                        defaultFileList={this.state.renderResume ? [this.state.renderResume] : null}
                                        multiple={false}
                                        action={`${URl_BACKEND}/api/auth/profile/upload_cv/`}
                                        headers={{
                                            authorization: `Bearer ${this.props.access_token}`,
                                        }}>
                                        <Button>
                                            <UploadOutlined/> {t('upload_resume')}
                                        </Button>
                                    </Upload>
                                </Form.Item>
                                <Form.Item label={t('resume_text')}
                                           name={"resume_text"} initialValue={this.state.resume_text}>
                                    <TextArea rows={4}/>
                                </Form.Item>

                                <Form.Item label={t('hack_visited')}
                                           name={"hack_visited"} initialValue={this.state.hack_visited}>
                                    <Select>
                                        <Select.Option value="0-1">0-1</Select.Option>
                                        <Select.Option value="2-5">2-5</Select.Option>
                                        <Select.Option value="5-8">5-8</Select.Option>
                                        <Select.Option value="10+">10+</Select.Option>
                                    </Select>
                                </Form.Item>
                                <Form.Item label={t('skills')}
                                           name={"skills"}
                                           initialValue={this.state.render_skills}
                                           extra={t('skills__extra')}>

                                    <Select
                                        mode="tags"
                                        labelInValue
                                        tokenSeparators={[',']}

                                        placeholder={t('skills__placeholder')}
                                        notFoundContent={this.state.fetchingCountry ? <Spin size="small"/> : null}
                                        onSearch={this.fetchUser}
                                        onChange={this.handleChange}
                                        style={{width: '100%'}}>
                                        {this.state.autoSkillComplete.map(d => (
                                            <Option key={d.value} value={d.label}>{d.label}</Option>
                                        ))}
                                    </Select>

                                </Form.Item>


                            </Col>
                        </div>
                    </div>

                    <div className="code-box mt-4">
                        <div className="container my-4">
                            <Title style={{textAlign: "center"}} className={"mb-4 text-primary"}
                                   level={1}>{t('final_question')}</Title>

                            <Col xs={24} sm={24} md={24} lg={{span: 14, offset: 5}}>
                                <Form.Item label={t('select_task')}
                                           name={"select_task"}
                                           initialValue={this.props.profile.select_task ? this.props.profile.select_task.map((item) => {
                                               return item
                                           }) : []}
                                           extra={t('select_task__extra')}>
                                    <Select mode="multiple" style={{width: '100%'}}
                                            onChange={this.handleChangeTasks}
                                            notFoundContent={t('select_task__no_found')}
                                            tokenSeparators={[',']} maxTagCount={3}>
                                        {
                                            this.state.tasksList && this.state.selected_tasks.length < 3 && this.state.tasksList.map(item => {
                                                return <Option key={item.name} value={item.id}>{item.name}</Option>
                                            })
                                        }
                                        {
                                            this.state.selected_tasks.length >= 3 ? this.state.tasksList.filter(item => this.state.selected_tasks.includes(item.id)).map(item => {
                                                return <Option key={item.name} value={item.id}>{item.name}</Option>
                                            }) : null
                                        }
                                    </Select>
                                </Form.Item>

                                <Form.Item
                                    initialValue={this.state.want_take_part} name={"want_take_part"}
                                    label={t('want_take_part')}>
                                    <TextArea rows={4}/>
                                </Form.Item>
                                <Form.Item label={t("expect_from_hack")}
                                           name={"expect_from_hack"} initialValue={this.state.expect_from_hack}>
                                    <TextArea rows={4}/>
                                </Form.Item>
                                <Form.Item label={t('something_else')}
                                           name={"something_else"} initialValue={this.state.something_else}>
                                    <TextArea rows={4}/>
                                </Form.Item>
                                <Form.Item
                                    label={t("where_did_you_know")}
                                    name="where_did_you_know" initialValue={this.state.where_did_you_know}>

                                    <Select>
                                        <Select.Option value="hack">{t('hack')}</Select.Option>
                                        <Select.Option value="site_mai">{t('site_mai')}</Select.Option>
                                        <Select.Option value="mail">{t('mail')}</Select.Option>
                                        <Select.Option value="social">{t('social')}</Select.Option>
                                        <Select.Option value="friend">{t('friend')}</Select.Option>
                                        <Select.Option value="other">{t('other')}</Select.Option>
                                    </Select>

                                </Form.Item>

                            </Col>
                            <Form.Item wrapperCol={{span: 24}}>
                                <Button style={{width: "100%"}} className="mr-2" type="primary"
                                        onClick={() => this.onSubmit}
                                        htmlType="submit">
                                    {t('common:save')}
                                </Button>
                            </Form.Item>
                        </div>
                    </div>

                </Form>


            </React.Fragment>

        )
    }
}


const mapDispatchToProps = (dispatch) => {
    return {
        actionAuth: bindActionCreators(AuthAction, dispatch),
    };
};
export default withTranslation(withExpiredToken(withAuthAndPrivateHackerRoute(connect((state => (state.auth)), mapDispatchToProps)(questionary))), "questionary");

