import Link from "next/link";
import {connect} from "react-redux";
import React, {useState} from 'react';
import {bindActionCreators} from "redux";
import useTranslation from "next-translate/useTranslation";
import {LockOutlined, MailOutlined} from '@ant-design/icons';
import {Card, Row, Col, Button, Form, Input, notification, Checkbox, Result} from "antd";
// Дейсвтия
import {AuthAction} from '../redux/actions/auth.action'
// Роутинг
import withAuthRoute from '../route/withAuthRoute';
import {MathModeIcon} from "../components/ExternalIcon/MathMode";

const backgroundStyle = {
    backgroundImage: 'url(/img/others/img-17.jpg)',
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'cover'
}

interface Props {
    actionAuth: any,
    registerComplete: any
}

const secret_key = "mathematical-modeling-is-not-so-difficult"

const SecretForm = (props) => {
    const {t} = useTranslation('registration')

    const onSignUp = (form) => {
        if (secret_key === form['secret']) props.complete()
        else {
            notification.error({
                message: "Ошибка",
                description: "Неверный секретный код",
                duration: 5,
            })
        }
    }
    return (
        <div className="my-2">
            <div className="text-center">
                <MathModeIcon style={{fontSize: "200px"}}/>
            </div>
            <Row justify="center">
                <Col xs={24} sm={24} md={20} lg={20}>
                    <Form layout="vertical" name="register-form"
                          onFinish={onSignUp}>

                        <Form.Item
                            name="secret"
                            label="Введите секретный код"
                            rules={[
                                {
                                    required: true,
                                    message: "Введите секретный код",
                                },
                            ]}
                            hasFeedback>
                            <Input/>
                        </Form.Item>
                        <Form.Item>
                            <Button type="primary" htmlType="submit" block>
                                {t('common:send')}
                            </Button>
                        </Form.Item>
                    </Form>
                </Col>
            </Row>
        </div>

    )
}

export const registration = (props: Props) => {
    const [registerComplete, setRegisterComplete] = useState(false)
    const [existSecretKey, setExistSecretKey] = useState(false)
    const {t} = useTranslation('registration')
    let loading = false;

    const openRegForm = () => {
        setExistSecretKey(true)
    }

    const onSignUp = (form) => {
        loading = true;
        props.actionAuth.registerExpert(form).then((e) => {
            setRegisterComplete(true)
        }).catch(error => {
            error.response.json().then((e) => {
                try {
                    e.password.map(item => {
                        notification.error({
                            message: t('common:error'),
                            description: item,
                            duration: 5,
                        })
                    })
                } catch {
                    e.email.map(item => {
                        if (item === 'user с таким email уже существует.') {
                            notification.error({
                                message: t('common:error'),
                                description: t('user_exist'),
                                duration: 5,
                            })
                        }
                    })
                }
            })
        })
    }

    return (
        <div className={'auth-container'} style={registerComplete ? {height: "100vh"} : null}>
            <div className="h-100" style={backgroundStyle}>
                <div className="container d-flex flex-column justify-content-center h-100"
                     style={{padding: "20px 0"}}>
                    <Row justify="center">
                        <Col xs={20} sm={20} md={20} lg={8}>
                            <Card>
                                {registerComplete ?
                                    <Result
                                        status="success"
                                        title={t('success')}
                                        subTitle={t('check_email')}
                                        extra={[
                                            <Link href={"/login"}>
                                                <a>
                                                    <Button type="primary">
                                                        {t('login:enter')}
                                                    </Button>
                                                </a>
                                            </Link>,
                                        ]}
                                    /> : existSecretKey ? <div className="my-2">
                                        <div className="text-center">
                                            <img className="img-fluid"
                                                 style={{maxHeight: "240px"}}
                                                 src={`/images/Aviahackathon_logo_end_100х70 для светлого фона.png`}
                                                 alt=""/>

                                        </div>
                                        <Row justify="center">
                                            <Col xs={24} sm={24} md={20} lg={20}>
                                                <Form layout="vertical" name="register-form"
                                                      onFinish={onSignUp}>

                                                    <Form.Item
                                                        name="email"
                                                        label="Email"
                                                        rules={[
                                                            {
                                                                required: true,
                                                                message: t('login:enter_email'),
                                                            },
                                                            {
                                                                type: 'email',
                                                                message: t('login:enter_valid_email')
                                                            }
                                                        ]}
                                                        hasFeedback>
                                                        <Input prefix={<MailOutlined
                                                            className="text-primary"/>}/>
                                                    </Form.Item>
                                                    <Form.Item
                                                        name="password"
                                                        label={t('password')}
                                                        rules={[
                                                            {
                                                                required: true,
                                                                message: t('password_valid')
                                                            }
                                                        ]}
                                                        hasFeedback>
                                                        <Input.Password
                                                            prefix={<LockOutlined
                                                                className="text-primary"/>}/>
                                                    </Form.Item>
                                                    <Form.Item
                                                        name="password_confirm"
                                                        label={t('confirm_password')}
                                                        rules={[
                                                            {
                                                                required: true,
                                                                message: t('confirm_password')
                                                            },
                                                            ({getFieldValue}) => ({
                                                                validator(rule, value) {
                                                                    if (!value || getFieldValue('password') === value) {
                                                                        return Promise.resolve();
                                                                    }
                                                                    return Promise.reject(t('invalid_password'));
                                                                },
                                                            })
                                                        ]}
                                                        hasFeedback
                                                    >
                                                        <Input.Password
                                                            prefix={<LockOutlined
                                                                className="text-primary"/>}/>
                                                    </Form.Item>
                                                    <Form.Item
                                                        name="accept_reglament"
                                                        rules={[
                                                            {
                                                                required: true,
                                                                message: t('accept_reglament')
                                                            }
                                                        ]}
                                                        valuePropName="checked"
                                                        hasFeedback>
                                                        <Checkbox>
                                                            <Link href={"/soglasie_na_obrabotky.pdf"}>
                                                                <a target="_blank">{t('agree')}</a>
                                                            </Link>
                                                        </Checkbox>
                                                    </Form.Item>
                                                    <Form.Item>
                                                        <Button type="primary" htmlType="submit" block
                                                                loading={loading}>
                                                            {t('common:send')}
                                                        </Button>
                                                    </Form.Item>
                                                </Form>
                                            </Col>
                                        </Row>
                                    </div> : <SecretForm complete={openRegForm}/>


                                }
                            </Card>
                        </Col>
                    </Row>
                </div>
            </div>

        </div>
    )

}

const mapDispatchToProps = (dispatch) => {
    return {
        actionAuth: bindActionCreators(AuthAction, dispatch),
    };
};
export default withAuthRoute(connect(null, mapDispatchToProps)(registration))

