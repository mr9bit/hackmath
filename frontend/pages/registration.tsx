import Link from "next/link";
import {connect} from "react-redux";
import React, {useState} from 'react';
import {bindActionCreators} from "redux";
import useTranslation from "next-translate/useTranslation";
import {LockOutlined, MailOutlined} from '@ant-design/icons';
import {Card, Row, Col, Button, Form, Input, notification, Checkbox, Result} from "antd";
// Дейсвтия
import {AuthAction} from '../redux/actions/auth.action'
// Роутинг
import withAuthRoute from '../route/withAuthRoute';
import {index} from "./index";
import {URl_BACKEND} from "../redux/settings";
import {MathModeIcon} from "../components/ExternalIcon/MathMode";

const backgroundStyle = {
    backgroundImage: 'url(/img/others/img-17.jpg)',
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'cover'
}

interface Props {
    actionAuth: any,
    registerComplete: any
    close: boolean
}

export const registration = (props: Props) => {
    const [registerComplete, setRegisterComplete] = useState(false)
    const {t} = useTranslation('registration')
    let loading = false;

    const onSignUp = (form) => {
        loading = true;
        props.actionAuth.register(form).then((e) => {
            setRegisterComplete(true)
        }).catch(error => {
            error.response.json().then((e) => {
                try {
                    e.password.map(item => {
                        notification.error({
                            message: t('common:error'),
                            description: item,
                            duration: 5,
                        })
                    })
                } catch {
                    e.email.map(item => {
                        if (item === 'user с таким email уже существует.') {
                            notification.error({
                                message: t('common:error'),
                                description: t('user_exist'),
                                duration: 5,
                            })
                        }
                    })
                }
            })
        })
    }
    console.log(props);
    return (
        <div className={'auth-container'} style={registerComplete ? {height: "100vh"} : null}>
            <div className="h-100" style={backgroundStyle}>
                <div className="container d-flex flex-column justify-content-center h-100"
                     style={{padding: "20px 0"}}>
                    <Row justify="center">
                        <Col xs={20} sm={20} md={20} lg={8}>
                            <Card>
                                {
                                    props.close ? <Result
                                            status="success"
                                            title={"Регистрация завешена!"}
                                            subTitle={"Спасибо за проявление активности к нашему мероприятияю, посмотри какие мероприятия планируются в ближайшее время."}
                                            extra={[
                                                <Link href={"https://mai.ru/press/events/detail.php?ID=122028"}>
                                                    <a>
                                                        <Button type="primary">
                                                            Посмотреть
                                                        </Button>
                                                    </a>
                                                </Link>,
                                            ]}
                                        />
                                        :
                                        registerComplete ? <Result
                                                status="success"
                                                title={t('success')}
                                                subTitle={t('check_email')}
                                                extra={[
                                                    <Link href={"/login"}>
                                                        <a>
                                                            <Button type="primary">
                                                                {t('login:enter')}
                                                            </Button>
                                                        </a>
                                                    </Link>,
                                                ]}
                                            /> :
                                            <div className="my-2">
                                                <div className="text-center">
                                                    <MathModeIcon style={{fontSize: "200px"}}/>

                                                    <p className="text-muted">{t('create')}</p>
                                                </div>
                                                <Row justify="center">
                                                    <Col xs={24} sm={24} md={20} lg={20}>
                                                        <Form layout="vertical" name="register-form"
                                                              onFinish={onSignUp}>

                                                            <Form.Item
                                                                name="email"
                                                                label="Email"
                                                                rules={[
                                                                    {
                                                                        required: true,
                                                                        message: t('login:enter_email'),
                                                                    },
                                                                    {
                                                                        type: 'email',
                                                                        message: t('login:enter_valid_email')
                                                                    }
                                                                ]}
                                                                hasFeedback>
                                                                <Input prefix={<MailOutlined
                                                                    className="text-primary"/>}/>
                                                            </Form.Item>
                                                            <Form.Item
                                                                name="password"
                                                                label={t('password')}
                                                                rules={[
                                                                    {
                                                                        required: true,
                                                                        message: t('password_valid')
                                                                    }
                                                                ]}
                                                                hasFeedback>
                                                                <Input.Password
                                                                    prefix={<LockOutlined
                                                                        className="text-primary"/>}/>
                                                            </Form.Item>
                                                            <Form.Item
                                                                name="password_confirm"
                                                                label={t('confirm_password')}
                                                                rules={[
                                                                    {
                                                                        required: true,
                                                                        message: t('confirm_password')
                                                                    },
                                                                    ({getFieldValue}) => ({
                                                                        validator(rule, value) {
                                                                            if (!value || getFieldValue('password') === value) {
                                                                                return Promise.resolve();
                                                                            }
                                                                            return Promise.reject(t('invalid_password'));
                                                                        },
                                                                    })
                                                                ]}
                                                                hasFeedback
                                                            >
                                                                <Input.Password
                                                                    prefix={<LockOutlined
                                                                        className="text-primary"/>}/>
                                                            </Form.Item>
                                                            <Form.Item
                                                                name="accept_reglament"
                                                                rules={[
                                                                    {
                                                                        required: true,
                                                                        message: t('accept_reglament')
                                                                    }
                                                                ]}
                                                                valuePropName="checked"
                                                                hasFeedback>
                                                                <Checkbox>
                                                                    <Link href={"/soglasie_na_obrabotky.pdf"}>
                                                                        <a target="_blank">{t('agree')}</a>
                                                                    </Link>
                                                                </Checkbox>
                                                            </Form.Item>
                                                            <Form.Item>
                                                                <Button type="primary" htmlType="submit" block
                                                                        loading={loading}>
                                                                    {t('common:send')}
                                                                </Button>
                                                            </Form.Item>
                                                        </Form>
                                                    </Col>
                                                </Row>
                                            </div>
                                }

                            </Card>
                        </Col>
                    </Row>
                </div>
            </div>

        </div>
    )
}

registration.getInitialProps = async (ctx) => {
    try {
        let url = `${URl_BACKEND}/api/check_reg`;
        console.log(url)
        let response = await fetch(url);
        console.log(response)
        let commits = await response.json(); // читаем ответ в формате JSON
        const {close} = commits
        return {close}
    } catch (err) {
        console.log("regError", err)
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        actionAuth: bindActionCreators(AuthAction, dispatch),
    };
};
export default withAuthRoute(connect(null, mapDispatchToProps)(registration))
