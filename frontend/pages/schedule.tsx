import {ScheduleAction} from "../redux/actions/schedule.action";
import withTranslation from "next-translate/withTranslation";
import withExpiredToken from "../route/withExpiredToken";
import withAuthAndPrivateHackerRoute from "../route/withAuthAndPrivateHackerRoute";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {TeamAction} from "../redux/actions/team.action";
import {
    PushpinFilled,
    ClockCircleFilled,
    CalendarOutlined,
    UserOutlined
} from '@ant-design/icons';
import {Row, Col, Card} from 'antd'
import moment from "moment";

const Agenda = (props) => {
    const {list} = props
    return <div>
        {
            list.map(item => {
                return <div key={`${item.title}-${item.time_end}`} className="calendar-list-item">
                    <div className="d-flex">
                        <div>
                            <h5 className="mb-1">{item.title}</h5>
                            <span className="text-muted"><PushpinFilled/> {item.location}</span><br/>
                            {
                                item.speaker ? <><span
                                        className="text-muted"><UserOutlined/> {item.speaker}</span><br/></>
                                    : null
                            }
                            <span className="text-muted"><ClockCircleFilled/> {
                                moment(item.time_start).lang("ru").format('LT')
                            } - {moment(item.time_end).lang("ru").format('LT')}</span>
                        </div>
                    </div>
                </div>

            })
        }

    </div>
}


const schedule = (props) => {
    return (
        <div>
            <Row gutter={16}>
                {
                    props.schedule.schedule.map(item => {
                        return <Col md={8} xs={24}>
                            <Card className="calendar mb-1">

                                <div className="calendar-list">
                                    <h4>
                                        <CalendarOutlined/>
                                        <span className="ml-2">{moment(item.day).lang("ru").format('dddd')}</span>
                                    </h4>
                                    <Agenda list={item.results}/>

                                </div>
                            </Card>
                        </Col>

                    })
                }

            </Row>
        </div>
    )
}


schedule.getInitialProps = async (ctx) => {
    await ctx.store.dispatch(ScheduleAction.get())
    const {schedule} = mapStateToProps(ctx.store.getState())
    return {schedule}
}
const mapStateToProps = (state) => ({schedule: state.schedule,});

const mapDispatchToProps = (dispatch) => {
    return {scheduleAction: bindActionCreators(ScheduleAction, dispatch),};
};

export default withExpiredToken(withAuthAndPrivateHackerRoute(connect((state => ({
    auth: state.auth
})), mapDispatchToProps)(schedule)))

