import React, {Component, Fragment} from "react";
import {wrapper} from "../redux";
import {authLoginUserSuccess, authMeInfo, authLogout} from '../redux/actions/auth.action';
import {onSwitchTheme} from '../redux/actions/theme.action';
import jsHttpCookie from 'cookie';
import App, {AppContext} from 'next/app';
import Base from '../components/layout/base'
import Head from 'next/head'
import {tokenExpired, parseJwt} from "../utils/jwt";
import {MAIN_URL} from '../redux/settings'
import {ConfigProvider, Layout} from "antd";
import AppLayout from '../components/layout/main'
import {ThemeSwitcherProvider} from "react-css-theme-switcher";

const themes = {
    light: `${MAIN_URL}/css/light-theme.css`,
    dark: `${MAIN_URL}/css/dark-theme.css`,
};


class MyApp extends App {
    public static getInitialProps = async ({Component, ctx}: AppContext) => {
        if (ctx.req && ctx.req.headers) {
            try {
                const cookies = ctx.req.headers.cookie;
                if (typeof cookies === 'string') {
                    const cookiesJSON = jsHttpCookie.parse(cookies);
                    if (!tokenExpired(cookiesJSON.access_token)) { // Если куки просрочен, то игнорим
                        if (!parseJwt(cookiesJSON.access_token).user) {
                            ctx.store.dispatch(authMeInfo({user: JSON.parse(cookiesJSON.user)}));
                        } else {
                            ctx.store.dispatch(authMeInfo({user: parseJwt(cookiesJSON.access_token).user}));
                        }
                        ctx.store.dispatch(authLoginUserSuccess({access: cookiesJSON.access_token}));
                        if (cookiesJSON.theme) {
                            ctx.store.dispatch(onSwitchTheme(cookiesJSON.theme));
                        }
                    }

                }
            } catch (e) {
                // Failed to parse
                ctx.store.dispatch(authLogout());
            }
        }

        return {
            pageProps: {
                // Call page-level getInitialProps
                ...(Component.getInitialProps ? await Component.getInitialProps(ctx) : {}),
                // Some custom thing for all pages
            }
        };

    };

    public render() {
        const {Component, pageProps} = this.props;
        return (
            <Fragment>
                <Head>
                    <title>Весенняя школа Математического Моделирования МАИ 2021</title>
                    <link rel="shortcut icon" href="/favicon/icon.ico"/>
                    <link rel="shortcut icon" href="/favicon/icon.png"/>
                    <link rel="apple-touch-icon" sizes="72x72" href="/favicon/icon_apple_72.png"/>
                    <link rel="apple-touch-icon" sizes="114x114" href="/favicon/icon_apple_114.png"/>
                    <link rel="canonical" href="https://event.lambda-it.ru"/>
                    <meta name="yandex-verification" content="6ac6d5644b12ce64"/>
                    <meta name="viewport" content="initial-scale=1.0, width=device-width"/>
                    <meta property="og:locale" content="ru_RU"/>
                    <meta property="og:site_name" content="Весенняя школа Математического Моделирования 2021"/>
                    <meta property="og:title" content="Весенняя школа Математического Моделирования 2021"/>
                    <link rel="prefetch" type="text/css" id="theme-prefetch-light"
                          href={`${MAIN_URL}/css/light-theme.css`}/>
                    <link rel="prefetch" type="text/css" id="theme-prefetch-dark"
                          href={`${MAIN_URL}/css/dark-theme.css`}/>
                    <link rel="stylesheet" type="text/css" href={`${MAIN_URL}/css/index.css`}/>
                </Head>

                <ConfigProvider>
                    <ThemeSwitcherProvider themeMap={themes} defaultTheme={'light'}>
                        {pageProps.userAuth && pageProps.userAuth.isAuthenticated ?
                            <AppLayout>
                                <Component {...pageProps} />
                            </AppLayout>
                            :
                            <Base>
                                <Layout>
                                    <Component {...pageProps} />
                                </Layout>
                            </Base>
                        }
                    </ThemeSwitcherProvider>
                </ConfigProvider>

            </Fragment>
        );
    }
}

export default wrapper.withRedux(MyApp);
