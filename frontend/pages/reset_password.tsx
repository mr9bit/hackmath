import React from 'react';
import Link from "next/link";
import Router from "next/router";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {LockOutlined, MailOutlined} from '@ant-design/icons';
import {Button, Form, Input, Result, Card, Row, Col, notification} from 'antd';
// Дейсвтия
import {AuthAction} from '../redux/actions/auth.action'
// Роутинг
import withAuthRoute from '../route/withAuthRoute';
import withTranslation from "next-translate/withTranslation";

const backgroundStyle = {
    backgroundImage: 'url(/img/others/img-17.jpg)',
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'cover'
}

interface Props {
    actionAuth: any,
    query: any,
    reset: boolean,
    i18n: any
}

interface State {
    complete: boolean,
    password_reset_confirm: boolean,
}

const rules = {
    password: [
        {
            required: true,
            message: 'Ввидите новый пароль'
        }
    ],
    confirm: [
        {
            required: true,
            message: 'Подтвердите пароль!'
        },
        ({getFieldValue}) => ({
            validator(rule, value) {
                if (!value || getFieldValue('password') === value) {
                    return Promise.resolve();
                }
                return Promise.reject('Пароли не совподают!');
            },
        })
    ]
}

class reset_password extends React.Component<Props, State> {

    static async getInitialProps(ctx) {
        const query = ctx.query;
        if (Object.keys(query).length === 0) {
            let reset = false
            return {query, reset}
        } else {
            let reset = true
            return {query, reset}
        }
    }

    constructor(props) {
        super(props);
        this.onSend = this.onSend.bind(this);
        this.onConfirmReset = this.onConfirmReset.bind(this);
        this.state = {
            complete: false,
            password_reset_confirm: false
        }
    }


    onSend = (form) => {
        const {t, lang} = this.props.i18n;
        AuthAction.reset_password_link(form).then((e) => {
            this.setState({complete: true})
        }).catch((e) => {
            notification.error({
                message: t("common:error"),
                description: t('user_already_exist'),
            })
        })
    }

    onConfirmReset = (form) => {
        const data = {
            ...this.props.query,
            password: form.password
        }
        const {t, lang} = this.props.i18n;
        AuthAction.reset_password(data).then((e) => {
            this.setState({password_reset_confirm: true})
        }).catch((error) => {
            error.response.json().then((e) => {
                try {
                    e.password.map(item => {
                        notification.error({
                            message: t('common:error'),
                            description: item,
                            duration: 5,
                        })
                    })
                } catch {
                    if (e.detail === 'Signature expired') {
                        notification.error({
                            key: 'error',
                            message: t('common:error'),
                            description: t('time_out'),
                            btn: (
                                <Button type="primary" size="small" onClick={() => {
                                    Router.push('/reset_password')
                                    notification.close('error')
                                }}>
                                    {t('new_reset')}
                                </Button>
                            ),
                        })
                    } else if (e.detail === 'User not found') {
                        notification.error({
                            key: 'error',
                            message: t('time_out'),
                            description: t('login:user_not_found'),
                            btn: (
                                <Button type="primary" size="small" onClick={() => {
                                    Router.push('/reset_password')
                                    notification.close('error')
                                }}>
                                    {t('new_reset')}
                                </Button>
                            ),
                        })
                    } else if (e.detail === 'Invalid signature') {
                        notification.error({
                            key: 'error',
                            message: t('common:error'),
                            description: t('wrong_secret_key'),
                            btn: (
                                <Button type="primary" size="small" onClick={() => {
                                    Router.push('/reset_password')
                                    notification.close('error')
                                }}>
                                    {t('new_reset')}
                                </Button>
                            ),
                        })
                    }

                }
            })
        })
    }

    render() {
        const {t, lang} = this.props.i18n;
        return (
            <div className={'auth-container'}>
                <div className="h-100" style={backgroundStyle}>
                    <div className="container d-flex flex-column justify-content-center h-100">
                        <Row justify="center">
                            <Col xs={20} sm={20} md={20} lg={9}>
                                <Card>
                                    {
                                        this.props.reset ?
                                            this.state.password_reset_confirm ?
                                                <Result
                                                    status="success"
                                                    title={t('set_new_password')}
                                                    subTitle={t('set_new_password_sub')}
                                                    extra={[
                                                        <Link href={"/login"}>
                                                            <a>
                                                                <Button type="primary">
                                                                    {t('common:send')}
                                                                </Button>
                                                            </a>
                                                        </Link>,
                                                    ]}
                                                />
                                                :
                                                <div className="my-2">
                                                    <div className="text-center">
                                                        <img className="img-fluid"
                                                             style={{height: "240px"}}
                                                             src={`/images/Aviahackathon_logo_end_100х70 для светлого фона.png`}
                                                             alt=""/>
                                                        <h3 className="mt-3 font-weight-bold">{t('enter_new_password')}</h3>

                                                    </div>
                                                    <Row justify="center">
                                                        <Col xs={24} sm={24} md={20} lg={20}>
                                                            <Form layout="vertical" name="forget-password"
                                                                  onFinish={this.onConfirmReset}>
                                                                <Form.Item
                                                                    name="password"
                                                                    label={t('registration:password')}
                                                                    rules={[
                                                                        {
                                                                            required: true,
                                                                            message: t('enter_new_password')
                                                                        }
                                                                    ]}
                                                                    hasFeedback
                                                                >
                                                                    <Input.Password
                                                                        prefix={<LockOutlined
                                                                            className="text-primary"/>}/>
                                                                </Form.Item>
                                                                <Form.Item
                                                                    name="password_confirm"
                                                                    label={t('registration:confirm_password')}
                                                                    rules={rules.confirm}
                                                                    hasFeedback
                                                                >
                                                                    <Input.Password
                                                                        prefix={<LockOutlined
                                                                            className="text-primary"/>}/>
                                                                </Form.Item>
                                                                <Form.Item>
                                                                    <Button loading={this.state.complete}
                                                                            type="primary"
                                                                            htmlType="submit"
                                                                            block>{this.state.complete ? t('sending') : t('common:send')}</Button>
                                                                </Form.Item>
                                                            </Form>
                                                        </Col>
                                                    </Row>
                                                </div>


                                            :
                                            this.state.complete ? <Result
                                                    status="success"
                                                    title={t('check_email')}
                                                    subTitle={t('check_email__sub_text')}
                                                    extra={[
                                                        <Link href={"/login"}>
                                                            <a>
                                                                <Button type="primary">
                                                                    {t('common:to_main')}
                                                                </Button>
                                                            </a>
                                                        </Link>,
                                                    ]}
                                                /> :
                                                <div className="my-2">
                                                    <div className="text-center">
                                                        <img className="img-fluid"
                                                             style={{height: "240px"}}
                                                             src={`/images/Aviahackathon_logo_end_100х70 для светлого фона.png`}
                                                             alt=""/>
                                                        <h3 className="mt-3 font-weight-bold">{t('login:lost_password')}</h3>
                                                        <p className="mb-4">{t('enter_email_text')}</p>
                                                    </div>
                                                    <Row justify="center">
                                                        <Col xs={24} sm={24} md={20} lg={20}>
                                                            <Form layout="vertical" name="forget-password"
                                                                  onFinish={this.onSend}>
                                                                <Form.Item
                                                                    name="email"
                                                                    rules={
                                                                        [
                                                                            {
                                                                                required: true,
                                                                                message: t('enter_email')
                                                                            },
                                                                            {
                                                                                type: 'email',
                                                                                message: t('enter_email_invalid')
                                                                            }
                                                                        ]
                                                                    }>
                                                                    <Input placeholder="Email"
                                                                           prefix={<MailOutlined
                                                                               className="text-primary"/>}/>
                                                                </Form.Item>
                                                                <Form.Item>
                                                                    <Button loading={this.state.complete}
                                                                            type="primary"
                                                                            htmlType="submit"
                                                                            block>{this.state.complete ? t('sending') : t('common:send')}</Button>
                                                                </Form.Item>
                                                            </Form>
                                                        </Col>
                                                    </Row>
                                                </div>

                                    }

                                </Card>
                            </Col>
                        </Row>
                    </div>
                </div>
            </div>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        actionAuth: bindActionCreators(AuthAction, dispatch),
    };
};

export default withTranslation(withAuthRoute(connect(null, mapDispatchToProps)(reset_password)), "reset_password")

