import {DashboardOutlined, CheckCircleOutlined, MoneyCollectOutlined, TeamOutlined, CheckOutlined,ProfileOutlined, UserOutlined} from '@ant-design/icons';

const dashBoardNavTree = [{
    key: 'navigation',
    title: 'Навигация',
    icon: DashboardOutlined,
    breadcrumb: false,
    submenu: [
        {
            key: 'main_page',
            path: "/",
            title: 'Главная',
            icon: DashboardOutlined,
            breadcrumb: false,
            submenu: []
        },
        {
            key: 'application_form',
            path: `/expert/profile`,
            title: 'Анкета',
            icon: ProfileOutlined,
            breadcrumb: false,
            submenu: []
        },
        {
            key: 'checkpoints',
            path: `/expert/checkpoint`,
            title: 'Чек-поинт',
            icon: CheckCircleOutlined,
            breadcrumb: false,
            submenu: []
        },
        {
            key: 'results',
            path: `/expert/results`,
            title: 'Решения',
            icon: CheckOutlined,
            breadcrumb: false,
            submenu: []
        },
        {
            key: 'coins',
            path: `/expert/coins`,
            title: 'Баллы',
            icon: MoneyCollectOutlined,
            breadcrumb: false,
            submenu: []
        },
        {
            key: 'hackers',
            icon: UserOutlined,
            path: `/expert/hackers`,
            title: 'Хакеры',
            breadcrumb: false,
            submenu: []
        },
        {
            key: 'teams',
            path: `/expert/teams`,
            title: 'Команды',
            icon: TeamOutlined,
            breadcrumb: false,
            submenu: []
        },
    ]
}]

const navigationExpertConfig = [
    ...dashBoardNavTree
]

export default navigationExpertConfig;
