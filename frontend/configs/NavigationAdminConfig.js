import {
    DashboardOutlined, LayoutOutlined, BookOutlined, TeamOutlined, DatabaseOutlined, UserOutlined, FunnelPlotOutlined
} from '@ant-design/icons';

const dashBoardNavTree = [{
    key: 'navigation',
    title: 'Навигация',
    icon: DashboardOutlined,
    breadcrumb: false,
    submenu: [
        {
            key: 'main_page',
            path: "/",
            title: 'Главная',
            icon: DashboardOutlined,
            breadcrumb: false,
            submenu: []
        },
        {
            key: 'application_forms',
            title: 'Анкеты',
            icon: UserOutlined,
            breadcrumb: false,
            path: '/admin/hackers',
            submenu: [
                {
                    key: 'hackers',
                    path: `/admin/hackers`,
                    title: 'Хакеры',
                    breadcrumb: false,
                    submenu: []
                },
            ]
        },
        {
            key: 'teams',
            path: `/admin/teams`,
            title: 'Команды',
            icon: TeamOutlined,
            breadcrumb: false,
            submenu: []
        },
        {
            key: 'statistics',
            path: `/admin/statistics`,
            title: 'Статистика',
            icon: FunnelPlotOutlined,
            breadcrumb: false,
            submenu: []
        },
        {
            key: 'tracks',
            path: '',
            title: 'Треки',
            icon: BookOutlined,
            breadcrumb: false,
            submenu: [
                {
                    key: 'nominations',
                    path: `/admin/nominations`,
                    title: 'Номинации',
                    breadcrumb: false,
                    submenu: []
                }, {
                    key: 'tasks',
                    path: `/admin/tasks`,
                    title: 'Задачи',
                    breadcrumb: false,
                    submenu: []
                },
            ]
        },
        {
            key: 'companys',
            path: `/admin/company`,
            title: 'Компании',
            icon: LayoutOutlined,
            breadcrumb: false,
            submenu: []
        },
        {
            key: 'checkpoints',
            path: `/admin/checkpoints`,
            title: 'Чекпоинты',
            icon: DatabaseOutlined,
            breadcrumb: false,
            submenu: []
        },
    ]
}]

const navigationAdminConfig = [
    ...dashBoardNavTree
]

export default navigationAdminConfig;
