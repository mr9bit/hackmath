import {
    DashboardOutlined,
    ShareAltOutlined,
    BookOutlined,
    CommentOutlined,
    TeamOutlined,
    UserOutlined,
    ScheduleOutlined,
    CheckOutlined
} from '@ant-design/icons';

const dashBoardNavTree = [{
    key: 'navigation',
    title: 'Навигация',
    icon: DashboardOutlined,
    breadcrumb: false,
    submenu: [
        {
            key: 'main_page',
            path: "/",
            title: 'Главная',
            icon: DashboardOutlined,
            breadcrumb: false,
            submenu: []
        },
        {
            key: 'application_form',
            path: `/questionary`,
            title: 'Анкета',
            icon: UserOutlined,
            breadcrumb: false,
            submenu: []
        },
        {
            key: 'tasks',
            path: `/tasks`,
            title: 'Задачи',
            icon: BookOutlined,
            breadcrumb: false,
            submenu: []
        }, {
            key: 'checkpoints',
            path: `/checkpoints`,
            title: 'Чекпоинты',
            icon: CommentOutlined,
            breadcrumb: false,
            submenu: []
        }, {
            key: 'team',
            path: `/team`,
            title: 'Команда',
            icon: TeamOutlined,
            breadcrumb: false,
            submenu: []
        }, {
            key: 'schedule',
            path: `/schedule`,
            title: 'Расписание',
            icon: ScheduleOutlined,
            breadcrumb: false,
            submenu: []
        },
    ]
}]

const navigationConfig = [
    ...dashBoardNavTree
]

export default navigationConfig;
