import {URl_BACKEND} from "../settings";
import {checkHttpStatus} from "../utils";
import {Admin} from "../types";


export const hackerList = (data) => ({
    type: Admin.hackers.receive,
    payload: data
})

export const getHacker = (data) => ({
    type: Admin.getHacker.receive,
    payload: data
})

export const hackerNextPageList = (data) => ({
    type: Admin.hackersListNextPage.receive,
    payload: data
})

export const statisticsHackathon = (data) => ({
    type: Admin.getStatistics.receive,
    payload: data
})

export const getTeamsList = (data) => ({
    type: Admin.getTeamList.receive,
    payload: data
})

export const getTeam = (data) => ({
    type: Admin.getTeam.receive,
    payload: data
})

export const teamNextPageList = (data) => ({
    type: Admin.nextPageTeam.receive,
    payload: data
})

export const getCompanyList = (data) => ({
    type: Admin.getCompany.receive,
    payload: data
})

export const getCompany = (data) => ({
    type: Admin.getCompanyItem.receive,
    payload: data
})

export const removeCompany = (id) => ({
    type: Admin.deleteCompany.receive,
    payload: id
})

export const taskList = (data) => ({
    type: Admin.getTaskList.receive,
    payload: data
})

export const taskItem = (data) => ({
    type: Admin.getTaskItem.receive,
    payload: data
})

export const deleteTaskItem = (data) => ({
    type: Admin.deleteTask.receive,
    payload: data
})


///
export const nominationsList = (data) => ({
    type: Admin.getNominationList.receive,
    payload: data
})
export const getNominations = (data) => ({
    type: Admin.getNomination.receive,
    payload: data
})
export const getAnalytics = (data) => ({
    type: Admin.analytics.receive,
    payload: data
})

export const AdminAction = {

    checkpointList: (token) => {
        return (dispatch) => {
            return fetch(`${URl_BACKEND}/api/admin/checkpoint/`, {
                    method: 'GET',
                    headers: {
                        'Authorization': `Bearer ${token}`
                    }
                }
            ).then(checkHttpStatus)
                .then((response) => response.json())
                .then((response) => {
                    dispatch({
                        type: Admin.checkpointList.receive,
                        payload: response
                    });
                    return response;
                })
        }
    },
    checkpointCreate: (token, data) => {
        return (dispatch) => {
            return fetch(`${URl_BACKEND}/api/admin/checkpoint/`, {
                    method: 'POST',
                    headers: {
                        'Authorization': `Bearer ${token}`,
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify(data)
                }
            ).then(checkHttpStatus)
                .then((response) => response.json())
                .then((response) => {
                    dispatch({
                        type: Admin.checkpointCreate.receive,
                        payload: response
                    });
                    return response;
                })
        }
    },
    // Список участников
    hackersList: (token) => {
        return (dispatch) => {
            return fetch(`${URl_BACKEND}/api/admin/hackers/`, {
                    method: 'GET',
                    headers: {
                        'Authorization': `Bearer ${token}`
                    }
                }
            ).then(checkHttpStatus)
                .then((response) => response.json())
                .then((response) => {
                    dispatch(hackerList(response));
                    return response;
                })
        }
    },
    hackersListFilter: (token, filter_data) => {
        return (dispatch) => {
            return fetch(`${URl_BACKEND}/api/admin/hackers?${filter_data}`, {
                    method: 'GET',
                    headers: {
                        'Authorization': `Bearer ${token}`
                    }
                }
            ).then(checkHttpStatus)
                .then((response) => response.json())
                .then((response) => {
                    dispatch(hackerList(response));
                    return response;
                })
        }
    },
    hackersListNextPage: (token, page) => {
        return (dispatch) => {
            return fetch(`${URl_BACKEND}/api/admin/hackers/?page=${page}`, {
                    method: 'GET',
                    headers: {
                        'Authorization': `Bearer ${token}`
                    }
                }
            ).then(checkHttpStatus)
                .then((response) => response.json())
                .then((response) => {
                    dispatch(hackerNextPageList(response));
                    return response;
                })
        }
    },

    /// Хакеры
    getHacker: (token, id) => {
        return (dispatch) => {
            return fetch(`${URl_BACKEND}/api/admin/hackers/${id}/`, {
                    method: 'GET',
                    headers: {
                        'Authorization': `Bearer ${token}`
                    }
                }
            ).then(checkHttpStatus)
                .then((response) => response.json())
                .then((response) => {
                    dispatch(getHacker(response));
                    return response;
                })
        }
    },
    getStatistics: (token) => {
        return (dispatch) => {
            return fetch(`${URl_BACKEND}/api/admin/statistic/`, {
                    method: 'GET',
                    headers: {
                        'Authorization': `Bearer ${token}`
                    }
                }
            ).then(checkHttpStatus)
                .then((response) => response.json())
                .then((response) => {
                    dispatch(statisticsHackathon(response));
                    return response;
                })
        }
    },
    // Команда
    getTeamList: (token) => {
        return (dispatch) => {
            return fetch(`${URl_BACKEND}/api/admin/teams/`, {
                    method: 'GET',
                    headers: {
                        'Authorization': `Bearer ${token}`
                    }
                }
            ).then(checkHttpStatus)
                .then((response) => response.json())
                .then((response) => {
                    dispatch(getTeamsList(response));
                    return response;
                })
        }
    },
    getTeam: (token, id) => {
        return (dispatch) => {
            return fetch(`${URl_BACKEND}/api/admin/teams/${id}/`, {
                    method: 'GET',
                    headers: {
                        'Authorization': `Bearer ${token}`
                    }
                }
            ).then(checkHttpStatus)
                .then((response) => response.json())
                .then((response) => {
                    dispatch(getTeam(response));
                    return response;
                })
        }
    },
    teamNextPage: (token, page) => {
        return (dispatch) => {
            return fetch(`${URl_BACKEND}/api/admin/teams/?page=${page}`, {
                    method: 'GET',
                    headers: {
                        'Authorization': `Bearer ${token}`
                    }
                }
            ).then(checkHttpStatus)
                .then((response) => response.json())
                .then((response) => {
                    dispatch(teamNextPageList(response));
                    return response;
                })
        }
    },

    /// Компании
    getCompanyList: (token) => {
        return (dispatch) => {
            return fetch(`${URl_BACKEND}/api/admin/company/`, {
                    method: 'GET',
                    headers: {
                        'Authorization': `Bearer ${token}`
                    }
                }
            ).then(checkHttpStatus)
                .then((response) => response.json())
                .then((response) => {
                    dispatch(getCompanyList(response));
                    return response;
                })
        }
    },
    getCompany: (token, id) => {
        return (dispatch) => {
            return fetch(`${URl_BACKEND}/api/admin/company/${id}/`, {
                    method: 'GET',
                    headers: {
                        'Authorization': `Bearer ${token}`
                    }
                }
            ).then(checkHttpStatus)
                .then((response) => response.json())
                .then((response) => {
                    dispatch(getCompany(response));
                    return response;
                })
        }
    },
    editCompany: (token, id, data) => {
        return (dispatch) => {
            return fetch(`${URl_BACKEND}/api/admin/company/${id}/`, {
                    method: 'PUT',
                    headers: {
                        'Authorization': `Bearer ${token}`,
                    },
                    body: data
                }
            ).then(checkHttpStatus)
                .then((response) => response.json())
                .then((response) => {
                    dispatch(getCompany(response));
                    return response;
                })
        }
    },
    createCompany: (token, data) => {
        return (dispatch) => {
            return fetch(`${URl_BACKEND}/api/admin/company/`, {
                    method: 'POST',
                    headers: {
                        'Authorization': `Bearer ${token}`,
                    },
                    body: data
                }
            ).then(checkHttpStatus)
                .then((response) => response.json())
                .then((response) => {
                    dispatch(getCompany(response));
                    return response;
                })
        }
    },
    deleteCompany: (token, id) => {
        return (dispatch) => {
            return fetch(`${URl_BACKEND}/api/admin/company/${id}/`, {
                    method: 'DELETE',
                    headers: {
                        'Authorization': `Bearer ${token}`,
                    },
                }
            ).then(checkHttpStatus)
                .then((response) => {
                    dispatch(removeCompany(id));
                    return response;
                })
        }
    },

    /// ЗАДАЧИ
    getTaskList: (token) => {
        return (dispatch) => {
            return fetch(`${URl_BACKEND}/api/admin/task/`, {
                    method: 'GET',
                    headers: {
                        'Authorization': `Bearer ${token}`
                    }
                }
            ).then(checkHttpStatus)
                .then((response) => response.json())
                .then((response) => {
                    dispatch(taskList(response));
                    return response;
                })
        }
    },
    getTask: (token, id) => {
        return (dispatch) => {
            return fetch(`${URl_BACKEND}/api/admin/task/${id}`, {
                    method: 'GET',
                    headers: {
                        'Authorization': `Bearer ${token}`
                    }
                }
            ).then(checkHttpStatus)
                .then((response) => response.json())
                .then((response) => {
                    dispatch(taskItem(response));
                    return response;
                })
        }
    },
    updateTask: (token, id, data) => {
        return (dispatch) => {
            return fetch(`${URl_BACKEND}/api/admin/task/${id}/`, {
                    method: 'PUT',
                    headers: {
                        'Authorization': `Bearer ${token}`,
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify(data)
                }
            ).then(checkHttpStatus)
                .then((response) => response.json())
                .then((response) => {
                    dispatch(taskItem(response));
                    return response;
                })
        }
    },
    createTask: (token, data) => {
        return (dispatch) => {
            return fetch(`${URl_BACKEND}/api/admin/task/`, {
                    method: 'POST',
                    headers: {
                        'Authorization': `Bearer ${token}`,
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify(data)
                }
            ).then(checkHttpStatus)
                .then((response) => response.json())
                .then((response) => {
                    dispatch(taskItem(response));
                    return response;
                })
        }
    },
    deleteTask: (token, id) => {
        return (dispatch) => {
            return fetch(`${URl_BACKEND}/api/admin/task/${id}/`, {
                    method: 'DELETE',
                    headers: {
                        'Authorization': `Bearer ${token}`,
                    },
                }
            ).then(checkHttpStatus)
                .then((response) => {
                    dispatch(deleteTaskItem(id));
                    return response;
                })
        }
    },

    //// НОМИНАЦИИИИ
    getNominationList: (token) => {
        return (dispatch) => {
            return fetch(`${URl_BACKEND}/api/admin/nominations/`, {
                    method: 'GET',
                    headers: {
                        'Authorization': `Bearer ${token}`
                    }
                }
            ).then(checkHttpStatus)
                .then((response) => response.json())
                .then((response) => {
                    dispatch(nominationsList(response));
                    return response;
                })
        }
    },
    getNomination: (token, id) => {
        return (dispatch) => {
            return fetch(`${URl_BACKEND}/api/admin/nominations/${id}`, {
                    method: 'GET',
                    headers: {
                        'Authorization': `Bearer ${token}`
                    }
                }
            ).then(checkHttpStatus)
                .then((response) => response.json())
                .then((response) => {
                    dispatch(getNominations(response));
                    return response;
                })
        }
    },
    removeNomination: (token, id) => {
        return (dispatch) => {
            return fetch(`${URl_BACKEND}/api/admin/nominations/${id}`, {
                    method: 'DELETE',
                    headers: {
                        'Authorization': `Bearer ${token}`
                    }
                }
            ).then(checkHttpStatus)
                .then((response) => {
                    dispatch({
                        type: Admin.removeNomination.receive,
                        payload: id
                    });
                    return response;
                })
        }
    },
    updateNomination: (token, id, data) => {
        return (dispatch) => {
            return fetch(`${URl_BACKEND}/api/admin/nominations/${id}/`, {
                    method: 'PUT',
                    headers: {
                        'Authorization': `Bearer ${token}`,
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify(data)
                }
            ).then(checkHttpStatus)
                .then((response) => response.json())
                .then((response) => {
                    dispatch({
                        type: Admin.updateNomination.receive,
                        payload: response
                    });
                    return response;
                })
        }
    },
    createNomination: (token, data) => {
        return (dispatch) => {
            return fetch(`${URl_BACKEND}/api/admin/nominations/`, {
                    method: 'POST',
                    headers: {
                        'Authorization': `Bearer ${token}`,
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify(data)
                }
            ).then(checkHttpStatus)
                .then((response) => response.json())
                .then((response) => {
                    dispatch({
                        type: Admin.createNomination.receive,
                        payload: response
                    });
                    return response;
                })
        }
    },

    // АНАЛ ИТИКА
    getAnalytics: (token) => {
        return (dispatch) => {
            return fetch(`${URl_BACKEND}/api/admin/analytics/`, {
                    method: 'GET',
                    headers: {
                        'Authorization': `Bearer ${token}`
                    }
                }
            ).then(checkHttpStatus)
                .then((response) => response.json())
                .then((response) => {
                    dispatch(getAnalytics(response));
                    return response;
                })
        }
    }
}


