// Команда
import {URl_BACKEND} from "../settings";
import {checkHttpStatus} from "../utils";
import {Admin, Expert} from "../types";
import {getHacker} from "./admin.action";

export const ExpertAction = {

    resultTeam: (token) => {
        return (dispatch) => {
            return fetch(`${URl_BACKEND}/api/expert/team_results/`, {
                    method: 'GET',
                    headers: {
                        'Authorization': `Bearer ${token}`,
                    },
                }
            ).then(checkHttpStatus)
                .then((response) => response.json())
                .then((response) => {
                    dispatch({
                        type: Expert.resultList.receive,
                        payload: response
                    });
                    return response;
                })
        }
    },

    getResultTeam: (token, id) => {
        return (dispatch) => {
            return fetch(`${URl_BACKEND}/api/expert/team_results/${id}/`, {
                    method: 'GET',
                    headers: {
                        'Authorization': `Bearer ${token}`,
                    },
                }
            ).then(checkHttpStatus)
                .then((response) => response.json())
                .then((response) => {
                    dispatch({
                        type: Expert.getResult.receive,
                        payload: response
                    });
                    return response;
                })
        }
    },

    checkpointResponseSend: (token, data) => {
        return (dispatch) => {
            return fetch(`${URl_BACKEND}/api/expert/checkpoint_response/`, {
                    method: 'POST',
                    headers: {
                        'Authorization': `Bearer ${token}`,
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify(data)
                }
            ).then(checkHttpStatus)
                .then((response) => response.json())
                .then((response) => {
                    dispatch({
                        type: Expert.sendResponseCheckPoint.receive,
                        payload: response
                    });
                    return response;
                })
        }
    },
    checkpointList: (token) => {
        return (dispatch) => {
            return fetch(`${URl_BACKEND}/api/expert/checkpoint/`, {
                    method: 'GET',
                    headers: {
                        'Authorization': `Bearer ${token}`
                    }
                }
            ).then(checkHttpStatus)
                .then((response) => response.json())
                .then((response) => {
                    dispatch({
                        type: Expert.checkpointList.receive,
                        payload: response
                    });
                    return response;
                })
        }
    },
    myCheckpointList: (token) => {
        return (dispatch) => {
            return fetch(`${URl_BACKEND}/api/expert/my_check_point/`, {
                    method: 'GET',
                    headers: {
                        'Authorization': `Bearer ${token}`
                    }
                }
            ).then(checkHttpStatus)
                .then((response) => response.json())
                .then((response) => {
                    dispatch({
                        type: Expert.myResponseCheckPoint.receive,
                        payload: response
                    });
                    return response;
                })
        }
    },
    // Список участников
    hackersList: (token) => {
        return (dispatch) => {
            return fetch(`${URl_BACKEND}/api/expert/hackers/`, {
                    method: 'GET',
                    headers: {
                        'Authorization': `Bearer ${token}`
                    }
                }
            ).then(checkHttpStatus)
                .then((response) => response.json())
                .then((response) => {
                    dispatch({
                        type: Admin.hackers.receive,
                        payload: response
                    });
                    return response;
                })
        }
    },
    /// Хакеры
    getHacker: (token, id) => {
        return (dispatch) => {
            return fetch(`${URl_BACKEND}/api/expert/hackers/${id}/`, {
                    method: 'GET',
                    headers: {
                        'Authorization': `Bearer ${token}`
                    }
                }
            ).then(checkHttpStatus)
                .then((response) => response.json())
                .then((response) => {
                    dispatch(getHacker(response));
                    return response;
                })
        }
    },
    getTeamList: (token) => {
        return (dispatch) => {
            return fetch(`${URl_BACKEND}/api/expert/teams/`, {
                    method: 'GET',
                    headers: {
                        'Authorization': `Bearer ${token}`
                    }
                }
            ).then(checkHttpStatus)
                .then((response) => response.json())
                .then((response) => {
                    dispatch({
                        type: Expert.teamList.receive,
                        payload: response
                    });
                    return response;
                })
        }
    },
    getTeam: (token, id) => {
        return (dispatch) => {
            return fetch(`${URl_BACKEND}/api/expert/teams/${id}/`, {
                    method: 'GET',
                    headers: {
                        'Authorization': `Bearer ${token}`
                    }
                }
            ).then(checkHttpStatus)
                .then((response) => response.json())
                .then((response) => {
                    dispatch({
                        type: Expert.teamDetail.receive,
                        payload: response
                    });
                    return response;
                })
        }
    },
    sendTeamCoins: (token, form) => {
        return (dispatch) => {
            return fetch(`${URl_BACKEND}/api/expert/coins/`, {
                    method: 'POST',
                    headers: {
                        'Authorization': `Bearer ${token}`,
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify(form)
                }
            ).then(checkHttpStatus)
                .then((response) => response.json())
                .then((response) => {
                    return response;
                })
        }
    },
    getTeamListForCoins: (token) => {
        return (dispatch) => {
            return fetch(`${URl_BACKEND}/api/expert/team_coins/`, {
                    method: 'GET',
                    headers: {
                        'Authorization': `Bearer ${token}`,
                    },
                }
            ).then(checkHttpStatus)
                .then((response) => response.json())
                .then((response) => {
                    dispatch({
                        type: Expert.getTeamListForCoins.receive,
                        payload: response
                    });
                    return response;
                })
        }
    }
}
