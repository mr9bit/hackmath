import {URl_BACKEND} from "../settings";
import {Team} from "../types";
import {checkHttpStatus} from '../utils'

export function getTeam(data) {
    return {
        type: Team.get.receive,
        payload: data
    }
}

export function leaveTeam(data) {
    return {
        type: Team.leave.receive,
        payload: data
    }
}

export function createSearchForm(data) {
    return {
        type: Team.search.receive,
        payload: data
    }
}

export function searchListForm(data) {
    return {
        type: Team.search_list.receive,
        payload: data
    }
}

export function searchLeaveForm() {
    return {
        type: Team.leave_search.receive,
        payload: null
    }
}


export function sendInviteTeam(data) {
    return {
        type: Team.sendInvite.receive,
        payload: data
    }
}

export const TeamAction = {
    sendResult: (token, data) => {
        return (dispatch) => {
            return fetch(`${URl_BACKEND}/api/org/team/send_result`, {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            }).then(checkHttpStatus).then((response) => response.json()).then((response) => {
                dispatch({
                    type: Team.sendResult.receive,
                    payload: response
                });
                return response;
            })
        }
    },
    getResult: (token) => {
        return (dispatch) => {
            return fetch(`${URl_BACKEND}/api/org/team/send_result`, {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${token}`,
                },
            }).then((response) => response.json()).then((response) => {
                dispatch({
                    type: Team.getResult.receive,
                    payload: response
                });
                return response;
            })
        }
    },
    checkPointResponse: (token) => {
        return (dispatch) => {
            return fetch(`${URl_BACKEND}/api/org/checkpoint_response/`, {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${token}`
                },
            }).then(checkHttpStatus).then((response) => response.json()).then((response) => {
                dispatch({
                    type: Team.checkPointResponse.receive,
                    payload: response
                });
                return response;
            })
        }
    },
    checkPoint: (token) => {
        return (dispatch) => {
            return fetch(`${URl_BACKEND}/api/org/checkpoint/`, {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${token}`
                },
            }).then(checkHttpStatus).then((response) => response.json()).then((response) => {
                dispatch({
                    type: Team.checkPoint.receive,
                    payload: response
                });
                return response;
            })
        }
    },
    create: (token, data) => {
        return (dispatch) => {
            return fetch(`${URl_BACKEND}/api/org/team/`, {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `Bearer ${token}`
                    },
                    body: JSON.stringify(data)
                }
            ).then(checkHttpStatus).then((response) => response.json()).then((response) => {
                dispatch(getTeam(response));
                return response;
            })
        }
    },
    get: (token) => {
        return (dispatch) => {
            return fetch(`${URl_BACKEND}/api/org/team/`, {
                    method: 'GET',
                    headers: {
                        'Authorization': `Bearer ${token}`
                    }
                }
            ).then((response) => response.json()).then((response) => {
                dispatch(getTeam(response));
                return response;
            })
        }
    },
    leave: (token, data) => {
        return (dispatch) => {
            return fetch(`${URl_BACKEND}/api/org/team/`, {
                    method: 'DELETE',
                    headers: {
                        'Authorization': `Bearer ${token}`,
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify(data)
                }
            ).then(checkHttpStatus).then((response) => response.json()).then((response) => {
                dispatch(leaveTeam(response));
                return response;
            })
        }
    },
    invite: (token, data) => {
        return (dispatch) => {
            return fetch(`${URl_BACKEND}/api/org/team/invite`, {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `Bearer ${token}`
                    },
                    body: JSON.stringify(data)
                }
            ).then(checkHttpStatus).then((response) => response.json()).then((response) => {
                dispatch(getTeam(response));
                return response;
            })
        }
    },
    search_list: (token) => {
        return (dispatch) => {
            return fetch(`${URl_BACKEND}/api/org/team/search`, {
                    method: 'GET',
                    headers: {
                        'Authorization': `Bearer ${token}`
                    },
                }
            ).then((response) => response.json()).then((response) => {
                dispatch(searchListForm(response))
                return response;
            })
        }
    },
    search: (token, data) => {
        return (dispatch) => {
            return fetch(`${URl_BACKEND}/api/org/team/search`, {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `Bearer ${token}`
                    },
                    body: JSON.stringify(data)
                }
            ).then(checkHttpStatus).then((response) => response.json()).then((response) => {
                dispatch(createSearchForm(response))
                return response;
            })
        }
    },
    leave_search: (token, data) => {
        return (dispatch) => {
            return fetch(`${URl_BACKEND}/api/org/team/search`, {
                    method: 'DELETE',
                    headers: {
                        'Authorization': `Bearer ${token}`
                    },
                }
            ).then(checkHttpStatus).then((response) => response.json()).then((response) => {
                dispatch(searchLeaveForm())
                return response;
            })
        }
    },
    sendInvite: (token, data) => {
        return (dispatch) => {

            return fetch(`${URl_BACKEND}/api/org/team/search/invite`, {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `Bearer ${token}`
                    },
                    body: JSON.stringify(data)
                }
            ).then(checkHttpStatus).then((response) => response.json()).then((response) => {
                dispatch(sendInviteTeam(response))
                return response;
            })

        }
    }
}
