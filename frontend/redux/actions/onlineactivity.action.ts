import {URl_BACKEND} from "../settings";
import {OnlineAcivity} from "../types";


export function getOnlineActivity(data) {
    return {
        type: OnlineAcivity.get.receive,
        payload: {activity: data}
    }
}

export function saveOnlineActivity(data){
    return {
        type: OnlineAcivity.save.receive,
        payload: {activity: data}
    }
}



export const OnlineActivityAction = {
    get: (token) => {
        return (dispatch) => {
            return fetch(`${URl_BACKEND}/api/org/onlineactivity/`, {
                    method: 'GET',
                    headers: {
                        'Authorization': `Bearer ${token}`
                    },
                }
            ).then((response) => response.json()).then((response) => {
                dispatch(getOnlineActivity(response))
                return response;
            })
        }
    },
    save: (token, data) => {
        return (dispatch) => {
            return fetch(`${URl_BACKEND}/api/org/onlineactivity/`, {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `Bearer ${token}`
                    },
                    body: JSON.stringify(data)
                }
            ).then((response) => response.json()).then((response) => {
                dispatch(saveOnlineActivity(response))
                return response;
            })

        }
    }
}
