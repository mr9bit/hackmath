import {URl_BACKEND} from "../settings";
import {Schedule} from "../types";

export const getScheduleList = (data) => ({
    type: Schedule.getSchedule.receive,
    payload: data
})
export const ScheduleAction = {
    get: () => {
        return (dispatch) => {
            return fetch(`${URl_BACKEND}/api/org/schedule/`, {
                    method: 'GET',
                }
            ).then((response) => response.json()).then((response) => {
                dispatch(getScheduleList(response))
                return response;
            })
        }
    },
}
