import {Auth} from "../types";
import Router from 'next/router';
import jsCookie from 'js-cookie';
import {URl_BACKEND} from '../settings'
import {checkHttpStatus} from "../utils";

const requestLogin = (data) => ({
    type: Auth.login.request,
    payload: data
})


export function authLoginUserSuccess(data) {
    jsCookie.set('access_token', data.access, {expires: 115200});

    return {
        type: Auth.login.success,
        payload: data
    };
}

export function authMeInfo(data) {
    return {
        type: Auth.info.receive,
        payload: data
    };

}

export function authGetProfile(data) {
    return {
        type: Auth.profile.receive,
        payload: data
    };

}

export function authRegisterSuccess(data) {
    return {
        type: Auth.register.receive,
        payload: data
    }
}

export function authLogout() {
    jsCookie.remove("access_token");
    jsCookie.remove("user");
    return {
        type: Auth.logout.receive,
        payload: {
            token: null,
            user: null
        }
    };
}

export function refreshToken(data) {
    jsCookie.set('access_token', data.access, {expires: 115200});
    return {
        type: Auth.refresh.receive,
        payload: data
    };
}

export function authUpdateProfile(data) {
    return {
        type: Auth.update.receive,
        payload: data
    }
}

export interface IAuthAction {
    login: Function;
}

export function verifyAction(data) {
    return {
        type: Auth.verify.receive,
        payload: data
    }
}

export const AuthAction = {
    register: (data) => {
        return (dispatch) => {
            return fetch(`${URl_BACKEND}/api/auth/register/`, {
                    method: 'POST', headers: {
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify(data)
                }
            ).then(checkHttpStatus).then((response) => response.json()).then((r) => {
                dispatch(authRegisterSuccess(r));
                return r;
            })
        }
    },
    registerExpert: (data) => {
        return (dispatch) => {
            return fetch(`${URl_BACKEND}/api/auth/register-expert/`, {
                    method: 'POST', headers: {
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify(data)
                }
            ).then(checkHttpStatus).then((response) => response.json()).then((r) => {
                dispatch(authRegisterSuccess(r));
                return r;
            })
        }
    },

    updateExpertProfile: (token, data) => {
        return (dispatch) => {
            return fetch(`${URl_BACKEND}/api/expert/profile/`, {
                method: 'PUT',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(data)
            }).then(checkHttpStatus).then((response) => response.json())
                .then((response) => {
                    dispatch(authUpdateProfile(response))
                    return response
                })
        }
    },

    verify: (data) => {
        return (dispatch) => {
            return fetch(`${URl_BACKEND}/api/auth/verify-registration/`, {
                    method: 'POST', headers: {
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify(data),
                }
            ).then(checkHttpStatus).then((response) => response.json())
                .then((response) => {
                    return response
                })
        }
    },
    confirm: (data) => {
        return (dispatch) => {
            return fetch(`${URl_BACKEND}/api/auth/confirm/`, {
                    method: 'POST', headers: {
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify(data),
                }
            ).then(checkHttpStatus).then((response) => response.json())
                .then((response) => {
                    return response
                })
        }
    },
    refresh: (token) => {
        return (dispatch) => {
            return fetch(`${URl_BACKEND}/api/auth/jwt/refresh`, {
                    method: 'POST', headers: {
                        'Authorization': `Bearer ${token}`
                    },
                }
            ).then((response) => response.json())
                .then((response) => {
                    dispatch(refreshToken(response));
                })
        }
    },
    logout: () => {
        setTimeout(() => {
            Router.push('/login')
        }, 5)
        return (dispatch) => {
            dispatch(authLogout());
        }
    },
    getMeInfo: (token) => {
        return (dispatch) => {
            return fetch(`${URl_BACKEND}/api/auth/me/`, {
                    method: 'GET',
                    headers: {
                        'Authorization': `Bearer ${token}`
                    },
                }
            ).then(checkHttpStatus)
                .then((response) => response.json())
                .then((response) => {
                    dispatch(authMeInfo({user: response}))
                }).catch((e) => {
                    dispatch(authLogout());

                })
        }
    },
    updateProfile: (token, data) => {
        return (dispatch) => {
            return fetch(`${URl_BACKEND}/api/auth/profile/`, {
                method: 'PUT',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(data)
            }).then(checkHttpStatus).then((response) => response.json())
                .then((response) => {
                    dispatch(authUpdateProfile(response))
                    return response
                })
        }
    },
    getProfile: (token) => {
        return (dispatch) => {
            return fetch(`${URl_BACKEND}/api/auth/profile/`, {
                    method: 'GET',
                    headers: {
                        'Authorization': `Bearer ${token}`
                    },
                }
            ).then(checkHttpStatus)
                .then((response) => response.json())
                .then((response) => {
                    dispatch(authGetProfile({profile: response}))
                }).catch((e) => {
                    dispatch(authLogout());
                })
        }
    },
    login: (data) => {
        return (dispatch) => {
            return fetch(`${URl_BACKEND}/api/auth/token/`, {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify(data)
                }
            ).then(checkHttpStatus)
                .then((response) => response.json())
                .then((response) => {
                    dispatch(authLoginUserSuccess(response));
                    Router.push('/');
                })
        }
    },
    reset_password_link: (data) => {
        return fetch(`${URl_BACKEND}/api/auth/send-reset-password-link/`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(data)
            }
        ).then(checkHttpStatus)
            .then((response) => response.json())
            .then((response) => {
                return response
            })
    },
    reset_password: (data) => {
        return fetch(`${URl_BACKEND}/api/auth/reset-password/`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(data)
            }
        ).then(checkHttpStatus)
            .then((response) => response.json())
            .then((response) => {
                return response
            })
    },
    resend_email: (token) => {
        return fetch(`${URl_BACKEND}/api/auth/resend_email/`, {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`
                },
            }
        ).then(checkHttpStatus)
            .then((response) => response.json())
            .then((response) => {
                return response
            })
    }
}
