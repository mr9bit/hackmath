import {
    TOGGLE_COLLAPSED_NAV,
    SIDE_NAV_STYLE_CHANGE,
    CHANGE_LOCALE,
    NAV_TYPE_CHANGE,
    TOP_NAV_COLOR_CHANGE,
    HEADER_NAV_COLOR_CHANGE,
    TOGGLE_MOBILE_NAV,
    SWITCH_THEME
} from '../types';
import jsCookie from 'js-cookie';

export function toggleCollapsedNav(navCollapsed) {
    return {
        type: TOGGLE_COLLAPSED_NAV,
        payload: navCollapsed
    };
}

export function onNavStyleChange(sideNavTheme) {
    return {
        type: SIDE_NAV_STYLE_CHANGE,
        payload: sideNavTheme
    };
}

export function onLocaleChange(locale) {
    return {
        type: CHANGE_LOCALE,
        payload: locale
    };
}

export function onNavTypeChange(navType) {
    return {
        type: NAV_TYPE_CHANGE,
        payload: navType
    };
}

export function onTopNavColorChange(topNavColor) {
    return {
        type: TOP_NAV_COLOR_CHANGE,
        payload: topNavColor
    };
}

export function onHeaderNavColorChange(headerNavColor) {
    return {
        type: HEADER_NAV_COLOR_CHANGE,
        payload: headerNavColor
    };
}

export function onMobileNavToggle(mobileNav) {
    return {
        type: TOGGLE_MOBILE_NAV,
        payload: mobileNav
    };
}

export function onSwitchTheme(currentTheme) {
    jsCookie.set('theme', currentTheme);
    return {
        type: SWITCH_THEME,
        payload: currentTheme
    };
}
