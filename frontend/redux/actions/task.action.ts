import {URl_BACKEND} from "../settings";
import {Admin, Task} from "../types";

export const getTaskList = (data) => ({
    type: Task.list.receive,
    payload: data
})

export const getTaskDetail = (data) => ({
    type: Task.detail.receive,
    payload: data
})


export const TaskAction = {
    list: (token) => {
        return (dispatch) => {
            return fetch(`${URl_BACKEND}/api/org/track/`, {
                    method: 'GET',
                    headers: {
                        'Authorization': `Bearer ${token}`
                    },
                }
            ).then((response) => response.json()).then((response) => {
                dispatch(getTaskList(response))
                return response;
            })
        }
    },
    get: (token, id) => {
        return (dispatch) => {
            return fetch(`${URl_BACKEND}/api/org/track/${id}`, {
                    method: 'GET',
                    headers: {
                        'Authorization': `Bearer ${token}`
                    },
                }
            ).then((response) => response.json()).then((response) => {
                dispatch(getTaskDetail(response))
                return response;
            })

        }
    }
}
