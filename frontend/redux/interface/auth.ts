interface ResumeFile {
    file_name: string;
    size: string;
    url: string;
}

interface Skill {
    value: string,
    text: string
}

interface Task {
    name: string;
    id: number;
}

export interface Profile {
    id: string;
    email: string;
    select_task: number[];
    is_active: true;
    is_superuser: false;
    is_verified: false;
    first_name: string;
    second_name: string;
    middle_name: string;
    gender: string;
    role: string;
    specialization: string;
    status: number;
    birth_date: string;
    country: string;
    country_live: string;
    city: string;
    born_city: string;
    telegram_link: string;
    phone: string;
    institute: string;
    year_end: string;
    science_degree: string;
    type_developer: string;
    employment_form: string;
    seniority: string;
    github: string;
    gitlab: string;
    linkedin: string;
    hh: string;
    site: string;
    resume_file: ResumeFile;
    resume_text: string;
    hack_visited: string;
    want_take_part: string;
    expect_from_hack: string;
    traks: string;
    something_else: string;
    accept_reglament: boolean;
    where_to_study: string;
    learning_class: number;
    skills: string[];
    render_skills: Skill[];
    where_did_you_know: string;
}

export interface User {
    id: string;
    email: string;
    is_active: true;
    is_superuser: false;
    type_developer: string;
    is_verified: false;
    first_name: string;
    second_name: string;
    middle_name: string;
    gender: string;
    role: string;
    specialization: string;
    status: 0
}




export interface ExpertProfile {

    first_name: string;
    second_name: string;
    middle_name: string;
    gender: string;

    birth_date: string;
    city: string;
    telegram_link: string;
    phone: string;
    expert_task: string;
    type_developer: string;
    company: string;

}

