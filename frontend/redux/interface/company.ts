
    export interface Company {
        id: number;
        name: string;
        type: string;
        url: string;
        logo: string;
        logo_eng: string;
        datetime: Date;
        datetime_update: Date;
    }
