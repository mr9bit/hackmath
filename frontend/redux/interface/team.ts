interface Capitan {
    id: string;
    email: string;
    telegram_link: string;
    first_name: string;
    phone: string;
    second_name: string;
    type_developer: string;
    institute: string;
    birth_date: string;
}

interface Teammate {
    id: string;
    email: string;
    phone: string;
    telegram_link: string;
    institute: string;
    first_name: string;
    second_name: string;
    birth_date: string;
    type_developer: string;
}

export interface Team {
    id: number;
    name: string;
    selected_tracks: any[];
    capitan: Capitan;
    secret_code: string;
    report: string;
    datetime: Date;
    datetime_update: Date;
    teammate: Teammate[];
}
