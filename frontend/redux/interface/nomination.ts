export interface Nomination {
    id: number;
    name: string;
    name_eng: string;
    color: string
}

