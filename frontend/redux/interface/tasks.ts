import {Company} from './company'

export interface Tasks {
    id: number;
    name: string;
    name_eng: string;
    description: any;
    description_eng: any;
    short_description: string;
    short_description_eng: string;
    maintainer: Company;
    partner: Company[];
    nomination?: any;
    datetime: Date;
}

