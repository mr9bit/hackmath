interface Team {
    name: string;
}

export interface Hacker {
    id: string;
    full_name: string;
    team: Team[];
    password: string;
    last_login?: any;
    email: string;
    first_name: string;
    second_name: string;
    middle_name?: any;
    gender: string;
    birth_date: string;
    country_live?: any;
    born_city?: any;
    city?: any;
    telegram_link: string;
    phone: string;
    status: number;
    role: string;
    institute?: any;
    specialization?: any;
    year_end: number;
    science_degree?: any;
    type_developer: string;
    employment_form?: any;
    seniority?: any;
    github?: any;
    gitlab?: any;
    linkedin?: any;
    hh?: any;
    site?: any;
    resume_file?: any;
    resume_text?: any;
    hack_visited?: any;
    traks: string;
    is_staff: boolean;
    is_superuser: boolean;
    is_active: boolean;
    where_to_study: string;
    learning_class?: any;
    where_did_you_know: string;
    want_take_part?: any;
    expect_from_hack?: any;
    something_else?: any;
    accept_reglament: boolean;
    form_complete: boolean;
    datetime: Date;
    skills: string[];
    datetime_update: Date;
}
