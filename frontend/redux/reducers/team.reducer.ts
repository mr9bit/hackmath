import {HYDRATE} from "next-redux-wrapper";
import {Team} from "../types";

const initialState = {
    name: null,
    secret_code: null,
    search: {
        available_of_requests: null,
        my: null,
        hackers: [],
        teams: []
    },
    check: {
        checkPoint: [],
        response: []
    },
    result: {}
};


export default function teamReducer(state = initialState, {type, payload}) {
    switch (type) {
        case HYDRATE:
            if (payload.onlineActivity) {
                return {
                    ...state,
                    ...payload.team
                }
            }
            return {
                ...state,
                ...payload
            }
        case Team.sendResult.receive:
            return {
                ...state,
                result: payload
            }
        case Team.getResult.receive:
            return {
                ...state,
                result: payload
            }
        case Team.checkPointResponse.receive:
            return {
                ...state,
                check: {
                    checkPoint: state.check.checkPoint,
                    response: payload
                }
            }
        case Team.checkPoint.receive:
            return {
                ...state,
                check: {
                    checkPoint: payload,
                    response: state.check.response
                }
            }
        case Team.create.receive:
            return {
                ...payload,
                search: {
                    my: state.search.my,
                    hackers: null,
                    teams: null,
                }
            }
        case Team.leave.receive:
            return {
                id: null,
                name: null,
                selected_tracks: [],
                capitan: null,
                secret_code: null,
                datetime: null,
                datetime_update: null,
                teammate: [],
                search: state.search
            }
        case Team.get.receive:
            return {
                ...state,
                ...payload
            }
        case Team.search.receive:
            return {
                ...state,
                search: {
                    available_of_requests: payload.available_of_requests ? payload.available_of_requests : 0,
                    my: payload.my ? payload.my : null,
                    hackers: payload.hackers ? payload.hackers : null,
                    teams: payload.teams ? payload.teams : null,
                }
            }
        case Team.search_list.receive:
            return {
                ...state,
                search: {
                    available_of_requests: payload.available_of_requests ? payload.available_of_requests : 0,
                    my: payload.my ? payload.my : null,
                    hackers: payload.hackers ? payload.hackers : null,
                    teams: payload.teams ? payload.teams : null,
                }
            }
        case Team.leave_search.receive:
            return {
                ...state,
                search: {
                    available_of_requests: state.search.available_of_requests,
                    my: state.search.my,
                    hackers: null,
                    teams: null,
                }
            }
        case Team.sendInvite.receive:
            return {
                ...state,
                search: {
                    available_of_requests: payload.available_of_requests,
                    my: state.search.my,
                    hackers: state.search.hackers,
                    teams: state.search.teams,
                }
            }
        default:
            return state
    }
}
