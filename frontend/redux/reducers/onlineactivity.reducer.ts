import {HYDRATE} from "next-redux-wrapper";
import {OnlineAcivity} from "../types";

const initialState = {
    comment: null,

};


export default function onlineActivity(state = initialState, {type, payload}) {
    switch (type) {
        case HYDRATE:
            if (payload.onlineActivity) {
                return {
                    ...state,
                    ...payload.onlineActivity
                }
            }
            return {
                ...state,
                ...payload
            }
        case OnlineAcivity.get.receive:
            return {
                ...state,
                ...payload
            }
        case OnlineAcivity.save.receive:
            return {
                ...state,
                ...payload
            }

        default:
            return state
    }
}
