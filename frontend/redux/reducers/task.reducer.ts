import {HYDRATE} from "next-redux-wrapper";
import {Task} from "../types";

const initialState = {
    active: {},
    list: {}

};


export default function taskReducer(state = initialState, {type, payload}) {
    switch (type) {
        case HYDRATE:
            if (payload.tasks) {
                return {
                    ...state,
                    ...payload.tasks
                }
            }
            return {
                ...state,
                ...payload
            }
        case Task.list.receive:
            return {
                active: null,
                list: payload
            }
        case Task.detail.receive:
            return {
                ...state,
                active: payload
            }

        default:
            return state
    }
}
