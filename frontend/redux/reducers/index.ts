import {combineReducers} from 'redux';
import authReducer from './auth.reducer';
import onlineActivity from './onlineactivity.reducer';
import themeReducer from './theme.reducer';
import teamReducer from './team.reducer';
import adminReducer from './admin.reducer';
import taskReducer from './task.reducer';
import expertReducer from './expert.reducer';
import scheduleReducer from "./schedule.reducer";

const rootReducer = combineReducers({
    admin: adminReducer,
    schedule: scheduleReducer,
    expert: expertReducer,
    auth: authReducer,
    tasks: taskReducer,
    onlineActivity: onlineActivity,
    team: teamReducer,
    theme: themeReducer,
});

export default rootReducer;
