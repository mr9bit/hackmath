import {Auth, User} from "../types";
import {HYDRATE} from "next-redux-wrapper";
import Router from "next/router";

const initialState = {
    access_token: null,
    user: {
        first_name: '',
        middle_name: '',
        second_name: '',
        role: '',
        specialization: ''
    },
    isAuthenticated: false,
    profile: null
};

export default function authReducer(state = initialState, {type, payload}) {
    switch (type) {
        case HYDRATE:
            if (payload.auth) {
                return {
                    ...state,
                    ...payload.auth
                }
            }
            return {
                ...state,
                ...payload
            }

        case  Auth.logout.receive: {
            return {
                ...state,
                isAuthenticated: false,
                access_token: null
            }
        }
        case Auth.login.success: {
            return {
                ...state,
                access_token: payload.access,
                isAuthenticated: true,
            }
        }
        case Auth.refresh.receive: {
            return {
                ...state,
                access_token: payload.access,
                isAuthenticated: true,
            }
        }
        case Auth.info.receive: {
            return {
                ...state,
                access_token: state.access_token,
                user: payload.user,
                isAuthenticated: true,
            }
        }
        case Auth.profile.receive: {
            return {
                ...state,
                profile: payload.profile,
            }
        }
        case Auth.update.receive: {
            return {
                ...state,
                profile: payload,
                user: {
                    ...state.user,
                    first_name: payload.first_name,
                    second_name: payload.second_name,
                    middle_name: payload.middle_name,
                    specialization: payload.specialization
                }
            }
        }
        default:
            return state
    }
}
