import {
    TOGGLE_COLLAPSED_NAV,
    CHANGE_LOCALE,
    SIDE_NAV_STYLE_CHANGE,
    NAV_TYPE_CHANGE,
    TOP_NAV_COLOR_CHANGE,
    HEADER_NAV_COLOR_CHANGE,
    TOGGLE_MOBILE_NAV,
    SWITCH_THEME
} from '../types';
import {THEME_CONFIG} from '../../configs/AppConfig'
import {HYDRATE} from "next-redux-wrapper";

const initTheme = {
    ...THEME_CONFIG
};

const theme = (state = initTheme, {type, payload}) => {
    switch (type) {
        case HYDRATE:
            if (payload.theme) {
                return {
                    ...state,
                    ...payload.theme
                }
            }
            return {
                ...state,
                ...payload
            }
        case TOGGLE_COLLAPSED_NAV:
            return {
                ...state,
                navCollapsed: payload
            };
        case SIDE_NAV_STYLE_CHANGE:
            return {
                ...state,
                sideNavTheme: payload
            };
        case CHANGE_LOCALE:
            return {
                ...state,
                locale: payload
            };
        case NAV_TYPE_CHANGE:
            return {
                ...state,
                navType: payload
            };
        case TOP_NAV_COLOR_CHANGE:
            return {
                ...state,
                topNavColor: payload
            };
        case HEADER_NAV_COLOR_CHANGE:
            return {
                ...state,
                headerNavColor: payload
            };
        case TOGGLE_MOBILE_NAV:
            return {
                ...state,
                mobileNav: payload
            };
        case SWITCH_THEME:
            return {
                ...state,
                currentTheme: payload
            }
        default:
            return state;
    }
};

export default theme
