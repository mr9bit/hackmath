import {HYDRATE} from "next-redux-wrapper";
import {Admin, Expert} from "../types";

const initialState = {
    result: {
        list: [],
        active: []
    },
    team: {
        list: [],
        active: []
    },
    hacker: {
        list: [],
        active: []
    },
    checkpoints: [],
    my: [],
    coins: {
        teams: [],
    }
};

export default function expertReducer(state = initialState, {type, payload}) {
    switch (type) {
        case HYDRATE:
            if (payload.expert) {
                return {
                    ...state,
                    ...payload.expert
                }
            }
            return {
                ...state,
                ...payload
            }
        case  Expert.resultList.receive: {
            return {
                ...state,
                result: {
                    active: state.result.active,
                    list: payload
                }
            }
        }
        case  Expert.getResult.receive: {
            return {
                ...state,
                result: {
                    active: payload,
                    list: state.result.list
                }
            }
        }
        case  Expert.checkpointList.receive: {
            return {
                ...state,
                checkpoints: payload,
            }
        }
        case  Expert.myResponseCheckPoint.receive: {
            return {
                ...state,
                my: payload.results,
            }
        }
        case Expert.teamList.receive:
            return {
                ...state,
                team: {
                    list: payload,
                    active: state.team.active
                }
            }
        case Expert.teamDetail.receive:
            return {
                ...state,
                team: {
                    list: state.team.list,
                    active: payload
                }
            }
        case Expert.hackerList.receive:
            return {
                ...state,
                hacker: {
                    list: payload,
                    active: state.hacker.active
                }
            }
        case Expert.hackerDetail.receive:
            return {
                ...state,
                hacker: {
                    list: state.hacker.list,
                    active: payload
                }
            }
        case Expert.getTeamListForCoins.receive:
            return {
                ...state,
                coins: {
                    teams: payload.results,
                }
            }
        default:
            return state
    }
}
