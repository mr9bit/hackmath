import {Schedule} from "../types";
import {HYDRATE} from "next-redux-wrapper";


const initialState = {
    schedule: []
};

export default function adminReducer(state = initialState, {type, payload}) {
    switch (type) {
        case HYDRATE:
            if (payload.auth) {
                return {
                    ...state,
                    ...payload.admin
                }
            }
            return {
                ...state,
                ...payload
            }
        case  Schedule.getSchedule.receive: {
            return {
                schedule: payload,
            }
        }

        default:
            return state
    }
}
