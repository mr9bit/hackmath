import {Admin, Auth, User} from "../types";
import {HYDRATE} from "next-redux-wrapper";


const initialState = {
    hackers: {
        list: {
            results: []
        },
        active: null
    },
    teams: {
        list: null,
        active: null
    },
    statistics: {},
    company: {
        list: null,
        active: null
    },
    tasks: {
        list: null,
        active: null
    },
    nominations: {
        list: null,
        active: null
    },
    analytics: {},
    checkpoints: []
};

export default function adminReducer(state = initialState, {type, payload}) {
    switch (type) {
        case HYDRATE:
            if (payload.auth) {
                return {
                    ...state,
                    ...payload.admin
                }
            }
            return {
                ...state,
                ...payload
            }
        case  Admin.checkpointList.receive: {
            return {
                ...state,
                checkpoints: payload,
            }
        }
        case  Admin.hackers.receive: {
            return {
                ...state,
                hackers: {
                    list: payload,
                    active: null
                },
            }
        }
        case  Admin.getHacker.receive: {
            return {
                ...state,
                hackers: {
                    list: state.hackers.list,
                    active: payload
                },
            }
        }
        case  Admin.getStatistics.receive: {
            return {
                ...state,
                statistics: payload
            }
        }
        case  Admin.hackersListNextPage.receive: {
            return {
                ...state,
                hackers: {
                    list: payload,
                    active: state.hackers.active
                },
            }
        }
        case  Admin.getTeamList.receive: {
            return {
                ...state,
                teams: {
                    list: payload,
                    active: null
                },
            }
        }
        case  Admin.getTeam.receive: {
            return {
                ...state,
                teams: {
                    list: state.teams.list,
                    active: payload
                },
            }
        }

        case  Admin.nextPageTeam.receive: {
            return {
                ...state,
                teams: {
                    list: payload,
                    active: null
                },
            }
        }

        case  Admin.getCompany.receive: {
            return {
                ...state,
                company: {
                    list: payload,
                    active: null
                },
            }
        }
        case  Admin.getCompanyItem.receive: {
            return {
                ...state,
                company: {
                    list: state.company.list,
                    active: payload
                },
            }
        }
        case  Admin.deleteCompany.receive: {
            return {
                ...state,
                company: {
                    list: {
                        ...state.company.list,
                        results: state.company.list.results.filter(item => item.id !== payload),
                        count: state.company.list.count - 1,
                    },
                    active: null
                },
            }
        }
        case  Admin.getTaskList.receive: {
            return {
                ...state,
                tasks: {
                    list: payload,
                    active: null
                },
            }
        }
        case  Admin.getTaskItem.receive: {
            return {
                ...state,
                tasks: {
                    list: state.tasks.list,
                    active: payload
                },
            }
        }
        case  Admin.deleteTask.receive: {
            return {
                ...state,
                tasks: {
                    list: {
                        ...state.tasks.list,
                        results: state.tasks.list.results.filter(item => item.id !== payload),
                        count: state.tasks.list.count - 1,
                    },
                    active: null
                },
            }
        }
        /// NOMINATION REDUCER
        case  Admin.getNominationList.receive: {
            return {
                ...state,
                nominations: {
                    list: payload.results,
                    active: null
                },
            }
        }
        case  Admin.getNomination.receive: {
            return {
                ...state,
                nominations: {
                    list: null,
                    active: payload
                },
            }
        }
        case  Admin.updateNomination.receive: {
            return {
                ...state,
                nominations: {
                    list: null,
                    active: payload
                },
            }
        }
        case  Admin.removeNomination.receive: {
            return {
                ...state,
                nominations: {
                    list: state.nominations.list.filter(item => item.id !== payload),
                    active: null
                },
            }
        }

        // Аналитика
        case Admin.analytics.receive: {
            return {
                ...state,
                analytics: payload
            }
        }


        default:
            return state
    }
}
