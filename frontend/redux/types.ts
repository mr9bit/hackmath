export const LOGIN = 'LOGIN'
export const CLEAR = 'CLEAR'
export const REGISTER = 'REGISTER'


export const Schedule = {
    getSchedule: {
        receive: 'Schedule_LIST',
    },
}


export const Expert = {
    getTeamListForCoins: {
        receive: 'RECEIVE_EXPERT_TEAM_LIST_COINS',
    },
    sendPoints: {
        receive: 'RECEIVE_EXPERT_COIN',
    },
    teamList: {
        receive: 'RECEIVE_EXPERT_TEAM_LIST',
    },
    resultList: {
        receive: 'RECEIVE_EXPERT_RESULT_LIST',
    },
    getResult: {
        receive: 'RECEIVE_EXPERT_GET_RESULT',
    },
    teamDetail: {
        receive: 'RECEIVE_EXPERT_TEAM',
    },
    hackerList: {
        receive: 'RECEIVE_EXPERT_HACKERS_LIST',
    },
    hackerDetail: {
        receive: 'RECEIVE_EXPERT_HACKERS',
    },
    checkpointList: {
        receive: 'RECEIVE_EXPERT_CHECKPOINT_LIST',
    },
    sendResponseCheckPoint: {
        receive: 'RECEIVE_EXPERT_RESPONSE_CHECK_POINT',
    },
    myResponseCheckPoint: {
        receive: 'RECEIVE_EXPERT_MY_RESPONSE_CHECK_POINT',
    }
}


export const Task = {

    list: {
        receive: 'RECEIVE_HACKERS_TRACKS_LIST',
    },
    detail: {
        receive: 'RECEIVE_HACKERS_TRACK_GET',
    },
}

export const Admin = {
    checkpointList: {
        receive: 'RECEIVE_CHECKPOINT_LIST',
    },
    checkpointCreate: {
        receive: 'RECEIVE_CHECKPOINT_CREATE',
    },
    analytics: {
        receive: 'RECEIVE_ADMIN_ANALYTICS',
    },
    hackers: {
        receive: 'RECEIVE_ADMIN_HACKERS_LIST',
    },
    getHacker: {
        receive: 'RECEIVE_ADMIN_HACKERS',
    },
    hackersListNextPage: {
        receive: 'RECEIVE_ADMIN_NEXT_PAGE_HACKERS',
    },
    getStatistics: {
        receive: 'RECEIVE_ADMIN_STATISTICS',
    },
    getTeamList: {
        receive: 'RECEIVE_ADMIN_TEAMS_LIST',
    },
    getTeam: {
        receive: 'RECEIVE_ADMIN_TEAMS',
    },
    nextPageTeam: {
        receive: 'RECEIVE_ADMIN_NEXT_PAGE_TEAMS',
    },
    getCompany: {
        receive: 'RECEIVE_ADMIN_COMPANY_LIST',
    },
    deleteCompany: {
        receive: 'RECEIVE_ADMIN_COMPANY_DELETE',
    },
    getCompanyItem: {
        receive: 'RECEIVE_ADMIN_COMPANY_ITEM',
    },
    getTaskList: {
        receive: 'RECEIVE_ADMIN_TASK_LIST',
    },
    getTaskItem: {
        receive: 'RECEIVE_ADMIN_TASK_ITEM',
    },
    createTask: {
        receive: 'RECEIVE_ADMIN_TASK_CREATE',
    },
    deleteTask: {
        receive: 'RECEIVE_ADMIN_TASK_DELETE',
    },

    /// CRUD FOR NOMINATION
    getNominationList: {
        receive: 'RECEIVE_ADMIN_NOMINATION_LIST',
    },
    getNomination: {
        receive: 'RECEIVE_ADMIN_NOMINATION_GET',
    },
    removeNomination: {
        receive: 'RECEIVE_ADMIN_NOMINATION_REMOVE',
    },
    updateNomination: {
        receive: 'RECEIVE_ADMIN_NOMINATION_UPDATE',
    },
    createNomination: {
        receive: 'RECEIVE_ADMIN_NOMINATION_CREATE',
    },


}

export const Team = {
    sendResult: {
        receive: 'RECEIVE_HACKER_SEND_RESULT'
    },
    getResult: {
        receive: 'RECEIVE_HACKER_GET_RESULT'
    },
    checkPoint: {
        receive: 'RECEIVE_HACKER_CHECKPOINT'
    },
    checkPointResponse: {
        receive: 'RECEIVE_HACKER_CHECKPOINTRESPONSE'
    },
    create: {
        receive: 'RECEIVE_CREATE_TEAM'
    },
    update: {
        receive: 'RECEIVE_UPDATE_TEAM'
    },
    leave: {
        receive: 'RECEIVE_LEAVE_TEAM'
    },
    get: {
        receive: 'RECEIVE_GET_TEAM'
    },
    search: {
        receive: 'RECEIVE_SEARCH_TEAMMATE'
    },
    search_list: {
        receive: 'RECEIVE_SEARCH_LIST_TEAMMATE'
    },
    leave_search: {
        receive: 'RECEIVE_LEAVE_SEARCH_TEAMMATE'
    },
    sendInvite: {
        receive: 'RECEIVE_SEND_INVITE_SEARCH_TEAMMATE'
    }
}

export const Article = {
    List: {
        request: "ArticleRequest",
        receive: "ArticleReceive"
    },
    viewBySlug: {
        request: "ArticleViewBySlugRequest"
    },
    loadMore: {
        receive: 'RECEIVE_LOADMORE'
    }
}

export const Auth = {
    login: {
        request: "LoginRequest",
        receive: "LoginReceive",
        success: "LoginSuccess"
    },
    verify: {
        receive: "VerifyReceive"
    },
    logout: {
        request: "LogoutRequest",
        receive: "LogoutReceive"
    },
    register: {
        receive: "RECEIVE_REGISTER_USER"
    },
    info: {
        receive: "RECEIVE_ME_INFO"
    },
    refresh: {
        receive: "RECEIVE_REFRESH"
    },
    profile: {
        receive: "RECEIVE_PROFILE"
    },
    update: {
        receive: "RECEIVE_UPDATE_PROFILE"
    }
}

export const OnlineAcivity = {
    get: {
        receive: 'RECEIVE_ONLINE_ACTIVITY'
    },
    save: {
        receive: 'RECEIVE_SAVE_ONLINE_ACTIVITY'
    }
}

export const UserArticle = {
    getModerationList: {
        request: 'REQUEST_USER_MODERATION_ARTICLE',
        receive: 'RECEIVE_USER_MODERATION_ARTICLE'
    },
    getDraftList: {
        request: 'REQUEST_USER_DRAFT_ARTICLE',
        receive: 'RECEIVE_USER_DRAFT_ARTICLE'
    },
    getPublishList: {
        request: 'REQUEST_USER_PUBLISH_ARTICLE',
        receive: 'RECEIVE_USER_PUBLISH_ARTICLE'
    },
    updatePublishList: {
        receive: 'RECEIVE_UPDATE_USER_PUBLISH_ARTICLE'
    },
    getDraft: {
        request: 'REQUEST_DRAFT_ARTICLE',
        receive: 'RECEIVE_DRAFT_ARTICLE',
    },
    createDraft: {
        request: 'REQUEST_CREATE_DRAFT_ARTICLE',
        receive: 'RECEIVE_CREATE_DRAFT_ARTICLE',
    },
    updateDraft: {
        request: 'REQUEST_UPDATE_DRAFT_ARTICLE',
    },
    removeDraft: {
        request: 'REQUEST_REMOVE_DRAFT_ARTICLE',
    },
    changeStatus: {
        request: 'REQUEST_CHANGE_STATUS_ARTICLE',
    }
}


export const User = {
    updateProfile: {
        request: 'REQUEST_UPDATE_PROFILE'
    }
}
export const TOGGLE_COLLAPSED_NAV = 'TOGGLE_COLLAPSED_NAV';
export const CHANGE_LOCALE = 'CHANGE_LOCALE';
export const SIDE_NAV_STYLE_CHANGE = 'SIDE_NAV_STYLE_CHANGE';
export const NAV_TYPE_CHANGE = 'NAV_TYPE_CHANGE';
export const TOP_NAV_COLOR_CHANGE = 'TOP_NAV_COLOR_CHANGE';
export const HEADER_NAV_COLOR_CHANGE = 'HEADER_NAV_COLOR_CHANGE';
export const TOGGLE_MOBILE_NAV = 'TOGGLE_MOBILE_NAV';
export const SWITCH_THEME = 'SWITCH_THEME';

export const ROW_GUTTER = 16;
export const SIDE_NAV_WIDTH = 250;
export const SIDE_NAV_COLLAPSED_WIDTH = 80;
export const SIDE_NAV_LIGHT = 'light';
export const SIDE_NAV_DARK = 'dark';
export const NAV_TYPE_SIDE = 'SIDE'
export const NAV_TYPE_TOP = 'TOP'
