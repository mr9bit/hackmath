import React from "react";
import Link from "next/link";
import {Menu, Grid} from "antd";
import {connect} from "react-redux";
import useTranslation from 'next-translate/useTranslation'


import utils from '../../utils'
import Icon from "../util-components/Icon";
import IntlMessage from "../util-components/IntlMessage";
import {SIDE_NAV_LIGHT, NAV_TYPE_SIDE} from "../../redux/types";
import {onMobileNavToggle} from "../../redux/actions/theme.action";

import navigationConfig from "../../configs/NavigationConfig";
import navigationAdminConfig from "../../configs/NavigationAdminConfig";
import navigationExpertConfig from "../../configs/NavigationExpertConfig";


const {SubMenu} = Menu;
const {useBreakpoint} = Grid;

const setLocale = (isLocaleOn, localeKey) =>
    isLocaleOn ? <IntlMessage id={localeKey}/> : localeKey.toString();

const setDefaultOpen = (key) => {
    let keyList = [];
    let keyString = "";
    if (key) {
        const arr = key.split("-");
        for (let index = 0; index < arr.length; index++) {
            const elm = arr[index];
            index === 0 ? (keyString = elm) : (keyString = `${keyString}-${elm}`);
            keyList.push(keyString);
        }
    }
    return keyList;
};


const SideNavContent = (props) => {
        const {sideNavTheme, hideGroupTitle, localization, onMobileNavToggle} = props.theme;
        const {user} = props.auth;
        const {routeInfo} = props;
        let navigationConf = [];
        const {t, lang} = useTranslation('common')
        const isMobile = !utils.getBreakPoint(useBreakpoint()).includes('lg')
        const closeMobileNav = () => {
            if (isMobile) {
                onMobileNavToggle(false)
            }
        }
        if (user.role === "admin") {
            navigationConf = navigationAdminConfig;
        } else {
            if (user.role === "expert") {
                navigationConf = navigationExpertConfig;
            } else {
                navigationConf = navigationConfig;
            }
        }
        return (
            <Menu
                theme={sideNavTheme === SIDE_NAV_LIGHT ? "light" : "dark"}
                mode="inline"
                style={{height: "100%", borderRight: 0}}
                defaultSelectedKeys={[routeInfo?.key]}
                defaultOpenKeys={setDefaultOpen(routeInfo?.key)}
                className={hideGroupTitle ? "hide-group-title" : ""}>
                {navigationConf.map((menu) =>
                    menu.submenu.length > 0 ? (
                        <Menu.ItemGroup
                            key={menu.key}
                            title={t(menu.key)}>
                            {menu.submenu.map((subMenuFirst) =>

                                subMenuFirst.submenu.length > 0 ? (
                                    <SubMenu
                                        icon={
                                            subMenuFirst.icon ? (
                                                <Icon type={subMenuFirst?.icon}/>
                                            ) : null
                                        }
                                        key={subMenuFirst.key}
                                        title={t(subMenuFirst.key)}
                                    >
                                        {subMenuFirst.submenu.map((subMenuSecond) => (
                                            <Menu.Item key={subMenuSecond.key}>
                                                {subMenuSecond.icon ? (
                                                    <Icon type={subMenuSecond?.icon}/>
                                                ) : null}
                                                <span>
                                                    {t(subMenuSecond.key)}
                                                </span>
                                                <Link onClick={() => closeMobileNav()}
                                                      href={subMenuSecond.path}><a></a></Link>
                                            </Menu.Item>
                                        ))}
                                    </SubMenu>
                                ) : (

                                    user.status === 0 && subMenuFirst.title === "Анкета" || user.status <= 1 && subMenuFirst.title === "Команда" ?
                                        null : <Menu.Item key={subMenuFirst.key}>
                                            {subMenuFirst.icon ? <Icon type={subMenuFirst.icon}/> : null}
                                            <span>
                                                {t(subMenuFirst.key)}
                                            </span>
                                            <Link onClick={() => closeMobileNav()}
                                                  href={subMenuFirst.path}><a></a></Link>
                                        </Menu.Item>


                                )
                            )}
                        </Menu.ItemGroup>
                    ) : (
                        <Menu.Item key={menu.key}>
                            {menu.icon ? <Icon type={menu?.icon}/> : null}
                            <span>{menu?.title}</span>
                            {menu.path ?
                                <Link onClick={() => closeMobileNav()} href={subMenuFirst.path}><a></a></Link> : null}
                        </Menu.Item>
                    )
                )}
            </Menu>
        );
    }
;


const MenuContent = (props) => {
        return props.type === NAV_TYPE_SIDE ? (
            <SideNavContent {...props} />
        ) : (
            <TopNavContent {...props} />
        );
    }
;

export default connect((state => (state)),
    {
        onMobileNavToggle
    }
)(MenuContent);
