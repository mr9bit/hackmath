import React from "react";
import {Menu, Dropdown, Avatar} from "antd";
import {connect} from 'react-redux'
import {QuestionCircleOutlined, LogoutOutlined} from '@ant-design/icons';
import {AuthAction} from "../../redux/actions/auth.action";
import {bindActionCreators} from "redux";
import Router from "next/router";
import useTranslation from "next-translate/useTranslation";

const menuItem = [
    {
        title: "Помощь",
        icon: QuestionCircleOutlined,
        path: "/help"
    }
]

export const NavProfile = (props) => {


    const signOut = () => {
        setTimeout(() => {
            Router.push('/login');
        }, 0)
        props.actionAuth.logout();
    }
    const {t, lang} = useTranslation('common');

    const profileMenu = (
        <div className="nav-profile nav-dropdown">
            <div className="nav-profile-header">
                <div className="d-flex">
                    <Avatar
                        style={{
                            backgroundColor: "#f56a00",
                            verticalAlign: 'middle',
                        }}
                        size="large">
                        {props.user ? (props.user.first_name ? props.user.first_name[0] : props.user.email[0]) : null}
                    </Avatar>
                    <div className="pl-3">
                        {props.user ?
                            (props.user && props.user.first_name ?
                                <h4 className="mb-0">{props.user.first_name} {props.user.second_name}</h4> :
                                <h4 className="mb-0">{props.user.email}</h4>)
                            : null}

                        <span className="text-muted">
                            {props.user && props.user.type_developer ? props.user.type_developer : "Hacker"}
                        </span>
                    </div>
                </div>
            </div>
            <div className="nav-profile-body">
                <Menu>
                    {/*{menuItem.map((el, i) => {*/}
                    {/*    return (*/}
                    {/*        <Menu.Item key={i}>*/}
                    {/*            <a href={el.path}>*/}
                    {/*                <Icon className="mr-3" type={el.icon}/>*/}
                    {/*                <span className="font-weight-normal">{el.title}</span>*/}
                    {/*            </a>*/}
                    {/*        </Menu.Item>*/}
                    {/*    );*/}
                    {/*})}*/}
                    <Menu.Item key={menuItem.legth + 1} onClick={e => signOut()}>
                        <span>
                          <LogoutOutlined className="mr-3"/>
                          <span className="font-weight-normal">{t("exit")}</span>
                        </span>
                    </Menu.Item>
                </Menu>
            </div>
        </div>
    );
    return (
        <Dropdown placement="bottomRight" overlay={profileMenu} trigger={["click"]}>
            <Menu className="d-flex align-item-center" mode="horizontal">
                <Menu.Item>
                    <Avatar
                        style={{
                            backgroundColor: "#f56a00",
                            verticalAlign: 'middle',
                        }}
                        size="large">
                        {props.user ? (props.user.first_name ? props.user.first_name[0] : props.user.email[0]) : null}
                    </Avatar>
                </Menu.Item>
            </Menu>
        </Dropdown>
    );
}
const mapDispatchToProps = (dispatch) => {
    return {
        actionAuth: bindActionCreators(AuthAction, dispatch),
    };
};
export default connect((state => (state.auth)), mapDispatchToProps)(NavProfile);
