import React, {useState, useEffect} from 'react';
import {Drawer, Menu} from 'antd';
import {useRouter} from 'next/router'
import {SettingOutlined} from '@ant-design/icons';
import ThemeConfigurator from './ThemeConfigurator';
import setLanguage from 'next-translate/setLanguage';
import useTranslation from 'next-translate/useTranslation';

export const NavPanel = (props) => {
    const [visible, setVisible] = useState(0);
    const {t, lang} = useTranslation('common');
    const {locale, defaultLocale} = useRouter()
    useEffect(persistLocaleCookie, [locale, defaultLocale])

    function persistLocaleCookie() {
        const date = new Date()
        const expireMs = 100 * 365 * 24 * 60 * 60 * 1000 // 100 days
        date.setTime(date.getTime() + expireMs)
        document.cookie = `NEXT_LOCALE=${locale};expires=${date.toUTCString()};path=/`
    }

    return (
        <>
            <Menu mode="horizontal">


            </Menu>

            <Drawer
                title="Настройки"
                placement="right"
                width={350}
                onClose={() => setVisible(false)}
                visible={visible}
            >
                <ThemeConfigurator/>
            </Drawer>
        </>
    );

}

export default NavPanel;
