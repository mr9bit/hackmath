import React, { Component } from 'react';
import { Breadcrumb } from 'antd';
import navigationConfig from "../../configs/NavigationConfig";
import IntlMessage from '../util-components/IntlMessage';
import Link from 'next/link'
let breadcrumbData = {
	'/app' : <IntlMessage id="home" />
};

navigationConfig.forEach((elm, i) => {
	const assignBreadcrumb = (obj) => breadcrumbData[obj.path] = <IntlMessage id={obj.title} />;
	assignBreadcrumb(elm);
	if (elm.submenu) {
		elm.submenu.forEach( elm => {
			assignBreadcrumb(elm)
			if(elm.submenu) {
				elm.submenu.forEach( elm => {
					assignBreadcrumb(elm)
				})
			}
		})
	}
})

const BreadcrumbRoute = (props => {
	const { location } = props;


  return (
		<Breadcrumb>
			<Breadcrumb.Item key="1">
	      </Breadcrumb.Item>
		</Breadcrumb>
  );
});

export class AppBreadcrumb extends Component {
	render() {
		return (
			<BreadcrumbRoute />
		)
	}
}

export default AppBreadcrumb
