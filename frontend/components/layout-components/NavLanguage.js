import React from "react";
import {CheckOutlined, GlobalOutlined, DownOutlined} from '@ant-design/icons';
import {Menu, Dropdown} from "antd";


export const NavLanguage = (props) => {

    return (
        <Dropdown placement="bottomRight"  trigger={["click"]}>
            {

                <Menu mode="horizontal">
                    <Menu.Item>
                        <a href="#/" onClick={e => e.preventDefault()}>
                            <GlobalOutlined className="nav-icon mr-0"/>
                        </a>
                    </Menu.Item>
                </Menu>

            }
        </Dropdown>
    )
}


export default NavLanguage
