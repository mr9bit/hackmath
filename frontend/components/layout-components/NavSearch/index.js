import React from 'react';
import { connect } from "react-redux";
import {
	CloseOutlined,
} from '@ant-design/icons';
import SearchInput from './SearchInput';

export const NavSearch = (props) => {
	const { active, close, headerNavColor } = props

	return (
		<div className={`nav-search ${active ? 'nav-search-active' : ''} light`} style={{backgroundColor: headerNavColor}}>
			<div className="d-flex align-items-center w-100">
				<SearchInput close={close} active={active}/>
			</div>
			<div className="nav-close" onClick={close}>
				<CloseOutlined />
			</div>
		</div>
	)
}

export default NavSearch
