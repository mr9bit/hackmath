import React from 'react'
import useTranslation from 'next-translate/useTranslation'
import {APP_NAME} from '../../configs/AppConfig';

export default function Footer() {
    const {t, lang} = useTranslation('common')
    return (
        <footer className="footer">
            <span>Copyright  &copy;  {`${new Date().getFullYear()}`} <span
                className="font-weight-semibold">{t('name_org')}</span> {t('reserved')}</span>
            <div>
                <a className="text-gray" href="mailto:it-centre@mai.ru">{t('report')}</a>
            </div>
        </footer>
    )
}

