import {Table, Tag, Space, Button} from 'antd';
import Link from 'next/link'
import React from "react";
import {Breakpoint} from "antd/lib/_util/responsiveObserve";

export const getColumns = (my, check) => {
    const onThisCheckPoint = my.map(item => item.checkpoint == check.id ? item.team : null)
    console.log(onThisCheckPoint)
    return [
        {
            title: 'Название команды',
            dataIndex: 'name',
            key: 'name',
            render: (_, item) => {
                return onThisCheckPoint.includes(item.id) ? <Link href={`/expert/teams/${item.id}`}>
                    <a>{item.name}</a>
                </Link> : <Link href={`/expert/teams/${item.id}`}>
                    <a>{item.name}</a>
                </Link>
            },
        },
        {
            title: 'Количество участников',
            dataIndex: 'count',
            key: 'count',
            render: (_, item) => <span>{item?.teammate.length}</span>,
        },
        {
            title: '',
            dataIndex: 'action',
            key: 'action',
            render: (_, item) => {
                return onThisCheckPoint.includes(item.id) ?
                 <Button style={{whiteSpace: "normal", height: 'auto'}} size={"middle"}
                                   type={"default"}>Отправлено</Button>

                    :
                    <Link href={{
                        pathname: '/expert/checkpoint/team',
                        query: {
                            team_id: item.id,
                            check: check.id
                        },
                    }}>
                        <a>
                            <Button style={{whiteSpace: "normal", height: 'auto'}} size={"middle"}
                                    type={"primary"}>Оставить обратную связь</Button>
                        </a>
                    </Link>

            },
        }
    ]
}
