import {DiscordIcon} from "./Discord";
import {ElevatorIcon} from "./Elevator";
import {ITCenterIcon} from "./Itcenter";
import {MaiIcon} from "./MAI";
import {ZoomIcon} from "./zoom";
import {MathModeLongIcon} from "./MathModLong";

export {DiscordIcon, ElevatorIcon, MathModeLongIcon, ITCenterIcon, MaiIcon, ZoomIcon}
