export const LinkTool = props => {
    return <div>
        <div className={"articlecontent-wrapper_linkBlock"}>
            {props.meta ? <div className={"articlecontent-wrapper_linkBlock"} style={{width:"100%"}} >
                    <a className={"articlecontent_linkBlock-content"} style={{width:"100%"}} target={"_blank"} href={props.link}>
                        {props.meta.image ?
                            <div className={"articlecontent_linkBlock-image"}
                                 style={{"backgroundImage": `url(${props.meta.image.url})`}}/> : null}
                        <div className={"articlecontent_linkBlock-title ant-typography"}>
                            {props.meta.title}
                        </div>
                        <p className={"articlecontent_linkBlock-description ant-typography"}>{props.meta.description}</p>
                        <span className={"articlecontent_linkBlock-domain ant-typography"}>{(new URL(props.link)).hostname}</span>
                    </a>
                </div>
                : null}
        </div>
    </div>
}
