import React from "react";
import {connect} from "react-redux";
import {Layout, Grid,} from "antd";
import {useThemeSwitcher} from "react-css-theme-switcher";
import {useRouter} from "next/router";

import SideNav from '../../components/layout-components/SideNav';
import TopNav from '../../components/layout-components/TopNav';
import Loading from '../../components/shared-components/Loading';
import MobileNav from '../../components/layout-components/MobileNav'
import HeaderNav from '../../components/layout-components/HeaderNav';
import PageHeader from '../../components/layout-components/PageHeader';
import Footer from '../../components/layout-components/Footer';
import navigationConfig from "../../configs/NavigationConfig";
import {SIDE_NAV_WIDTH, SIDE_NAV_COLLAPSED_WIDTH, NAV_TYPE_SIDE, NAV_TYPE_TOP} from '../../constants/ThemeConstant';
import utils from '../../utils';

const {Content} = Layout;
const {useBreakpoint} = Grid;

const AppLayout = (props) => {
    const {navCollapsed, navType, location} = props;
    const router = useRouter();
    const currentRouteInfo = utils.getRouteInfo(navigationConfig, router.pathname)
    const screens = utils.getBreakPoint(useBreakpoint());
    const isMobile = !screens.includes('lg')
    const isNavSide = navType === NAV_TYPE_SIDE
    const isNavTop = navType === NAV_TYPE_TOP
    const getLayoutGutter = () => {
        if (isNavTop || isMobile) {
            return 0
        }
        return navCollapsed ? SIDE_NAV_COLLAPSED_WIDTH : SIDE_NAV_WIDTH
    }

    // const {status} = useThemeSwitcher();
    // if (status === 'loading') {
    //     return <div style={{
    //         width: '100%', height: '100%', display: "flex", alignItems: 'baseline', alignContent: 'center',
    //         justifyContent: 'center', justifyItems: 'center'
    //     }}>
    //         <Loading cover="page"/>
    //     </div>;
    // } else {
    const {status} = useThemeSwitcher();
    return (
        <Layout>
            <HeaderNav isMobile={isMobile}/>
            {(isNavTop && !isMobile) ? <TopNav routeInfo={currentRouteInfo}/> : null}
            <Layout className="app-container">
                {(isNavSide && !isMobile) ? <SideNav routeInfo={currentRouteInfo}/> : null}
                <Layout className="app-layout" style={{paddingLeft: getLayoutGutter()}}>
                    <div className={`app-content ${isNavTop ? 'layout-top-nav' : ''}`}>
                        <PageHeader display={currentRouteInfo?.breadcrumb} title={currentRouteInfo?.title}/>
                        <Content>
                            {
                                status == 'loading' ?
                                    <div style={{
                                        width: "100vw",
                                        height: "100vh",
                                        display: "flex",
                                        placeItems: "baseline-center",
                                        placeContent: 'center',
                                        position: 'fixed',
                                        background: 'white',
                                        zIndex: 1000,
                                        top: 0,
                                        left: 0,
                                        right: 0,
                                        bottom: 0,

                                    }}>
                                        <Loading cover="page"/>
                                    </div> : null
                            }

                            {props.children}

                        </Content>
                    </div>
                    <Footer/>
                </Layout>
            </Layout>
            {isMobile && <MobileNav/>}
        </Layout>
    )

}


export default connect((state => state.theme), null)(AppLayout);
