import {useThemeSwitcher} from "react-css-theme-switcher";
import Loading from '../../components/shared-components/Loading';
import {connect} from 'react-redux';

import React from "react";


const LoadCircle = (props) => {
    // const {status} = useThemeSwitcher();
    //
    // if (status === 'loading') {
    //     return <div style={{
    //         width: '100%', height: 'calc(100vh - 30px)', display: "flex", alignItems: 'center', alignContent: 'center',
    //         justifyContent: 'center', justifyItems: 'center'
    //     }}><Loading cover="page"/>
    //     </div>
    // }
    return props.children
}

const Base = (props) => {


    return <LoadCircle>
        {props.children}
    </LoadCircle>
}
export default connect((state => state.theme), null)(Base);


