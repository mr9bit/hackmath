import {Card, Col, Row} from "antd";
import React from "react";
import moment from 'moment'
import {Team} from "../../../redux/interface/team";
import Link from "next/link";
import AvatarStatus from '../../shared-components/AvatarStatus';


interface Props {
    team: Team,
    prefix: string
}

const TeamMate = (props: Props) => {
    const {team, prefix} = props;
    const dateRegister = moment(team?.datetime).locale('ru');

    return (
        <Card title="Состав команды">
            <Row gutter={30}>
                <Col sm={24} md={24}>
                    <div className="mb-3">

                        <Row gutter={16}>
                            <Col xs={24} sm={24} md={6}>
                                Участник
                            </Col>
                            <Col xs={24} sm={24} md={2}>
                                Возраст
                            </Col>
                            <Col xs={24} sm={24} md={4}>
                                Email
                            </Col>
                            <Col xs={24} sm={24} md={3}>
                                Telegram
                            </Col>
                            <Col xs={24} sm={24} md={4}>
                                Телефон
                            </Col>
                            <Col xs={24} sm={24} md={4}>
                                Институт
                            </Col>
                        </Row>
                        {
                            team.teammate.map(teammate => {
                                return <Row gutter={16}
                                            className="d-flex mt-2"
                                            key={teammate.id}


                                >

                                    <Col xs={24} sm={24} md={6}>
                                        <Link href={{
                                            pathname: `/expert/${prefix}/[id]`,
                                            query: {id: teammate.id}
                                        }}>
                                            <a>
                                                {team.capitan.id === teammate.id ?
                                                    <AvatarStatus text={teammate?.first_name[0]}
                                                                  name={`${teammate?.first_name} ${teammate?.second_name}`}
                                                                  subTitle={"Капитан"}/>
                                                    :
                                                    <AvatarStatus text={teammate?.first_name[0]}
                                                                  name={`${teammate?.first_name} ${teammate?.second_name}`}
                                                                  subTitle={teammate?.type_developer}/>
                                                }
                                            </a>
                                        </Link>
                                    </Col>

                                    <Col xs={24} sm={24} md={2}>
                                        <div>
                                                        <span
                                                            className="font-weight-semibold">
                                                            <a href={`mailto:${teammate?.email}`}
                                                               className={"text-dark"}
                                                            >
                                                                {moment().diff(teammate?.birth_date, 'years')}
                                                            </a>
                                                        </span>
                                        </div>
                                    </Col>
                                    <Col xs={24} sm={24} md={4}>
                                        <div>
                                                        <span
                                                            className="font-weight-semibold">
                                                            <a href={`mailto:${teammate?.email}`}
                                                               className={"text-dark"}
                                                            >
                                                                {teammate?.email}
                                                            </a>
                                                        </span>
                                        </div>
                                    </Col>

                                    <Col xs={24} sm={24} md={3}>
                                        <div>
                                                              <span className="font-weight-semibold"><a
                                                                  className={"text-dark"}
                                                                  href={`https://t.me/${teammate?.telegram_link}`}>{teammate?.telegram_link}</a></span>

                                        </div>
                                    </Col>

                                    <Col xs={24} sm={24} md={4}>
                                        <div>
                                            {
                                                teammate.phone !== '+7/' ?
                                                    <span className="ml-3 text-dark"><a
                                                        href={`tel:+${teammate?.phone.split('/')[0]}${teammate?.phone.split('/')[1]}`}>+{teammate?.phone.split('/')[0]}{teammate?.phone.split('/')[1]}</a> </span>
                                                    :
                                                    null
                                            }
                                        </div>
                                    </Col>

                                    <Col xs={24} sm={24} md={4}>
                                        <div>
                                            <span className="font-weight-semibold">{teammate?.institute}</span>
                                        </div>
                                    </Col>


                                </Row>
                            })
                        }
                    </div>
                </Col>
            </Row>
        </Card>
    )
}
export default TeamMate;
