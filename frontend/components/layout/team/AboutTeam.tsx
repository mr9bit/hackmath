import {Card, Col, Row} from "antd";
import React from "react";
import {Hacker} from '../../../redux/interface/hacker';
import moment from 'moment'
import {Team} from "../../../redux/interface/team";


interface Props {
    team: Team,
}

const AboutTeam = (props: Props) => {
    const {team} = props;
    const dateRegister = moment(team?.datetime).locale('ru');

    return (
        <Card title="О Команде">
            <Row gutter={30}>
                <Col sm={24} md={12}>
                    <div className="mb-3">
                        <p><b>Дата создания:</b>
                            {dateRegister.format(' hh:mm, MMM D YYYY')}
                        </p>
                    </div>
                </Col>
            </Row>
        </Card>
    )
}
export default AboutTeam;
