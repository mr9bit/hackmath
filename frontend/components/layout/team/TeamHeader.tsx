import {Avatar, Button, Card, Col, Row} from "antd";
import {GlobalOutlined, SendOutlined, MailOutlined, PhoneOutlined} from "@ant-design/icons";
import React from "react";
import Flex from '../../shared-components/Flex'
import {Icon} from '../../util-components/Icon'
import {Team} from '../../../redux/interface/team';

interface Props {
    team: Team,
}

const TeamHeader = (props: Props) => {
    const avatarSize = 150;
    const {team} = props;
    return (
        <Card>
            <Row justify="center">
                <Col sm={24} md={23}>
                    <div className="d-md-flex">

                        <div className="ml-md-4 w-100">
                            <Flex alignItems="center" mobileFlex={false}
                                  className="mb-3 text-md-left text-center">
                                <h2 className="mb-0">{team?.name} </h2>
                            </Flex>

                            <Row gutter={16}>

                                <Col xs={24} sm={24} md={16}>
                                    <Row className="mb-2">

                                        <Col xs={12} sm={12} md={15}>
                                            <span className="text-muted mb-2">Контакты капитана:</span>
                                        </Col>
                                    </Row>
                                    <Row className="mb-2">

                                        <Col xs={12} sm={12} md={7}>
                                            <Icon type={MailOutlined}
                                                  className="text-primary font-size-md"/>
                                            <span className="text-muted ml-2">Email:</span>
                                        </Col>
                                        <Col xs={12} sm={12} md={15}>
                                                        <span
                                                            className="font-weight-semibold">
                                                            <a href={`mailto:${team.capitan?.email}`}
                                                               className={"text-dark"}
                                                            >
                                                                {team.capitan?.email}
                                                            </a>
                                                        </span>
                                        </Col>
                                    </Row>

                                    {
                                        team.capitan?.phone !== '+7/' ?
                                            <Row className="mb-2">
                                                <Col xs={12} sm={12} md={7}>
                                                    <Icon type={PhoneOutlined}
                                                          className="text-primary font-size-md"/>
                                                    <span className="text-muted ml-2">Телефон:</span>
                                                </Col>
                                                <Col xs={12} sm={12} md={15}>
                                                    <span className="font-weight-semibold"><a
                                                        className={"text-dark"}
                                                        href={`tel:${team.capitan.phone}`}>{team.capitan.phone}</a></span>
                                                </Col>
                                            </Row> :
                                            null
                                    }


                                    <Row className="mb-2">

                                        <Col xs={12} sm={12} md={7}>
                                            <Icon type={SendOutlined}
                                                  className="text-primary font-size-md"/>
                                            <span className="text-muted ml-2">Телеграмм:</span>
                                        </Col>
                                        <Col xs={12} sm={12} md={15}>
                                            <span className="font-weight-semibold"><a
                                                className={"text-dark"}
                                                href={`https://t.me/${team.capitan?.telegram_link}`}>{team.capitan?.telegram_link}</a></span>
                                        </Col>
                                    </Row>
                                </Col>

                            </Row>
                        </div>
                    </div>
                </Col>
            </Row>
        </Card>
    )
}
export default TeamHeader;
