import {Tag, Card} from "antd";
import React from "react";
import Link from "next/link";
import AvatarStatus from '../../shared-components/AvatarStatus';


const TeamCard = (props) => {
    const {team, hacker_id, prefix_url} = props;
    return (
        <Card title="Команда">
            {team ?

                team.map((elm, i) => {
                    return (
                        <div className={`mb-4'`} key={`connection-${i}`}>
                            <h4 className="font-weight-semibold">{elm.name}</h4>
                            {
                                elm.teammate && elm.teammate.map(teammate => {
                                    return <div
                                        key={teammate.id}
                                        className="d-flex mt-2">


                                        {teammate.id === hacker_id ?
                                            <AvatarStatus text={teammate.first_name[0]}
                                                          name={"Этот Хакер"}

                                                          subTitle={elm.capitan === teammate.id ? "Капитан" : teammate.type_developer}/>
                                            :
                                            <Link
                                                href={{
                                                    pathname: `/${prefix_url}/hackers/[id]`,
                                                    query: {id: teammate.id}
                                                }}>
                                                <a>
                                                    {elm.capitan === teammate.id ?
                                                        <AvatarStatus text={teammate.first_name[0]}
                                                                      name={`${teammate.first_name} ${teammate.second_name}`}
                                                                      subTitle={"Капитан"}/>
                                                        :
                                                        <AvatarStatus text={teammate.first_name[0]}
                                                                      name={`${teammate.first_name} ${teammate.second_name}`}
                                                                      subTitle={teammate.type_developer}/>
                                                    }
                                                </a>
                                            </Link>
                                        }


                                    </div>
                                })
                            }
                        </div>
                    )
                })
                : <Tag>Хакер не состоит в команде</Tag>}
        </Card>
    )
}
export default TeamCard
