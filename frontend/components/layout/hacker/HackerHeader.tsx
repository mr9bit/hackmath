import {Avatar, Button, Card, Col, notification, Row} from "antd";
import {SendOutlined, MailOutlined, PhoneOutlined} from "@ant-design/icons";
import React from "react";
import Flex from '../../shared-components/Flex'
import {Icon} from '../../util-components/Icon'
import {Hacker} from '../../../redux/interface/hacker';
import {getStatusTag} from '../../../pages/admin/hackers'
import {URl_BACKEND} from "../../../redux/settings";

interface Props {
    hacker: Hacker,
    auth: any
}

const HackerHeader = (props: Props) => {
    const avatarSize = 150;
    const {hacker} = props;

    function sendConfirmEmail() {

        fetch(`${URl_BACKEND}/api/admin/send_confirm_email/ `, {
            method: 'POST',
            headers: {
                'Authorization': `Bearer ${props.auth.access_token}`,
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({"user_id": props.hacker.id})

        }).then((e) => e.json()).then((res) => {
            notification.success({
                message: "Письмо с подтверждением было отправлено",
                duration: 2.3
            })
        }).catch((error) => {
            error.response.json().then((e) => {
                console.log(e)
                notification.error({
                    message: e,
                    duration: 2.3,
                })
            })
        })

    }

    return (
        <Card>
            <Row justify="center">
                <Col sm={24} md={23}>
                    <div className="d-md-flex">
                        <div className="rounded p-2 bg-white shadow-sm mx-auto"
                             style={{'marginTop': '-70px', 'height': '170px', 'maxWidth': `${avatarSize + 16}px`}}>
                            <Avatar style={{backgroundColor: '#f56a00'}} shape="square" size={avatarSize}> A</Avatar>
                        </div>
                        <div className="ml-md-4 w-100">
                            <Flex alignItems="center" mobileFlex={false}
                                  className="mb-3 text-md-left text-center">
                                <h2 className="mb-0">{hacker.first_name} {hacker.second_name} </h2>
                                <div className={"ml-2"}>
                                    {getStatusTag(hacker.status)}
                                </div>
                            </Flex>

                            <Row gutter={16}>
                                <Col sm={24} md={8}>
                                    <p className="mt-0 mr-3 text-muted text-md-left text-center">
                                        {hacker.type_developer}
                                    </p>

                                </Col>
                                <Col xs={24} sm={24} md={16}>
                                    <Row className="mb-2">
                                        <Col xs={12} sm={12} md={7}>
                                            <Icon type={MailOutlined}
                                                  className="text-primary font-size-md"/>
                                            <span className="text-muted ml-2">Email:</span>
                                        </Col>
                                        <Col xs={12} sm={12} md={15}>
                                                        <span
                                                            className="font-weight-semibold">
                                                            <a href={`mailto:${hacker.email}`}
                                                               className={"text-dark"}
                                                            >
                                                                {hacker.email}
                                                            </a>
                                                        </span>
                                        </Col>
                                    </Row>

                                    {
                                        hacker.phone !== '+7/' ?
                                            <Row className="mb-2">
                                                <Col xs={12} sm={12} md={7}>
                                                    <Icon type={PhoneOutlined}
                                                          className="text-primary font-size-md"/>
                                                    <span className="text-muted ml-2">Телефон:</span>
                                                </Col>
                                                <Col xs={12} sm={12} md={15}>
                                                    <span className="font-weight-semibold"><a
                                                        className={"text-dark"}
                                                        href={`tel:+${hacker?.phone.split('/')[0]}${hacker?.phone.split('/')[1]}`}>+{hacker?.phone.split('/')[0]}{hacker?.phone.split('/')[1]}</a></span>
                                                </Col>
                                            </Row> :
                                            null
                                    }


                                    <Row className="mb-2">

                                        <Col xs={12} sm={12} md={7}>
                                            <Icon type={SendOutlined}
                                                  className="text-primary font-size-md"/>
                                            <span className="text-muted ml-2">Телеграмм:</span>
                                        </Col>
                                        <Col xs={12} sm={12} md={15}>
                                            <span className="font-weight-semibold"><a
                                                className={"text-dark"}
                                                href={`https://t.me/${hacker.telegram_link}`}>{hacker.telegram_link}</a></span>
                                        </Col>
                                    </Row>
                                </Col>
                                <Col xs={24}>
                                    {
                                        props.auth.user.role === "admin" && hacker.status <= 2 ?
                                            <Button type={"primary"} onClick={sendConfirmEmail}>Выслать письмо с
                                                подтверждением</Button> : null
                                    }

                                </Col>
                            </Row>
                        </div>
                    </div>
                </Col>
            </Row>
        </Card>
    )
}
export default HackerHeader
