import {Card, Col, Row} from "antd";
import React from "react";
import {Hacker} from '../../../redux/interface/hacker';

import HHSvg from './hh'

interface Props {
    hacker: Hacker,
}

const AboutHack = (props: Props) => {
    const {hacker} = props;
    return (
        <Card title="О Хакатоне">
            <Row gutter={30}>
                <Col sm={24} md={24}>
                    <div className="mb-3">
                        {
                            hacker?.want_take_part ?
                                <>
                                    <h4 className="font-weight-semibold">Почему хочет принять участие</h4>
                                    <p>{hacker?.want_take_part}</p>
                                </>
                                : null
                        }

                        {
                            hacker?.expect_from_hack ?
                                <>
                                    <h4 className="font-weight-semibold">Ожидает от Хакатона</h4>
                                    <p>{hacker?.expect_from_hack}</p>
                                </>
                                : null
                        }
                        {
                            hacker?.where_did_you_know ?
                                <>
                                    <h4 className="font-weight-semibold">Откуда узнал о Хакатоне</h4>
                                    <p>{hacker?.where_did_you_know}</p>
                                </>
                                : null
                        }
                        {
                            hacker?.something_else ?
                                <>
                                    <h4 className="font-weight-semibold">Что то еще</h4>
                                    <p>{hacker?.something_else}</p>
                                </>
                                : null
                        }
                    </div>
                </Col>
            </Row>
        </Card>
    )
}

export default AboutHack
