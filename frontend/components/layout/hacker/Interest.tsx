import {Tag, Card, Col, Row, Button} from "antd";
import {UploadOutlined} from "@ant-design/icons";
import React from "react";
import {Hacker} from '../../../redux/interface/hacker';
import Link from 'next/link'


interface Props {
    hacker: Hacker,
}


const Interest = (props: Props) => {
    const {hacker} = props;
    return (
        <Card title="Резюме">
            <Row gutter={30}>
                <Col sm={24} md={24}>
                    <div className="mb-3">
                        {hacker?.resume_text ? <p>{hacker?.resume_text}</p> : null}
                        {hacker?.resume_file ?
                            <Link href={hacker?.resume_file}>
                                <a target="_blank">
                                    <Button>
                                        <UploadOutlined/> Скачать резюме
                                    </Button>
                                </a>
                            </Link>

                            : null
                        }
                    </div>
                </Col>
                <Col sm={24} md={24}>
                    <div className="mb-3">
                        <h4 className="font-weight-semibold">Навыки</h4>
                        {
                            hacker?.skills && hacker?.skills.map(item => {
                                return <Tag className={"mt-2"} key={item}
                                            style={{fontSize: "16px", padding: "5px 15px"}}> {item} </Tag>
                            })
                        }
                    </div>
                </Col>
            </Row>
        </Card>
    )
}

export default Interest
