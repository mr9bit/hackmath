import {Card, Col, Row} from "antd";
import React from "react";
import {Hacker} from '../../../redux/interface/hacker';

interface Props {
    hacker: Hacker,
}

const Education = (props: Props) => {
    const {hacker} = props;
    return (
        <Card title="Образование">
            <div className="mb-3">
                <Row>
                    <Col sm={24} md={16}>
                        <div className={'mb-4'}>
                            {
                                hacker?.where_to_study === 'college' ?
                                    <>
                                        <h2 className="mb-0">{hacker?.institute} </h2>
                                        <p className="mt-2 mb-0"><b>Курс/Класс:</b> {hacker?.learning_class}</p>
                                        <p className="mt-2 mb-0"><b>Специальность:</b> {hacker?.specialization}</p>

                                    </>
                                    : null
                            }{
                            hacker?.where_to_study === 'school' ?
                                <>
                                    <h2 className="mb-0">{hacker?.institute} </h2>
                                    <p className="mt-2 mb-0"><b>Класс:</b> {hacker?.learning_class}</p>

                                </>
                                : null
                        }{
                            hacker?.where_to_study === 'univ' ?
                                <>
                                    <h2 className="mb-0">{hacker?.institute} </h2>
                                    <p className="mt-2 mb-0"><b>Курс:</b> {hacker?.learning_class}</p>
                                    <p className="mt-2 mb-0"><b>Специальность:</b> {hacker?.specialization}</p>
                                    <p className="mt-2 mb-0"><b>Уровень образования:</b> {hacker?.science_degree}
                                    </p>
                                </>
                                : null
                        }
                            {
                                hacker?.where_to_study === 'no_learning' ?
                                    <>
                                        <h2 className="mb-0">Не учится </h2>
                                    </>
                                    : null
                            }
                            <p className="pl-5 mt-2 mb-0">{}</p>
                        </div>
                    </Col>
                    {hacker?.employment_form || hacker?.seniority ?
                        <Col sm={24} md={8}>
                            <div className="mb-3">
                                <h2 className="font-weight-semibold">Работа</h2>
                                {
                                    hacker?.employment_form ?
                                        <p><b>Форма занятости:</b> {hacker?.employment_form}</p>
                                        :
                                        null
                                }
                                {

                                    hacker?.seniority ?
                                        <p><b>Трудовой стаж:</b> {hacker?.seniority}</p>
                                        :
                                        null
                                }
                            </div>
                        </Col> : null
                    }
                </Row>
            </div>
        </Card>
    )
}

export default Education;
