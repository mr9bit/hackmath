import {Avatar, Drawer, Divider, Tag} from 'antd';
import {
    CalendarOutlined,
    TeamOutlined,
    SendOutlined,
    GithubOutlined,
    FieldTimeOutlined,
    PhoneOutlined
} from '@ant-design/icons';
import React from "react";
import {getStatusTag} from '../../../pages/admin/hackers'
import moment from 'moment'

const DrawerInfoUser = (props) => {
    const {data, visible, close} = props;
    const datetime = moment(data?.datetime).locale('ru')
    return (
        <Drawer
            width={300}
            placement="right"
            onClose={close}
            closable={false}
            visible={visible}
        >
            <div className="text-center mt-3">
                <Avatar style={{backgroundColor: '#f56a00'}}
                        size={80}>{data?.first_name ? data?.first_name[0] : null}</Avatar>
                <h3 className="mt-2 mb-0">{data?.full_name}</h3>
                <span className="text-muted">{data?.type_developer}</span><br/>
                {getStatusTag(data?.status)}
            </div>
            <Divider dashed/>
            <div className="">
                <h6 className="text-muted text-uppercase mb-3">О хакере</h6>
                <p>
                    <CalendarOutlined/>
                    <span className="ml-3 text-dark">Возраст {moment().diff(data?.birth_date, 'years')}</span>
                </p>
                {
                    data?.team.length ?
                        <p>
                            <TeamOutlined/>
                            <span className="ml-3 text-dark">Команда <Tag
                                color="#8c8c8c">{data?.team[0].name}</Tag></span>
                        </p>
                        : null
                }
                <p>
                    <FieldTimeOutlined/>
                    <span className="ml-3 text-dark">Дата регистрации {datetime.format("ll")}
                    </span>
                </p>
            </div>
            <Divider dashed/>
            <div className="">
                <h6 className="text-muted text-uppercase mb-3">Контакты</h6>
                <p>
                    <SendOutlined/>
                    <span className="ml-3 text-dark">Телеграмм <a
                        href={`https://t.me/${data?.telegram_link}`}>{data?.telegram_link}</a></span>
                </p>

                {data?.phone !== '+7/' ?
                    <p>
                        <PhoneOutlined/>
                        <span className="ml-3 text-dark">Телефон <a
                            href={`tel:+${data?.phone?.split('/')[0]}${data?.phone?.split('/')[1]}`}>+{data?.phone?.split('/')[0]}{data?.phone?.split('/')[1]}</a> </span>
                    </p>
                    : null}

                {data?.github ?
                    <p>
                        <GithubOutlined/>
                        <span className="ml-3 text-dark"><a
                            href={`${data?.github}`}>{data?.github.split('/')[data?.github.split('/').length - 1]}</a> </span>
                    </p>
                    : null}
            </div>


        </Drawer>
    )
}

export default DrawerInfoUser
