import {Card, Col, Row} from "antd";
import React from "react";
import {Hacker} from '../../../redux/interface/hacker';
import moment from 'moment'


interface Props {
    hacker: Hacker,
}

const AboutHacker = (props: Props) => {
    const {hacker} = props;
    const dateRegister = moment(hacker?.datetime).locale('ru');
    const dateUpdate = moment(hacker?.datetime_update).locale('ru');

    return (
        <Card title="О Хакере">
            <Row gutter={30}>
                <Col sm={24} md={12}>
                    <div className="mb-3">
                        <p><b>Полное имя:</b> {hacker?.full_name}</p>
                        <p><b>Пол:</b> {hacker?.gender}</p>
                        <p><b>Дата рождения:</b> {hacker?.birth_date} ({moment().diff(hacker?.birth_date, 'years')})</p>
                        {hacker?.hack_visited ?
                            <p><b>Хакатонов посетил:</b> {hacker?.hack_visited}</p>
                            : null}
                    </div>
                </Col>
                <Col sm={24} md={12}>
                    <div className="mb-3">
                        <p><b>Город рождения:</b> {hacker?.born_city}</p>
                        <p><b>Город участия:</b> {hacker?.city}</p>
                        <p><b>Дата регистрации:</b>
                            {dateRegister.format(' hh:mm, MMM D YYYY')}
                        </p>
                        <p><b>Дата последнего
                            обновления:</b> {dateUpdate.format(' hh:mm, MMM D YYYY')}
                        </p>
                    </div>
                </Col>
            </Row>
        </Card>
    )
}

export default AboutHacker
