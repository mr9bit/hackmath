import Link from "next/link";
import Icon, {GithubOutlined, GitlabOutlined, GlobalOutlined, LinkedinOutlined} from "@ant-design/icons";
import React from "react";
import {Hacker} from "../../../redux/interface/hacker";
import {Card, Col, Row} from "antd";
import HHSvg from "./hh";

const HHIcon = props => <Icon component={HHSvg} {...props} />;

interface Props {
    hacker: Hacker,
    theme: string
}


const SocialHacker = (props: Props) => {
    const {hacker, theme} = props;
    const colorIcon = theme === 'light' ? '#000' : '#b4bed2'
    return (
        <Card title="Портфолио">
            <Row gutter={30}>

                <Col sm={24} md={24}>
                    <div className="mb-3">
                        {hacker?.github ?
                            <p>
                                <Link href={hacker?.github}>
                                    <a target="_blank" style={{display: 'flex', alignItems: 'center'}}>
                                        <GithubOutlined


                                            style={{color: colorIcon, fontSize: '30px'}}

                                        />
                                        <span className="ml-3 text-dark">
                                            {hacker?.github.split('/')[hacker?.github.split('/').length - 1]}
                                        </span>
                                    </a>
                                </Link>
                            </p>
                            : null
                        }
                        {hacker?.gitlab ?
                            <p>
                                <Link href={hacker?.gitlab}>
                                    <a target="_blank" style={{display: 'flex', alignItems: 'center'}}>
                                        <GitlabOutlined style={{color: colorIcon, fontSize: '30px'}}/>
                                        <span className="ml-3 text-dark">
                                            {hacker?.gitlab.split('/')[hacker?.gitlab.split('/').length - 1]}
                                        </span>
                                    </a>
                                </Link>
                            </p>
                            : null
                        }
                        {hacker?.linkedin ?
                            <p>
                                <Link href={hacker?.linkedin}>
                                    <a target="_blank" style={{display: 'flex', alignItems: 'center'}}>
                                        <LinkedinOutlined style={{color: colorIcon, fontSize: '30px'}}/>
                                        <span className="ml-3 text-dark">
                                           LinkedIn
                                        </span>
                                    </a>
                                </Link>
                            </p>
                            : null
                        }


                        {hacker?.hh ?
                            <p>
                                <Link href={hacker?.hh}>
                                    <a target="_blank" style={{display: 'flex', alignItems: 'center'}}>
                                        <HHIcon/>
                                        <span className="ml-3 text-dark">
                                           hh.ru
                                        </span>
                                    </a>
                                </Link>
                            </p>
                            : null
                        }

                        {hacker?.site ?
                            <p>
                                <Link href={hacker?.site}>
                                    <a target="_blank" style={{display: 'flex', alignItems: 'center'}}>
                                        <GlobalOutlined style={{color: colorIcon, fontSize: '30px'}}/>
                                        <span className="ml-3 text-dark">
                                            Сайт
                                        </span>
                                    </a>
                                </Link>
                            </p>
                            : null
                        }

                    </div>
                </Col>
            </Row>
        </Card>
    )
}
export default SocialHacker
