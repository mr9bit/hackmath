import React from "react";

import ApexChart from "react-apexcharts";
import {Card} from "antd";
import PropTypes from "prop-types";

const Chart = props => {
    return (
        <ApexChart {...props} />
    )
}

const PieChart = (props) => {
    const {options, series, height, width} = props
    return         <div className="text-center">
            <Chart
                options={options}
                series={series}
                type="pie"
                height={height}
                width={width}
            />
        </div>


}
PieChart.propTypes = {
    series: PropTypes.array.isRequired,
    labels: PropTypes.array,
    title: PropTypes.string
}

PieChart.defaultProps = {
    series: [],
    labels: [],
    title: '',
    height: 250,
    width: '100%'
};

export default PieChart
