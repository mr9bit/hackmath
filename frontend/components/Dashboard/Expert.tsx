import useTranslation from "next-translate/useTranslation";
import {Card, Col, Divider, Row, Timeline, Typography} from "antd";
import React, {useEffect, useState} from "react";
import {URl_BACKEND} from "../../redux/settings";
import {checkHttpStatus} from "../../redux/utils";
import moment from "moment";
import {COLORS} from "../../constants/ChartConstant";
import Link from "next/link";
import {DiscordIcon} from "../ExternalIcon";
import {getStatusTag} from "../Utils/StatusTag";
import dynamic from "next/dynamic";

const {Title, Paragraph} = Typography;
const ApexChart = dynamic(() => import("react-apexcharts"), {ssr: false});
const PieChart = dynamic(() => import("../shared-components/PieChart"), {ssr: false});
import StatisticWidget from '../shared-components/StatisticWidget';


export const Expert = (props) => {

    const {t, lang} = useTranslation('index')
    const [countTeam, setCountTeam] = useState(0)
    const [countUsers, setCountUsers] = useState(0)
    const [where_to_study, setWhere_to_study] = useState({series: [], categories: []})
    const [birthDate, setBirthDate] = useState({series: [], categories: []})
    const [hackVisited, setHackVisited] = useState({series: [], categories: []})

    useEffect(() => {
        if (props.auth.user.role === 'expert') {
            fetch(`${URl_BACKEND}/api/expert/statistics/`, {
                    method: 'GET',
                    headers: {
                        'Authorization': `Bearer ${props.auth.access_token}`
                    }
                }
            ).then(checkHttpStatus)
                .then((response) => response.json())
                .then((response) => {
                    setCountTeam(response.count_team);
                    setCountUsers(response.count_users);

                    setBirthDate({
                        series: response.birth_date.map(item => item.count),
                        categories: response.birth_date.map(item => moment().diff(new Date(item.year, 12, 12), 'years'))
                    })

                    setHackVisited({
                        series: response.hack_visited.map(item => item.count),
                        categories: response.hack_visited.map(item => item.hack_visited)
                    })

                    const letsas = response.where_to_study.map((item) => {
                        if (item.where_to_study === "college") return "Колледж"
                        if (item.where_to_study === "univ") return "Институт"
                        if (item.where_to_study === "school") return "Школа"
                        if (item.where_to_study === "no_learning") return "Не учится"
                    })

                    setWhere_to_study({
                        series: response.where_to_study.map(item => item.count),
                        categories: letsas
                    })

                })
            setTimeout(() => window.dispatchEvent(new Event('resize')), 500)
        }
    }, [])
    return (
        <div>
            <Row gutter={16}>
                <Col xs={24} sm={24} md={24} lg={24}>
                    <>
                        {
                            props.user.status > 2 ?

                                <div className="container my-4">

                                    <Row gutter={16}>
                                        <Col xs={24} sm={24} md={24} lg={12} xl={12}>
                                            <Row gutter={16}>
                                                <Col xs={24} sm={24} md={12} lg={24} xl={12}>
                                                    <StatisticWidget
                                                        title={"Количество команд"}
                                                        value={`${countTeam}`}
                                                    />
                                                </Col> <Col xs={24} sm={24} md={12} lg={24} xl={12}>
                                                <StatisticWidget
                                                    title={"Количество участников"}
                                                    value={`${countUsers}`}
                                                />
                                            </Col>
                                                <Col xs={24} sm={24} md={12} lg={24} xl={24}>

                                                    <Card title={"Из каких учреждений мои участники"}>
                                                        <PieChart
                                                            options={{
                                                                colors: COLORS,
                                                                labels: where_to_study.categories,
                                                                responsive: [{
                                                                    breakpoint: 480,
                                                                    options: {
                                                                        chart: {
                                                                            width: 200
                                                                        },
                                                                        legend: {
                                                                            position: 'bottom'
                                                                        }
                                                                    }
                                                                }]
                                                            }}
                                                            series={where_to_study.series}
                                                        />
                                                    </Card>
                                                </Col>
                                            </Row>
                                        </Col>
                                        <Col xs={24} sm={24} md={24} lg={12} xl={12}>
                                            <Card>
                                                <Typography style={{width: "100%"}}>
                                                    <Paragraph style={{textAlign: "center", fontSize: "16px"}}>
                                                        <Link
                                                            href={"https://discord.gg/YTehKrkt"}><a>Ссылка</a></Link> на
                                                        Дискорд (чат экспертов)
                                                        <br/>
                                                        Напоминаем когда вы будете необходимы
                                                    </Paragraph>
                                                </Typography>
                                                <Timeline mode={"left"}>
                                                    <Timeline.Item
                                                        label="Пятница 19:45-20:30"
                                                        dot={<DiscordIcon style={{fill: "#7289DA"}}/>}>
                                                        <p>Общение с экспертами в Discord</p>
                                                    </Timeline.Item>

                                                    <Timeline.Item
                                                        label="Суббота 13:30-15:00"
                                                        dot={<DiscordIcon style={{fill: "#7289DA"}}/>}>
                                                        <p>Чек-поинт (15 минут на команду)</p>
                                                    </Timeline.Item>

                                                    <Timeline.Item
                                                        label="Суббота 18:30-20:30"
                                                        dot={<DiscordIcon style={{fill: "#7289DA"}}/>}>
                                                        <p>Чек-поинт (15 минут на команду) </p>
                                                    </Timeline.Item>


                                                    <Timeline.Item
                                                        label="Воскресенье 12:00-14:00"
                                                        dot={<DiscordIcon style={{fill: "#7289DA"}}/>}>
                                                        <p>Чек-поинт (15 минут на команду) </p>
                                                    </Timeline.Item>

                                                    <Timeline.Item
                                                        label="Воскресенье 16:00-19:00"
                                                        dot={<DiscordIcon style={{fill: "#7289DA"}}/>}>
                                                        <p>Презентация проектов по задачам (3-минуты на выступление,
                                                            2-на вопросы) </p>
                                                    </Timeline.Item>
                                                </Timeline>
                                            </Card>
                                        </Col>
                                    </Row>
                                    <Row gutter={16}>
                                        <Col xs={24} sm={24} md={12} lg={12}>
                                            <Card title="Возраст участников">

                                                <ApexChart
                                                    options={{
                                                        plotOptions: {
                                                            bar: {
                                                                horizontal: false,
                                                            }
                                                        },
                                                        colors: COLORS,
                                                        xaxis: {
                                                            categories: birthDate.categories,
                                                        }
                                                    }}
                                                    series={[{
                                                        data: birthDate.series
                                                    }]}
                                                    type="bar"
                                                    height={250}
                                                />
                                            </Card>
                                        </Col>

                                        <Col xs={24} sm={24} md={12} lg={12}>
                                            <Card title={"Сколько хакатонов посетили"}>
                                                <PieChart
                                                    options={{
                                                        colors: COLORS,
                                                        labels: hackVisited.categories,
                                                        responsive: [{
                                                            breakpoint: 480,
                                                            options: {
                                                                chart: {
                                                                    width: 200
                                                                },
                                                                legend: {
                                                                    position: 'bottom'
                                                                }
                                                            }
                                                        }]
                                                    }}
                                                    series={hackVisited.series}
                                                />
                                            </Card>
                                        </Col>

                                    </Row>
                                </div>
                                : <div className="code-box">
                                    <div className="container my-4">
                                        <Row gutter={16}>
                                            <Title level={2}
                                                   style={{textAlign: "center", width: "100%"}}>{t('your_status')}</Title>
                                            <div style={{display: "flex", justifyContent: 'center', width: '100%'}}>
                                                {props.user ? getStatusTag(props.user.status, t) : null}
                                            </div>
                                        </Row>
                                    </div>
                                    <Divider/>
                                    <div className="container my-4">
                                        <Row gutter={16}>
                                            <Typography style={{width: "100%"}}>
                                                <Paragraph style={{textAlign: "center", fontSize: "16px"}}>
                                                    Заполните анкету, что бы приступить к заполнению чекпоинтов и доступу
                                                    командам
                                                </Paragraph>
                                            </Typography>
                                        </Row>
                                    </div>
                                </div>
                        }
                    </>
                </Col>
            </Row>
        </div>

    )
}
