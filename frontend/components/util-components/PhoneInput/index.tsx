import React, {useCallback, useState, useMemo, useEffect} from 'react';
import {Input, Select} from 'antd';
import {InputProps} from 'antd/lib/input';
import {OptionProps, SelectProps} from 'antd/lib/select';
import countries, {Country} from './country';
import MaskedInput from 'antd-mask-input'

interface PropTypes extends Omit<InputProps, 'value' | 'onChange'> {
    onChange?: (value: string) => void;
    value?: string;
    selectProps?: SelectProps<any>;
    optionProps?: OptionProps;
}

export type CountryPhoneInputValue = {
    code?: number;
    phone?: string;
    short?: string;
};

function CountryPhoneInput({
                               onChange,
                               value,
                               selectProps,
                               ...props
                           }: PropTypes) {
    const defaultCountry: Country | undefined = useMemo(() => {
        return countries.find((c) => c.short === 'RU');
    }, []);

    const [country, setCountry] = useState<Country | undefined>(defaultCountry);
    const [phone, setPhone] = useState<string | undefined>();


    useEffect(() => {
        if (value !== undefined) {
            const number = {
                code: Number(value.split('/')[0]),
                phone: value.split('/')[1],
                short: value.split('/')[2],
            }
            value = value.split('/')[1]
            if (number.short) {
                setCountry(countries.find((c) => c.short === number.short));
            } else {
                setCountry(countries.find((c) => Number(c.phoneCode) === number.code));
            }
            setPhone(number.phone);
        }
    }, [value]);

    const triggerChange = useCallback(
        (phone?: string, country?: Country) => {
            const new_result = `${country.phoneCode}/${phone}/${country.short}`
            onChange?.(new_result);
        },
        [onChange]
    );

    const handleCountryChange = useCallback(
        (value: string) => {
            const c = countries.find((c) => c.short === value);
            if (!c) {
                throw new Error(`Country not found: ${value}`);
            }
            setCountry(c);
            triggerChange(phone, c);
        },
        [setCountry, triggerChange, phone]
    );

    const handlePhoneChange = useCallback(
        (e: React.ChangeEvent<HTMLInputElement>) => {
            const currentValue = e.target.value;
            setPhone(currentValue);
            triggerChange(currentValue, country);
        },
        [setPhone, country, triggerChange]
    );

    return (
        <MaskedInput
            mask="111-111-11-11"
            prefix={
                <Select
                    bordered={false}
                    dropdownMatchSelectWidth={false}
                    {...selectProps}
                    style={{width: "100px"}}
                    optionLabelProp="label"
                    value={country && country.short}
                    onChange={handleCountryChange}>
                    {countries.map((item) => {
                        const fix = {
                            key: item.short,
                            value: item.short,
                            label: `${item.emoji}+${item.phoneCode}`,
                        };
                        return (
                            <Select.Option {...props.optionProps} {...fix}>
                                {item.emoji} {item.short}
                            </Select.Option>
                        );
                    })}
                </Select>
            }
            {...props}
            value={phone}
            onChange={handlePhoneChange}
        />
    );
}

export default CountryPhoneInput;
