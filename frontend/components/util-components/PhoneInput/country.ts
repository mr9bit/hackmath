export type Country = {
  zh: string;
  en: string;
  short: string;
  emoji: string;
  phoneCode: string;
};

const countries: Country[] = [

  {
    zh: '俄罗斯',
    en: 'Russia',
    short: 'RU',
    emoji: '🇷🇺',
    phoneCode: '7',
  },
  {
    zh: '白俄罗斯',
    en: 'Belarus',
    short: 'BY',
    emoji: '🇧🇾',
    phoneCode: '375',
  },
  {
    zh: '亚美尼亚',
    en: 'Armenia',
    short: 'AM',
    emoji: '🇦🇲',
    phoneCode: '374',
  },
  {
    zh: '乌克兰',
    en: 'Ukraine',
    short: 'UA',
    emoji: '🇺🇦',
    phoneCode: '380',
  },
  {
    zh: '吉布提',
    en: 'Djibouti',
    short: 'DJ',
    emoji: '🇩🇯',
    phoneCode: '253',
  },
  {
    zh: '肯尼亚',
    en: 'Kenya',
    short: 'KE',
    emoji: '🇰🇪',
    phoneCode: '254',
  },
  {
    zh: '坦桑尼亚',
    en: 'Tanzania',
    short: 'TZ',
    emoji: '🇹🇿',
    phoneCode: '255',
  },
  {
    zh: '乌干达',
    en: 'Uganda',
    short: 'UG',
    emoji: '🇺🇬',
    phoneCode: '256',
  },
  {
    zh: '布隆迪',
    en: 'Burundi',
    short: 'BI',
    emoji: '🇧🇮',
    phoneCode: '257',
  },
  {
    zh: '莫桑比克',
    en: 'Mozambique',
    short: 'MZ',
    emoji: '🇲🇿',
    phoneCode: '258',
  },
  {
    zh: '赞比亚',
    en: 'Zambia',
    short: 'ZM',
    emoji: '🇿🇲',
    phoneCode: '260',
  },

  {
    zh: '津巴布韦',
    en: 'Zimbabwe',
    short: 'ZW',
    emoji: '🇿🇼',
    phoneCode: '263',
  },
  {
    zh: '纳米比亚',
    en: 'Namibia',
    short: 'NA',
    emoji: '🇳🇦',
    phoneCode: '264',
  },
  {
    zh: '马拉维',
    en: 'Malawi',
    short: 'MW',
    emoji: '🇲🇼',
    phoneCode: '265',
  },
  {
    zh: '莱索托',
    en: 'Lesotho',
    short: 'LS',
    emoji: '🇱🇸',
    phoneCode: '266',
  },
  {
    zh: '博茨瓦纳',
    en: 'Botswana',
    short: 'BW',
    emoji: '🇧🇼',
    phoneCode: '267',
  },

  {
    zh: '科摩罗',
    en: 'Comoros',
    short: 'KM',
    emoji: '🇰🇲',
    phoneCode: '269',
  },
  {
    zh: '马约特',
    en: 'Mayotte',
    short: 'YT',
    emoji: '🇾🇹',
    phoneCode: '269',
  },
  {
    zh: '厄立特里亚',
    en: 'Eritrea',
    short: 'ER',
    emoji: '🇪🇷',
    phoneCode: '291',
  },
  {
    zh: '法罗群岛',
    en: 'Faroe Islands',
    short: 'FO',
    emoji: '🇫🇴',
    phoneCode: '298',
  },
  {
    zh: '希腊',
    en: 'Greece',
    short: 'GR',
    emoji: '🇬🇷',
    phoneCode: '30',
  },
  {
    zh: '芬兰',
    en: 'Finland',
    short: 'FI',
    emoji: '🇫🇮',
    phoneCode: '358',
  },
  {
    zh: '保加利亚',
    en: 'Bulgaria',
    short: 'BG',
    emoji: '🇧🇬',
    phoneCode: '359',
  },
  {
    zh: '立陶宛',
    en: 'Lithuania',
    short: 'LT',
    emoji: '🇱🇹',
    phoneCode: '370',
  },
  {
    zh: '拉脱维亚',
    en: 'Latvia',
    short: 'LV',
    emoji: '🇱🇻',
    phoneCode: '371',
  },
  {
    zh: '爱沙尼亚',
    en: 'Estonia',
    short: 'EE',
    emoji: '🇪🇪',
    phoneCode: '372',
  },
  {
    zh: '摩尔多瓦',
    en: 'Moldova',
    short: 'MD',
    emoji: '🇲🇩',
    phoneCode: '373',
  },
  {
    zh: '斯洛文尼亚',
    en: 'Slovenia',
    short: 'SI',
    emoji: '🇸🇮',
    phoneCode: '386',
  },
  {
    zh: '捷克',
    en: 'Czech',
    short: 'CZ',
    emoji: '🇨🇿',
    phoneCode: '420',
  },
  {
    zh: '波兰',
    en: 'Poland',
    short: 'PL',
    emoji: '🇵🇱',
    phoneCode: '48',
  },
  {
    zh: '马来西亚',
    en: 'Malaysia',
    short: 'MY',
    emoji: '🇲🇾',
    phoneCode: '60',
  },
  {
    zh: '越南',
    en: 'Vietnam',
    short: 'VN',
    emoji: '🇻🇳',
    phoneCode: '84',
  },
  {
    zh: '中国香港',
    en: 'Hong Kong',
    short: 'HK',
    emoji: '🇭🇰',
    phoneCode: '852',
  },
  {
    zh: '中国',
    en: 'China',
    short: 'CN',
    emoji: '🇨🇳',
    phoneCode: '86',
  },
  {
    zh: '孟加拉国',
    en: 'Bangladesh',
    short: 'BD',
    emoji: '🇧🇩',
    phoneCode: '880',
  },
  {
    zh: '土耳其',
    en: 'Turkey',
    short: 'TR',
    emoji: '🇹🇷',
    phoneCode: '90',
  },
  {
    zh: '印度',
    en: 'India',
    short: 'IN',
    emoji: '🇮🇳',
    phoneCode: '91',
  },
  {
    zh: '缅甸',
    en: 'Myanmar',
    short: 'MM',
    emoji: '🇲🇲',
    phoneCode: '95',
  },
  {
    zh: '马尔代夫',
    en: 'Maldives',
    short: 'MV',
    emoji: '🇲🇻',
    phoneCode: '960',
  },
  {
    zh: '塔吉克斯坦',
    en: 'Tajikistan',
    short: 'TJ',
    emoji: '🇹🇯',
    phoneCode: '992',
  },
  {
    zh: '土库曼斯坦',
    en: 'Turkmenistan',
    short: 'TM',
    emoji: '🇹🇲',
    phoneCode: '993',
  },
  {
    zh: '阿塞拜疆',
    en: 'Azerbaijan',
    short: 'AZ',
    emoji: '🇦🇿',
    phoneCode: '994',
  },
  {
    zh: '格鲁吉亚',
    en: 'Georgia',
    short: 'GE',
    emoji: '🇬🇪',
    phoneCode: '995',
  },
  {
    zh: '吉尔吉斯斯坦',
    en: 'Kyrgyzstan',
    short: 'KG',
    emoji: '🇰🇬',
    phoneCode: '996',
  },
  {
    zh: '乌兹别克斯坦',
    en: 'Uzbekistan',
    short: 'UZ',
    emoji: '🇺🇿',
    phoneCode: '998',
  },
];

export default countries;
