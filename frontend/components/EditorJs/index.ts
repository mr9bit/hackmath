const EditorJS = require('@editorjs/editorjs');
const Paragraph = require('@editorjs/paragraph');
const Header = require('@editorjs/header')
const Delimiter = require('@editorjs/delimiter');
const List = require('@editorjs/list');
const ImageTool = require('@editorjs/image');
import {URl_BACKEND} from "../../redux/settings";

const EJLaTeX = require('./editorjs-latex.bundle-min');
const LinkTool = require('@editorjs/link');

const handleLatex = () => {
    if (typeof window !== 'undefined') {

        let url = "https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.12.0/katex.min.css";
        let head = document.head;
        let link = document.createElement("link");
        link.type = "text/css";
        link.rel = "stylesheet";
        link.href = url;
        head.appendChild(link);
    }
}

export const initEditorJs = (holder, data, handleChange, token) => {
    return new EditorJS({
        holder: holder,
        tools: {
            paragraph: {
                class: Paragraph,
                inlineToolbar: true
            },
            header: {
                class: Header,
                inlineToolbar: true,
            },
            math: {
                class: EJLaTeX,
                shortcut: 'CMD+SHIFT+M'
            },
            delimiter: {
                class: Delimiter,
                inlineToolbar: true,
            },
            list: {
                class: List,
                inlineToolbar: true,
            },
            linkTool: {
                class: LinkTool,
                config: {
                    endpoint: `${URl_BACKEND}/api/admin/linkupload`, // Your backend endpoint for url data fetching
                    additionalRequestHeaders: {
                        'authorization': `Bearer ${token}`,
                    }
                }
            },
            image: {
                class: ImageTool,
                config: {
                    endpoints: {
                        byFile: `${URl_BACKEND}/api/admin/uploadimage/`, // Your backend file uploader endpoint
                    },
                    additionalRequestHeaders: {
                        'authorization': `Bearer ${token}`,
                    }
                }
                ,
            },

        },
        onReady: () => {
            handleLatex();
        },
        onChange: handleChange,
        data: data,
    })
}
