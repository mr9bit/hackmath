import Funnel from './components/Funnel'
import Bar from './components/Bar'

// export Foo and Bar as named exports
export { Funnel, Bar }

// alternative, more concise syntax for named exports
// export { default as Foo } from './Foo'

export default { Funnel, Bar }
