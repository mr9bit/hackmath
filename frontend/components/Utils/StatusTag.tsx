import {Tag} from "antd";
import React from "react";

export const getStatusTag = (status, t) => {
    switch (status) {
        case 0:
            return <Tag style={{
                width: "90%",
                textAlign: "center",
                height: "60px",
                fontSize: "20px",
                display: "flex",
                justifyContent: "center",
                alignItems: "center"
            }} color="#8c8c8c">{t('unconfirmed')}</Tag>
        case 1:
            return <Tag style={{
                width: "90%",
                textAlign: "center",
                height: "60px",
                fontSize: "20px",
                display: "flex",
                justifyContent: "center",
                alignItems: "center"
            }} color="geekblue">{t('incomplete')}</Tag>
        case 2:
            return <Tag style={{
                width: "90%",
                textAlign: "center",
                height: "60px",
                fontSize: "20px",
                display: "flex",
                justifyContent: "center",
                alignItems: "center"
            }} color="green">{t('admitted')}</Tag>
        case 3:
            return <Tag style={{
                width: "90%",
                textAlign: "center",
                height: "60px",
                fontSize: "20px",
                display: "flex",
                justifyContent: "center",
                alignItems: "center"
            }} color="#87d068">{t('confirm')}</Tag>
    }
}
